FROM node:12.19.0 as builder

ARG MAINTAINER_NAME
ARG MAINTAINER_EMAIL

LABEL ${MAINTAINER_NAME} <${MAINTAINER_EMAIL}>
ENV REFRESHED_AT 2020-09-01

# Install yarn globally
RUN npm install -g npm

# Change work directory for subsequent commands
WORKDIR /var/www/site/

# Copy our package.json and install *before* adding our application files
# for faster deploys
COPY package.json ./
COPY yarn.lock ./
RUN yarn &&\
    yarn list

# Copy code base
COPY . /var/www/site/

# Build
RUN NODE_ENV=production yarn run build

FROM alpine:latest
LABEL ${MAINTAINER_NAME} <${MAINTAINER_EMAIL}>
ENV REFRESHED_AT 2020-02-07b

#==================================================
# Base installs
#==================================================
RUN apk -U add lighttpd syslog-ng curl grep && rm -rf /tmp/local-ca.sh /var/cache/apk/*

#==================================================
# Setup user
#==================================================
RUN adduser s-app -g s-app -s /sbin/nologin -D -h /home/s-app

ADD server-config/lighttpd.conf /etc/lighttpd/lighttpd.conf
ADD server-config/syslog-ng.conf /etc/syslog-ng/syslog-ng.conf
COPY --from=builder /var/www/site/dist /home/s-app/htdocs

RUN chown -R s-app:s-app /home/s-app

EXPOSE 8005

HEALTHCHECK --interval=15s --timeout=120s --retries=8 CMD STR="GREEN"; if [ "$(curl --fail -s http://127.0.0.1:8005/_service/status/ | grep -o "$STR")" = "$STR" ]; then exit 0; else exit 1; fi

ENTRYPOINT /usr/sbin/syslog-ng -f /etc/syslog-ng/syslog-ng.conf && /usr/sbin/lighttpd -D -f /etc/lighttpd/lighttpd.conf
