FROM node:12.19.0

ARG NODE_ENV
ARG INTERNAL_CA_SCRIPT_URL
ARG MAINTAINER_NAME
ARG MAINTAINER_EMAIL

LABEL ${MAINTAINER_NAME} <${MAINTAINER_EMAIL}>
ENV REFRESHED_AT 2020-02-07


# internal CAs
RUN echo $INTERNAL_CA_SCRIPT_URL
ADD $INTERNAL_CA_SCRIPT_URL /tmp/
RUN bash /tmp/local-ca.sh

# Install global dependencies
# RUN npm install -g yarn
RUN npm install -g webpack
RUN npm install -g webpack-cli
RUN npm install -g webpack-dev-server

# Change work directory for subsequent commands
WORKDIR /var/www/site/

# Copy our package.json and install *before* adding our application files
# for faster deploys
COPY package.json ./
COPY yarn.lock ./
RUN yarn

# Copy code base
COPY . /var/www/site/

# Test
RUN yarn test

# Build
RUN NODE_ENV=$NODE_ENV yarn build

# Run webpack dev server
EXPOSE 8005

HEALTHCHECK --interval=20s --timeout=120s --retries=8 CMD STR="GREEN"; if [ "$(curl --fail -s http://127.0.0.1:8005/_service/status | grep -o "$STR")" = "$STR" ]; then exit 0; else exit 1; fi

ENTRYPOINT yarn run dev
