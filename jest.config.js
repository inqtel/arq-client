const { defaults } = require('jest-config'); // eslint-disable-line import/no-extraneous-dependencies

module.exports = {
  verbose: true,
  modulePaths: ['<rootDir>/src/'],
  moduleFileExtensions: [...defaults.moduleFileExtensions, 'ts', 'tsx'],
  transform: {
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
    '^.+\\.(js|jsx)?$': 'babel-jest',
  },
  collectCoverage: true,
  collectCoverageFrom: [
    './src/components/**/*.{js,jsx}',
    '!**/node_modules/**',
    '!**/vendor/**',
  ],
  coverageThreshold: {
    global: {
      branches: 15,
      functions: 14,
      lines: 35,
      statements: 35,
    },
  },
  setupFiles: ['<rootDir>/.env', '<rootDir>/test/setup.js'],
};
