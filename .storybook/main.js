const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const merge = require('webpack-merge');

const storybookConfig = {
  module: {
    rules: [{
      test: /\.scss$/,
      use: [{
        loader: 'style-loader'
      }, {
        loader: 'css-loader',
        options: {
          url: false
        }
      }, {
        loader: 'postcss-loader'
      }, {
        loader: 'sass-loader',
        options: {
          outputStyle: 'expanded',
          includePaths: [
            path.resolve(__dirname, '../src/styles')
          ]
        }
      }]
    }, {
      test: /\.(jpg|png|gif|svg|woff|woff2|ttf|otf|eot)$/,
      loader: 'file-loader'
    }]
  },
  resolve: {
    modules: [path.resolve(__dirname, '../src'), 'node_modules']
  },
};

const environmentConfig = (() => {
  return require('../webpack/local-dev.js');
})();

module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)",
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials"
  ],
  webpackFinal: async (config, { configType }) => {
    return merge(config, storybookConfig, environmentConfig);
  }
}
