import client from 'apollo';
import { GET_DOC, GET_SHARED_DOC, SAVE_DOCUMENT } from 'queries/documents';

export function getDoc(id) {
  return client.query({
    query: GET_DOC,
    variables: { id },
    fetchPolicy: 'network-only',
  })
    .then(response => response.data);
}

export function getSharedDoc(id) {
  return client.query({
    query: GET_SHARED_DOC,
    variables: { id },
    fetchPolicy: 'network-only',
  })
    .then(response => response.data);
}

export function saveDoc({
  id, title,
}) {
  return client.mutate({
    mutation: SAVE_DOCUMENT,
    variables: {
      id,
      title,
    },
  })
    .then(response => response.data);
}
