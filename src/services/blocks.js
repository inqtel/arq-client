import client from 'apollo';
import {
  UPDATE_DOC_HTML_BLOCK,
  DELETE_DOC_ARCHITECTURE_BLOCK,
  APPEND_DOC_ARCHITECTURE_BLOCK,
} from 'queries/blocks';
import { replacePunctuation } from 'utils/blocks';


export function deleteArchitectureBlock(id) {
  return client.mutate({
    mutation: DELETE_DOC_ARCHITECTURE_BLOCK,
    variables: { id },
  })
    .then(response => response.data);
}

export function modifyBlock(documentQID, blocks) {
  const blockStrings = blocks.map(b => `(${b.body ? replacePunctuation(b.body) : ''}\\,${b.architecture ? b.architecture.id : ''})`);

  return client.mutate({
    mutation: UPDATE_DOC_HTML_BLOCK,
    variables: {
      id: documentQID,
      document_blocks: `{${blockStrings}}`,
    },
  })
    .then(response => response.data);
}

export function appendArchitectureBlock(documentQID, architectureID, packageID) {
  return client.mutate({
    mutation: APPEND_DOC_ARCHITECTURE_BLOCK,
    variables: {
      id: documentQID,
      architectureID,
      packageID,
    },
  })
    .then(response => response.data);
}
