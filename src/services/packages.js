import client from 'apollo';
import {
  createMergePackageInputsFromPackageTree,
  createReplacePackageInputsFromPackageTree,
} from 'utils/gql';
import {
  LINK_PACKAGE_TAXONOMY,
  UNLINK_PACKAGE_TAXONOMY,
  LINK_PACKAGE_REFERENCE_PACKAGE,
  UNLINK_PACKAGE_REFERENCE_PACKAGE,
  UPDATE_DOC_PACKAGE,
  DELETE_DOC_PACKAGE,
  MOVE_PACKAGE_UP,
  MOVE_PACKAGE_DOWN,
  INSERT_PACKAGE_BEFORE,
  INSERT_PACKAGE_AFTER,
  TOGGLE_PACKAGE_CHILDREN,
  INDENT_PACKAGE,
  UNINDENT_PACKAGE,
  MERGE_PACKAGE,
  REPLACE_PACKAGE,
  INSERT_CHILD_PACKAGE,
} from 'queries/packages';

export function linkTaxonomyToPackage(id, term, taxonomyId) {
  return client.mutate({
    mutation: LINK_PACKAGE_TAXONOMY,
    variables: {
      id,
      term,
      taxonomyId,
    },
  })
    .then(response => response.data);
}

export function unlinkTaxonomyToPackage(id, term) {
  return client.mutate({
    mutation: UNLINK_PACKAGE_TAXONOMY,
    variables: {
      id,
      term,
    },
  })
    .then(response => response.data);
}

// note: link/unlink reference to package not used
// no architecture results in doc search any more
export function linkReferenceToPackage(pkg) {
  return client.mutate({
    mutation: LINK_PACKAGE_REFERENCE_PACKAGE,
    variables: {
      packageQID: pkg.qid,
      referencePackageQID: pkg.referencePackage.qid,
    },
  })
    .then(response => response.data);
}

export function unlinkReferenceToPackage(pkg) {
  return client.mutate({
    mutation: UNLINK_PACKAGE_REFERENCE_PACKAGE,
    variables: { packageQID: pkg.qid },
  })
    .then(response => response.data);
}

export function modifyPackage(pkg) {
  return client.mutate({
    mutation: UPDATE_DOC_PACKAGE,
    variables: {
      id: pkg.id,
      term: pkg.term,
      linkBroken: pkg.linkBroken || false, // pkg from utils/entities may have undefined linkBroken
      description: pkg.description,
    },
  })
    .then(response => response.data);
}

export function deletePackage(id) {
  return client.mutate({
    mutation: DELETE_DOC_PACKAGE,
    variables: { id },
  })
    .then(response => response.data);
}

export function movePackageUp(packageID) {
  return client.mutate({
    mutation: MOVE_PACKAGE_UP,
    variables: { packageID },
  })
    .then(response => response.data);
}

export function movePackageDown(packageID) {
  return client.mutate({
    mutation: MOVE_PACKAGE_DOWN,
    variables: { packageID },
  })
    .then(response => response.data);
}

export function insertPackageBefore(pkg, targetID) {
  return client.mutate({
    mutation: INSERT_PACKAGE_BEFORE,
    variables: {
      packageID: pkg.id,
      targetID,
    },
  })
    .then(response => response.data);
}

export function insertPackageAfter(pkg, targetID) {
  return client.mutate({
    mutation: INSERT_PACKAGE_AFTER,
    variables: {
      packageID: pkg.id,
      targetID,
    },
  })
    .then(response => response.data);
}

export function indentPackage(packageID) {
  return client.mutate({
    mutation: INDENT_PACKAGE,
    variables: {
      packageID,
    },
  })
    .then(response => response.data);
}

export function unindentPackage(packageID) {
  return client.mutate({
    mutation: UNINDENT_PACKAGE,
    variables: {
      packageID,
    },
  })
    .then(response => response.data);
}

export function modifyPackageChildren(packageID) {
  return client.mutate({
    mutation: TOGGLE_PACKAGE_CHILDREN,
    variables: { packageID },
  })
    .then(response => response.data);
}

export function mergePackage(pkg) {
  const objects = createMergePackageInputsFromPackageTree(pkg);
  return client.mutate({
    mutation: MERGE_PACKAGE,
    variables: { objects },
  })
    .then(response => response.data);
}

export function replacePackage(pkg) {
  const objects = createReplacePackageInputsFromPackageTree(pkg);
  return client.mutate({
    mutation: REPLACE_PACKAGE,
    variables: { objects },
  })
    .then(response => response.data);
}

export function insertPackageChild(id) {
  return client.mutate({
    mutation: INSERT_CHILD_PACKAGE,
    variables: { id },
  })
    .then(response => response.data);
}
