import { v4 as uuid } from 'uuid';
import { initTree, createEmptyPackage } from 'utils/tree';
import { ARCHITECTURE_DRAFT } from 'constants/architectureStates.js';

// TODO: Remove; no type field in postgres
import { ARCHITECTURE_BLOCK, HTML_BLOCK } from 'constants/blocktypes';

export function isArchitectureBlock(block) {
  return !!block.architecture;
}

export function replacePunctuation(block) {
  return block.replace(/,/g, '&comma;').replace(/\)/g, '&rpar;').replace(/"/g, '&quot;').replace(/{/g, '&lbrace;')
    .replace(/}/g, '&rbrace;');
}

export function createBlock() {
  return {
    id: uuid(),
    body: '',
    architecture: null,
  };
}

export function createHTMLBlock() {
  return {
    ...createBlock(),
    type: HTML_BLOCK,
  };
}

export function createArchitectureBlock() {
  const pkg = createEmptyPackage();

  return {
    ...createBlock(),
    type: ARCHITECTURE_BLOCK,
    architecture: {
      id: uuid(),
      active: '',
      title: '',
      status: ARCHITECTURE_DRAFT,
      metadata: {},
      allPackages: [pkg],
      ...initTree(pkg),
    },
  };
}
