/**
 * @name generateSearchLink
 * @description Generates a link with search parameters
 * @param {String} filter Filter name, i.e. taxonomies
 * @param {String} path Path to link to, i.e. problems
 * @param {String} propName Item Property name where filterOption is set
 * @param {String} items Items to filter
 * @return {String} /problems?taxonomies[0]=taxonom1&taxonomies[1]=taxonom2&
 */

// eslint-disable-next-line import/prefer-default-export
export function generateSearchLink(filter, path, propName, items) {
  let link = `/${path}?`;
  items.forEach((filterOption, index) => {
    link = link.concat(`${filter}[${index}]=${encodeURIComponent(filterOption[propName])}`);
    if (index !== (items.length - 1)) {
      link = `${link}&`;
    }
  });
  return link;
}
