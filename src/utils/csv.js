export function toCSV(json) {
  const columns = ['ancestors'];

  let csv = '';
  csv += `${csv}${columns.join(',')}\n`;

  json.forEach((line) => {
    csv += `${columns.map((key) => {
      if (key === 'ancestors') {
        let ancestorsString = '';
        line[key].forEach((a) => {
          let { term } = a;

          if (term.indexOf(',') > -1) {
            term = `"${term}"`;
          }

          ancestorsString += `${term}, `;
        });
        return ancestorsString;
      }
      return line[key];
    }).join(',')}\n`;
  });

  return csv;
}

export function sortCSVString(csvString) {
  const sortFunction = (a, b) => {
    // values with commas get extra quotation marks; ignore in sort
    a = a.replace(/"/g, '');
    b = b.replace(/"/g, '');

    return (a < b ? -1 : 1);
  };

  // join needs double quotes for newline
  // eslint-disable-next-line
  return csvString.split('\n').sort(sortFunction).join("\n");
}
