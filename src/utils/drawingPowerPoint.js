import { getArrowCoordsFromAnnotation } from 'utils/drawingTool';

const numRows = 12;
const rowWidth = 1;
const rowHeight = 1;
const smallPadding = 0.25;
const padding = 0.6;

const verticalPackageWidth = 0.5;
const verticalPackageMinHeight = rowHeight * 3;

const level2PackagePadding = 0.15;
const level2PackageHeight = 0.50;

const fontSize = 12;
const fontSizeScalingThreshold = 25;

const footerLogoSize = 0.35;

export function drawPowerPoint(architecture, packages, rect) {
  const { width: pixelWidth, height: pixelHeight } = rect;

  // to be determined once we know how many left and right packages there are
  let pptWidth = 0;
  let pptHeight = 0;

  const pptx = new PptxGenJS(); // eslint-disable-line

  const slide = pptx.addNewSlide({
    bkgd: 'ffffff',
    color: '000000',
  });

  // accumulates with the height of each root package as they're added
  let lastYPosition = 0;

  // loop over root packages to add roots and their children
  packages.visibleRootQIDs.forEach((rootQID) => {
    const rotatedLeftPackages = [];
    const rotatedRightPackages = [];
    const centerPackages = [];

    const { byQID } = packages;

    const rootPkg = byQID[rootQID];
    const pkgChildren = rootPkg.packages.map(p => byQID[p.id]);

    // split children into center, left, and right packages
    // TODO: root packages are always treated as center packages... address?
    pkgChildren.forEach((pkg) => {
      if (pkg.level1position === 0) {
        centerPackages.push(pkg);
      } else if (pkg.level1position === 1) {
        rotatedLeftPackages.push(pkg);
      } else if (pkg.level1position === 2) {
        rotatedRightPackages.push(pkg);
      }
    });

    // calculate height of center children
    let centerChildrenHeight = 0;
    centerPackages.forEach((pkg) => {
      const { level1height, level1y } = pkg;
      centerChildrenHeight = Math.max(centerChildrenHeight, level1height + level1y - smallPadding);
    });
    centerChildrenHeight = Math.max(verticalPackageMinHeight, centerChildrenHeight);

    // calculate width of center children
    const centerChildrenWidth = numRows * rowWidth - smallPadding;

    // calculate slide width
    const width = centerChildrenWidth
      + 2 * padding
      + (rotatedLeftPackages.length + rotatedRightPackages.length)
        * (verticalPackageWidth + smallPadding);

    // update overall ppt width
    pptWidth = width > pptWidth ? width : pptWidth;

    // calculate slide height
    const height = centerChildrenHeight
      + 2 * padding;

    // update overall ppt height
    pptHeight += height + padding;

    const opts = {
      shape: pptx.shapes.RECTANGLE,
      x: 0,
      y: lastYPosition,
      w: width,
      h: height,
      align: 'left',
      valign: 'top',
      line: '000000',
      line_size: 1,
      font_size: fontSize,
    };

    // add root package to ppt
    slide.addText(rootPkg.term, opts);

    if (pkgChildren.length > 0) {
      // add root's rotated right packages
      rotatedRightPackages.forEach((rightPkg, rightInd) => {
        slide.addText(rightPkg.term, {
          shape: pptx.shapes.RECTANGLE,
          x:
            width
            - (rightInd + 1) * verticalPackageWidth
            - rightInd * smallPadding
            - padding
            - (height - 2 * (smallPadding + padding)) / 2,
          y:
            (height - 2 * (smallPadding + padding)) / 2
            + lastYPosition
            + padding,
          h: verticalPackageWidth,
          w: height - padding * 2,
          rotate: 90,
          align: 'left',
          valign: 'top',
          line: '000000',
          line_size: 1,
          font_size: fontSize,
          fill: 'edf1f2',
        });
      });

      // add root's rotated left packages
      // packages are rotated about the center
      rotatedLeftPackages.forEach((leftPkg, leftInd) => {
        slide.addText(leftPkg.term, {
          shape: pptx.shapes.RECTANGLE,
          x:
            leftInd * (verticalPackageWidth + smallPadding)
            + padding
            - (height - 2 * (smallPadding + padding)) / 2,
          y:
            (height - 2 * (smallPadding + padding)) / 2
            + lastYPosition
            + padding,
          h: verticalPackageWidth,
          w: height - padding * 2,
          rotate: 270,
          align: 'left',
          valign: 'top',
          line: '000000',
          line_size: 1,
          font_size: fontSize,
          fill: 'edf1f2',
        });
      });

      const leftPackageIndent = padding
        + rotatedLeftPackages.length * (smallPadding + verticalPackageWidth);

      // add root's center packages
      centerPackages.forEach((childPkg) => {
        const {
          level1x: x,
          level1y: y,
          level1width: w,
          level1height: h,
        } = childPkg;

        const titlePosition = childPkg.level1titlePosition;

        const childPackageWidth = w * rowWidth - smallPadding;
        const childPackageHeight = h * rowHeight - smallPadding;
        const childXPosition = leftPackageIndent + x;
        const childYPosition = y // y coord
          + padding // add top padding
          + lastYPosition; // offset for previous roots

        const fontScalingFactor = fontSizeScalingThreshold * childPackageWidth;

        const scaledFontSize = childPkg.term.length < fontScalingFactor
          ? fontSize
          : fontSize * (fontScalingFactor / childPkg.term.length);

        let level1PackageOpts = {
          shape: pptx.shapes.RECTANGLE,
          x: childXPosition,
          y: childYPosition,
          w: childPackageWidth,
          h: childPackageHeight,
          align: 'center',
          valign: 'top',
          line: '000000',
          line_size: 1,
          font_size: scaledFontSize,
          fill: 'edf1f2',
        };

        if (titlePosition === 1) {
          level1PackageOpts = {
            ...level1PackageOpts,
            w: childPackageHeight,
            h: childPackageWidth,
            rotate: -90,
          };
        }

        if (titlePosition === 2) {
          level1PackageOpts = {
            ...level1PackageOpts,
            w: childPackageHeight,
            h: childPackageWidth,
            rotate: -270,
          };
        }

        slide.addText(childPkg.term, level1PackageOpts);

        if (childPkg.packages.length > 0) {
          childPkg.packages.forEach((childChildPkg, childChildInd) => {
            // get full pkg obj
            childChildPkg = byQID[childChildPkg.id];

            // determine if room for level 2 package
            const level2PackagesOffset = childChildInd
              * (level2PackageHeight + level2PackagePadding);

            const showChildChildPkg = childPackageHeight
              - padding - level2PackagesOffset
              >= level2PackageHeight;

            if (showChildChildPkg) {
              let level2PackageX = childXPosition + smallPadding;
              const level2PackageY = childYPosition + level2PackagesOffset + padding;
              let level2PackageWidth = childPackageWidth - smallPadding * 2;

              if (titlePosition === 1) {
                level2PackageX += smallPadding;
                level2PackageWidth -= smallPadding;
              }

              if (titlePosition === 2) {
                level2PackageWidth -= smallPadding;
              }

              const level2scaledFontSize = childChildPkg.term.length < fontScalingFactor
                ? fontSize
                : fontSize * (fontScalingFactor / childChildPkg.term.length);

              slide.addText(childChildPkg.term, {
                shape: pptx.shapes.RECTANGLE,
                x: level2PackageX,
                y: level2PackageY,
                w: level2PackageWidth,
                h: level2PackageHeight,
                align: 'center',
                valign: 'middle',
                line: '000000',
                line_size: 1,
                font_size: level2scaledFontSize,
                fill: 'ffffff',
              });
            }
          });
        }
      });

      // add root's annotations
      rootPkg.annotations.forEach((annotation) => {
        const {
          x,
          y,
          width: w,
          height: h,
          rotation: r,
        } = annotation;

        const annotationX = x / pixelWidth * width;
        const annotationY = y / pixelHeight * height;
        const annotationWidth = w / pixelWidth * width;
        const annotationHeight = h / pixelHeight * height;

        const coords = getArrowCoordsFromAnnotation(
          annotationWidth, annotationHeight, r,
        );
        const {
          x1,
          y1,
          x2,
          y2,
        } = coords;

        slide.addShape(pptx.shapes.LINE, {
          x: annotationX + x1,
          y: annotationY + y1,
          w: x2 - x1,
          h: y2 - y1,
          line: '000000',
          lineSize: 10,
          lineTail: 'triangle',
          rotate: 0,
        });
      });
    }

    lastYPosition += height + padding;
  });

  slide.addImage({
    path: '/logo.png',
    x: pptWidth - padding,
    y: lastYPosition,
    w: footerLogoSize,
    h: footerLogoSize,
  });

  pptHeight += footerLogoSize;

  pptx.setLayout({ name: 'DrawingTool', width: pptWidth, height: pptHeight });

  pptx.save(architecture.title || 'ARQ Architecture Drawing');
}

export default drawPowerPoint;
