import {
  nodeTypes,
} from 'constants/graph.js';
import {
  ARCHITECTURE_TYPENAME,
  PACKAGE_TYPENAME,
  TAXONOMY_TYPENAME,
  PROBLEM_TYPENAME,
  COMPANY_TYPENAME,
  CUSTOMER_TYPENAME,
} from 'constants/gql';
import { ARCHITECTURE_PUBLISHED } from 'constants/architectureStates.js';

/**
 * GENERAL UTILS
 */

// Helper function to create a cytoscape edge
function createCyEdge(sourceQID, targetQID, reverse) {
  let from = sourceQID;
  let to = targetQID;

  if (reverse) {
    from = targetQID;
    to = sourceQID;
  }

  return {
    data: {
      id: `${from}_${to}`,
      source: from,
      target: to,
    },
    selectable: true,
    grabbable: true,
  };
}

// Helper function to create a cytoscape node from a Neo node entity
// i.e. an Architecture, Package, Taxonomy, or Problem
function createCyNode(entity, label) {
  const { id } = entity;
  const type = nodeTypes[label];
  const content = type.getContent(entity);
  const { classes } = type;

  return {
    data: {
      id,
      label,
      content,
      entity,
    },
    classes,
    selectable: true,
    grabbable: true,
  };
}

// Helper function to create cy nodes from a list of packages, one level deep
// Edge direction can be reversed
function packagesToCy(packages, parentQID, reverseEdges) {
  const elements = [];

  if (packages && packages.length > 0) {
    packages.forEach((pkg) => {
      elements.push(createCyNode(pkg, PACKAGE_TYPENAME));
      elements.push(createCyEdge(parentQID, pkg.id, reverseEdges));
    });
  }
  return elements;
}

// Helper function to create cy nodes from a list of taxonomies
// Edge direction can be reversed
function taxonomiesToCy(taxonomies, parentQID, reverseEdges) {
  const elements = [];

  if (taxonomies && taxonomies.length > 0) {
    taxonomies.forEach((taxonomy) => {
      elements.push(createCyNode(taxonomy, TAXONOMY_TYPENAME));
      elements.push(createCyEdge(parentQID, taxonomy.id, reverseEdges));
    });
  }
  return elements;
}

// Helper function to create cy nodes from a list of problems
// Edge direction can be reversed
function problemsToCy(problems, parentQID, reverseEdges) {
  const elements = [];
  if (problems && problems.length > 0) {
    problems.forEach((problem) => {
      elements.push(createCyNode(problem, PROBLEM_TYPENAME));
      elements.push(createCyEdge(parentQID, problem.id, reverseEdges));
    });
  }
  return elements;
}

// Helper function to check if an architecture should be viewable on graph
export function isViewableArchitecture(architecture, rootEntity = {}) {
  const { id, status, isLatestArch } = architecture;

  if (id === rootEntity.id) return true;
  return status === ARCHITECTURE_PUBLISHED && isLatestArch;
}

export function isViewableLink(entity, relatedEntity, rootEntity) {
  if (!relatedEntity) return false;

  const { __typename: entityType } = entity;
  const { __typename: relatedEntityType } = relatedEntity;

  // only include packages from viewable architectures (i.e. published or graph root)
  if (relatedEntityType === PACKAGE_TYPENAME) {
    return isViewableArchitecture(relatedEntity.architecture, rootEntity);
  }

  // only include architectures if viewable and package has no other parent
  if (relatedEntityType === ARCHITECTURE_TYPENAME) {
    return !entity.parent && isViewableArchitecture(relatedEntity, rootEntity);
  }

  // only include capability if package link isn't broken
  // TODO: Do we need to perform linkBroken check when taxonomy is expanding too?
  if (relatedEntityType === TAXONOMY_TYPENAME && entityType === PACKAGE_TYPENAME) {
    return entity.linkBroken !== true;
  }

  return true;
}

/**
 * INIT GRAPH ELEMENTS BY ROOT TYPE
 */
export function initGraphElements(item) {
  const { __typename: typename } = item;
  let elements;

  switch (typename) {
    case ARCHITECTURE_TYPENAME:
      elements = [
        createCyNode(item, ARCHITECTURE_TYPENAME),
        ...createArchitecturePackagesCy(item.packages, item.id),
      ];
      break;
    case TAXONOMY_TYPENAME:
      elements = [
        createCyNode(item, TAXONOMY_TYPENAME),
        ...createTaxonomyRelatedEls(item),
      ];

      break;
    case PROBLEM_TYPENAME:
      elements = [
        createCyNode(item, PROBLEM_TYPENAME),
        ...createProblemRelatedEls(item, item.id),
      ];
      break;
    case CUSTOMER_TYPENAME:
      elements = [
        createCyNode(item, CUSTOMER_TYPENAME),
        ...createCustomerProblemsCy(item.problemSets, item.id),
      ];
      break;
    case COMPANY_TYPENAME:
      elements = [
        createCyNode(item, COMPANY_TYPENAME),
        ...createCompanyTaxonomiesCy(item.capabilities, item.id),
      ];
      break;
    default:
      elements = [];
  }

  return elements;
}


/**
 * ARCHITECTURE
 */
// Create cy nodes and edges for the child packages of a parent architecture
export function createArchitecturePackagesCy(packages = [], parentQID) {
  return packagesToCy(packages, parentQID);
}

/**
 * PROBLEM
 */
// Create cy node and edge for the child taxonomies of a parent problem
export function createProblemTaxonomiesCy(taxonomies = [], parentQID) {
  return taxonomiesToCy(taxonomies, parentQID);
}

export function createProblemCustomerCy(customer, parentQID) {
  const elements = [];
  if (customer) {
    elements.push(createCyNode(customer, CUSTOMER_TYPENAME));
    elements.push(createCyEdge(customer.id, parentQID));
  }
  return elements;
}

export function createProblemRelatedEls(problem) {
  return [
    ...createProblemTaxonomiesCy(problem.capabilities, problem.id),
    ...createProblemCustomerCy(problem.customer, problem.id),
  ];
}

/**
 * CUSTOMER
 */
// Create cy node and edge for the child taxonomies of a parent customer
export function createCustomerProblemsCy(problemSets = [], parentQID) {
  return problemsToCy(problemSets, parentQID);
}

/**
 * COMPANY
 */
// Create cy node and edge for the child taxonomies of a parent company
export function createCompanyTaxonomiesCy(taxonomies = [], parentQID) {
  return taxonomiesToCy(taxonomies, parentQID);
}

/**
 * TAXONOMY
 */
// Create cy nodes and edges for the child problems of a parent taxonomy
export function createTaxonomyProblemsCy(problems, parentQID) {
  const elements = [];
  problems.forEach((problem) => {
    elements.push(createCyNode(problem, PROBLEM_TYPENAME));
    elements.push(createCyEdge(problem.id, parentQID));
  });
  return elements;
}

// Create cy nodes and edges for the referencing packages of a taxonomy
export function createTaxonomyPackagesCy(packages = [], parentQID) {
  return packagesToCy(packages, parentQID, true);
}

// Create cy node and edge for the child taxonomy of a parent package
export function createTaxonomyCompaniesCy(companies = [], parentQID) {
  const elements = [];
  companies.forEach((company) => {
    elements.push(createCyNode(company, COMPANY_TYPENAME));
    elements.push(createCyEdge(company.id, parentQID));
  });
  return elements;
}

export function createTaxonomyChildrenTaxonomiesCy(taxonomies = [], parentQID) {
  const elements = [];
  taxonomies.forEach((taxonomy) => {
    elements.push(createCyNode(taxonomy, TAXONOMY_TYPENAME));
    elements.push(createCyEdge(taxonomy.id, parentQID, true));
  });
  return elements;
}

export function createTaxonomyParentTaxonomyCy(taxonomy, parentQID) {
  const elements = [];
  if (taxonomy) {
    elements.push(createCyNode(taxonomy, TAXONOMY_TYPENAME));
    elements.push(createCyEdge(taxonomy.id, parentQID));
  }
  return elements;
}

// Create cy nodes and edges for all taxonomy related nodes
export function createTaxonomyRelatedEls(taxonomy, rootEntity) {
  const packages = taxonomy.referencedBy
    .filter(pkg => isViewableLink(taxonomy, pkg, rootEntity));

  return [
    ...createTaxonomyProblemsCy(taxonomy.problemSets, taxonomy.id),
    ...createTaxonomyCompaniesCy(taxonomy.companies, taxonomy.id),
    ...createTaxonomyPackagesCy(packages, taxonomy.id),
    ...createTaxonomyChildrenTaxonomiesCy(taxonomy.children, taxonomy.id),
    ...createTaxonomyParentTaxonomyCy(taxonomy.parent, taxonomy.id),
  ];
}

/**
 * PACKAGE
 */
// Create cy nodes and edges for the child packages of a parent package
export function createPackagePackagesCy(packages = [], parentQID) {
  return packagesToCy(packages, parentQID);
}

// Create cy node and edge for the parent package of a child package
export function createPackageParentPackageCy(parentPackage, parentQID) {
  return parentPackage ? packagesToCy([parentPackage], parentQID, true) : [];
}

// Create cy node and edge for the reference package of a package
export function createPackageReferencePackageCy(referencePackage, parentQID) {
  return referencePackage ? packagesToCy([referencePackage], parentQID) : [];
}

// Create cy node and edge for the child taxonomy of a parent package
export function createPackageTaxonomyCy(taxonomy, parentQID) {
  return taxonomy ? (taxonomiesToCy([taxonomy], parentQID)) : [];
}

// Create cy node and edge for the child taxonomy of a parent package
export function createPackageArchitectureCy(architecture, parentQID) {
  return architecture ? [
    createCyNode(architecture, ARCHITECTURE_TYPENAME),
    createCyEdge(architecture.id, parentQID),
  ] : [];
}

// Create cy nodes and edges for all package related nodes
export function createPackageRelatedEls(pkg, rootEntity) {
  const {
    packages,
    taxonomy,
    parent,
    referencePackage,
    architecture,
  } = pkg;

  const linkedTaxonomy = isViewableLink(pkg, taxonomy, rootEntity) ? taxonomy : null;
  const parentArchitectures = isViewableLink(pkg, architecture, rootEntity) ? architecture : null;

  return [
    ...createPackagePackagesCy(packages, pkg.id),
    ...createPackageTaxonomyCy(linkedTaxonomy, pkg.id),
    ...createPackageParentPackageCy(parent, pkg.id),
    ...createPackageReferencePackageCy(referencePackage, pkg.id),
    ...createPackageArchitectureCy(parentArchitectures, pkg.id),
  ];
}
