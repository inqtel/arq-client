import { extend } from 'lodash';

/**
 * @name createProblemFromElasticHit
 * @description Creates a problem from an Elasticsearch hit
 * @param {Object} hit The ES hit object
 * @return {Object} The problem object
 */
export function createProblemFromElasticHit(result) {
  const source = extend(
    {},
    result._source, // eslint-disable-line no-underscore-dangle
    result.highlight,
  );

  const {
    // @timestamp,
    // @version,
    id,
    number,
    year,
    priority,
    category,
    categoryofneed,
    taxonomies,
    customer,
    // description,
    // synced_at,
  } = source;

  return {
    id,
    customerNumber: number,
    fiscalYear: year,
    originationYear: null,
    priority,
    customerCategory: category,
    categoryOfNeed: categoryofneed,
    taxonomies,
    customer: {
      name: customer,
    },
  };
}

/**
 * @name createTaxonomyFromElasticHit
 * @description Creates a taxonomy from an Elasticsearch hit
 * @param {Object} hit The ES hit object
 * @return {Object} The taxonomy object
 */
export function createTaxonomyFromElasticHit(hit) {
  const { _source: taxonomy } = hit;
  return {
    ...taxonomy,
    expand: true,
    children: (taxonomy.children || []).map(child => ({ ...child, children: [] })),
  };
}

/**
 * @name orderAncestorsByParentId
 * @description Orders an Elasticsearch taxonomy's ancestors array by ancestor parent id
 * @param {Object} ancestors The ancestors array
 * @return {Object} The ordered array
 */
export function orderAncestorsByParentId(ancestors) {
  const addChild = (acc) => {
    const nextItem = ancestors.find(anc => anc.parent_id === acc[acc.length - 1].id);

    if (nextItem) {
      acc.push(nextItem);
      return addChild(acc);
    }

    return acc;
  };

  const root = ancestors.find(anc => !anc.parent_id);
  return addChild([root]);
}
