import { EditorState } from 'draft-js';
import { stateFromHTML } from 'draft-js-import-html';
import { now } from 'utils/time';
import { constructPackageTree } from 'utils/gql';
import { v4 as uuid } from 'uuid';
import { get } from 'lodash';

export function createHTMLBlockEntity({ id, type = 'html' }) {
  return { id, type, editor: id };
}

export function createArchitectureBlockEntity({ id, type = 'architecture', architecture }) {
  return { id, type, architecture: architecture.id };
}

export function createEditorEntity({ id, body }) {
  return {
    id,
    editorState: body
      ? EditorState.createWithContent(stateFromHTML(body))
      : EditorState.createEmpty(),
  };
}

function createPackageFromTaxonomy(taxonomy) {
  const createdAt = now();
  const updatedAt = createdAt;

  return {
    id: uuid(),
    createdAt,
    updatedAt,
    description: '',
    packages: [],
    show: true,
    term: taxonomy.term,
    taxonomy,
  };
}

function createPackagesFromTaxonomyChildren(children) {
  return children.map((child) => {
    const newPackage = createPackageFromTaxonomy(child);

    if (child.children.length > 0) {
      newPackage.packages = createPackagesFromTaxonomyChildren(child.children);
    }

    return newPackage;
  });
}

export function createPackagesFromTaxonomy(taxonomy) {
  const rootPackage = createPackageFromTaxonomy(taxonomy);

  if (taxonomy.children.length > 0) {
    rootPackage.packages = createPackagesFromTaxonomyChildren(taxonomy.children);
  }

  return rootPackage;
}

function createPackageFromReferencePackage(referencePackage) {
  const createdAt = now();
  const updatedAt = createdAt;

  return {
    id: uuid(),
    createdAt,
    updatedAt,
    description: '',
    packages: [],
    show: true,
    term: referencePackage.term,
    referencePackage,
    taxonomy: referencePackage.taxonomy,
  };
}

function createPackagesFromReferencePackageChildren(children) {
  return children.map((child) => {
    const newPackage = createPackageFromReferencePackage(child);

    if (child.packages.length > 0) {
      newPackage.packages = createPackagesFromReferencePackageChildren(
        child.packages,
      );
    }

    return newPackage;
  });
}

export function createPackagesFromReferencePackage(referencePackage) {
  const rootPackage = createPackageFromReferencePackage(referencePackage);

  if (referencePackage.packages.length > 0) {
    rootPackage.packages = createPackagesFromReferencePackageChildren(
      referencePackage.packages,
    );
  }

  return rootPackage;
}

function setPackageOldTermField(pkg) {
  pkg.oldTerm = pkg.term;

  if (pkg.packages.length > 0) {
    pkg.packages.forEach((child) => {
      setPackageOldTermField(child);
    });
  }
}

export function createArchitectureEntity({ architecture }) {
  const packages = constructPackageTree(architecture.allPackages);

  if (packages.length > 0) {
    packages.forEach((pkg) => {
      setPackageOldTermField(pkg);
    });
  }

  const metadata = {
    publishedAt: get(architecture, 'latestArch[0].publishedAt'),
    rejectedAt: architecture.rejectedAt,
  };

  return {
    ...architecture,
    packages,
    metadata,
  };
}
