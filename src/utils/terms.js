/**
 * @name convertTermToTaxonomy
 * @description Creates a new taxonomy from a term
 * @param {Object} term The new architecture term
 * @return {Object} New taxonomy
 */
export function convertTermToTaxonomy(term) {
  return {
    id: term.packageID,
    term: term.term,
    children: [],
    temp: true,
  };
}

/**
 * @name createTermsListFromTree
 * @description Creates an array of term strings from a taxonomy tree for admin submit new terms
 * @param {String} taxonomy The root taxonomy
 * @param {Array} acc Optional accummulator
 * @return {Array} Array of terms as strings
 */
export function createTermsListFromTree(taxonomy, acc = []) {
  if (taxonomy.term) {
    acc.push(taxonomy.term);

    if (taxonomy.children && taxonomy.children.length) {
      createTermsListFromTree(taxonomy.children[0], acc);
    }
  }

  return acc;
}
