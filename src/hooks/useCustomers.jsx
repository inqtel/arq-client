import { useQuery } from '@apollo/client';
import { GET_CUSTOMERS } from 'queries/customers';
import { CUSTOMERS_QUERY_ROOT } from 'constants/gql';
import { get } from 'lodash';

const useCustomers = () => {
  const {
    loading,
    error,
    data,
  } = useQuery(GET_CUSTOMERS, {
    fetchPolicy: 'cache-and-network',
  });

  const customers = get(data, CUSTOMERS_QUERY_ROOT, []);

  return {
    customers,
    error,
    loading,
  };
};

export default useCustomers;
