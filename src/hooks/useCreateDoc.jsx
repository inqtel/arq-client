import { v4 as uuid } from 'uuid';
import { useMutation } from '@apollo/client';
import { CREATE_DOCUMENT } from 'queries/documents';
import { useHistory } from 'react-router-dom';

const useCreateDoc = () => {
  const history = useHistory();
  const [createDocument] = useMutation(CREATE_DOCUMENT);

  const createDoc = () => {
    const id = uuid();
    createDocument({ variables: { id } }).then(() => {
      history.push(`/documents/${id}`);
    });
  };

  return {
    createDoc,
  };
};

export default useCreateDoc;
