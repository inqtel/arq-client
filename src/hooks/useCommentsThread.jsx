import { get } from 'lodash';
import { useLazyQuery } from '@apollo/client';
import { useState, useEffect } from 'react';
import { GET_PACKAGE_COMMENTS } from 'queries/comments';
import { PACKAGE_QUERY_ROOT } from 'constants/gql';

const useCommentsThread = () => {
  const [activePackageQID, setActivePackageQID] = useState(null);

  const [getPackageComments, {
    loading: commentsThreadLoading,
    error: commentsThreadError,
    data,
  }] = useLazyQuery(GET_PACKAGE_COMMENTS);

  useEffect(() => {
    activePackageQID && getPackageComments({ variables: { id: activePackageQID } });
  }, [getPackageComments, activePackageQID]);

  const setDocActiveComments = pkg => setActivePackageQID(pkg.id);
  const unsetDocActiveComments = () => setActivePackageQID(null);

  const comments = get(data, [PACKAGE_QUERY_ROOT, 'comments'], []);

  return {
    setDocActiveComments,
    unsetDocActiveComments,
    activeComments: {
      commentsThreadLoading,
      commentsThreadError,
      packageQID: activePackageQID,
      commentsThread: {
        comments,
      },
    },
  };
};

export default useCommentsThread;
