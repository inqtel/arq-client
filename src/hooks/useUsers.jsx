import { useQuery } from '@apollo/client';
import { GET_USERS } from 'queries/users';
import { USERS_QUERY_ROOT } from 'constants/gql';
import { get } from 'lodash';

const useUsers = () => {
  const { loading, error, data } = useQuery(GET_USERS);
  const users = get(data, USERS_QUERY_ROOT, []);

  return { loading, error, users };
};

export default useUsers;
