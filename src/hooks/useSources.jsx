import { useQuery } from '@apollo/client';
import { GET_SOURCES } from 'queries/sources';
import { SOURCES_QUERY_ROOT } from 'constants/gql';
import { get } from 'lodash';

const useSources = () => {
  const { loading, error, data } = useQuery(GET_SOURCES);
  const sources = get(data, SOURCES_QUERY_ROOT, []);

  return {
    error,
    loading,
    sources,
  };
};

export default useSources;
