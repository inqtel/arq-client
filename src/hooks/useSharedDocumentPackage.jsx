import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { packageToggleChildren as packageToggleChildrenRaw } from 'actions/sharedDoc';

const useSharedDocumentPackage = () => {
  // redux
  const dispatch = useDispatch();

  const packageToggleChildren = useCallback(
    (
      architectureQID,
      packageQID,
    ) => dispatch(packageToggleChildrenRaw(architectureQID, packageQID)),
    [dispatch],
  );

  return {
    packageToggleChildren,
  };
};

export default useSharedDocumentPackage;
