import { useMedia } from 'react-media';

const GLOBAL_MEDIA_QUERIES = {
  desktopSm: '(max-width: 899px)',
};

const useViewport = () => {
  const viewport = useMedia({ queries: GLOBAL_MEDIA_QUERIES });

  return { viewport };
};

export default useViewport;
