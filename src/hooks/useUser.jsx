import { useQuery } from '@apollo/client';
import { get } from 'lodash';

import {
  GET_AUTHENTICATED_USER,
} from 'queries/auth';

const useUser = () => {
  const { data } = useQuery(GET_AUTHENTICATED_USER, {
    fetchPolicy: 'cache-only',
  });

  const user = get(data, ['arq_authenticated_user', 0]);

  return { user };
};

export default useUser;
