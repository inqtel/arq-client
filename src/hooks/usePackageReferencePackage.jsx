import { useQuery } from '@apollo/client';
import { GET_PACKAGE_REFERENCE_PACKAGE_ANCESTORS } from 'queries/packages';
import { PACKAGE_QUERY_ROOT } from 'constants/gql';
import { get } from 'lodash';
import { listToTree } from 'utils/tree';

const usePackageReferencePackage = (pkg) => {
  const { loading, error, data } = useQuery(
    GET_PACKAGE_REFERENCE_PACKAGE_ANCESTORS, { variables: { id: pkg.id } },
  );

  const ancestors = get(data, [PACKAGE_QUERY_ROOT, 'referencePackage', 'ancestors'], []);
  const tree = listToTree(ancestors)[0];

  return { loading, error, tree };
};

export default usePackageReferencePackage;
