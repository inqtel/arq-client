import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies
import fragments from 'queries/fragments';

export const GET_PROBLEM_DETAILS = gql`
  query ProblemDetails($id: String!) {
    ProblemSetsByID(id: $id) {
      ...ProblemFields
      customer {
        ...CustomerFields
      }
      capabilities {
        ...TaxonomyCardFields
      }
    }
  }
  ${fragments.problemFields}
  ${fragments.customerFields}
  ${fragments.taxonomyCardFields}
`;

export const GET_PROBLEM_COMPANIES = gql`
  query ProblemCompanies($id: String!) {
    ProblemSetsByID(id: $id) {
      id
      capabilities {
        id
        term
        source {
          name
        }
        companies {
          ...CompanyCardFields
        }
      }
    }
  }
  ${fragments.companyCardFields}
`;

export const GET_PROBLEM_PROBLEMS = gql`
  query ProblemProblems($id: String!) {
    ProblemSetsByID(id: $id) {
      id
      capabilities {
        id
        term
        problemSets {
          ...ProblemCardFields
        }
      }
    }
  }
  ${fragments.problemCardFields}
`;

export const GET_PROBLEMS_TAXONOMIES = gql`
  query ProblemsTaxonomies {
    ProblemSets {
      id
      capabilities {
        id
        term
      }
    }
  }
`;
