import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies
import fragments from 'queries/fragments';

export const GET_PACKAGE_COMMENTS = gql`
  query GetPackageComments($id: uuid!) {
    PackagesByID(id: $id) {
      id
      comments(order_by: {createdAt: asc}) {
        ...CommentFields
      }
    }
  }
  ${fragments.commentFields}
`;

export const ADD_PACKAGE_COMMENT = gql`
  mutation AddDocPackageComment($object: arq_package_comments_insert_input!) {
    InsertPackageCommentsOne(object:$object) {
      id
      text
      authorEmail
      packageId
      resolvedAt
    }
  }
`;

export const SOFT_DELETE_PACKAGE_COMMENT = gql`
  mutation SoftDeleteDocPackageComment($id: uuid!, $deletedAt: timestamp) {
    UpdateByIDPackageComments(
      pk_columns: {id: $id},
      _set: {deletedAt: $deletedAt}
    ) {
        id
    }
  }
`;
