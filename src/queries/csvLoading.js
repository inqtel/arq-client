import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies

// eslint-disable-next-line import/prefer-default-export
export const GET_TAXONOMY_CSV = gql`
  query CsvLoading {
    state @client {
      taxonomies {
        csvLoading
      }
    }
  }
`;
