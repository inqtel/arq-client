import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies
import fragments from 'queries/fragments';

export const GET_PITCHBOOK_INTERSECTION = gql`
  query pitchbookIntersection($pitchbookIds: [String!]) {
    Organizations(where: { pitchbookId: { _in: $pitchbookIds } }) {
      id
      name
      pitchbookId
    }
  }
`;

export const GET_COMPANY_DETAILS = gql`
  query CompanyDetails($id: String!) {
    OrganizationsByID(id: $id) {
      id
      name
      pitchbookId
      capabilities {
        ...TaxonomyCardFields
      }
    }
  }
  ${fragments.taxonomyCardFields}
`;

export const GET_COMPANY_COMPANIES = gql`
  query CompanyCompanies($id: String!) {
    OrganizationsByID(id: $id) {
      id
      capabilities {
        id
        term
        companies {
          ...CompanyCardFields
        }
      }
    }
  }
  ${fragments.companyCardFields}
`;

export const GET_COMPANY_PROBLEMS = gql`
  query CompanyProblems($id: String!) {
    OrganizationsByID(id: $id) {
      id
      capabilities {
        id
        term
        problemSets {
          ...ProblemCardFields
        }
      }
    }
  }
  ${fragments.problemCardFields}
`;

export const GET_COMPANIES_TAXONOMIES = gql`
  query CompaniesTaxonomies {
    Organizations {
      id
      capabilities {
        id
        term
      }
    }
  }
`;
