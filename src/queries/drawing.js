import gql from 'graphql-tag'; // eslint-disable-line import/no-extraneous-dependencies
import fragments from 'queries/fragments';

export const GET_DRAWING_ARCHITECTURE = gql`
  query GetArchitecture($id: uuid!) {
    ArchitecturesByID(id: $id) {
      id
      createdAt
      publishedAt
      updatedAt
      title
      packages {
        id
      }
      allPackages {
        id
        term
        sequence
        level0position
        level0titlePosition
        level1x
        level1y
        level1width
        level1height
        level1drawn
        level1hideChildren
        level1columns
        level1position
        level1titlePosition
        level2position
        parent {
          id
        }
        packages {
          id
        }
        annotations {
          id
          x
          y
          width
          height
          rotation
          organization {
            id
            logo
          }
        }
      }
    }
  }
`;

export const GET_DRAWING_STATE = gql`
  query GetDrawingState {
    drawingState @client {
      architectureID
      layoutLastGenerated
      drawingRasterProps {
        html
        width
        height
        devicePixelRatio
        gutter
      }
      focusID
      selectedThemeName
      editable
      companyModalOpen
    }
  }
`;

export const UPDATE_DRAWING_SPEC = gql`
  mutation UpdatePackageDrawing(
    $id: uuid!,
    $level0position: bigint!,
    $level0titlePosition: bigint!,
    $level1x: bigint!,
    $level1y: bigint!,
    $level1width: bigint!,
    $level1height: bigint!,
    $level1drawn: Boolean!,
    $level1hideChildren: Boolean!,
    $level1columns: bigint!,
    $level1position: bigint!,
    $level1titlePosition: bigint!,
    $level2position: bigint!
  ) {
    UpdateByIDPackages(pk_columns: {id: $id}, _set: {
      level0position: $level0position,
      level0titlePosition: $level0titlePosition,
      level1x: $level1x,
      level1y: $level1y,
      level1width: $level1width,
      level1height: $level1height,
      level1drawn: $level1drawn,
      level1hideChildren: $level1hideChildren,
      level1columns: $level1columns,
      level1position: $level1position,
      level1titlePosition: $level1titlePosition,
      level2position: $level2position,
    }) {
      id
    }
  }
`;

export const GET_ANNOTATION_SPEC = gql`
  query GetAnnotationSpec($id: uuid!) {
    AnnotationSpecsByID(id: $id) {
      ...AnnotationFields
    }
  }
  ${fragments.annotationFields}
`;

export const CREATE_ANNOTATION_SPEC = gql`
  mutation CreateAnnotationSpec($object: arq_package_annotations_insert_input!) {
    # {
    #   "object": {
    #     "x": 40,
    #     "y": 40,
    #     "width": 100,
    #     "rotation": 0,
    #     "height": 100,
    #     "organizationId": null,
    #     "packageId": "8d4ea9e7-7506-448b-ba1b-bdb68a6da40c"
    #   }
    # }
    InsertPackageAnnotationsOne(object: $object) {
      id
    }
  }
`;

export const UPDATE_ANNOTATION_SPEC = gql`
  mutation UpdateAnnotationSpec($id: uuid!, $x: bigint, $y: bigint, $width: bigint, $height: bigint, $rotation: bigint) {
    # "id": "19326517-0b8d-4d03-82b0-3a8c74390ff1",
    # "x": 500,
    # "y": 100,
    # "width": 110,
    # "height": 110,
    # "rotation": 90
    UpdateByIDAnnotationSpecs(pk_columns: {id: $id}, _set: {x: $x, y: $y, width: $width, rotation: $rotation, height: $height}) {
      id
    }
  }
`;


export const DELETE_ANNOTATION_SPEC = gql`
  mutation DeleteAnnotationSpec($id: uuid!) {
    DeleteByIDAnnotationSpecs(id: $id) {
      id
    }
  }
`;


export const GET_PACKAGE_ANNOTATIONS = gql`
  query GetPackageAnnotations($id: uuid!) {
    PackagesByID(id: $id) {
      id
      annotations {
        ...Annotation
      }
    }
  }
  ${fragments.annotationFields}
`;

export const RESET_DRAWING = gql`
  mutation ResetDrawing {
    resetDrawing @client
  }
`;
