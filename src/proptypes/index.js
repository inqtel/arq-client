import PropTypes from 'prop-types';
import {
  SOURCE_PLOS,
  SOURCE_ACM,
  SOURCE_IEEE,
  SOURCE_ARQ,
  SOURCE_MISSION,
} from 'constants/sources.js';

export const userPropType = PropTypes.shape({
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  email: PropTypes.string,
});

export const taxonomyPropType = PropTypes.shape({
  children: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.string,
  })),
  expand: PropTypes.bool,
  id: PropTypes.string,
  references: PropTypes.arrayOf(packagePropType),
  source: sourcePropType,
  term: PropTypes.string,
});

export const sourcePropType = PropTypes.shape({
  id: PropTypes.string,
  name: PropTypes.string,
  children: PropTypes.arrayOf(taxonomyPropType),
});

export const architecturePropType = PropTypes.shape({
  author: userPropType,
  id: PropTypes.string,
  createdAt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  updatedAt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  rejectedAt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  publisher: userPropType,
  publishedAt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  status: PropTypes.string,
  title: PropTypes.string,
  packages: packageArray,
});

export const packagePropType = PropTypes.shape({
  id: PropTypes.string,
  createdAt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  updatedAt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  term: PropTypes.string,
  description: PropTypes.string,
  taxonomy: taxonomyPropType,
  referencePackage: packagePropType,
  packages: packageArray,
  order: PropTypes.number,
  show: PropTypes.bool,
  linkBroken: PropTypes.bool,
  architecture: architecturePropType,
});

const packageArray = PropTypes.arrayOf(packagePropType);

export const documentPropType = PropTypes.shape({
  id: PropTypes.string,
  title: PropTypes.string,
  blocks: PropTypes.arrayOf[PropTypes.string],
  author: userPropType,
  createdAt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  updatedAt: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  status: PropTypes.string,
  collaborationInvitationQID: PropTypes.string,
});

export const companyPropType = PropTypes.shape({
  id: PropTypes.string,
  externalID: PropTypes.string,
  name: PropTypes.node,
  capabilities: PropTypes.arrayOf(taxonomyPropType),
});

// TODO: Fill out problem props
export const problemPropType = PropTypes.shape({
  id: PropTypes.string,
});

export const customerPropType = PropTypes.shape({
  id: PropTypes.string,
  name: PropTypes.string,
  capabilities: PropTypes.arrayOf(taxonomyPropType),
});

export const treeItemPropType = PropTypes.shape({
  id: PropTypes.string,
  term: PropTypes.string,
  source: sourcePropType,
  expand: PropTypes.bool,
});

export const treeItemActionPropType = PropTypes.shape({
  title: PropTypes.string,
  icon: PropTypes.node,
  handler: PropTypes.func,
});

export const treeItemActionsPropType = PropTypes.shape({
  primary: PropTypes.arrayOf(treeItemActionPropType),
});

export const onboardingPopupPropType = PropTypes.shape({
  panel: PropTypes.number,
  completed: PropTypes.bool,
});

export const commentPropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  text: PropTypes.string,
  createdAt: PropTypes.string,
  updatedAt: PropTypes.string,
  deletedAt: PropTypes.string,
});

// apollo
export const queryPropType = PropTypes.shape({
  kind: PropTypes.string,
  definitions: PropTypes.arrayOf(PropTypes.object),
});

export const apolloErrorPropType = PropTypes.shape({
  graphQLErrors: PropTypes.arrayOf(PropTypes.shape({
    message: PropTypes.string,
  })),
  networkError: PropTypes.shape({}),
});

export const apolloClientPropType = PropTypes.shape({
  query: PropTypes.func.isRequired,
});

// react router
export const historyPropType = PropTypes.shape({
  length: PropTypes.number,
  action: PropTypes.string,
  location: PropTypes.shape({}),
  pathname: PropTypes.string,
  search: PropTypes.string,
  hash: PropTypes.string,
  state: PropTypes.shape({}),
  push: PropTypes.func,
  replace: PropTypes.func,
  go: PropTypes.func,
  goBack: PropTypes.func,
  goForward: PropTypes.func,
  block: PropTypes.func,
});

export const matchPropType = PropTypes.shape({
  params: PropTypes.shape({}).isRequired,
  isExact: PropTypes.bool.isRequired,
  path: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
});

// elastic
export const esSearchResultPropType = PropTypes.shape({
  _source: PropTypes.shape({}),
});

// redux stores
export const searchStorePropType = PropTypes.shape({
  results: searchResultsPropType,
  term: PropTypes.string,
  error: requestErrorPropType,
  sourceFilters: sourceFiltersPropType,
  capabilitySourceFilters: capabilitySourceFiltersPropType,
});

export const requestErrorPropType = PropTypes.shape({
  config: PropTypes.any,
  request: PropTypes.any,
  response: PropTypes.any,
  message: PropTypes.string,
  stack: PropTypes.string,
});

export const searchResultsPropType = PropTypes.shape({
  architectures: PropTypes.arrayOf(architecturePropType),
  taxonomies: PropTypes.arrayOf(taxonomyPropType),
});

export const capabilitySourceFiltersPropType = PropTypes.shape({
  [SOURCE_ARQ]: PropTypes.bool,
  [SOURCE_MISSION]: PropTypes.bool,
});

export const sourceFiltersPropType = PropTypes.shape({
  [SOURCE_PLOS]: PropTypes.bool,
  [SOURCE_ACM]: PropTypes.bool,
  [SOURCE_IEEE]: PropTypes.bool,
  [SOURCE_ARQ]: PropTypes.bool,
  [SOURCE_MISSION]: PropTypes.bool,
});

export const authStorePropType = PropTypes.shape({
  user: userPropType,
  error: PropTypes.any,
});

export const adminArchitecturesStorePropType = PropTypes.shape({
  approved: adminArchitecturePropType,
  pending: adminArchitecturePropType,
});

export const adminArchitecturePropType = PropTypes.shape({
  entities: PropTypes.any,
  qids: PropTypes.arrayOf(PropTypes.string),
  ui: uiPropType,
});

export const uiPropType = PropTypes.shape({
  loading: PropTypes.bool,
  error: PropTypes.any,
});

export const drawingRasterPropsPropType = PropTypes.shape({
  html: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  devicePixelRatio: PropTypes.number.isRequired,
  gutter: PropTypes.number.isRequired,
});

export const drawingToolThemePropType = PropTypes.shape({
  name: PropTypes.string,
  swatches: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string,
      fill: PropTypes.string,
    }),
  ),
});

export const drawingToolBreadcrumbPropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
});

export const drawingToolPropType = PropTypes.shape({
  doc: documentPropType.isRequired,
  architecture: architecturePropType.isRequired,
  packages: PropTypes.arrayOf(packagePropType).isRequired,
  drawingRasterProps: drawingRasterPropsPropType.isRequired,
  ui: PropTypes.shape({
    selectedTheme: drawingToolThemePropType,
    breadcrumbs: PropTypes.arrayOf(drawingToolBreadcrumbPropType),
  }).isRequired,
});

const navigationLinkPropType = PropTypes.shape({
  name: PropTypes.string,
  path: PropTypes.string,
  shortname: PropTypes.string,
});

export const navigationPropType = PropTypes.shape({
  navigationLinkPropType,
  sublinks: PropTypes.arrayOf(navigationLinkPropType),
});

// responsiveness
export const viewportPropType = PropTypes.shape({
  desktopSm: PropTypes.bool,
});
