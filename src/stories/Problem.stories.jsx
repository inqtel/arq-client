import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { storiesOf } from '@storybook/react';
import { customer } from 'mocks/customers';
import {
  problemPriorityHigh,
  problemPriorityMedium,
  problemPriorityLow,
  problemPriorityNotAssigned,
  esProblem,
} from 'mocks/problem';
import Problem from 'components/Problem/Problem.jsx';
import 'components/SearchKit/SearchKit.scss';

storiesOf('Problem', module)
  .addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .add('priority high', () => (
    <Problem customer={customer} problem={problemPriorityHigh} />
  ))
  .add('priority medium', () => (
    <Problem customer={customer} problem={problemPriorityMedium} />
  ))
  .add('priority low', () => (
    <Problem customer={customer} problem={problemPriorityLow} />
  ))
  .add('priority not assigned', () => (
    <Problem customer={customer} problem={problemPriorityNotAssigned} />
  ))
  .add('linkTitle', () => (
    <Problem
      customer={customer}
      problem={problemPriorityNotAssigned}
      linkTitle
    />
  ))
  .add('highlight (inner HTML)', () => (
    <Problem customer={customer} problem={esProblem} highlight />
  ));
