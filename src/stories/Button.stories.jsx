import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Button from 'components/Button/Button.jsx';
import { IconPlus } from 'components/Icons/Icons.jsx';

storiesOf('Button', module)
  .add('default', () => <Button>Hello world</Button>)
  .add('display block', () => (
    <Button display="block">Display block button</Button>
  ))
  .add('appearance primary', () => (
    <Button appearance="primary">Appearance primary</Button>
  ))
  .add('appearance secondary', () => (
    <Button appearance="secondary">Appearance secondary</Button>
  ))
  .add('size small', () => <Button size="sm">Size small</Button>)
  .add('size medium', () => <Button size="md">Size medium</Button>)
  .add('icon only', () => (
    <Button iconOnly>
      <IconPlus fill="#fff" />
    </Button>
  ))
  .add('unstyled', () => <Button unstyled>I&apos;m unstyled</Button>)
  .add('wich onClick callback', () => (
    <Button onClick={action('onClick')}>Click me</Button>
  ));
