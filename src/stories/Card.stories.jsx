import React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { storiesOf } from '@storybook/react';
import Card from 'components/Card/Card.jsx';

storiesOf('Card', module)
  .addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .add('default', () => <Card url="/">Card content</Card>)
  .add('with title', () => (
    <Card url="/">
      <Card.Header title="Title" />
      Card content
    </Card>
  ))
  .add('with header, subheader', () => (
    <Card url="/">
      <Card.Header title="Title" />
      <Card.Meta>Subheader</Card.Meta>
      Card content
    </Card>
  ))
  .add('with header, subheader, footer', () => (
    <Card url="/">
      <Card.Header title="Title" />
      <Card.Meta>Subheader</Card.Meta>
      Card content
      <Card.Footer>Footer</Card.Footer>
    </Card>
  ))
  .add('with header, subheader, footer, preview', () => (
    <Card url="/">
      <Card.Header title="Title" />
      <Card.Meta>Subheader</Card.Meta>
      <Card.Preview>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit,&nbsp;
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.&nbsp;
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi&nbsp;
        ut aliquip ex ea commodo consequat.&nbsp;
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum&nbsp;
        dolore eu fugiat nulla pariatur.&nbsp;
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia&nbsp;
        deserunt mollit anim id est laborum.
      </Card.Preview>
      <Card.Footer>Footer</Card.Footer>
    </Card>
  ));
