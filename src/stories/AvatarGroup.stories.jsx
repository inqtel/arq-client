import React from 'react';
import { storiesOf } from '@storybook/react';
import { users } from 'mocks/users';
import Avatar from 'components/Avatar/Avatar.jsx';

storiesOf('AvatarGroup', module)
  .add('default', () => <Avatar.Group users={users} />)
  .add('align right', () => <Avatar.Group users={users} align="right" />);
