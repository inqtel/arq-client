import React from 'react';
import Navigation from 'components/Navigation/Navigation.jsx';
import { storiesOf } from '@storybook/react';

storiesOf('Navigation', module)
  .add('with one Navigation.Item', () => <Navigation><Navigation.Item>Item 1</Navigation.Item></Navigation>)
  .add('with several Navigation.Item', () => (
    <Navigation>
      <Navigation.Item>Item 1</Navigation.Item>
      <Navigation.Item>Item 2</Navigation.Item>
      <Navigation.Item>Item 3</Navigation.Item>
      <Navigation.Item>Item 4</Navigation.Item>
    </Navigation>
  ))
  .add('align right', () => (
    <Navigation align="right">
      <Navigation.Item>Item 1</Navigation.Item>
      <Navigation.Item>Item 2</Navigation.Item>
      <Navigation.Item>Item 3</Navigation.Item>
      <Navigation.Item>Item 4</Navigation.Item>
    </Navigation>
  ));
