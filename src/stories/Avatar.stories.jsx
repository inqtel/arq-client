import React from 'react';
import { storiesOf } from '@storybook/react';
import Avatar from 'components/Avatar/Avatar.jsx';

const user = {
  email: 'fwright@email.com',
};

storiesOf('Avatar', module)
  .add('default', () => <Avatar />)
  .add('with user', () => <Avatar user={user} />)
  .add('size sm', () => <Avatar user={user} size="sm" />)
  .add('size md', () => <Avatar user={user} size="md" />)
  .add('size lg', () => <Avatar user={user} size="lg" />)
  .add('size xl', () => <Avatar user={user} size="xl" />);
