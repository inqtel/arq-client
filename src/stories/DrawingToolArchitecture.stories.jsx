import React from 'react';
import { storiesOf } from '@storybook/react';
import cx from 'classnames';
import { themes } from 'constants/drawingTool.js';
import { architectureDrawingTool } from 'mocks/architectures';
import DrawingToolArchitecture from 'components/DrawingTool/DrawingToolArchitecture.jsx';
import 'components/DrawingTool/DrawingTool.scss';

// TODO: Build out DrawingTool stories
// 1) Create wrapper for <DrawingToolArchitecture /> with simpler props?
// 2) Add actions
storiesOf('DrawingToolArchitecture', module)
  .add('arq theme', () => (
    <div className={cx(
      'DrawingTool',
      `DrawingTool--theme-${themes[0].name.toLowerCase()}`,
    )}
    >
      <DrawingToolArchitecture
        architectureQID={architectureDrawingTool.id}
        architectureTitle={architectureDrawingTool.title}
        visibleRootPackageQIDs={architectureDrawingTool.packages.map(pkg => pkg.id)}
        packagesByQID={{}} // TODO: update mock architecture for gql
        selectedTheme={themes[0]}
        // drawingRasterProps={}
        packagesUpdateLayout={() => {}}
        setFocusID={() => {}}
        packagePositionDefault={() => {}}
        packagePositionLeft={() => {}}
        packagePositionRight={() => {}}
        packageTitlePositionDefault={() => {}}
        packageTitlePositionLeft={() => {}}
        packageTitlePositionRight={() => {}}
        setDrawingRasterProps={() => {}}
      />
    </div>
  ))
  .add('greenery theme', () => (
    <div className={cx(
      'DrawingTool',
      `DrawingTool--theme-${themes[1].name.toLowerCase()}`,
    )}
    >
      <DrawingToolArchitecture
        architectureQID={architectureDrawingTool.id}
        architectureTitle={architectureDrawingTool.title}
        visibleRootPackageQIDs={architectureDrawingTool.packages.map(pkg => pkg.id)}
        packagesByQID={{}} // TODO: update mock architecture for gql
        selectedTheme={themes[1]}
        // drawingRasterProps={}
        packagesUpdateLayout={() => {}}
        setFocusID={() => {}}
        packagePositionDefault={() => {}}
        packagePositionLeft={() => {}}
        packagePositionRight={() => {}}
        packageTitlePositionDefault={() => {}}
        packageTitlePositionLeft={() => {}}
        packageTitlePositionRight={() => {}}
        setDrawingRasterProps={() => {}}
      />
    </div>
  ));
