import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { taxonomy, taxonomyFromArchitecture } from 'mocks/taxonomy';
import { user } from 'mocks/users';
import TaxonomyTree from 'components/TaxonomyTree/TaxonomyTree.jsx';
import { IconLink, IconInsert } from 'components/Icons/Icons.jsx';

const TreeActions = {
  primary: [
    {
      title: 'Link to this term',
      icon: <IconLink />,
      handler: action('Link to this term'),
    },
    {
      title: 'Import this tree',
      icon: <IconInsert />,
      handler: action('Import this tree'),
    },
  ],
};

storiesOf('TaxonomyTree', module)
  .addDecorator(story => (
    <div
      style={{ padding: '2rem', maxWidth: '48rem', backgroundColor: '#fff' }}
    >
      {story()}
    </div>
  ))
  .add('default', () => <TaxonomyTree taxonomy={taxonomy} />)
  .add('with actions', () => (
    <TaxonomyTree
      taxonomy={taxonomy}
      actions={TreeActions}
    />
  ))
  .add('custom render function', () => (
    <TaxonomyTree
      taxonomy={taxonomy}
      renderItem={item => item.id}
    />
  ))
  .add('architecture', () => (
    <TaxonomyTree
      taxonomy={taxonomyFromArchitecture}
      author={user.email}
    />
  ));
