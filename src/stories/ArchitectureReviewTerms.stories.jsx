import React from 'react';
import { storiesOf } from '@storybook/react';
import { terms } from 'mocks/terms';
import ArchitectureReviewTerms from 'components/ArchitectureReviewTerms/ArchitectureReviewTerms.jsx';

storiesOf('ArchitectureReviewTerms', module)
  .addDecorator(story => (
    <div style={{ maxWidth: '48rem', backgroundColor: '#fff' }}>{story()}</div>
  ))
  .add('default', () => {
    const byQID = terms.map(term => term.id);
    const entities = {};

    terms.forEach((term) => {
      entities[term.id] = term;
    });

    const termData = { byQID, entities };

    return <ArchitectureReviewTerms terms={termData} />;
  });
