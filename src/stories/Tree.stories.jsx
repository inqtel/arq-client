import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { taxonomy } from 'mocks/taxonomy';
import { user } from 'mocks/users';
import { architecture } from 'mocks/architectures';
import Tree from 'components/Tree/Tree.jsx';
import { IconLink, IconInsert } from 'components/Icons/Icons.jsx';

const TreeActions = {
  primary: [
    {
      title: 'Link to this term',
      icon: <IconLink />,
      handler: action('Link to this term'),
    },
    {
      title: 'Import this tree',
      icon: <IconInsert />,
      handler: action('Import this tree'),
    },
  ],
};

storiesOf('Tree', module)
  .addDecorator(story => (
    <div
      style={{ padding: '2rem', maxWidth: '48rem', backgroundColor: '#fff' }}
    >
      {story()}
    </div>
  ))
  .add('default', () => <Tree item={taxonomy}>{taxonomy.children}</Tree>)
  .add('size xs', () => (
    <Tree
      item={taxonomy}
      size="xs"
    >
      {taxonomy.children}
    </Tree>
  ))
  .add('size sm', () => (
    <Tree
      item={taxonomy}
      size="sm"
    >
      {taxonomy.children}
    </Tree>
  ))
  .add('with actions', () => (
    <Tree
      item={taxonomy}
      actions={TreeActions}
    >
      {taxonomy.children}
    </Tree>
  ))
  .add('highlight matches', () => (
    <Tree
      item={taxonomy}
      searchTerm="micro"
    >
      {taxonomy.children}
    </Tree>
  ))
  .add('hide meta', () => (
    <Tree
      item={taxonomy}
      hideMeta
    >
      {taxonomy.children}
    </Tree>
  ))
  .add('custom render function', () => (
    <Tree
      item={taxonomy}
      renderItem={item => item.id}
    >
      {taxonomy.children}
    </Tree>
  ))
  .add('architecture', () => (
    <Tree
      item={architecture.packages[1]}
      architectureTitle={architecture.title}
      author={user.email}
    >
      {architecture.packages[1].packages}
    </Tree>
  ));
