import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router-dom';
import configureStore from 'configureStore';
import { architecture } from 'mocks/architectures';
import ArchitectureContainer from 'stories/ArchitectureContainer.jsx';

const store = configureStore();

storiesOf('Architecture', module)
  .addDecorator(story => <Provider store={store}>{story()}</Provider>)
  .addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .add('default', () => <ArchitectureContainer architecture={architecture} />)
  .add('architecture, editable', () => (
    <ArchitectureContainer architecture={architecture} editable />
  ))
  .add('architecture, editable, interactive', () => (
    <ArchitectureContainer architecture={architecture} editable interactive />
  ))
  .add('architecture, editable, interactive, actionable', () => (
    <ArchitectureContainer
      architecture={architecture}
      editable
      interactive
      actionable
    />
  ))
  .add('architecture, editable, interactive, actionable, commentable', () => (
    <ArchitectureContainer
      architecture={architecture}
      editable
      interactive
      actionable
      commentable
      onPackageToggleComments={action('Toggle comments')}
    />
  ));
