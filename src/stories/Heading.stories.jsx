import React from 'react';
import Heading from 'components/Heading/Heading.jsx';
import { storiesOf } from '@storybook/react';

storiesOf('Heading', module)
  .add('default', () => <Heading>Hello world</Heading>)
  .add('level 1', () => <Heading level={1}>Hello world</Heading>)
  .add('level 2', () => <Heading level={2}>Hello world</Heading>)
  .add('level 3', () => <Heading level={3}>Hello world</Heading>);
