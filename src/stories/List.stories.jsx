import React from 'react';
import { storiesOf } from '@storybook/react';
import List from 'components/List/List.jsx';

storiesOf('List', module).add('default', () => (
  <List>
    <List.Item>Item 1</List.Item>
    <List.Item>Item 2</List.Item>
    <List.Item>Item 3</List.Item>
    <List.Item>Item 4</List.Item>
    <List.Item>Item 5</List.Item>
  </List>
));
