import React from 'react';
import { storiesOf } from '@storybook/react';
import Tabs from 'components/Tabs/Tabs.jsx';

storiesOf('Tabs', module)
  .addDecorator(story => (
    <div style={{ padding: '2rem', backgroundColor: '#fff' }}>{story()}</div>
  ))
  .add('default', () => (
    <Tabs>
      <Tabs.Pane label="Tab 1">Tab 1 info</Tabs.Pane>
      <Tabs.Pane label="Tab 2">Tab 2 info</Tabs.Pane>
    </Tabs>
  ))
  .add('three tabs', () => (
    <Tabs>
      <Tabs.Pane label="Tab 1">Tab 1 info</Tabs.Pane>
      <Tabs.Pane label="Tab 2">Tab 2 info</Tabs.Pane>
      <Tabs.Pane label="Tab 3">Tab 3 info</Tabs.Pane>
    </Tabs>
  ))
  .add('with second tab selected', () => (
    <Tabs selected={1}>
      <Tabs.Pane label="Tab 1">Tab 1 info</Tabs.Pane>
      <Tabs.Pane label="Tab 2">Tab 2 info</Tabs.Pane>
    </Tabs>
  ))
  .add('with out of range tab selected', () => (
    <Tabs selected={3}>
      <Tabs.Pane label="Tab 1">Tab 1 info</Tabs.Pane>
      <Tabs.Pane label="Tab 2">Tab 2 info</Tabs.Pane>
    </Tabs>
  ))
  .add('default compact', () => (
    <Tabs compact>
      <Tabs.Pane label="Tab 1">Tab 1 info</Tabs.Pane>
      <Tabs.Pane label="Tab 2">Tab 2 info</Tabs.Pane>
    </Tabs>
  ))
  .add('compact with three tabs', () => (
    <Tabs compact>
      <Tabs.Pane label="Tab 1">Tab 1 info</Tabs.Pane>
      <Tabs.Pane label="Tab 2">Tab 2 info</Tabs.Pane>
      <Tabs.Pane label="Tab 3">Tab 3 info</Tabs.Pane>
    </Tabs>
  ))
  .add('compact with second tab selected', () => (
    <Tabs compact selected={1}>
      <Tabs.Pane label="Tab 1">Tab 1 info</Tabs.Pane>
      <Tabs.Pane label="Tab 2">Tab 2 info</Tabs.Pane>
      <Tabs.Pane label="Tab 3">Tab 3 info</Tabs.Pane>
    </Tabs>
  ));
