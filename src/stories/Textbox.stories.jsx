import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Textbox from 'components/Textbox/Textbox.jsx';

storiesOf('Textbox', module)
  .add('default', () => <Textbox />)
  .add('secondary appearance', () => <Textbox appearance="secondary" />)
  .add('with placeholder', () => <Textbox placeholder="Placeholder text" />)
  .add('with default value', () => <Textbox value="Default value" />)
  .add('with onChange callback', () => (
    <Textbox onChange={action('onChange')} />
  ))
  .add('with onEnter callback', () => <Textbox onEnter={action('onEnter')} />);
