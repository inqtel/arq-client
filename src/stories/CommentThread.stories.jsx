import React from 'react';
import { storiesOf } from '@storybook/react';
import { comments } from 'mocks/comments';
import { user } from 'mocks/users';
import CommentThread from 'components/CommentThread/CommentThread.jsx';

storiesOf('CommentThread', module)
  .addDecorator(story => <div style={{ maxWidth: '21rem' }}>{story()}</div>)
  .add('default', () => <CommentThread user={user} />)
  .add('with comments', () => <CommentThread user={user} comments={comments} />);
