import React from 'react';
import { storiesOf } from '@storybook/react';
import Tooltip from 'components/Tooltip/Tooltip.jsx';

storiesOf('Tooltip', module)
  .addDecorator(story => <div style={{ padding: '2rem 0' }}>{story()}</div>)
  .add('default', () => (
    <Tooltip content="I am a tooltip">
      <Tooltip.Wrapper>Hello!</Tooltip.Wrapper>
    </Tooltip>
  ));
