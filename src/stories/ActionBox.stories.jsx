import React from 'react';
import { storiesOf } from '@storybook/react';
import ActionBox from 'components/ActionBox/ActionBox.jsx';

storiesOf('ActionBox', module)
  .add('default with content', () => (
    <ActionBox>
      <ActionBox.Content>Content</ActionBox.Content>
    </ActionBox>
  ));
