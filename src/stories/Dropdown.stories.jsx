import React from 'react';
import PropTypes from 'prop-types';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import Dropdown from 'components/Dropdown/Dropdown.jsx';
import Button from 'components/Button/Button.jsx';

const Box = ({ children }) => (
  <div style={{ padding: '1rem', backgroundColor: '#fff' }}>{children}</div>
);

Box.propTypes = ({ children: PropTypes.node.isRequired });

storiesOf('Dropdown', module)
  .addDecorator(story => (
    <div style={{ display: 'inline-block' }}>{story()}</div>
  ))
  .add('default', () => (
    <Dropdown>
      <Dropdown.Toggle>
        <Button>Toggle Dropdown</Button>
      </Dropdown.Toggle>
      <Dropdown.Menu>
        <Box>dropdown menu</Box>
      </Dropdown.Menu>
    </Dropdown>
  ))
  .add('position right', () => (
    <Dropdown>
      <Dropdown.Toggle>
        <Button>Toggle Dropdown</Button>
      </Dropdown.Toggle>
      <Dropdown.Menu position="right">
        <Box>dropdown menu</Box>
      </Dropdown.Menu>
    </Dropdown>
  ))
  .add('onOpen and onClose callbacks', () => (
    <Dropdown onOpen={action('onOpen')} onClose={action('onClose')}>
      <Dropdown.Toggle>
        <Button>Toggle Dropdown</Button>
      </Dropdown.Toggle>
      <Dropdown.Menu>
        <Box>dropdown menu</Box>
      </Dropdown.Menu>
    </Dropdown>
  ));
