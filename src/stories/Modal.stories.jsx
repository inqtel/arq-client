import React from 'react';
import Modal from 'components/Modal/Modal.jsx';
import Button from 'components/Button/Button.jsx';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

const ACTIONS = (
  <Button.Group>
    <Button onClick={() => {}}>
      Action 1
    </Button>
    <Button onClick={() => {}} appearance="secondary">
      Action 2
    </Button>
  </Button.Group>
);

storiesOf('Modal', module)
  .add('default', () => <Modal />)
  .add('isOpened', () => <Modal isOpened>Modal Content</Modal>)
  .add('isOpened, with title', () => <Modal isOpened title="Modal Title">Modal Content</Modal>)
  .add('isOpened, with title, isProgressing', () => <Modal isOpened isProgressing title="Modal Title">Modal Content</Modal>)
  .add('isOpened, with title, with actions', () => (
    <Modal
      isOpened
      title="Modal Title"
      actions={ACTIONS}
    >
      Modal Content
    </Modal>
  ))
  .add('isOpened, with title, with closeModal callback', () => (
    <Modal
      isOpened
      title="Modal Title"
      closeModal={action('closeModal')}
    >
      Modal Content
    </Modal>
  ))
  .add('isOpened, learningModule', () => <Modal isOpened learningModule>Modal Content</Modal>);
