import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { v4 as uuid } from 'uuid';
import FilterableList from 'components/FilterableList/FilterableList.jsx';
import Dropdown from 'components/Dropdown/Dropdown.jsx';
import Button from 'components/Button/Button.jsx';

const usersNoneSelected = [
  {
    qid: uuid(),
    email: 'bob@email.com',
    firstName: '',
    lastName: '',
    status: 'none',
  },
  {
    qid: uuid(),
    email: 'sally@email.com',
    firstName: '',
    lastName: '',
    status: 'none',
  },
  {
    qid: uuid(),
    email: 'jim@email.com',
    firstName: '',
    lastName: '',
    status: 'none',
  },
  {
    qid: uuid(),
    email: 'john@email.com',
    firstName: '',
    lastName: '',
    status: 'none',
  },
  {
    qid: uuid(),
    email: 'mike@email.com',
    firstName: '',
    lastName: '',
    status: 'none',
  },
  {
    qid: uuid(),
    email: 'joyce@email.com',
    firstName: '',
    lastName: '',
    status: 'none',
  },
];

storiesOf('FilterableList', module)
  .add('default', () => (
    <FilterableList
      collection={usersNoneSelected}
      idKey="qid"
      titleKey="email"
      checkedKey="status"
      checkedValue="none"
    />
  ))
  .add('with select and deselect callbacks', () => (
    <FilterableList
      collection={usersNoneSelected}
      idKey="qid"
      titleKey="email"
      checkedKey="status"
      checkedValue="none"
      onSelect={action('onSelect')}
      onDeselect={action('onDeselect')}
    />
  ))
  .add('inside dropdown', () => (
    <Dropdown>
      <Dropdown.Toggle>
        <Button>Toggle Dropdown</Button>
      </Dropdown.Toggle>
      <Dropdown.Menu>
        <FilterableList
          collection={usersNoneSelected}
          idKey="qid"
          titleKey="email"
          checkedKey="status"
          checkedValue="none"
        />
      </Dropdown.Menu>
    </Dropdown>
  ))
  .add('collection loading', () => (
    <FilterableList
      collection={usersNoneSelected}
      loading
      idKey="qid"
      titleKey="email"
      checkedKey="status"
      checkedValue="none"
    />
  ))
  .add('collection loading inside dropdown', () => (
    <Dropdown>
      <Dropdown.Toggle>
        <Button>Toggle Dropdown</Button>
      </Dropdown.Toggle>
      <Dropdown.Menu>
        <FilterableList
          collection={usersNoneSelected}
          loading
          idKey="qid"
          titleKey="email"
          checkedKey="status"
          checkedValue="none"
        />
      </Dropdown.Menu>
    </Dropdown>
  ))
  .add('with submit callback', () => (
    <FilterableList
      collection={usersNoneSelected}
      idKey="qid"
      titleKey="email"
      checkedKey="status"
      checkedValue="none"
      actionText="Update"
      onSubmit={action('onSubmit')}
    />
  ));
