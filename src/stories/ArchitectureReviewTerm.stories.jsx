import React from 'react';
import { storiesOf } from '@storybook/react';
import { term, rejectedTerm } from 'mocks/terms';
import { taxonomySearchResults } from 'mocks/searchResults';
import ArchitectureReviewTerm from 'components/ArchitectureReviewTerms/ArchitectureReviewTerm.jsx';

const search = {
  results: {
    taxonomies: taxonomySearchResults,
  },
};

storiesOf('ArchitectureReviewTerm', module)
  .addDecorator(story => (
    <div style={{ maxWidth: '48rem', backgroundColor: '#fff' }}>{story()}</div>
  ))
  .add('default', () => <ArchitectureReviewTerm termEntity={term} />)
  .add('rejected', () => <ArchitectureReviewTerm termEntity={rejectedTerm} />)
  .add('with search results', () => (
    <ArchitectureReviewTerm termEntity={term} search={search} />
  ));
