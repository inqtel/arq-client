import React from 'react';
import { storiesOf } from '@storybook/react';
import EntityError from 'components/EntityError/EntityError.jsx';

const ERROR_403 = {
  status: 403,
  data: {
    internalError: {
      data: {
        author: {
          email: 'bob@example.com',
        },
      },
    },
  },
};

const ERROR_404 = {
  status: 404,
  data: {
    internalError: {
      data: {
        author: {
          email: 'bob@example.com',
        },
      },
    },
  },
};

storiesOf('EntityError', module)
  .add('default', () => (
    <EntityError error={ERROR_403} entityName="Document" />
  ))
  .add('error 403', () => (
    <EntityError error={ERROR_403} entityName="Document" />
  ))
  .add('error 404', () => (
    <EntityError error={ERROR_404} entityName="Document" />
  ))
  .add('error 403, can request access', () => (
    <EntityError error={ERROR_403} entityName="Document" canRequestAccess />
  ));
