import React from 'react';
import Loader from 'components/Loader/Loader.jsx';
import { storiesOf } from '@storybook/react';

storiesOf('Loader', module)
  .add('default', () => <Loader />)
  .add('size 36', () => <Loader size={36} />)
  .add('size 36, isFullscreen', () => <Loader size={36} isFullscreen />)
  .add('size 36, isCentered', () => <Loader size={36} isCentered />)
  .add('size 36, isHeight100', () => <Loader size={36} isHeight100 />)
  .add('size 36, isAbsolute', () => <Loader size={36} isAbsolute />);
