import React from 'react';
import { storiesOf } from '@storybook/react';
import Grid from 'components/Grid/Grid.jsx';

const renderContent = content => (
  <div style={{
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    width: '10rem',
    height: '10rem',
    border: '1px solid #fff',
    color: '#fff',
    background: '#6a71d7',
  }}
  >
    {content}
  </div>
);

storiesOf('Grid', module)
  .add('default', () => (
    <Grid />
  ))
  .add('with two cells', () => (
    <Grid>
      <Grid.Cell><>{renderContent('1')}</></Grid.Cell>
      <Grid.Cell><>{renderContent('2')}</></Grid.Cell>
    </Grid>
  ))
  .add('direction column', () => (
    <Grid direction="column">
      <Grid.Cell><>{renderContent('1')}</></Grid.Cell>
      <Grid.Cell><>{renderContent('2')}</></Grid.Cell>
    </Grid>
  ));
