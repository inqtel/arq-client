import { cloneDeep } from 'lodash';
import { convertTermToTaxonomy } from 'utils/terms';
import { removeTermFromTaxonomies, updateTaxonomiesPropertyAt } from 'utils/tree';
import { createTaxonomyFromElasticHit } from 'utils/elastic';
import {
  INIT_TERMS,
  APPROVE_TERM,
  REJECT_TERM,
  UNREJECT_TERM,
  SUBMIT_APPROVED_TERMS_LOADING,
  SUBMIT_APPROVED_TERMS_SUCCESS,
  SUBMIT_APPROVED_TERMS_ERROR,
  SEARCH_FOR_PARENT_TERM_LOADING,
  SEARCH_FOR_PARENT_TERM_SUCCESS,
  SEARCH_FOR_PARENT_TERM_ERROR,
  SEARCH_FOR_PARENT_TERM_RESET,
  INSERT_TERM_BELOW_TAXONOMY,
  REMOVE_TERM_FROM_TAXONOMY,
  LINK_TERM_TO_TAXONOMY,
  UNLINK_TERM_FROM_TAXONOMY,
} from 'actions/adminArchitectureTerms';

const initialState = {
  terms: {
    byID: [],
    entities: {},
    error: null,
  },
  searches: {
    byID: [],
    entities: {},
    error: null,
  },
  ui: {
    submitting: false,
  },
};

export default function adminArchitectureTerms(state = initialState, action) {
  switch (action.type) {
    case INIT_TERMS:
      return initTerms(state, action);
    case APPROVE_TERM:
      return approveTerm(state, action);
    case REJECT_TERM:
      return rejectTerm(state, action);
    case UNREJECT_TERM:
      return unrejectTerm(state, action);
    case SEARCH_FOR_PARENT_TERM_LOADING:
      return searchForParentTermLoading(state, action);
    case SEARCH_FOR_PARENT_TERM_SUCCESS:
      return searchForParentTermSuccess(state, action);
    case SEARCH_FOR_PARENT_TERM_ERROR:
      return searchForParentTermError(state, action);
    case SEARCH_FOR_PARENT_TERM_RESET:
      return searchForParentTermReset(state, action);
    case SUBMIT_APPROVED_TERMS_LOADING:
      return submitApprovedTermsLoading(state, action);
    case SUBMIT_APPROVED_TERMS_SUCCESS:
      return submitApprovedTermsSuccess(state, action);
    case SUBMIT_APPROVED_TERMS_ERROR:
      return submitApprovedTermsError(state, action);
    case INSERT_TERM_BELOW_TAXONOMY:
      return insertTermBelowTaxonomy(state, action);
    case REMOVE_TERM_FROM_TAXONOMY:
      return removeTermFromTaxonomy(state, action);
    case LINK_TERM_TO_TAXONOMY:
      return linkTermToTaxonomy(state, action);
    case UNLINK_TERM_FROM_TAXONOMY:
      return unlinkTermFromTaxonomy(state, action);
    default:
      return state;
  }
}

function initTerms(state, action) {
  const byID = action.terms.map(term => term.packageID);
  const termEntities = {};
  const searchEntities = {};

  action.terms.forEach((term) => {
    termEntities[term.packageID] = term;
    searchEntities[term.packageID] = {
      loading: false,
      error: null,
      results: null,
    };
  });

  return {
    ...state,
    terms: {
      byID,
      entities: termEntities,
    },
    searches: {
      byID,
      entities: searchEntities,
    },
  };
}

function submitApprovedTermsLoading(state) {
  return {
    ...state,
    ui: {
      ...state.ui,
      submitting: true,
    },
  };
}

function submitApprovedTermsSuccess(state) {
  const newPendingEntities = {};
  const newPendingTerms = [];

  state.terms.byID.forEach((id) => {
    if (!state.terms.entities[id].approved) {
      newPendingEntities[id] = state.terms.entities[id];
      newPendingTerms.push[id];
    }
  });

  return {
    ...state,
    terms: {
      ...state.terms,
      byID: newPendingTerms,
      entities: newPendingEntities,
    },
    ui: {
      ...state.ui,
      submitting: false,
    },
  };
}

function submitApprovedTermsError(state, action) {
  return {
    ...state,
    terms: {
      ...state.terms,
      error: action.error,
    },
    ui: {
      ...state.ui,
      submitting: false,
    },
  };
}

function approveTerm(state, action) {
  return {
    ...state,
    terms: {
      ...state.terms,
      entities: {
        ...state.terms.entities,
        [action.id]: {
          ...state.terms.entities[action.id],
          taxonomy: action.taxonomy,
          approved: true,
        },
      },
    },
  };
}

function rejectTerm(state, action) {
  return {
    ...state,
    terms: {
      ...state.terms,
      entities: {
        ...state.terms.entities,
        [action.id]: {
          ...state.terms.entities[action.id],
          approved: false,
        },
      },
    },
  };
}

function unrejectTerm(state, action) {
  const unrejectedTerm = {
    ...state.terms.entities[action.id],
    approved: null,
  };

  return {
    ...state,
    terms: {
      ...state.terms,
      entities: {
        ...state.terms.entities,
        [action.id]: unrejectedTerm,
      },
    },
  };
}

function searchForParentTermLoading(state, action) {
  const loadingSearchEntity = {
    ...state.searches.entities[action.termID],
    loading: true,
  };

  return {
    ...state,
    searches: {
      ...state.searches,
      entities: {
        ...state.searches.entities,
        [action.termID]: loadingSearchEntity,
      },
    },
  };
}

function searchForParentTermSuccess(state, action) {
  const successSearchEntity = {
    ...state.searches.entities[action.termID],
    loading: false,
    error: null,
    results: { taxonomies: action.results.map(hit => createTaxonomyFromElasticHit(hit)) },
  };

  return {
    ...state,
    searches: {
      ...state.searches,
      entities: {
        ...state.searches.entities,
        [action.termID]: successSearchEntity,
      },
    },
  };
}

function searchForParentTermError(state) {
  return {
    ...state,
    // search: {
    //   ...state.search,
    //   loading: false,
    //   error: action.error
    // }
  };
}

function searchForParentTermReset(state) {
  return {
    ...state,
    // search: initialState.search
  };
}

function insertTermBelowTaxonomy(state, action) {
  const { term, taxonomy } = action;
  const newTaxonomy = convertTermToTaxonomy(term);

  const pkgEntity = state.terms.entities[term.packageID];

  const searchEntity = state.searches.entities[term.packageID];
  const taxonomies = cloneDeep(searchEntity.results.taxonomies);

  if (pkgEntity.approved) {
    if (pkgEntity.parentID) {
      removeTermFromTaxonomies(taxonomies, term);
    }
    if (pkgEntity.taxonomyID) {
      updateTaxonomiesPropertyAt(taxonomies, pkgEntity.taxonomyID, 'unlink', false);
    }
  }

  let found = false;

  for (let i = 0; i < taxonomies.length; i++) {
    if (taxonomies[i].id === taxonomy.id) {
      taxonomies[i] = {
        ...taxonomies[i],
        expand: true,
        children: [newTaxonomy, ...taxonomies[i].children],
      };
      found = true;
    }

    if (!found && taxonomies[i].children && taxonomies[i].children.length > 0) {
      // TODO: fix lint errors
      // eslint-disable-next-line no-loop-func
      (function sub(parent) {
        const foundInsertIndex = parent.children.findIndex(
          child => child.id === taxonomy.id,
        );

        if (foundInsertIndex > -1) {
          parent.children[foundInsertIndex] = {
            ...parent.children[foundInsertIndex],
            expand: true,
            children: [
              newTaxonomy,
              ...parent.children[foundInsertIndex].children,
            ],
          };
          found = true;
        }

        if (!found) {
          parent.children.forEach((child) => {
            if (child.children.length > 0) {
              return sub(child);
            }
            return false;
          });
        }
      }(taxonomies[i]));
    }
  }

  return {
    ...state,
    terms: {
      ...state.terms,
      entities: {
        ...state.terms.entities,
        [term.packageID]: {
          ...state.terms.entities[term.packageID],
          parentID: taxonomy.id,
          taxonomyID: null,
          approved: true,
        },
      },
    },
    searches: {
      ...state.searches,
      entities: {
        ...state.searches.entities,
        [term.packageID]: {
          ...searchEntity,
          results: {
            ...state.searches.entities[term.packageID].results,
            taxonomies,
          },
        },
      },
    },
  };
}

function removeTermFromTaxonomy(state, action) {
  const { term } = action;

  const searchEntity = state.searches.entities[term.packageID];
  const taxonomies = cloneDeep(searchEntity.results.taxonomies);

  removeTermFromTaxonomies(taxonomies, term);

  return {
    ...state,
    terms: {
      ...state.terms,
      entities: {
        ...state.terms.entities,
        [term.packageID]: {
          ...state.terms.entities[term.packageID],
          approved: null,
          parentID: null,
        },
      },
    },
    searches: {
      ...state.searches,
      entities: {
        ...state.searches.entities,
        [term.packageID]: {
          ...searchEntity,
          results: {
            ...state.searches.entities[term.packageID].results,
            taxonomies,
          },
        },
      },
    },
  };
}

function linkTermToTaxonomy(state, action) {
  const { term, taxonomy } = action;

  const pkgEntity = state.terms.entities[term.packageID];

  const searchEntity = state.searches.entities[term.packageID];
  const taxonomies = cloneDeep(searchEntity.results.taxonomies);

  if (pkgEntity.approved) {
    if (pkgEntity.parentID) {
      removeTermFromTaxonomies(taxonomies, term);
    }
    if (pkgEntity.taxonomyID) {
      updateTaxonomiesPropertyAt(taxonomies, pkgEntity.taxonomyID, 'unlink', false);
    }
  }

  let found = false;

  for (let i = 0; i < taxonomies.length; i++) {
    if (taxonomies[i].id === taxonomy.id) {
      taxonomies[i] = {
        ...taxonomies[i],
        expand: true,
        unlink: true,
      };
      found = true;
    }

    if (!found && taxonomies[i].children && taxonomies[i].children.length > 0) {
      // TODO: fix lint errors
      // eslint-disable-next-line no-loop-func
      (function sub(parent) {
        const foundInsertIndex = parent.children.findIndex(
          child => child.id === taxonomy.id,
        );

        if (foundInsertIndex > -1) {
          parent.children[foundInsertIndex] = {
            ...parent.children[foundInsertIndex],
            expand: true,
            unlink: true,
          };
          found = true;
        }

        if (!found) {
          parent.children.forEach((child) => {
            if (child.children.length > 0) {
              return sub(child);
            }
            return false;
          });
        }
      }(taxonomies[i]));
    }
  }

  return {
    ...state,
    terms: {
      ...state.terms,
      entities: {
        ...state.terms.entities,
        [term.packageID]: {
          ...state.terms.entities[term.packageID],
          approved: true,
          parentID: null,
          taxonomyID: taxonomy.id,
        },
      },
    },
    searches: {
      ...state.searches,
      entities: {
        ...state.searches.entities,
        [term.packageID]: {
          ...searchEntity,
          results: {
            ...state.searches.entities[term.packageID].results,
            taxonomies,
          },
        },
      },
    },
  };
}

function unlinkTermFromTaxonomy(state, action) {
  const { term } = action;

  const pkgEntity = state.terms.entities[term.packageID];

  const searchEntity = state.searches.entities[term.packageID];
  const taxonomies = cloneDeep(searchEntity.results.taxonomies);

  updateTaxonomiesPropertyAt(taxonomies, pkgEntity.taxonomyID, 'unlink', false);

  return {
    ...state,
    terms: {
      ...state.terms,
      entities: {
        ...state.terms.entities,
        [term.packageID]: {
          ...state.terms.entities[term.packageID],
          approved: null,
          taxonomyID: null,
        },
      },
    },
    searches: {
      ...state.searches,
      entities: {
        ...state.searches.entities,
        [term.packageID]: {
          ...searchEntity,
          results: {
            ...state.searches.entities[term.packageID].results,
            taxonomies,
          },
        },
      },
    },
  };
}
