import { combineReducers } from 'redux';
import data from 'reducers/sharedDocData';
import ui from 'reducers/sharedDocUI';
import entities from 'reducers/sharedDocEntities';

export default combineReducers({
  data,
  entities,
  ui,
});
