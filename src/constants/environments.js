export const NODE_ENV_DEVELOPMENT = 'development';
export const NODE_ENV_TEST = 'test';
export const NODE_ENV_STAGING = 'staging';
export const NODE_ENV_PRODUCTION = 'production';

export const ENV_LOCAL = 'local-dev';
export const ENV_DEVELOPMENT = 'development';
export const ENV_TEST = 'test';
export const ENV_STAGING = 'staging';
export const ENV_PRODUCTION = 'production';
