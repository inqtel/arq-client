export const SORT_OPTION_COUNT = {
  term: '9-1',
  key: '_count',
  direction: 'desc',
};

export const SORT_OPTION_TERM = {
  term: 'A-Z',
  key: '_term',
  direction: 'asc',
};

export const SORT_OPTIONS = [
  SORT_OPTION_COUNT,
  SORT_OPTION_TERM,
];

export const OPERATOR_OPTIONS = ['AND', 'OR'];
