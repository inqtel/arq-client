import { InMemoryCache, makeVar } from '@apollo/client';
import { THEME_GREENERY } from 'constants/drawingTool';
import { SORT_OPTION_COUNT } from 'constants/elastic';

export const cache = new InMemoryCache({
  typePolicies: {
    // TODO: reset drawing mutation
    Query: {
      fields: {
        // onboarding
        onboardingModuleShown() {
          return onboardingModuleShownVar();
        },
        controlsPopup() {
          return controlsPopupVar();
        },
        architecturePopup() {
          return architecturePopupVar();
        },
        // UI
        toggleUserShown() {
          return toggleUserShownVar();
        },
        csvLoading() {
          return csvLoadingVar();
        },
        selectedSourceIndex() {
          return selectedSourceIndexVar();
        },
        // state
        drawingState() {
          return drawingStateVar();
        },
        companySearch() {
          return companySearchVar();
        },
        problemSearch() {
          return problemSearchVar();
        },
        mergeState() {
          return mergeStateVar();
        },
      },
    },
  },
});

/**
 * UI - ONBOARDING
 * TODO: better var names -- "shown" behaves differently in onboarding and user modals
 */
const onboardingModuleShown = JSON.parse(window.localStorage.getItem('learningModuleCompleted'));

const controlsPopup = {
  completed: JSON.parse(window.localStorage.getItem('controlsPopupCompleted')),
  panel: JSON.parse(window.localStorage.getItem('controlsPopupCurrentPanel')) || 0,
};

const architecturePopup = {
  completed: JSON.parse(window.localStorage.getItem('architecturePopupCompleted')),
  panel: JSON.parse(window.localStorage.getItem('architecturePopupCurrentPanel')) || 0,
};

export const onboardingModuleShownVar = makeVar(onboardingModuleShown);
export const controlsPopupVar = makeVar(controlsPopup);
export const architecturePopupVar = makeVar(architecturePopup);

/**
 * UI - GENERAL
 */
export const toggleUserShownVar = makeVar(false);
export const csvLoadingVar = makeVar(false);
export const selectedSourceIndexVar = makeVar(0);

/**
 * DRAWING TOOL STATE
 */
export const drawingState = {
  layoutLastGenerated: Date.now(),
  architectureID: null,
  focusID: null,
  drawingRasterProps: {
    html: '',
    width: 0,
    height: 0,
    devicePixelRatio: 1,
    gutter: 15,
  },
  selectedThemeName: THEME_GREENERY,
  editable: false,
  companyModalOpen: false,
};

export const drawingStateVar = makeVar(drawingState);

/**
 * ELASTICSEARCH STATE
 */
const search = {
  filterOperator: null,
  filterSort: SORT_OPTION_COUNT,
};

export const companySearchVar = makeVar(search);
export const problemSearchVar = makeVar(search);

/**
 * MERGE TOOL STATE
 */
const mergeState = {
  target: null,
  source: null,
};

export const mergeStateVar = makeVar(mergeState);
