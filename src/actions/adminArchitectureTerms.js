import { searchTaxonomies } from 'services/search';
import * as services from 'services/admin';
import { createTermsListFromTree } from 'utils/terms';
import { getAdminArchitecture } from 'actions/adminArchitecture';

export const INIT_TERMS = 'INIT_TERMS';

export const APPROVE_TERM = 'APPROVE_TERM';
export const REJECT_TERM = 'REJECT_TERM';
export const UNREJECT_TERM = 'UNREJECT_TERM';

export const SEARCH_FOR_PARENT_TERM = 'SEARCH_FOR_PARENT_TERM';
export const SEARCH_FOR_PARENT_TERM_LOADING = 'SEARCH_FOR_PARENT_TERM_LOADING';
export const SEARCH_FOR_PARENT_TERM_SUCCESS = 'SEARCH_FOR_PARENT_TERM_SUCCESS';
export const SEARCH_FOR_PARENT_TERM_ERROR = 'SEARCH_FOR_PARENT_TERM_ERROR';
export const SEARCH_FOR_PARENT_TERM_RESET = 'SEARCH_FOR_PARENT_TERM_RESET';

export const SUBMIT_APPROVED_TERMS_LOADING = 'SUBMIT_APPROVED_TERMS_LOADING';
export const SUBMIT_APPROVED_TERMS_SUCCESS = 'SUBMIT_APPROVED_TERMS_SUCCESS';
export const SUBMIT_APPROVED_TERMS_ERROR = 'SUBMIT_APPROVED_TERMS_ERROR';

export const INSERT_TERM_BELOW_TAXONOMY = 'INSERT_TERM_BELOW_TAXONOMY';
export const REMOVE_TERM_FROM_TAXONOMY = 'REMOVE_TERM_FROM_TAXONOMY';

export const LINK_TERM_TO_TAXONOMY = 'LINK_TERM_TO_TAXONOMY';
export const UNLINK_TERM_FROM_TAXONOMY = 'UNLINK_TERM_FROM_TAXONOMY';

export function initTerms() {
  return (dispatch, getState) => {
    const { adminArchitecture } = getState();

    return dispatch({
      type: INIT_TERMS,
      terms: adminArchitecture.newTerms,
    });
  };
}

export function submitApprovedTerms() {
  return (dispatch, getState) => {
    dispatch(submitApprovedTermsLoading());

    const { adminArchitectureTerms, adminArchitecture } = getState();

    const approvedTerms = adminArchitectureTerms.terms.byID
      .filter(id => adminArchitectureTerms.terms.entities[id].approved);

    const linkTerms = [];
    const insertTerms = [];

    approvedTerms.forEach((termID) => {
      const termEntity = adminArchitectureTerms.terms.entities[termID];

      const {
        term,
        taxonomy = {},
        parentID,
        taxonomyID,
      } = termEntity;

      // if term is being linked to existing taxonomy
      if (taxonomyID) {
        linkTerms.push({
          id: termID, // id of package
          term: termEntity.term, // term of package (required by mutation)
          taxonomyId: taxonomyID, // taxonomy to link term to
        });
      } else {
        // if term is being linked to newly created taxonomy line
        const termsList = createTermsListFromTree(taxonomy);
        termsList.push(term);

        insertTerms.push({
          id: termID, // id of package
          taxonomy_id: parentID, // parent taxonomy
          terms: `{${termsList}}`, // terms of new taxonomies to create under parent
        });
      }
    });

    return services
      .submitApprovedTerms(linkTerms, insertTerms)
      .then(() => {
        dispatch(submitApprovedTermsSuccess());
        dispatch(getAdminArchitecture(adminArchitecture.id));
      })
      .catch((error) => {
        dispatch(submitApprovedTermsError(error));
      });
  };
}

export function submitApprovedTermsLoading() {
  return {
    type: SUBMIT_APPROVED_TERMS_LOADING,
  };
}

export function submitApprovedTermsSuccess() {
  return {
    type: SUBMIT_APPROVED_TERMS_SUCCESS,
  };
}

export function submitApprovedTermsError(error) {
  return {
    type: SUBMIT_APPROVED_TERMS_ERROR,
    error,
  };
}

export function approveTerm(id, taxonomy) {
  return {
    type: APPROVE_TERM,
    id,
    taxonomy,
  };
}

export function rejectTerm(id) {
  return {
    type: REJECT_TERM,
    id,
  };
}

export function unrejectTerm(id) {
  return {
    type: UNREJECT_TERM,
    id,
  };
}

export function searchForParentTerm(contains, termID) {
  return (dispatch) => {
    dispatch(searchForParentTermLoading(termID));

    return searchTaxonomies(contains)
      .then((results) => {
        dispatch(searchForParentTermSuccess(results, termID));
      })
      .catch((error) => {
        dispatch(searchForParentTermError(error, termID));
      });
  };
}

export function searchForParentTermLoading(termID) {
  return {
    type: SEARCH_FOR_PARENT_TERM_LOADING,
    termID,
  };
}

export function searchForParentTermSuccess(results, termID) {
  return {
    type: SEARCH_FOR_PARENT_TERM_SUCCESS,
    results,
    termID,
  };
}

export function searchForParentTermError(error, termID) {
  return {
    type: SEARCH_FOR_PARENT_TERM_ERROR,
    error,
    termID,
  };
}

export function searchForParentTermReset() {
  return {
    type: SEARCH_FOR_PARENT_TERM_RESET,
  };
}

export function insertTermBelowTaxonomy(term, taxonomy) {
  return {
    type: INSERT_TERM_BELOW_TAXONOMY,
    term,
    taxonomy,
  };
}

export function removeTermFromTaxonomy(term) {
  return {
    type: REMOVE_TERM_FROM_TAXONOMY,
    term,
  };
}

export function linkTermToTaxonomy(term, taxonomy) {
  return {
    type: LINK_TERM_TO_TAXONOMY,
    term,
    taxonomy,
  };
}

export function unlinkTermFromTaxonomy(term) {
  return {
    type: UNLINK_TERM_FROM_TAXONOMY,
    term,
  };
}
