import * as services from 'services/admin';
import { ARCHITECTURE_QUERY_ROOT } from 'constants/gql';

// fetch the architecture
export const GET_ADMIN_ARCHITECTURE = 'GET_ADMIN_ARCHITECTURE';
export const GET_ADMIN_ARCHITECTURE_FETCHING = 'GET_ADMIN_ARCHITECTURE_FETCHING';
export const GET_ADMIN_ARCHITECTURE_SUCCESS = 'GET_ADMIN_ARCHITECTURE_SUCCESS';
export const GET_ADMIN_ARCHITECTURE_ERROR = 'GET_ADMIN_ARCHITECTURE_ERROR';
export const RESET_ADMIN_ARCHITECTURE = 'RESET_ADMIN_ARCHITECTURE';

export function getAdminArchitecture(id) {
  return (dispatch) => {
    dispatch(getAdminArchitectureFetching());

    return services
      .getAdminArchitecture(id)
      .then((data) => {
        const architecture = data[ARCHITECTURE_QUERY_ROOT];
        dispatch(getAdminArchitectureSuccess(architecture));
      })
      .catch((error) => {
        dispatch(getAdminArchitectureError(error));
      });
  };
}

function getAdminArchitectureFetching() {
  return {
    type: GET_ADMIN_ARCHITECTURE_FETCHING,
  };
}

function getAdminArchitectureSuccess(architecture) {
  return {
    type: GET_ADMIN_ARCHITECTURE_SUCCESS,
    architecture,
  };
}

function getAdminArchitectureError(error) {
  return {
    type: GET_ADMIN_ARCHITECTURE_ERROR,
    error,
  };
}

export function resetAdminArchitecture() {
  return {
    type: RESET_ADMIN_ARCHITECTURE,
  };
}

export const PATCH_ADMIN_ARCHITECTURE_STATUS_IN_REVIEW = 'PATCH_ADMIN_ARCHITECTURE_STATUS_IN_REVIEW';
export const PATCH_ADMIN_ARCHITECTURE_STATUS_IN_REVIEW_FETCHING = 'PATCH_ADMIN_ARCHITECTURE_STATUS_IN_REVIEW_FETCHING';
export const PATCH_ADMIN_ARCHITECTURE_STATUS_IN_REVIEW_SUCCESS = 'PATCH_ADMIN_ARCHITECTURE_STATUS_IN_REVIEW_SUCCESS';
export const PATCH_ADMIN_ARCHITECTURE_STATUS_IN_REVIEW_ERROR = 'PATCH_ADMIN_ARCHITECTURE_STATUS_IN_REVIEW_ERROR';

export function patchAdminArchitectureStatusInReview(architectureID) {
  return (dispatch) => {
    dispatch(patchAdminArchitectureStatusInReviewFetching());

    return services
      .patchAdminArchitectureInReview(architectureID)
      .then(() => {
        dispatch(patchAdminArchitectureStatusInReviewSuccess(architectureID));
      })
      .catch((error) => {
        dispatch(patchAdminArchitectureStatusInReviewError(error));
      });
  };
}

function patchAdminArchitectureStatusInReviewFetching() {
  return {
    type: PATCH_ADMIN_ARCHITECTURE_STATUS_IN_REVIEW_FETCHING,
  };
}

function patchAdminArchitectureStatusInReviewSuccess(architectureID) {
  return {
    type: PATCH_ADMIN_ARCHITECTURE_STATUS_IN_REVIEW_SUCCESS,
    architectureID,
  };
}

function patchAdminArchitectureStatusInReviewError(error) {
  return {
    type: PATCH_ADMIN_ARCHITECTURE_STATUS_IN_REVIEW_ERROR,
    error,
  };
}

// accept/publish the architecture
export const PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED = 'PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED';
export const PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_FETCHING = 'PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_FETCHING';
export const PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_SUCCESS = 'PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_SUCCESS';
export const PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_ERROR = 'PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_ERROR';

export function patchAdminArchitectureStatusPublished(id) {
  return (dispatch) => {
    dispatch(patchAdminArchitectureStatusPublishedFetching());

    return services
      .publishArchitecture(id)
      .then((response) => {
        dispatch(patchAdminArchitectureStatusPublishedSuccess(response));
      })
      .catch((error) => {
        dispatch(patchAdminArchitectureStatusPublishedError(error));
      });
  };
}

function patchAdminArchitectureStatusPublishedFetching() {
  return {
    type: PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_FETCHING,
  };
}

// the new, published architecture is returned
function patchAdminArchitectureStatusPublishedSuccess(architecture) {
  return {
    type: PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_SUCCESS,
    architecture,
  };
}

function patchAdminArchitectureStatusPublishedError(error) {
  return {
    type: PATCH_ADMIN_ARCHITECTURE_STATUS_PUBLISHED_ERROR,
    error,
  };
}

// reject the architecture
export const PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED = 'PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED';
export const PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_FETCHING = 'PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_FETCHING';
export const PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_SUCCESS = 'PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_SUCCESS';
export const PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_ERROR = 'PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_ERROR';

export function patchAdminArchitectureStatusRejected(architectureID) {
  return (dispatch) => {
    dispatch(patchAdminArchitectureStatusRejectedFetching());

    return services
      .patchAdminArchitectureRejected(architectureID)
      .then(() => {
        dispatch(patchAdminArchitectureStatusRejectedSuccess());
      })
      .catch((error) => {
        dispatch(patchAdminArchitectureStatusRejectedError(error));
      });
  };
}

function patchAdminArchitectureStatusRejectedFetching() {
  return {
    type: PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_FETCHING,
  };
}

// 204 returned
function patchAdminArchitectureStatusRejectedSuccess() {
  return {
    type: PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_SUCCESS,
  };
}

function patchAdminArchitectureStatusRejectedError() {
  return {
    type: PATCH_ADMIN_ARCHITECTURE_STATUS_REJECTED_ERROR,
  };
}

export const HIGHLIGHT_PACKAGE = 'HIGHLIGHT_PACKAGE';

export function highlightPackage(packageID) {
  return {
    type: HIGHLIGHT_PACKAGE,
    packageID,
  };
}

export const TOGGLE_ADMIN_ARCHITECTURE_PACKAGE_CHILDREN = 'TOGGLE_ADMIN_ARCHITECTURE_PACKAGE_CHILDREN';

export function toggleAdminArchitecturePackageChildren(architectureID, packageID) {
  return {
    type: TOGGLE_ADMIN_ARCHITECTURE_PACKAGE_CHILDREN,
    architectureID,
    packageID,
  };
}
