export const RESET_ADMIN_ARCHITECTURES_STATE = 'RESET_ADMIN_ARCHITECTURES_STATE';

export function resetAdminArchitecturesState() {
  return {
    type: RESET_ADMIN_ARCHITECTURES_STATE,
  };
}
