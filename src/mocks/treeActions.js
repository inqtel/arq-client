export const treeActions = {
  primary: [{
    title: 'Test 1',
    icon: 'Icon 1',
    handler: () => {},
  }, {
    title: 'Test 2',
    icon: 'Icon 2',
    handler: () => {},
  }],
};

export default treeActions;
