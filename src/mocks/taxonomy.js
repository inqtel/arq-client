import { source } from 'mocks/source';
import { architecture } from 'mocks/architectures';

export const taxonomy = {
  id: '1f3ed545-8457-4615-a592-ad87511bec39',
  term: 'Microwave theory and techniques',
  source,
  expand: true,
  children: [
    {
      id: '5afb8d55-c22d-4754-a776-25b5d4a5527d',
      term: 'Millimeter wave technology',
      source,
      expand: true,
      children: [
        {
          id: '5b1fad06-6dc3-41c3-80be-c26580e9f7ca',
          term: 'Millimeter wave integrated circuits',
          source,
          expand: false,
          children: [
            {
              id: 'c3d04171-4aea-4b10-b960-2130a724a9f8',
              term: 'MIMICs',
              source,
              expand: false,
              children: [],
              references: [],
            },
          ],
          references: [],
        },
        {
          id: '0228c2c2-ffcb-493a-bdb9-d19b979577b9',
          term: 'Millimeter wave circuits',
          source,
          expand: true,
          children: [
            {
              id: 'fc0be109-5423-4f15-bda0-5e20c610bf18',
              term: 'Millimeter wave integrated circuits',
              source,
              expand: false,
              children: [],
              references: [],
            },
          ],
          references: [],
        },
      ],
      references: [],
    },
    {
      id: '6a217377-9087-4ddd-bb60-bfa6d13630d9',
      term: 'Submillimeter wave technology',
      source,
      expand: true,
      children: [
        {
          id: 'e4514a43-87fc-4344-987d-415f3d6b0be8',
          term: 'Submillimeter wave circuits',
          source,
          expand: true,
          children: [
            {
              id: '46289743-6c48-4a14-9208-a923749285d2',
              term: 'Submillimeter wave integrated circuits',
              source,
              expand: false,
              children: [],
              references: [],
            },
          ],
          references: [],
        },
        {
          id: 'fc945e27-369d-4b6f-957a-de5e11c05754',
          term: 'Submillimeter wave integrated circuits',
          source,
          expand: false,
          children: [],
          references: [],
        },
      ],
      references: [],
    },
    {
      id: '6a9b7e96-aff4-4a73-bdb9-fad0090e375a',
      term: 'Microwave technology',
      source,
      expand: true,
      children: [
        {
          id: '749544a9-698f-4dec-98dc-d2df099adbab',
          term: 'Microwave circuits',
          source,
          expand: false,
          children: [],
          references: [],
        },
      ],
      references: [],
    },
  ],
  references: [],
};

export const taxonomyFromArchitecture = {
  id: '1f3ed545-8457-4615-a592-ad87511bec39',
  term: 'Microwave theory and techniques',
  architecture,
  expand: true,
  children: [
    {
      id: '5afb8d55-c22d-4754-a776-25b5d4a5527d',
      term: 'Millimeter wave technology',
      architecture,
      expand: true,
      children: [
        {
          id: '5b1fad06-6dc3-41c3-80be-c26580e9f7ca',
          term: 'Millimeter wave integrated circuits',
          architecture,
          expand: false,
          children: [
            {
              id: 'c3d04171-4aea-4b10-b960-2130a724a9f8',
              term: 'MIMICs',
              architecture,
              expand: false,
              children: [],
              references: [],
            },
          ],
          references: [],
        },
        {
          id: '0228c2c2-ffcb-493a-bdb9-d19b979577b9',
          term: 'Millimeter wave circuits',
          architecture,
          expand: true,
          children: [
            {
              id: 'fc0be109-5423-4f15-bda0-5e20c610bf18',
              term: 'Millimeter wave integrated circuits',
              source,
              expand: false,
              children: [],
              references: [],
            },
          ],
          references: [],
        },
      ],
      references: [],
    },
    {
      id: '6a217377-9087-4ddd-bb60-bfa6d13630d9',
      term: 'Submillimeter wave technology',
      architecture,
      expand: true,
      children: [
        {
          id: 'e4514a43-87fc-4344-987d-415f3d6b0be8',
          term: 'Submillimeter wave circuits',
          architecture,
          expand: true,
          children: [
            {
              id: '46289743-6c48-4a14-9208-a923749285d2',
              term: 'Submillimeter wave integrated circuits',
              architecture,
              expand: false,
              children: [],
              references: [],
            },
          ],
          references: [],
        },
        {
          id: 'fc945e27-369d-4b6f-957a-de5e11c05754',
          term: 'Submillimeter wave integrated circuits',
          architecture,
          expand: false,
          children: [],
          references: [],
        },
      ],
      references: [],
    },
    {
      id: '6a9b7e96-aff4-4a73-bdb9-fad0090e375a',
      term: 'Microwave technology',
      architecture,
      expand: true,
      children: [
        {
          id: '749544a9-698f-4dec-98dc-d2df099adbab',
          term: 'Microwave circuits',
          architecture,
          expand: false,
          children: [],
          references: [],
        },
      ],
      references: [],
    },
  ],
  references: [],
};

export default taxonomy;
