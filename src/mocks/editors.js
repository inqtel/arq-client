import { EditorState } from 'draft-js';
import { stateFromHTML } from 'draft-js-import-html';

export const html = '<h1>This is a heading</h1><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Itaque quasi voluptates, alias modi facilis earum. Laboriosam, odio modi vitae veritatis, vel temporibus aliquam autem voluptates sint tempora adipisci. Incidunt, reprehenderit.</p><ul><li>List item 1</li><li>List item 2</li><li>List item 3</li><li>List item 4</li><li>List item 5</li></ul><h2>This is a second heading</h2><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero, libero aperiam dolorum laboriosam, quod in quidem est illo, neque placeat hic corporis eius. Quasi quaerat perferendis tenetur aspernatur adipisci natus.</p>';

export const contentState = stateFromHTML(html);

export const editorState = EditorState.createWithContent(contentState);
