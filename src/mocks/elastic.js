export const elasticResult = {
  took: 7,
  timed_out: false,
  _shards: {
    total: 1, successful: 1, skipped: 0, failed: 0,
  },
  hits: {
    total: { value: 9, relation: 'eq' },
    max_score: 1.0,
    hits: [{
      _index: 'taxonomies',
      _type: '_doc',
      _id: 'cba49d59-69fc-4366-813b-6f79d41b851e',
      _score: 1.0,
      _source: {
        children: [{ term: 'Extrasolar planets', source_name: 'IEEE', id: 'e2980641-4e41-41fa-9c69-5963432ba06d' }, { term: 'Venus', source_name: 'IEEE', id: '85877b23-14d7-4bae-8a6d-3ed4edcb696a' }, { term: 'Mercury (planets)', source_name: 'IEEE', id: 'f940df14-257a-44e5-adf1-2c18d60b8ee0' }, { term: 'Earth', source_name: 'IEEE', id: '46edb04b-ec13-47e7-9b4c-01d53462f675' }, { term: 'Sun', source_name: 'IEEE', id: '2e9fb4fd-1920-4ac0-88a1-96b112356063' }, { term: 'Mars', source_name: 'IEEE', id: 'f2427662-bedd-40f4-bfdb-1465bb85cd64' }, { term: 'Jupiter', source_name: 'IEEE', id: '72a0c478-383d-4390-820a-32b70428b63e' }, { term: 'Saturn', source_name: 'IEEE', id: '85198803-a09c-444f-b79d-dfb685dc53ac' }, { term: 'Pluto', source_name: 'IEEE', id: '6118baf2-102b-4edd-9561-cdf94d347ca5' }], term: 'Planets', source_name: 'IEEE', '@version': '1', updated_at: '2020-07-22T18:57:25.000Z', id: 'cba49d59-69fc-4366-813b-6f79d41b851e', '@timestamp': '2020-10-20T21:04:03.631Z', ancestors: [{ term: 'Science - general', source_name: 'IEEE', id: 'c93ac025-fbad-4e8c-9baa-267941bc535a' }, { term: 'Astronomy', source_name: 'IEEE', id: '2fb74b2c-6cd6-4928-80f6-c64234428218' }, { term: 'Planets', source_name: 'IEEE', id: 'cba49d59-69fc-4366-813b-6f79d41b851e' }],
      },
    }, {
      _index: 'taxonomies',
      _type: '_doc',
      _id: 'f940df14-257a-44e5-adf1-2c18d60b8ee0',
      _score: 1.0,
      _source: {
        term: 'Mercury (planets)', source_name: 'IEEE', '@version': '1', updated_at: '2020-07-22T18:57:25.000Z', id: 'f940df14-257a-44e5-adf1-2c18d60b8ee0', '@timestamp': '2020-10-20T21:04:03.632Z', children_json: null, ancestors: [{ term: 'Mercury (planets)', source_name: 'IEEE', id: 'f940df14-257a-44e5-adf1-2c18d60b8ee0' }, { term: 'Science - general', source_name: 'IEEE', id: 'c93ac025-fbad-4e8c-9baa-267941bc535a' }, { term: 'Astronomy', source_name: 'IEEE', id: '2fb74b2c-6cd6-4928-80f6-c64234428218' }, { term: 'Planets', source_name: 'IEEE', id: 'cba49d59-69fc-4366-813b-6f79d41b851e' }],
      },
    }, {
      _index: 'taxonomies',
      _type: '_doc',
      _id: '0118ac05-6f89-4108-aa7f-d23c49312767',
      _score: 1.0,
      _source: {
        children: [{ term: 'Mars', source_name: 'PLOS', id: '69fde6ab-5ee9-4f84-b3cd-89e6d7035e57' }, { term: 'Uranus', source_name: 'PLOS', id: '71fa7ea2-e0e7-4578-8f25-410fb7bcf8f8' }, { term: 'Venus', source_name: 'PLOS', id: 'fc83602b-22d1-4748-a25c-ebfdf6667bc6' }, { term: 'Mercury (planet)', source_name: 'PLOS', id: '2aaf216e-8217-483e-9d86-a637e9b03656' }, { term: 'Neptune', source_name: 'PLOS', id: '87953c96-ffa2-4347-b325-ecbc8bff89d1' }, { term: 'Jupiter', source_name: 'PLOS', id: 'ced8e2ad-59c5-49ad-8d2e-e6ac8a8d69b8' }, { term: 'Saturn', source_name: 'PLOS', id: '1a979817-4053-4c02-ae0f-ee0c2eebf15b' }], term: 'Planets', source_name: 'PLOS', '@version': '1', updated_at: '2020-07-22T18:57:25.000Z', id: '0118ac05-6f89-4108-aa7f-d23c49312767', '@timestamp': '2020-10-20T21:04:07.047Z', ancestors: [{ term: 'Planets', source_name: 'PLOS', id: '0118ac05-6f89-4108-aa7f-d23c49312767' }, { term: 'Physical sciences', source_name: 'PLOS', id: '78ef11b1-528a-450c-8d9f-75596b2af88f' }, { term: 'Celestial objects', source_name: 'PLOS', id: 'b4c6f63c-a08c-47eb-8547-84a712f0a8ef' }, { term: 'Astronomical sciences', source_name: 'PLOS', id: 'f6a43fb4-aa6e-4afa-a7d4-332471f2b95f' }],
      },
    }, {
      _index: 'taxonomies',
      _type: '_doc',
      _id: '1db66429-1fe0-42f0-9fe4-c7e5bde916c3',
      _score: 1.0,
      _source: {
        term: 'Dwarf planets', source_name: 'PLOS', '@version': '1', updated_at: '2020-07-22T18:57:25.000Z', id: '1db66429-1fe0-42f0-9fe4-c7e5bde916c3', '@timestamp': '2020-10-20T21:04:07.048Z', children_json: null, ancestors: [{ term: 'Dwarf planets', source_name: 'PLOS', id: '1db66429-1fe0-42f0-9fe4-c7e5bde916c3' }, { term: 'Physical sciences', source_name: 'PLOS', id: '78ef11b1-528a-450c-8d9f-75596b2af88f' }, { term: 'Celestial objects', source_name: 'PLOS', id: 'b4c6f63c-a08c-47eb-8547-84a712f0a8ef' }, { term: 'Astronomical sciences', source_name: 'PLOS', id: 'f6a43fb4-aa6e-4afa-a7d4-332471f2b95f' }],
      },
    }, {
      _index: 'taxonomies',
      _type: '_doc',
      _id: '2eb6cb59-323b-485b-a8be-17bac9d93854',
      _score: 1.0,
      _source: {
        children: [{ term: 'Uranus', source_name: 'PLOS', id: 'c656aad3-f380-4633-a228-66f937efcef3' }, { term: 'Mercury (planet)', source_name: 'PLOS', id: '1365284c-e6f8-4334-8772-4a4babd8f124' }, { term: 'Venus', source_name: 'PLOS', id: 'ef653f74-59b1-4262-a67e-653cec391945' }, { term: 'Saturn', source_name: 'PLOS', id: '87aac177-a1ea-42d4-a12e-24add87587d0' }, { term: 'Jupiter', source_name: 'PLOS', id: 'fcbed586-512e-4147-aba1-ea8406289b98' }, { term: 'Neptune', source_name: 'PLOS', id: 'e9ad13e5-7554-4175-82f1-29d3d40b6502' }, { term: 'Mars', source_name: 'PLOS', id: '7ba85495-8bbb-435b-9a21-432a2a0f55c5' }], term: 'Planets', source_name: 'PLOS', '@version': '1', updated_at: '2020-07-22T18:57:25.000Z', id: '2eb6cb59-323b-485b-a8be-17bac9d93854', '@timestamp': '2020-10-20T21:04:07.048Z', ancestors: [{ term: 'Physical sciences', source_name: 'PLOS', id: '78ef11b1-528a-450c-8d9f-75596b2af88f' }, { term: 'Planetary sciences', source_name: 'PLOS', id: 'ab5f6a15-88f3-42f0-8890-e5382e948320' }, { term: 'Astronomical sciences', source_name: 'PLOS', id: 'f6a43fb4-aa6e-4afa-a7d4-332471f2b95f' }, { term: 'Planets', source_name: 'PLOS', id: '2eb6cb59-323b-485b-a8be-17bac9d93854' }],
      },
    }, {
      _index: 'taxonomies',
      _type: '_doc',
      _id: '3a4572df-6ee4-4349-a123-6fd6fdeef245',
      _score: 1.0,
      _source: {
        term: 'Extrasolar planets', source_name: 'PLOS', '@version': '1', updated_at: '2020-07-22T18:57:25.000Z', id: '3a4572df-6ee4-4349-a123-6fd6fdeef245', '@timestamp': '2020-10-20T21:04:07.049Z', children_json: null, ancestors: [{ term: 'Extrasolar planets', source_name: 'PLOS', id: '3a4572df-6ee4-4349-a123-6fd6fdeef245' }, { term: 'Physical sciences', source_name: 'PLOS', id: '78ef11b1-528a-450c-8d9f-75596b2af88f' }, { term: 'Planetary sciences', source_name: 'PLOS', id: 'ab5f6a15-88f3-42f0-8890-e5382e948320' }, { term: 'Astronomical sciences', source_name: 'PLOS', id: 'f6a43fb4-aa6e-4afa-a7d4-332471f2b95f' }],
      },
    }, {
      _index: 'taxonomies',
      _type: '_doc',
      _id: '20ce1bb5-21de-4634-a2fa-ac196ce2b019',
      _score: 1.0,
      _source: {
        term: 'Dwarf planets', source_name: 'PLOS', '@version': '1', updated_at: '2020-07-22T18:57:25.000Z', id: '20ce1bb5-21de-4634-a2fa-ac196ce2b019', '@timestamp': '2020-10-20T21:04:07.049Z', children_json: null, ancestors: [{ term: 'Dwarf planets', source_name: 'PLOS', id: '20ce1bb5-21de-4634-a2fa-ac196ce2b019' }, { term: 'Physical sciences', source_name: 'PLOS', id: '78ef11b1-528a-450c-8d9f-75596b2af88f' }, { term: 'Planetary sciences', source_name: 'PLOS', id: 'ab5f6a15-88f3-42f0-8890-e5382e948320' }, { term: 'Astronomical sciences', source_name: 'PLOS', id: 'f6a43fb4-aa6e-4afa-a7d4-332471f2b95f' }],
      },
    }, {
      _index: 'taxonomies',
      _type: '_doc',
      _id: 'c4c64c69-f98c-4d61-a169-1e38948494a9',
      _score: 1.0,
      _source: {
        children: [{ term: 'Earthshine', source_name: 'PLOS', id: 'd8c6d76a-786d-46e5-8f45-bab9b9cb5023' }], term: 'Planetshine', source_name: 'PLOS', '@version': '1', updated_at: '2020-07-22T18:57:25.000Z', id: 'c4c64c69-f98c-4d61-a169-1e38948494a9', '@timestamp': '2020-10-20T21:04:07.045Z', ancestors: [{ term: 'Planetshine', source_name: 'PLOS', id: 'c4c64c69-f98c-4d61-a169-1e38948494a9' }, { term: 'Physical sciences', source_name: 'PLOS', id: '78ef11b1-528a-450c-8d9f-75596b2af88f' }, { term: 'Astronomical sciences', source_name: 'PLOS', id: 'f6a43fb4-aa6e-4afa-a7d4-332471f2b95f' }, { term: 'Observational astronomy', source_name: 'PLOS', id: 'd12d798e-e347-4984-84a2-729315ea40d5' }, { term: 'Astronomy', source_name: 'PLOS', id: '5d67a05d-917a-41eb-ad2d-a956ea913ca3' }],
      },
    }, {
      _index: 'taxonomies',
      _type: '_doc',
      _id: 'e2980641-4e41-41fa-9c69-5963432ba06d',
      _score: 1.0,
      _source: {
        term: 'Extrasolar planets', source_name: 'IEEE', '@version': '1', updated_at: '2020-07-22T18:57:25.000Z', id: 'e2980641-4e41-41fa-9c69-5963432ba06d', '@timestamp': '2020-10-20T21:04:03.631Z', children_json: null, ancestors: [{ term: 'Science - general', source_name: 'IEEE', id: 'c93ac025-fbad-4e8c-9baa-267941bc535a' }, { term: 'Astronomy', source_name: 'IEEE', id: '2fb74b2c-6cd6-4928-80f6-c64234428218' }, { term: 'Planets', source_name: 'IEEE', id: 'cba49d59-69fc-4366-813b-6f79d41b851e' }, { term: 'Extrasolar planets', source_name: 'IEEE', id: 'e2980641-4e41-41fa-9c69-5963432ba06d' }],
      },
    }],
  },
};

export const elasticResultCompanies = {
  took: 10,
  timed_out: false,
  _shards: {
    total: 1,
    successful: 1,
    skipped: 0,
    failed: 0,
  },
  hits: {
    total: {
      value: 3,
      relation: 'eq',
    },
    max_score: 1,
    hits: [
      {
        _index: 'organizations',
        _type: '_doc',
        _id: 'a7d7cc8d-b1ba-4432-b776-0d0a3779b992',
        _score: 1,
        _source: {
          '@version': '1',
          pitchbookid: null,
          portfolio: true,
          name: 'Acme Corporation',
          logo: null,
          synced_at: '2020-09-29T23:40:52.289Z',
          '@timestamp': '2020-11-10T20:00:01.230Z',
          capabilities: [
            {
              term: 'Computing methodologies',
              id: '94ce08d3-be24-4204-b0fb-4ce1dca28cc4',
            },
            {
              term: 'Computer systems organization',
              id: '9a4e5cf9-46e9-46e3-857b-314d8efb71d5',
            },
            {
              term: 'Applied computing',
              id: 'dc3fe7af-18c9-4a37-a820-300c811f7f27',
            },
          ],
          id: 'a7d7cc8d-b1ba-4432-b776-0d0a3779b992',
        },
      },
      {
        _index: 'organizations',
        _type: '_doc',
        _id: 'd70dd68b-ab55-430a-95e3-4d77e3e47fa7',
        _score: 1,
        _source: {
          '@version': '1',
          pitchbookid: null,
          portfolio: false,
          name: 'Oceanic Airlines',
          logo: null,
          synced_at: '2020-09-29T23:40:52.289Z',
          '@timestamp': '2020-11-10T20:00:01.244Z',
          capabilities: [
            {
              term: 'Hardware',
              id: '52148e47-c936-468b-9805-66e03cec7115',
            },
            {
              term: 'Human-centered computing',
              id: 'c04ae8f3-838e-40d8-8282-13e420e64706',
            },
            {
              term: 'Information systems',
              id: 'fc8978e6-423e-4f43-8abf-e892eab215e5',
            },
          ],
          id: 'd70dd68b-ab55-430a-95e3-4d77e3e47fa7',
        },
      },
      {
        _index: 'organizations',
        _type: '_doc',
        _id: 'd70dd68b-ab55-430a-95e3-0d0a3779b992',
        _score: 1,
        _source: {
          '@version': '1',
          pitchbookid: null,
          portfolio: true,
          name: 'Krusty Burger',
          logo: null,
          synced_at: '2020-09-29T23:40:52.289Z',
          '@timestamp': '2020-11-10T20:00:01.246Z',
          capabilities: [
            {
              term: 'Networks',
              id: '52caa4fa-ee54-4622-8ead-223e732a341f',
            },
            {
              term: 'Software and its engineering',
              id: '5e156257-7c2c-4568-80ba-42a12d39b150',
            },
            {
              term: 'Security and privacy',
              id: '7a6ac5f8-5147-4b81-9d92-96930e67ad4d',
            },
            {
              term: 'Mathematics of computing',
              id: 'becad4cd-3695-4ebe-96d3-e46cc02e1a87',
            },
          ],
          id: 'd70dd68b-ab55-430a-95e3-0d0a3779b992',
        },
      },
    ],
  },
  aggregations: {
    portfolio8: {
      doc_count: 3,
      portfolio_count: {
        value: 2,
      },
      portfolio: {
        doc_count_error_upper_bound: 0,
        sum_other_doc_count: 0,
        buckets: [
          {
            key: 0,
            key_as_string: 'false',
            doc_count: 1,
          },
          {
            key: 1,
            key_as_string: 'true',
            doc_count: 2,
          },
        ],
      },
    },
    taxonomies7: {
      doc_count: 3,
      inner: {
        doc_count: 10,
        'capabilities.term.raw_count': {
          value: 10,
        },
        'capabilities.term.raw': {
          doc_count_error_upper_bound: 0,
          sum_other_doc_count: 6,
          buckets: [
            {
              key: 'Applied computing',
              doc_count: 1,
            },
            {
              key: 'Computer systems organization',
              doc_count: 1,
            },
            {
              key: 'Computing methodologies',
              doc_count: 1,
            },
            {
              key: 'Hardware',
              doc_count: 1,
            },
          ],
        },
      },
    },
  },
};

export default elasticResult;
