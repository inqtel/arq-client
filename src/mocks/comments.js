import { v4 as uuid } from 'uuid';
import { users } from 'mocks/users';

export const comments = [
  {
    id: uuid(),
    author: users[0],
    text: 'This is a new comment that is mostly helpful.',
  },
  {
    id: uuid(),
    author: users[1],
    text: 'What about this link? Can it be useful at all?',
  },
  {
    id: uuid(),
    author: users[2],
    text: 'Hey, check out circuits on Wikipedia. That should help.',
  },
];

export const comment = comments[0];
