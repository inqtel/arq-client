import { v4 as uuid } from 'uuid';

export const terms = [
  {
    id: uuid(),
    term: 'Jetpack',
    approved: null,
  },
  {
    id: uuid(),
    term: 'Air Propulsion',
    approved: null,
  },
  {
    id: uuid(),
    term: 'Oxygen Cylinder',
    approved: null,
  },
  {
    id: uuid(),
    term: 'Water Recycling',
    approved: null,
  },
  {
    id: uuid(),
    term: 'Atmosphere',
    approved: null,
  },
];

export const term = terms[0];

export const rejectedTerm = {
  ...terms[0],
  approved: false,
};
