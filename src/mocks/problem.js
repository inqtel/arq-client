export const problemPriorityHigh = {
  id: '712397a0-0713-4daa-8c87-61874e9d5444',
  createdAt: '0001-01-01T00:00:00Z',
  updatedAt: '0001-01-01T00:00:00Z',
  fiscalYear: '2017',
  customerNumber: '1.1',
  customer: 'a9352fb4-9397-42e2-bf8d-476dfa3fbd28',
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  priority: 'High',
  categoryOfNeed: '',
  customerCategory: 'Mission Tools/Tradecraft|Communication',
  capabilities: null,
};

export const problemPriorityMedium = {
  id: '712397a0-0713-4daa-8c87-61874e9d5444',
  createdAt: '0001-01-01T00:00:00Z',
  updatedAt: '0001-01-01T00:00:00Z',
  fiscalYear: '2017',
  customerNumber: '1.1',
  customer: 'a9352fb4-9397-42e2-bf8d-476dfa3fbd28',
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  priority: 'Medium',
  categoryOfNeed: '',
  customerCategory: 'Mission Tools/Tradecraft|Communication',
  capabilities: null,
};

export const problemPriorityLow = {
  id: '712397a0-0713-4daa-8c87-61874e9d5444',
  createdAt: '0001-01-01T00:00:00Z',
  updatedAt: '0001-01-01T00:00:00Z',
  fiscalYear: '2017',
  customerNumber: '1.1',
  customer: 'a9352fb4-9397-42e2-bf8d-476dfa3fbd28',
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  priority: 'Low',
  categoryOfNeed: '',
  customerCategory: 'Mission Tools/Tradecraft|Communication',
  capabilities: null,
};

export const problemPriorityNotAssigned = {
  id: '712397a0-0713-4daa-8c87-61874e9d5444',
  createdAt: '0001-01-01T00:00:00Z',
  updatedAt: '0001-01-01T00:00:00Z',
  fiscalYear: '2017',
  customerNumber: '1.1',
  customer: 'a9352fb4-9397-42e2-bf8d-476dfa3fbd28',
  description:
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  priority: 'Not Assigned',
  categoryOfNeed: '',
  customerCategory: 'Mission Tools/Tradecraft|Communication',
  capabilities: null,
};

export const esProblem = {
  id: '712397a0-0713-4daa-8c87-61874e9d5444',
  createdAt: '0001-01-01T00:00:00Z',
  updatedAt: '0001-01-01T00:00:00Z',
  fiscalYear: '2017',
  customerNumber: ['<em>1.1</em>'],
  customer: 'a9352fb4-9397-42e2-bf8d-476dfa3fbd28',
  description: [
    'Lorem ipsum dolor sit amet, consectetur <em>data</em> elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
  ],
  priority: 'Not Assigned',
  categoryOfNeed: '',
  customerCategory: 'Mission Tools/Tradecraft|Communication',
  capabilities: null,
};
