import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { IconLoader } from 'components/Icons/Icons.jsx';
import 'components/Loader/Loader.scss';

const Loader = ({
  size,
  isFullscreen,
  isCentered,
  isHeight100,
  isAbsolute,
}) => (
  <div
    className={cx('Loader', {
      'Loader--isFullscreen': isFullscreen,
      'Loader--isCentered': isCentered,
      'Loader--isHeight100': isHeight100,
      'Loader--isAbsolute': isAbsolute,
    })}
  >
    <IconLoader height={size} />
  </div>
);

Loader.propTypes = {
  size: PropTypes.number,
  isFullscreen: PropTypes.bool,
  isCentered: PropTypes.bool,
  isHeight100: PropTypes.bool,
  isAbsolute: PropTypes.bool,
};

Loader.defaultProps = {
  size: 12,
  isFullscreen: false,
  isCentered: false,
  isHeight100: false,
  isAbsolute: false,
};

export default Loader;
