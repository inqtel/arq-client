import React from 'react';

const LearningModuleSideNav = () => (
  <div className="LearningModuleSideNav">
    <div className="LearningModule--tab">
      <a href="#intro">What is ArQ?</a>
    </div>
    <div className="LearningModule--tab">
      <a href="#documents">Documents</a>
    </div>
    <div className="LearningModule--tab">
      <a href="#outline">Outline</a>
    </div>
    <div className="LearningModule--tab">
      <a href="#search">Search</a>
    </div>
    <div className="LearningModule--tab">
      <a href="#submitting">Publishing</a>
    </div>
    <div className="LearningModule--tab">
      <a href="#drawing">Drawing</a>
    </div>
  </div>
);

export default LearningModuleSideNav;
