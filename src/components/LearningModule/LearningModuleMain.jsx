import React, { Component } from 'react';
import {
  isIE,
  isEdge,
  isSafari,
  isFirefox,
} from 'utils/browser';
import LearningModuleNoVideo from 'components/LearningModule/LearningModuleNoVideo.jsx';

class LearningModuleMain extends Component {
  componentWillUnmount() {
    window.localStorage.setItem('learningModuleCompleted', true);
  }

  render() {
    const showNoVideoMessage = isIE() || isEdge() || isSafari() || isFirefox();

    return (
      <div className="LearningModule">
        <div className="LearningModule-column">
          <section id="intro" className="LearningModule--section">
            <h3 className="LearningModule--header">What is ArQ?</h3>
            <p>
              ArQ is working to bridge the gap between architectures, problem
              sets and companies. It is a tool that will help you formulate
              architectures that can be published, collaborated on and analyzed.
            </p>
            {showNoVideoMessage ? (
              <LearningModuleNoVideo />
            ) : (
              // eslint-disable-next-line jsx-a11y/media-has-caption
              <video className="LearningModule--video" controls>
                <source
                  src="/videos/onboarding_intro.mp4"
                  type="video/mp4"
                />
              </video>
            )}
            <p>
              Users can create documents that contain 2 main ingredients,
              freeform text for notetaking and objects such as an architecture
              outline.
            </p>
          </section>
          <section id="documents" className="LearningModule--section">
            <h3 className="LearningModule--header">Documents</h3>
            {showNoVideoMessage ? (
              <LearningModuleNoVideo />
            ) : (
              // eslint-disable-next-line jsx-a11y/media-has-caption
              <video className="LearningModule--video" controls>
                <source
                  src="/videos/onboarding_documents.mp4?raw=true"
                  type="video/mp4"
                />
              </video>
            )}
            <p>
              To create a new document, click the new button on your dashboard
            </p>
            <p>
              The title and text of a document are for you to organize your
              thoughts and take notes. They will have no effect on your final
              architecture.
            </p>
            <p>Any changes made will be automatically saved.</p>
          </section>
          <section id="outline" className="LearningModule--section">
            <h3 className="LearningModule--header">Architecture Outline</h3>
            {showNoVideoMessage ? (
              <LearningModuleNoVideo />
            ) : (
              // eslint-disable-next-line jsx-a11y/media-has-caption
              <video className="LearningModule--video" controls>
                <source
                  src="/videos/onboarding_outline.mp4?raw=true"
                  type="video/mp4"
                />
              </video>
            )}
            <p>
              An Architecture is the scaffolding you’ll be implementing to
              compose your architecture. It&apos;s composed of 2 component packages
              and capabilities. Packages are essentially freeform text that you
              can use to organize your thoughts and group children. A Capability
              is a term that is officially recognized by the science community
              so that you can communicate and collaborate better with your
              peers.
            </p>
          </section>
          <section id="search" className="LearningModule--section">
            <h3 className="LearningModule--header">Search</h3>
            {showNoVideoMessage ? (
              <LearningModuleNoVideo />
            ) : (
              // eslint-disable-next-line jsx-a11y/media-has-caption
              <video className="LearningModule--video" controls>
                <source
                  src="'/videos/onboarding_search.mp4?raw=true"
                  type="video/mp4"
                />
              </video>
            )}
            <p>
              Searching and linking for capabilities can serve two purposes
              within ArQ. You can find single terms used by the science
              community as well as find the children for these terms.
            </p>
            <p>
              You can action a search by using the Search Bar on the top right
              corner or start the search straight in the document with a &quot;/&quot;.
            </p>
            <p>
              When you&apos;ve found a capability, there are 2 actions you can do,
              either import the whole tree or simply link a term.
            </p>
            <p>
              If the term you&apos;ve searched for doesn&apos;t exist, you can mark is a
              new term to be assessed in the review process.
            </p>
          </section>
          <section id="submitting" className="LearningModule--section">
            <h3 className="LearningModule--header">Publishing</h3>
            {showNoVideoMessage ? (
              <LearningModuleNoVideo />
            ) : (
              // eslint-disable-next-line jsx-a11y/media-has-caption
              <video className="LearningModule--video" controls>
                <source
                  src="/videos/onboarding_publishing.mp4?raw=true"
                  type="video/mp4"
                />
              </video>
            )}
            <p>
              When you&apos;re happy with your Architecture, you can submit it for
              review to the architectures team. A submitted Architecture cannot
              be edited. If you do want to make more changes, remove it from
              review at anytime.
            </p>
          </section>
          <section id="drawing" className="LearningModule--section">
            <h3 className="LearningModule--header">Drawing</h3>
            {showNoVideoMessage ? (
              <LearningModuleNoVideo />
            ) : (
              // eslint-disable-next-line jsx-a11y/media-has-caption
              <video className="LearningModule--video" controls>
                <source
                  src="/videos/onboarding_drawing.mp4?raw=true"
                  type="video/mp4"
                />
              </video>
            )}
            <p>
              You can preview your Architecture as a drawing at anytime. The
              drawing view will automatically format your architecture visually.
              It shows 3 levels at a time. You can zoom in by double clicking
              and use the breadcrumbs above to navigate back.
            </p>
            <p>
              The drawing tool has basic formatting features to help your
              drawing have more balance. If you want to make more edits, you can
              always export it to a powerpoint friendly format.
            </p>
          </section>
        </div>
      </div>
    );
  }
}

LearningModuleMain.propTypes = {};

LearningModuleMain.defaultProps = {};

export default LearningModuleMain;
