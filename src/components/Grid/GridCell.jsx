import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const GridCell = ({ children }) => <div className={cx('Grid-cell', {})}>{children}</div>;

GridCell.propTypes = {
  children: PropTypes.node,
};

GridCell.defaultProps = {
  children: null,
};

export default GridCell;
