import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import StatusText from 'components/Text/StatusText.jsx';

class ArchitectureStatus extends PureComponent {
  render() {
    const { status } = this.props;

    return (
      <div className="Architecture-status">
        <StatusText>{status}</StatusText>
      </div>
    );
  }
}

ArchitectureStatus.propTypes = {
  status: PropTypes.string.isRequired,
};

ArchitectureStatus.defaultProps = {};

export default ArchitectureStatus;
