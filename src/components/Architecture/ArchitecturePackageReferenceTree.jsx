import React from 'react';
import { packagePropType } from 'proptypes';
import Loader from 'components/Loader/Loader.jsx';
import TaxonomyTree from 'components/TaxonomyTree/TaxonomyTree.jsx';
import usePackageReferencePackage from 'hooks/usePackageReferencePackage.jsx';

const ArchitecturePackageReferenceTree = ({ pkg }) => {
  const { loading, error, tree } = usePackageReferencePackage(pkg);

  if (loading) return <Loader size={21} isCentered />;

  if (error) return 'Error!';

  return <TaxonomyTree taxonomy={tree} />;
};

ArchitecturePackageReferenceTree.propTypes = {
  pkg: packagePropType.isRequired,
};

export default ArchitecturePackageReferenceTree;
