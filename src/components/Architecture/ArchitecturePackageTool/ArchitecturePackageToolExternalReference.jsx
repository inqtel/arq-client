import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { packagePropType } from 'proptypes';
import {
  IconPencil,
  IconCheckOutlined,
  IconPaperclip,
} from 'components/Icons/Icons.jsx';
import Popover from 'components/Popover/Popover.jsx';
import ArchitecturePackageTool from './ArchitecturePackageTool.jsx';

const ArchitecturePackageToolExternalReference = ({
  pkg,
  editable,
  actionable,
  setDocActivePackageQID,
  unsetDocActivePackageQID,
  updateDocArchitecturePackageDescription,
}) => {
  const inputRef = useRef();
  const popoverRef = useRef();

  const [active, setActive] = useState(false);
  const [value, setValue] = useState(pkg.description);

  const resetInput = () => {
    setActive(false);
    setValue(pkg.description);
  };

  const setInputActive = async () => {
    await setActive(true);
    inputRef && inputRef.current.focus();
  };

  const addProtocol = (url) => {
    if (url.length === 0) return '';

    return url.indexOf('http') === 0 ? url : `https://${value}`;
  };

  const setInputInactive = () => {
    setActive(false);

    const withProtocol = addProtocol(value);
    updateDocArchitecturePackageDescription(pkg, withProtocol);

    if (withProtocol !== value) setValue(withProtocol);
  };

  const onChange = e => setValue(e.target.value);

  const onKeyUp = (e) => {
    switch (e.key) {
      case 'Enter':
        setInputInactive();
        break;
      case 'Escape':
        popoverRef && popoverRef.current.close();
        setDocActivePackageQID(pkg.id);
        break;
      default:
        break;
    }
  };

  const onPopoverOpened = () => value === '' && !active && setInputActive();

  const onPopoverClosed = () => active && resetInput();

  return (
    <div>
      <Popover
        ref={popoverRef}
        width="md"
        leftOrRight="right"
        onOpen={onPopoverOpened}
        onClose={onPopoverClosed}
      >
        <Popover.Toggle>
          <ArchitecturePackageTool
            className="Architecture-package-tool-button--reference"
            Icon={IconPaperclip}
          />
        </Popover.Toggle>
        <Popover.Box>
          <Popover.Box.Content>
            <div className="Popover-box-external-reference">
              {active && (
                <input
                  ref={inputRef}
                  id={pkg.id}
                  type="text"
                  placeholder="https://"
                  value={value}
                  onChange={onChange}
                  onKeyUp={onKeyUp}
                  onFocus={unsetDocActivePackageQID}
                  disabled={!active || !editable}
                />
              )}
              {active
                && actionable && (
                <span
                  onClick={setInputInactive}
                  onKeyDown={() => {}}
                  role="button"
                  tabIndex={0}
                >
                  <IconCheckOutlined height={18} />
                </span>
              )}
              {!active && (
                <a target="_blank" rel="noopener noreferrer" href={value}>
                  {value}
                </a>
              )}
              {!active
                && actionable && (
                  <span
                    onClick={setInputActive}
                    onKeyDown={() => {}}
                    role="button"
                    tabIndex={0}
                  >
                    <IconPencil height={18} />
                  </span>
              )}
            </div>
          </Popover.Box.Content>
        </Popover.Box>
      </Popover>
    </div>
  );
};

ArchitecturePackageToolExternalReference.propTypes = {
  actionable: PropTypes.bool,
  editable: PropTypes.bool,
  pkg: packagePropType.isRequired,
  setDocActivePackageQID: PropTypes.func.isRequired,
  unsetDocActivePackageQID: PropTypes.func.isRequired,
  updateDocArchitecturePackageDescription: PropTypes.func.isRequired,
};

ArchitecturePackageToolExternalReference.defaultProps = {
  actionable: false,
  editable: false,
};

export default ArchitecturePackageToolExternalReference;
