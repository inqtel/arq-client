import React from 'react';
import { taxonomyPropType } from 'proptypes';
import { IconProblems } from 'components/Icons/Icons.jsx';
import Popover from 'components/Popover/Popover.jsx';
import ArchitecturePackageProblemsList from 'components/Architecture/ArchitecturePackageTool/ArchitecturePackageProblemsList.jsx';

import ArchitecturePackageTool from './ArchitecturePackageTool.jsx';

const ArchitecturePackageToolProblems = ({ taxonomy }) => (
  <div>
    <Popover width="md" leftOrRight="right">
      <Popover.Toggle>
        <ArchitecturePackageTool
          className="Architecture-package-tool-button--problems"
          Icon={IconProblems}
        />
      </Popover.Toggle>
      <Popover.Box>
        <Popover.Box.Content>
          <div className="Popover-box-problems">
            <ArchitecturePackageProblemsList id={taxonomy.id} />
          </div>
        </Popover.Box.Content>
      </Popover.Box>
    </Popover>
  </div>
);

ArchitecturePackageToolProblems.propTypes = {
  taxonomy: taxonomyPropType.isRequired,
};

ArchitecturePackageToolProblems.defaultProps = {};

export default ArchitecturePackageToolProblems;
