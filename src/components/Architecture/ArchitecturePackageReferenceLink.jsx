import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { packagePropType } from 'proptypes';
import Popover from 'components/Popover/Popover.jsx';
import Button from 'components/Button/Button.jsx';
import ArchitecturePackageReferenceTree from 'components/Architecture/ArchitecturePackageReferenceTree.jsx';
import {
  IconTrash,
  IconExternalReference,
} from 'components/Icons/Icons.jsx';

class ArchitectureReferenceLink extends Component {
  onPackageReferenceLinkDelete = () => {
    const { pkg, onPackageReferenceLinkDelete } = this.props;
    onPackageReferenceLinkDelete(pkg.id);
  }

  render() {
    const {
      pkg,
      editable,
      interactive,
      actionable,
    } = this.props;

    const hasActions = editable && actionable;

    return (
      <span className="Architecture-package-link">
        <span className="Architecture-package-link-icon">
          {(() => {
            if (interactive) {
              return (
                <Popover
                  width="auto"
                  hasActions={hasActions}
                >
                  <Popover.Toggle>
                    <IconExternalReference
                      height={9}
                      display="block"
                    />
                  </Popover.Toggle>
                  <Popover.Box>
                    <Popover.Box.Content>
                      <ArchitecturePackageReferenceTree pkg={pkg} />
                    </Popover.Box.Content>
                    {hasActions
                      && (
                        <Popover.Box.Actions>
                          <Button.Group fill>
                            <Button
                              onClick={this.onPackageReferenceLinkDelete}
                              display="block"
                            >
                              <IconTrash position="left" fill="#fff" />
                              Delete Reference
                            </Button>
                          </Button.Group>
                        </Popover.Box.Actions>
                      )
                    }
                  </Popover.Box>
                </Popover>
              );
            }

            return (
              <IconExternalReference height={9} display="block" />
            );
          })()}
        </span>
      </span>
    );
  }
}

ArchitectureReferenceLink.propTypes = {
  actionable: PropTypes.bool.isRequired,
  editable: PropTypes.bool.isRequired,
  interactive: PropTypes.bool.isRequired,
  onPackageReferenceLinkDelete: PropTypes.func.isRequired,
  pkg: packagePropType.isRequired,
};

ArchitectureReferenceLink.defaultProps = {};

export default ArchitectureReferenceLink;
