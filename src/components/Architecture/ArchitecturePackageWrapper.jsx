import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { packagePropType } from 'proptypes';
import cx from 'classnames';

const ArchitecturePackageWrapper = ({
  activeCommentThreadPackageQID,
  children,
  commentable,
  pkg,
  commentCount,
}) => {
  const [isHovered, setIsHovered] = useState(false);

  useEffect(() => {
    if (activeCommentThreadPackageQID === pkg.id) {
      setIsHovered(true);
    } else {
      setIsHovered(false);
    }
  }, [pkg, activeCommentThreadPackageQID]);

  const onMouseOver = () => {
    if (commentable) {
      setIsHovered(true);
    }
  };

  const onMouseOut = () => {
    if (commentable && pkg.id !== activeCommentThreadPackageQID) {
      setIsHovered(false);
    }
  };

  return (
    <div
      className={cx('Architecture-package-wrapper', {
        'Architecture-package-wrapper--isHovered': isHovered,
        'Architecture-package-wrapper--commentIcon': commentCount > 0,
        'Architecture-package-wrapper--referenceIcon': pkg.description,
      })}
      onMouseOver={onMouseOver}
      onFocus={onMouseOver}
      onMouseOut={onMouseOut}
      onBlur={onMouseOut}
    >
      {children}
    </div>
  );
};

ArchitecturePackageWrapper.propTypes = {
  activeCommentThreadPackageQID: PropTypes.string,
  children: PropTypes.node.isRequired,
  commentable: PropTypes.bool.isRequired,
  pkg: packagePropType.isRequired,
};

ArchitecturePackageWrapper.defaultProps = {
  activeCommentThreadPackageQID: null,
};

export default ArchitecturePackageWrapper;
