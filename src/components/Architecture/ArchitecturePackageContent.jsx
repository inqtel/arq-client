import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class ArchitecturePackageContent extends PureComponent {
  render() {
    const { children } = this.props;
    return (
      <div className="Architecture-package-content">{children}</div>
    );
  }
}

ArchitecturePackageContent.propTypes = {
  children: PropTypes.node,
};

ArchitecturePackageContent.defaultProps = {
  children: null,
};

export default ArchitecturePackageContent;
