import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { architecturePropType } from 'proptypes';
import { noop } from 'lodash';
import { IconDrawing, IconGraph } from 'components/Icons/Icons.jsx';
import DrawingToolPopup from 'components/OnboardingPopups/DrawingToolPopup.jsx';

import Toolbar from 'components/Toolbar/Toolbar.jsx';

import ArchitectureToolbarActions from './ArchitectureToolbarActions.jsx';
import ArchitectureToolbarClipboard from './ArchitectureToolbarClipboard.jsx';
import ArchitectureToolbarButton from './ArchitectureToolbarButton.jsx';
import './ArchitectureToolbar.scss';

const ArchitectureToolbar = ({
  documentQID, // @note: this is a temp fix for v1 drawing navigation
  architecture,
  clipboard,
  shared,
  publishedView,
  actionable,
  onStatusChange,
  onDelete,
  enableOnboarding,
}) => {
  const documentPath = shared ? 'shared' : 'documents';

  return (
    <div className="Architecture-toolbar">
      <Toolbar noShadow>
        {clipboard && (
          <Toolbar.Group padded>
            <div className="Toolbar-item">
              <ArchitectureToolbarClipboard architecture={architecture} />
            </div>
          </Toolbar.Group>
        )}
        {enableOnboarding && <DrawingToolPopup />}
        {/* Doc architecture version */}
        {documentQID && (
          <Fragment>
            <Toolbar.Group padded>
              <div className="Toolbar-item">
                <ArchitectureToolbarButton
                  asLink
                  linkTo={`/${documentPath}/${documentQID}/architectures/${
                    architecture.id
                  }/drawing`}
                >
                  <IconDrawing display="block" position="left" />
                  {' '}
                  View Drawing
                </ArchitectureToolbarButton>
              </div>
            </Toolbar.Group>
            <Toolbar.Group padded>
              <div className="Toolbar-item">
                <ArchitectureToolbarButton
                  asLink
                  linkTo={`/${documentPath}/${documentQID}/architectures/${
                    architecture.id
                  }/graph`}
                >
                  <IconGraph display="block" position="left" />
                  {' '}
                  View Graph
                </ArchitectureToolbarButton>
              </div>
            </Toolbar.Group>
          </Fragment>
        )}
        {/* Published architecture version */}
        {publishedView && (
          <Fragment>
            <Toolbar.Group padded>
              <div className="Toolbar-item">
                <ArchitectureToolbarButton
                  asLink
                  linkTo={`/published_architectures/${
                    architecture.id
                  }/drawing`}
                >
                  <IconDrawing display="block" position="left" />
                  {' '}
                  View Drawing
                </ArchitectureToolbarButton>
              </div>
            </Toolbar.Group>
            <Toolbar.Group padded>
              <div className="Toolbar-item">
                <ArchitectureToolbarButton
                  asLink
                  linkTo={`/published_architectures/${
                    architecture.id
                  }/graph`}
                >
                  <IconGraph display="block" position="left" />
                  {' '}
                  View Graph
                </ArchitectureToolbarButton>
              </div>
            </Toolbar.Group>
          </Fragment>
        )}
        {actionable && (
          <Toolbar.Group alignRight paddedX>
            <ArchitectureToolbarActions
              architecture={architecture}
              onStatusChange={onStatusChange}
              onDelete={onDelete}
            />
          </Toolbar.Group>
        )}
      </Toolbar>
    </div>
  );
};

ArchitectureToolbar.propTypes = {
  actionable: PropTypes.bool,
  architecture: architecturePropType.isRequired,
  clipboard: PropTypes.bool,
  documentQID: PropTypes.string,
  enableOnboarding: PropTypes.bool,
  onDelete: PropTypes.func,
  onStatusChange: PropTypes.func,
  publishedView: PropTypes.bool,
  shared: PropTypes.bool,
};

ArchitectureToolbar.defaultProps = {
  documentQID: '',
  publishedView: false,
  clipboard: false,
  actionable: false,
  shared: false,
  onStatusChange: noop,
  onDelete: noop,
  enableOnboarding: false,
};

export default ArchitectureToolbar;
