import React from 'react';
import PropTypes from 'prop-types';
import { nicetime } from 'utils/time';
import Architecture from 'components/Architecture/Architecture.jsx';
import ArchitectureToolbar from 'components/Architecture/ArchitectureToolbar/ArchitectureToolbar.jsx';
import ArchitectureStatus from 'components/Architecture/ArchitectureStatus.jsx';
import 'components/Architecture/Architecture.scss';
import { architecturePropType } from 'proptypes';

const ArchitectureContent = ({
  publishedView,
  publishedArchitecture,
}) => (
  <div className="ArchitectureContent-architecture">
    <div>
      <ArchitectureStatus
        status={`Published ${nicetime(
          publishedArchitecture.publishedAt,
        )}`}
      />
      <ArchitectureToolbar
        architecture={publishedArchitecture}
        publishedView={publishedView}
        clipboard
      />
      <Architecture
        architecture={publishedArchitecture}
        publishedView={publishedView}
        interactive
      />
    </div>
  </div>
);

ArchitectureContent.propTypes = {
  publishedArchitecture: architecturePropType.isRequired,
  publishedView: PropTypes.bool,
};

ArchitectureContent.defaultProps = {
  publishedView: false,
};

export default ArchitectureContent;
