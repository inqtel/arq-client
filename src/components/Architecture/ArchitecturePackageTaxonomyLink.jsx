import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { packagePropType, apolloErrorPropType, taxonomyPropType } from 'proptypes';
import { getTaxonomyRootAncestor } from 'utils/taxonomies';
import Popover from 'components/Popover/Popover.jsx';
import Button from 'components/Button/Button.jsx';
import Link from 'components/Link/Link.jsx';
import {
  IconWordLinked,
  IconBrokenLink,
  IconRevert,
  IconTrash,
} from 'components/Icons/Icons.jsx';
import Loader from 'components/Loader/Loader.jsx';
import TaxonomyTree from 'components/TaxonomyTree/TaxonomyTree.jsx';

const ArchitecturePackageTaxonomyLink = ({
  architectureQID,
  pkg,
  editable,
  interactive,
  actionable,
  unlinkDocArchitecturePackageTaxonomy,
  revertDocArchitectureTaxonomy,
  getTaxonomyTree,
  loading,
  error,
  taxonomy,
}) => {
  const getPopoverTaxonomyTree = useCallback(() => {
    getTaxonomyTree(pkg.taxonomy.id);
  }, [pkg, getTaxonomyTree]);

  const onPackageTaxonomyLinkRevert = () => revertDocArchitectureTaxonomy(pkg);

  const onPackageTaxonomyLinkDelete = () => unlinkDocArchitecturePackageTaxonomy(
    pkg.id, architectureQID,
  );

  const renderItem = tax => (
    <Link to={`/taxonomies/${tax.id}`}>{tax.term}</Link>
  );

  return (
    <span className="Architecture-package-link">
      <span className="Architecture-package-link-icon">
        {!interactive && (pkg.linkBroken ? (
          <IconBrokenLink height={12} display="block" position="superscript" />
        ) : (
          <IconWordLinked height={12} display="block" position="superscript" />
        ))}
        {interactive && (
          <Popover
            width="sm"
            onOpen={getPopoverTaxonomyTree}
            hasActions={editable && actionable}
            topOrBottom="top"
          >
            <Popover.Toggle>
              {pkg.linkBroken ? (
                <IconBrokenLink height={12} display="block" position="superscript" />
              ) : (
                <IconWordLinked height={12} display="block" position="superscript" />
              )}
            </Popover.Toggle>
            <Popover.Box>
              <Popover.Box.Content>
                {loading && <Loader isCentered />}
                {error && 'Error!'}
                {!loading && !error && taxonomy && (
                  <TaxonomyTree
                    taxonomy={getTaxonomyRootAncestor(taxonomy.ancestors)}
                    allTaxonomies={taxonomy.ancestors}
                    renderItem={renderItem}
                    expanded
                  />
                )}
              </Popover.Box.Content>
              {editable
                && actionable
                && taxonomy && (
                <Popover.Box.Actions>
                  {!pkg.linkBroken ? (
                    <Button.Group fill>
                      <Button
                        onClick={onPackageTaxonomyLinkDelete}
                        display="block"
                      >
                        <IconTrash position="left" fill="#fff" />
                        Delete Link
                      </Button>
                    </Button.Group>
                  ) : (
                    <Button.Group fill>
                      <Button
                        onClick={onPackageTaxonomyLinkRevert}
                        display="inlineBlock"
                      >
                        <IconRevert position="left" fill="#fff" />
                        Revert Link
                      </Button>
                      <Button
                        onClick={onPackageTaxonomyLinkDelete}
                        display="inlineBlock"
                      >
                        <IconTrash position="left" fill="#fff" />
                        Delete Link
                      </Button>
                    </Button.Group>
                  )}
                </Popover.Box.Actions>
              )}
            </Popover.Box>
          </Popover>
        )}
      </span>
    </span>
  );
};

ArchitecturePackageTaxonomyLink.propTypes = {
  architectureQID: PropTypes.string.isRequired,
  pkg: packagePropType.isRequired,
  editable: PropTypes.bool.isRequired,
  interactive: PropTypes.bool.isRequired,
  actionable: PropTypes.bool.isRequired,
  revertDocArchitectureTaxonomy: PropTypes.func.isRequired,
  unlinkDocArchitecturePackageTaxonomy: PropTypes.func.isRequired,
  getTaxonomyTree: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  error: apolloErrorPropType,
  taxonomy: taxonomyPropType,
};

ArchitecturePackageTaxonomyLink.defaultProps = {
  loading: false,
  error: null,
  taxonomy: null,
};

export default ArchitecturePackageTaxonomyLink;
