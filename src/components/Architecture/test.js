import React from 'react';
import { MockedProvider } from '@apollo/client/testing';
import { architecture } from 'mocks/architectures';
import { setUp, checkProps } from 'utils/testing';
import Architecture from 'components/Architecture/Architecture.jsx';
import ArchitectureContent from 'components/Architecture/ArchitectureContent.jsx';
import ArchitectureHeader from 'components/Architecture/ArchitectureHeader.jsx';
import ArchitectureTree from 'components/Architecture/ArchitectureTree.jsx';

describe('<Architecture />', () => {
  let wrapper;

  const expectedProps = {
    architecture,
  };

  beforeEach(() => {
    wrapper = setUp(Architecture, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(Architecture, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .Architecture class', () => {
    expect(wrapper.is('.Architecture')).toBe(true);
  });

  it('should render an ArchitectureHeader', () => {
    expect(wrapper.exists('ArchitectureHeader')).toBe(true);
  });

  it('should render an ArchitectureTree', () => {
    expect(wrapper.exists('ArchitectureTree')).toBe(true);
  });
});

describe('<ArchitectureContent />', () => {
  it('should render with .ArchitectureContent class', () => {
    const wrapper = shallow(<MockedProvider><ArchitectureContent /></MockedProvider>);
    expect(wrapper.childAt(0).is('ArchitectureContent')).toBe(true);
  });
});

describe('<ArchitectureHeader />', () => {
  let wrapper;

  const expectedProps = {
    architecture,
    editable: false,
  };

  beforeEach(() => {
    wrapper = setUp(ArchitectureHeader, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(ArchitectureHeader, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .Architecture-header class', () => {
    expect(wrapper.is('.Architecture-header')).toBe(true);
  });
});

describe('<ArchitectureTree />', () => {
  let wrapper;

  const expectedProps = {
    architecture,
    editable: false,
    actionable: false,
    commentable: false,
    interactive: false,
  };

  beforeEach(() => {
    wrapper = setUp(ArchitectureTree, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(ArchitectureTree, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .Architecture-Tree class', () => {
    expect(wrapper.is('.Architecture-tree')).toBe(true);
  });
});
