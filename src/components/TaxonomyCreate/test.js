import { setUpMount, checkProps } from 'utils/testing';
import TaxonomyCreate from 'components/TaxonomyCreate/TaxonomyCreate.jsx';

describe('<TaxonomyCreate />', () => {
  let wrapper;

  const onApprove = jest.fn();

  const expectedProps = { onApprove };

  beforeEach(() => {
    wrapper = setUpMount(TaxonomyCreate, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(TaxonomyCreate, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .TaxonomyCreate class', () => {
    expect(wrapper.childAt(0).is('.TaxonomyCreate')).toBe(true);
  });

  it('should call onApprove when submit clicked', () => {
    wrapper.find('Textbox').simulate('change', { target: { value: 'Test' } });
    wrapper.find('Button').simulate('click');
    expect(onApprove).toHaveBeenCalled();
  });
});
