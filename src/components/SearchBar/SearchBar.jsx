import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { IconSearch, IconLoader } from 'components/Icons/Icons.jsx';
import SearchPopup from 'components/OnboardingPopups/SearchPopup.jsx';
import 'components/SearchBar/SearchBar.scss';

class SearchBar extends Component {
  componentDidMount() {
    const { autoFocus } = this.props;
    autoFocus && this.$input.focus();
  }

  render() {
    const {
      appearance,
      onChange,
      searching,
      readOnly,
      value,
      placeholder,
      enableOnboarding,
      hideIcon,
    } = this.props;

    return (
      <div
        className={cx('SearchBar', {
          [`SearchBar--appearance-${appearance}`]: appearance,
          'SearchBar--hideIcon': hideIcon,
        })}
      >
        {enableOnboarding && <SearchPopup />}
        <input
          className="SearchBar-input"
          ref={(e) => { this.$input = e; }}
          type="text"
          placeholder={placeholder}
          onChange={onChange}
          readOnly={readOnly}
          value={value}
        />
        {!hideIcon && (
          <span className="SearchBar-icon">
            {searching ? (
              <IconLoader />
            ) : (
              <IconSearch display="block" />
            )}
          </span>
        )}
      </div>
    );
  }
}

SearchBar.propTypes = {
  appearance: PropTypes.string,
  autoFocus: PropTypes.bool,
  enableOnboarding: PropTypes.bool,
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool,
  searching: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  hideIcon: PropTypes.bool,
};

SearchBar.defaultProps = {
  appearance: null,
  autoFocus: false,
  enableOnboarding: false,
  placeholder: 'Search',
  readOnly: false,
  searching: false,
  value: undefined,
  hideIcon: false,
};

export default SearchBar;
