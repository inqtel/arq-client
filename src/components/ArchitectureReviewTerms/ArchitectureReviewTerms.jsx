import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import ArchitectureReviewTerm from 'components/ArchitectureReviewTerms/ArchitectureReviewTerm.jsx';
import 'components/ArchitectureReviewTerms/ArchitectureReviewTerms.scss';

const ArchitectureReviewTerms = ({
  terms,
  searches,
  approveTerm,
  rejectTerm,
  unrejectTerm,
  searchForParentTerm,
  insertTermBelowTaxonomy,
  removeTermFromTaxonomy,
  linkTermToTaxonomy,
  unlinkTermFromTaxonomy,
  highlightPackage,
}) => {
  const [expandedTermID, setExpandedTermID] = useState(null);

  const onTermExpand = id => setExpandedTermID(id);

  const onTermCollapse = () => setExpandedTermID(null);

  return (
    <div className="ArchitectureReviewTerms">
      {terms.byID.map(id => (
        <ArchitectureReviewTerm
          key={id}
          termEntity={terms.entities[id]}
          searches={searches}
          onApprove={approveTerm}
          onReject={rejectTerm}
          onUnreject={unrejectTerm}
          onSearch={searchForParentTerm}
          onInsertTermBelowTaxonomy={insertTermBelowTaxonomy}
          onRemoveTermFromTaxonomy={removeTermFromTaxonomy}
          onLinkTermToTaxonomy={linkTermToTaxonomy}
          onUnlinkTermFromTaxonomy={unlinkTermFromTaxonomy}
          onTermExpand={onTermExpand}
          onTermCollapse={onTermCollapse}
          expandedTermID={expandedTermID}
          highlightPackage={highlightPackage}
        />
      ))}
    </div>
  );
};

ArchitectureReviewTerms.propTypes = {
  terms: PropTypes.shape({
    byID: PropTypes.arrayOf(PropTypes.string),
    entities: PropTypes.shape({}),
  }),
  searches: PropTypes.shape({}),
  approveTerm: PropTypes.func,
  rejectTerm: PropTypes.func,
  unrejectTerm: PropTypes.func,
  searchForParentTerm: PropTypes.func,
  insertTermBelowTaxonomy: PropTypes.func,
  removeTermFromTaxonomy: PropTypes.func,
  linkTermToTaxonomy: PropTypes.func,
  unlinkTermFromTaxonomy: PropTypes.func,
  highlightPackage: PropTypes.func,
};

ArchitectureReviewTerms.defaultProps = {
  terms: {
    byID: [],
    entities: {},
  },
  searches: {},
  approveTerm: noop,
  rejectTerm: noop,
  unrejectTerm: noop,
  searchForParentTerm: noop,
  insertTermBelowTaxonomy: noop,
  removeTermFromTaxonomy: noop,
  linkTermToTaxonomy: noop,
  unlinkTermFromTaxonomy: noop,
  highlightPackage: noop,
};

export default ArchitectureReviewTerms;
