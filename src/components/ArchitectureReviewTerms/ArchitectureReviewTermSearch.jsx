import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { searchStorePropType } from 'proptypes';
import ArchitectureReviewTermSearchResults from 'components/ArchitectureReviewTerms/ArchitectureReviewTermSearchResults.jsx';
import Button from 'components/Button/Button.jsx';
import { IconClose } from 'components/Icons/Icons.jsx';
import SearchBar from 'components/SearchBar/SearchBar.jsx';
import TaxonomyCreate from 'components/TaxonomyCreate/TaxonomyCreate.jsx';

const ArchitectureReviewTermSearch = ({
  id,
  onApprove,
  search,
  onSearch,
  closeSearch,
  onInsertTermBelowTaxonomy,
  onRemoveTermFromTaxonomy,
  onLinkTermToTaxonomy,
  onUnlinkTermFromTaxonomy,
}) => {
  const [searchPanel, setSearchPanel] = useState(false);

  const toggleSearchHiearchyState = (e) => {
    e.preventDefault();
    setSearchPanel(!searchPanel);
  };

  const hierarchyInputSubmit = (tree) => {
    onApprove(id, tree);
    closeSearch();
  };

  const { results } = search;

  return (
    <div className="ArchitectureReviewTerm-search">
      <div className="ArchitectureReviewTerm-search-header">
        <h4 className="ArchitectureReviewTerm-search-header-title">
          {searchPanel
            ? 'Choose a parent term'
            : 'Add a new parent term'}
        </h4>
        <Button unstyled onClick={closeSearch}>
          <IconClose display="block" height={12} />
        </Button>
      </div>
      {!searchPanel && (
        <div>
          <div className="ArchitectureReviewTerm-search-input">
            <TaxonomyCreate onApprove={hierarchyInputSubmit} />
          </div>
          <span className="ArchitectureReviewTerm-search-helper">
            {'Create a hiearchy with a ">". '}
            <Button unstyled onClick={toggleSearchHiearchyState}>
              Click here to search instead
            </Button>
          </span>
        </div>
      )}
      {searchPanel && (
        <div>
          <div className="ArchitectureReviewTerm-search-input">
            <SearchBar
              appearance="secondary"
              onChange={onSearch}
              readOnly={search.loading}
              searching={search.loading}
            />
          </div>
          <span className="ArchitectureReviewTerm-search-helper">
            {"Can't find your term? "}
            <Button unstyled onClick={toggleSearchHiearchyState}>
              Click here to add
            </Button>
          </span>
        </div>
      )}
      {searchPanel
        && results && (
        <ArchitectureReviewTermSearchResults
          results={results}
          onInsertTermBelowTaxonomy={onInsertTermBelowTaxonomy}
          onRemoveTermFromTaxonomy={onRemoveTermFromTaxonomy}
          onLinkTermToTaxonomy={onLinkTermToTaxonomy}
          onUnlinkTermFromTaxonomy={onUnlinkTermFromTaxonomy}
        />
      )}
    </div>
  );
};

ArchitectureReviewTermSearch.propTypes = {
  id: PropTypes.string.isRequired,
  search: searchStorePropType.isRequired,
  onSearch: PropTypes.func.isRequired,
  closeSearch: PropTypes.func.isRequired,
  onApprove: PropTypes.func.isRequired,
  onInsertTermBelowTaxonomy: PropTypes.func.isRequired,
  onRemoveTermFromTaxonomy: PropTypes.func.isRequired,
  onLinkTermToTaxonomy: PropTypes.func.isRequired,
  onUnlinkTermFromTaxonomy: PropTypes.func.isRequired,
};

ArchitectureReviewTermSearch.defaultProps = {};

export default ArchitectureReviewTermSearch;
