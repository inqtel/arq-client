import React from 'react';
import ArchitectureReviewTerms from 'components/ArchitectureReviewTerms/ArchitectureReviewTerms.jsx';

const terms = {
  byID: [
    'a07f73ca-3aa1-423f-b9d5-1b4c8b223d5b',
    '25f57435-bacb-472a-b86a-b71795994962',
    'b7428378-8701-45af-b045-54186786a4da',
  ],
  entities: {
    'a07f73ca-3aa1-423f-b9d5-1b4c8b223d5b': {
      packageID: 'a07f73ca-3aa1-423f-b9d5-1b4c8b223d5b',
      term: 'One',
    },
    '25f57435-bacb-472a-b86a-b71795994962': {
      packageID: '25f57435-bacb-472a-b86a-b71795994962',
      term: 'Two',
    },
    'b7428378-8701-45af-b045-54186786a4da': {
      packageID: 'b7428378-8701-45af-b045-54186786a4da',
      term: 'Three',
    },
  },
};

const searches = {
  byID: [
    'a07f73ca-3aa1-423f-b9d5-1b4c8b223d5b',
    '25f57435-bacb-472a-b86a-b71795994962',
    'b7428378-8701-45af-b045-54186786a4da',
  ],
  entities: {
    'a07f73ca-3aa1-423f-b9d5-1b4c8b223d5b': {
      loading: false,
      error: null,
      results: null,
    },
    '25f57435-bacb-472a-b86a-b71795994962': {
      loading: false,
      error: null,
      results: null,
    },
    'b7428378-8701-45af-b045-54186786a4da': {
      loading: false,
      error: null,
      results: null,
    },
  },
};

describe('<ArchitectureReviewTerms />', () => {
  it('should render with .ArchitectureReviewTerms class', () => {
    const wrapper = shallow(<ArchitectureReviewTerms />);
    expect(wrapper.is('.ArchitectureReviewTerms')).toBe(true);
  });

  it('should set expanded term', () => {
    const wrapper = mount(<ArchitectureReviewTerms terms={terms} searches={searches} />);

    const actions = wrapper.find('.ArchitectureReviewTerm-actions').first();
    actions.find('button').first().simulate('click');

    const term = wrapper.find('ArchitectureReviewTerm').first();

    expect(term.props().expandedTermID).toBe(terms.entities[terms.byID[0]].packageID);
  });
});
