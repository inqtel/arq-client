import React from 'react';
import PropTypes from 'prop-types';

const PopoverBoxContent = ({ children }) => <div className="Popover-box-content">{children}</div>;

PopoverBoxContent.propTypes = {
  children: PropTypes.node,
};

PopoverBoxContent.defaultProps = {
  children: null,
};

export default PopoverBoxContent;
