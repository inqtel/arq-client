import React, { PureComponent } from 'react';

class PopoverToggle extends PureComponent {
  render() {
    const { children, onToggle } = this.props;

    return (
      <div className="Popover-toggle" onClick={onToggle}>
        {children}
      </div>
    );
  }
}

PopoverToggle.propTypes = {};

PopoverToggle.defaultProps = {};

export default PopoverToggle;
