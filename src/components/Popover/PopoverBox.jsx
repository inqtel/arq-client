import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

class PopoverBox extends PureComponent {
  constructor(props) {
    super(props);

    const { topOrBottom, leftOrRight } = this.props;

    this.state = {
      topOrBottom,
      leftOrRight,
    };
  }

  componentDidMount() {
    const { topOrBottom, leftOrRight } = this.state;

    const rect = this.$popoverbox.getBoundingClientRect();

    if (!topOrBottom) {
      if (rect.top - rect.height < 300) {
        this.setState({ topOrBottom: 'bottom' });
      } else {
        this.setState({ topOrBottom: 'top' });
      }
    }

    if (!leftOrRight) {
      if (rect.left + rect.width > window.innerWidth) {
        this.setState({ leftOrRight: 'right' });
      } else {
        this.setState({ leftOrRight: 'left' });
      }
    }
  }

  render() {
    const { width, children } = this.props;

    const { topOrBottom, leftOrRight } = this.state;

    return (
      <div
        ref={(c) => { this.$popoverbox = c; }}
        className={cx('Popover-box', `Popover-box--width-${width}`, {
          [`Popover-box--position-${leftOrRight}`]: leftOrRight,
          [`Popover-box--position-${topOrBottom}`]: topOrBottom,
          'Popover-box--isVisible': topOrBottom,
        })}
      >
        {children}
      </div>
    );
  }
}

PopoverBox.propTypes = {
  width: PropTypes.string,
  leftOrRight: PropTypes.string,
  topOrBottom: PropTypes.string,
};

PopoverBox.defaultProps = {
  width: 'sm',
  leftOrRight: null,
  topOrBottom: null,
};

export default PopoverBox;
