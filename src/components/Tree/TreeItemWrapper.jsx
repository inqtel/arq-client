import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType } from 'proptypes';
import TreeItemTools from 'components/Tree/TreeItemTools.jsx';
import cx from 'classnames';

const TreeItemWrapper = ({
  item,
  selected,
  onSelect,
  moveTaxonomy,
  children,
  tools,
}) => {
  if (!onSelect) return <>{children}</>;

  const [isHovered, setIsHovered] = useState(false);

  useEffect(() => {
    if (selected && selected.id === item.id) {
      setIsHovered(true);
    } else {
      setIsHovered(false);
    }
  }, [item, selected]);

  const onMouseOver = () => setIsHovered(true);

  const onMouseOut = () => {
    if (!selected || item.id !== selected.id) {
      setIsHovered(false);
    }
  };

  return (
    <div
      className={cx('Tree-item-wrapper', {
        'Tree-item-wrapper--isHovered': isHovered,
        'Tree-item-wrapper--isOutlined': moveTaxonomy && moveTaxonomy.id === item.id,
      })}
      onClick={() => onSelect(item)}
      onMouseOver={onMouseOver}
      onFocus={onMouseOver}
      onMouseOut={onMouseOut}
      onBlur={onMouseOut}
      role="presentation"
    >
      {children}
      {tools && (
        <TreeItemTools
          tools={tools}
          item={item}
        />
      )}
    </div>
  );
};

TreeItemWrapper.propTypes = {
  item: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
  selected: taxonomyPropType,
  onSelect: PropTypes.func,
  children: PropTypes.node.isRequired,
  tools: PropTypes.arrayOf(
    PropTypes.shape({
      renderTool: PropTypes.func.isRequired,
      key: PropTypes.string.isRequired,
    }),
  ),
  moveTaxonomy: taxonomyPropType,
};

TreeItemWrapper.defaultProps = {
  selected: null,
  onSelect: null,
  tools: null,
  moveTaxonomy: null,
};

export default TreeItemWrapper;
