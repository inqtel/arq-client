/* eslint-disable import/no-cycle */
import React from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType } from 'proptypes';
import cx from 'classnames';
import TreeItem from 'components/Tree/TreeItem.jsx';
import 'components/Tree/Tree.scss';

const Tree = ({
  actionFlags,
  actions,
  bulletFill,
  depth,
  item,
  renderItem,
  renderBadges,
  size,
  spacing,
  expanded,
  selected,
  onSelect,
  moveTaxonomy,
  deleteTaxonomy,
  deleteTaxonomies,
  tools,
  highlightItem,
  allItems,
}) => (
  <ul className={cx('Tree',
    `Tree--depth-${depth}`,
    `Tree--size-${size}`,
    `Tree--spacing-${spacing}`)}
  >
    <TreeItem
      item={item}
      renderItem={renderItem}
      renderBadges={renderBadges}
      depth={depth}
      actions={actions}
      actionFlags={actionFlags}
      bulletFill={bulletFill}
      expanded={expanded}
      selected={selected}
      onSelect={onSelect}
      moveTaxonomy={moveTaxonomy}
      deleteTaxonomy={deleteTaxonomy}
      deleteTaxonomies={deleteTaxonomies}
      tools={tools}
      highlightItem={highlightItem}
      allItems={allItems}
    />
  </ul>
);

Tree.propTypes = {
  actionFlags: PropTypes.arrayOf(PropTypes.string),
  actions: PropTypes.shape({ primary: PropTypes.arrayOf(PropTypes.shape({})) }),
  bulletFill: PropTypes.string,
  depth: PropTypes.number,
  item: taxonomyPropType.isRequired,
  renderItem: PropTypes.func,
  renderBadges: PropTypes.func,
  size: PropTypes.string,
  spacing: PropTypes.string,
  expanded: PropTypes.bool,
  selected: taxonomyPropType,
  onSelect: PropTypes.func,
  moveTaxonomy: taxonomyPropType,
  deleteTaxonomy: taxonomyPropType,
  deleteTaxonomies: PropTypes.arrayOf(PropTypes.string),
  tools: PropTypes.arrayOf(PropTypes.shape({})),
  highlightItem: PropTypes.func,
  allItems: PropTypes.arrayOf(taxonomyPropType),
};

Tree.defaultProps = {
  actions: null,
  actionFlags: [],
  bulletFill: null,
  depth: 0,
  renderItem: item => item.term,
  renderBadges: null,
  size: 'xxs',
  spacing: null,
  expanded: false,
  selected: null,
  onSelect: null,
  moveTaxonomy: null,
  deleteTaxonomy: null,
  deleteTaxonomies: [],
  tools: null,
  highlightItem: null,
  allItems: null,
};

export default Tree;
