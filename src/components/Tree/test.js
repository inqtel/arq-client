import React from 'react';
import { taxonomy } from 'mocks/taxonomy';
import { treeActions } from 'mocks/treeActions';
import Tree from 'components/Tree/Tree.jsx';

describe('<Tree />', () => {
  it('should render with .Tree class', () => {
    const wrapper = shallow(
      <Tree item={taxonomy} />,
    );

    expect(wrapper.is('.Tree')).toBe(true);
  });

  it('should render actions when passed in', () => {
    const wrapper = mount(
      <Tree item={taxonomy} actions={treeActions} />,
    );

    expect(wrapper.exists('.Tree-item-actions')).toEqual(true);
  });
});
