import React from 'react';
import { problemPropType } from 'proptypes';
import Card from 'components/Card/Card.jsx';
import Problem from 'components/Problem/Problem.jsx';
import 'components/ProblemCard/ProblemCard.scss';

const ProblemCard = ({ problem }) => (
  <Card url={`/problems/${problem.id}`} className="ProblemCard">
    <Problem
      problem={problem}
      customer={problem.customer}
      size="sm"
    />
    <div className="ProblemCard-links">
      {`Capabilities: ${problem.capabilities.length}`}
    </div>
  </Card>
);

ProblemCard.propTypes = {
  problem: problemPropType.isRequired,
};

export default ProblemCard;
