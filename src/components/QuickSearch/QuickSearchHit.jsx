import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { esSearchResultPropType } from 'proptypes';

const QuickSearchHit = ({ onSelect, hit: { _source: item } }) => (
  <div
    className={cx(
      'QuickSearch-hit',
      { 'QuickSearch-hit--disabled': !item.logo },
    )}
    onClick={() => onSelect(item)}
    onKeyDown={() => {}}
    role="button"
    tabIndex={0}
  >
    <div className="QuickSearch-hit-item">
      <div className="QuickSearch-hit-item-logo">
        {(item.logo) ? (
          <img
            src={item.logo}
            alt={item.name}
          />
        ) : (
          <div className="QuickSearch-hit-item-logo-placeholder" />
        )}
      </div>
      {item.name}
    </div>
  </div>
);

QuickSearchHit.propTypes = {
  hit: esSearchResultPropType.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default QuickSearchHit;
