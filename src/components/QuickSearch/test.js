import { setUp, checkProps } from 'utils/testing';
import { elasticResultCompanies } from 'mocks/elastic';
import QuickSearch from 'components/QuickSearch/QuickSearch.jsx';

describe('<QuickSearch />', () => {
  let wrapper;

  const expectedProps = {
    onSelect: () => {},
    onCancel: () => {},
    onSearch: () => {},
    hits: elasticResultCompanies.hits.hits,
    searching: false,
  };

  beforeEach(() => {
    wrapper = setUp(QuickSearch, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(QuickSearch, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .QuickSearch class', () => {
    expect(wrapper.is('.QuickSearch')).toBe(true);
  });
});
