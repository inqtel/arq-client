import React from 'react';
import PropTypes from 'prop-types';
import QuickSearch from 'components/QuickSearch/QuickSearch.jsx';
import useSearch from 'hooks/useSearch.jsx';
import 'components/QuickSearch/QuickSearch.scss';

const QuickSearchInterface = ({
  onCancel,
  onSelect,
}) => {
  const {
    onSearch,
    hits,
    searching,
  } = useSearch('organizations', 'name');

  return (
    <QuickSearch
      onSearch={onSearch}
      searching={searching}
      hits={hits}
      onSelect={onSelect}
      onCancel={onCancel}
    />
  );
};

QuickSearchInterface.propTypes = {
  onCancel: PropTypes.func.isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default QuickSearchInterface;
