import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button/Button.jsx';
import { IconExternalReference } from 'components/Icons/Icons.jsx';
import Popover from 'components/Popover/Popover.jsx';

const EditorLink = ({
  href,
  children,
}) => (
  <span className="EditorLink">
    <Popover
      topOrBottom="top"
      leftOrRight="right"
      hasActions
      width="auto"
      inline
    >
      <Popover.Toggle>{children}</Popover.Toggle>
      <Popover.Box>
        <Popover.Box.Actions>
          <Button onClick={() => window.open(href, '_blank')} display="block">
            Open
            <IconExternalReference
              height={12}
              fill="#fff"
              position="right"
            />
          </Button>
        </Popover.Box.Actions>
      </Popover.Box>
    </Popover>
  </span>
);

EditorLink.propTypes = {
  children: PropTypes.node,
  href: PropTypes.string,
};

EditorLink.defaultProps = {
  children: null,
  href: '',
};

export default EditorLink;
