import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { RichUtils } from 'draft-js';
import DraftEditor from 'draft-js-plugins-editor';
import createLinkifyPlugin from 'draft-js-linkify-plugin';
import { noop } from 'lodash';
import cx from 'classnames';
import EditorLink from 'components/Editor/EditorLink.jsx';
import 'components/Editor/Editor.scss';

const linkifyPlugin = createLinkifyPlugin({
  target: '_blank',
  component: EditorLink,
});

class Editor extends Component {
  onFocus = () => {
    const { qid, onFocus } = this.props;

    onFocus(qid);
  }

  onBlur = () => {
    const { qid, onBlur } = this.props;

    onBlur(qid);
  }

  onChange = (editorState) => {
    const { qid, onChange } = this.props;

    onChange(qid, editorState);
  }

  handleKeyCommand = (command) => {
    const { editorState } = this.props;
    const newEditorState = RichUtils.handleKeyCommand(editorState, command);

    if (newEditorState) {
      this.onChange(newEditorState);
    }
  }

  onTab = (e) => {
    const { editorState } = this.props;
    const maxDepth = 8;

    this.onChange(RichUtils.onTab(e, editorState, maxDepth));
  }

  render() {
    const { editorState, placeholder, readOnly } = this.props;

    const contentState = editorState.getCurrentContent();
    let hidePlaceholder = false;

    if (!contentState.hasText()) {
      if (
        contentState
          .getBlockMap()
          .first()
          .getType() !== 'unstyled'
      ) {
        hidePlaceholder = true;
      }
    }

    return (
      <div
        className={cx('Editor', {
          'Editor--hidePlaceholder': placeholder.length === 0 || hidePlaceholder,
        })}
      >
        <DraftEditor
          editorState={editorState}
          onChange={this.onChange}
          onFocus={this.onFocus}
          onBlur={this.onBlur}
          handleKeyCommand={this.handleKeyCommand}
          onTab={this.onTab}
          placeholder={placeholder}
          readOnly={readOnly}
          plugins={[linkifyPlugin]}
        />
      </div>
    );
  }
}

Editor.propTypes = {
  qid: PropTypes.string.isRequired,
  editorState: PropTypes.object.isRequired,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  readOnly: PropTypes.bool,
};

Editor.defaultProps = {
  onFocus: noop,
  onBlur: noop,
  onChange: noop,
  placeholder: '',
  readOnly: false,
};

export default Editor;
