import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import ToolbarGroup from 'components/Toolbar/ToolbarGroup.jsx';
import ToolbarItem from 'components/Toolbar/ToolbarItem.jsx';
import 'components/Toolbar/Toolbar.scss';

class Toolbar extends PureComponent {
  render() {
    const {
      children, fill, padded, noShadow,
    } = this.props;

    return (
      <div
        className={cx('Toolbar', {
          'Toolbar--fill': fill,
          'Toolbar--padded': padded,
          'Toolbar--noShadow': noShadow,
        })}
      >
        {children}
      </div>
    );
  }
}

Toolbar.propTypes = {
  children: PropTypes.node,
  fill: PropTypes.bool,
  padded: PropTypes.bool,
  noShadow: PropTypes.bool,
};

Toolbar.defaultProps = {
  children: null,
  fill: false,
  padded: false,
  noShadow: false,
};

Toolbar.Group = ToolbarGroup;
Toolbar.Item = ToolbarItem;

export default Toolbar;
