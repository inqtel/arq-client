import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class ToolbarItem extends PureComponent {
  render() {
    const { children } = this.props;

    return <div className="Toolbar-item">{children}</div>;
  }
}

ToolbarItem.propTypes = {
  children: PropTypes.node,
};

ToolbarItem.defaultProps = {
  children: null,
};

export default ToolbarItem;
