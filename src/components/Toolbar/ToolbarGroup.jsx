import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

class ToolbarGroup extends PureComponent {
  render() {
    const {
      children, fill, padded, paddedX, alignRight,
    } = this.props;

    return (
      <div
        className={cx('Toolbar-group', {
          'Toolbar-group--fill': fill,
          'Toolbar-group--padded': padded,
          'Toolbar-group--paddedX': paddedX,
          'Toolbar-group--alignRight': alignRight,
        })}
      >
        {children}
      </div>
    );
  }
}

ToolbarGroup.propTypes = {
  children: PropTypes.node,
  fill: PropTypes.bool,
  padded: PropTypes.bool,
  paddedX: PropTypes.bool,
  alignRight: PropTypes.bool,
};

ToolbarGroup.defaultProps = {
  children: null,
  fill: false,
  padded: false,
  paddedX: false,
  alignRight: false,
};

export default ToolbarGroup;
