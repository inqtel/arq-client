import { setUp, checkProps } from 'utils/testing';
import EntityError from 'components/EntityError/EntityError.jsx';

describe('<EntityError />', () => {
  let wrapper;

  const expectedProps = {
    error: {},
    entityName: 'Entity',
  };

  beforeEach(() => {
    wrapper = setUp(EntityError, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(EntityError, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .EntityError class', () => {
    expect(wrapper.is('.EntityError')).toBe(true);
  });
});
