import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button/Button.jsx';
import { IconClose } from 'components/Icons/Icons.jsx';
import CommentThread from 'components/CommentThread/CommentThread.jsx';

class SharedDocumentCommentThread extends PureComponent {
  addSharedDocPackageComment = (text) => {
    const { user, activeComments, addSharedDocPackageComment } = this.props;

    addSharedDocPackageComment(
      user,
      text,
      activeComments.packageQID,
    );
  }

  deleteSharedDocPackageComment = (commentQID) => {
    const { activeComments, deleteSharedDocPackageComment } = this.props;

    deleteSharedDocPackageComment(
      activeComments.packageQID,
      commentQID,
    );
  }

  render() {
    const { user, activeCommentsThread, onDismiss } = this.props;

    return (
      <div className="SharedDocument-commentThread">
        <div className="SharedDocument-commentThread-dismiss">
          <Button unstyled onClick={onDismiss}>
            Dismiss
            <IconClose position="right" height={12} />
          </Button>
        </div>
        <div className="SharedDocument-commentThread-comments">
          <CommentThread
            user={user}
            comments={activeCommentsThread.comments}
            onAddNewComment={this.addSharedDocPackageComment}
            onDeleteComment={this.deleteSharedDocPackageComment}
            shared
          />
        </div>
      </div>
    );
  }
}

SharedDocumentCommentThread.propTypes = {
  user: PropTypes.object.isRequired,
  activeComments: PropTypes.object.isRequired,
  activeCommentsThread: PropTypes.object.isRequired,
  addSharedDocPackageComment: PropTypes.func.isRequired,
  deleteSharedDocPackageComment: PropTypes.func.isRequired,
  onDismiss: PropTypes.func.isRequired,
};

SharedDocumentCommentThread.defaultProps = {};

export default SharedDocumentCommentThread;
