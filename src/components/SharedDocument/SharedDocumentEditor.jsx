import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Editor from 'components/Editor/Editor.jsx';

class SharedDocumentArchitecture extends PureComponent {
  render() {
    const { editor } = this.props;

    return (
      <Editor
        qid={editor.id}
        editorState={editor.editorState}
        readOnly
      />
    );
  }
}

SharedDocumentArchitecture.propTypes = {
  editor: PropTypes.object.isRequired,
};

SharedDocumentArchitecture.defaultProps = {};

export default SharedDocumentArchitecture;
