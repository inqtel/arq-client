import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class SharedDocumentStatus extends PureComponent {
  render() {
    const { status } = this.props;

    return (
      <div className="SharedDocument-status">
        <span className="SharedDocument-status-text">{status}</span>
      </div>
    );
  }
}

SharedDocumentStatus.propTypes = {
  status: PropTypes.string.isRequired,
};

SharedDocumentStatus.defaultProps = {};

export default SharedDocumentStatus;
