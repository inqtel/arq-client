import React from 'react';
import PropTypes from 'prop-types';
import { architecturePropType } from 'proptypes';
import Architecture from 'components/Architecture/Architecture.jsx';
import ArchitectureToolbar from 'components/Architecture/ArchitectureToolbar/ArchitectureToolbar.jsx';
import SharedDocumentArchitecturePackageInterface from 'components/SharedDocument/SharedDocumentArchitecturePackageInterface.jsx';

const SharedDocumentArchitecture = ({
  architecture,
  documentQID,
  publishedView,
  setSharedDocActiveComments,
  activeCommentsPackageQID,
}) => (
  <div className="SharedDocument-architecture">
    <ArchitectureToolbar
      documentQID={documentQID}
      architecture={architecture}
      clipboard
      publishedView={publishedView}
      shared
    />
    <Architecture
      architecture={architecture}
      interactive
      commentable
      shared
      activeCommentThreadPackageQID={activeCommentsPackageQID}
      onPackageToggleComments={setSharedDocActiveComments}
      publishedView={publishedView}
      renderPackageInterface={props => <SharedDocumentArchitecturePackageInterface {...props} />}
    />
  </div>
);

SharedDocumentArchitecture.propTypes = {
  documentQID: PropTypes.string.isRequired,
  architecture: architecturePropType.isRequired,
  setSharedDocActiveComments: PropTypes.func.isRequired,
  activeCommentsPackageQID: PropTypes.string,
  publishedView: PropTypes.bool,
};

SharedDocumentArchitecture.defaultProps = {
  activeCommentsPackageQID: null,
  publishedView: false,
};

export default SharedDocumentArchitecture;
