import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

class SharedDocumentBlock extends PureComponent {
  render() {
    const { children } = this.props;

    return <div className="SharedDocument-block">{children}</div>;
  }
}

SharedDocumentBlock.propTypes = {
  children: PropTypes.node,
};

SharedDocumentBlock.defaultProps = {
  children: null,
};

export default SharedDocumentBlock;
