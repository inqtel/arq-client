import { setUp, checkProps } from 'utils/testing';
import DocumentsLibrary from 'components/DocumentsLibrary/DocumentsLibrary.jsx';

describe('<DocumentsLibrary />', () => {
  let wrapper;

  const expectedProps = {
    docs: [],
    active: 'home',
    refetch: () => {},
  };

  beforeEach(() => {
    wrapper = setUp(DocumentsLibrary, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(DocumentsLibrary, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .DocumentsLibrary class', () => {
    expect(wrapper.is('.DocumentsLibrary')).toBe(true);
  });
});
