import React from 'react';
import PropTypes from 'prop-types';
import { apolloErrorPropType, viewportPropType } from 'proptypes';
import { DEFAULT_DOC, SHARED_DOC } from 'constants/doctypes.js';
import WelcomeBackInterface from 'components/WelcomeBack/WelcomeBackInterface.jsx';
import SidebarNavigationInterface from 'components/SidebarNavigation/SidebarNavigationInterface.jsx';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import DocumentsLibraryCards from 'components/DocumentsLibrary/DocumentsLibraryCards.jsx';
import DocumentsLibraryEmptyState from 'components/DocumentsLibrary/DocumentsLibraryEmptyState.jsx';
import Grid from 'components/Grid/Grid.jsx';
import Button from 'components/Button/Button.jsx';
import Loader from 'components/Loader/Loader.jsx';
import Alert from 'components/Alert/Alert.jsx';
import Heading from 'components/Heading/Heading.jsx';
import { IconPlus } from 'components/Icons/Icons.jsx';
import 'components/DocumentsLibrary/DocumentsLibrary.scss';

const DocumentsLibrary = ({
  active,
  doctype,
  archiveDocument,
  createDoc,
  docs,
  error,
  loading,
  refetch,
  viewport,
}) => (
  <div className="DocumentsLibrary">
    <Dashboard>
      <Dashboard.Header />
      <Dashboard.Section fit>
        <Dashboard.Column padded scroll minWidth="sm" size="sm" theme="dark">
          <Grid direction="column" justify="spaceBetween">
            <SidebarNavigationInterface active={active} />
            <Dashboard.Footer>
              <WelcomeBackInterface />
            </Dashboard.Footer>
          </Grid>
        </Dashboard.Column>
        <Dashboard.Column fit theme="light">
          {active === 'home' && (
            <Dashboard.Toolbar padded>
              <Grid wrap="nowrap" align="center" justify="spaceBetween">
                <Grid.Cell>{/* library navigation */}</Grid.Cell>
                <Grid.Cell>
                  <Button onClick={createDoc}>
                    New
                    <IconPlus position="right" height={14} fill="#fff" />
                  </Button>
                </Grid.Cell>
              </Grid>
            </Dashboard.Toolbar>
          )}
          <Dashboard.Content padding={['top', 'right', 'bottom', 'left']}>
            {error && (
              <Alert
                heading="Uh Oh..."
                subheading="There was an error fetching your documents."
              />
            )}
            {!error && loading && <Loader size={36} isCentered isHeight100 />}
            {!error && !loading && docs.length === 0
              && active === 'home' && (
              <DocumentsLibraryEmptyState />
            )}
            {!error && !loading && docs.length === 0
              && active === 'shared' && (
              <Heading level={2}>No Shared Documents</Heading>
            )}
            {!error && !loading && docs.length > 0 && (
              <DocumentsLibraryCards
                docs={docs}
                doctype={doctype}
                archiveDoc={archiveDocument}
                refetch={refetch}
                column={viewport.desktopSm}
              />
            )}
          </Dashboard.Content>
        </Dashboard.Column>
      </Dashboard.Section>
    </Dashboard>
  </div>
);

DocumentsLibrary.propTypes = {
  active: PropTypes.string.isRequired,
  doctype: PropTypes.oneOf([DEFAULT_DOC, SHARED_DOC]),
  archiveDocument: PropTypes.func,
  createDoc: PropTypes.func,
  docs: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  error: apolloErrorPropType,
  loading: PropTypes.bool,
  refetch: PropTypes.func.isRequired,
  viewport: viewportPropType,
};

DocumentsLibrary.defaultProps = {
  doctype: DEFAULT_DOC,
  archiveDocument: null,
  createDoc: null,
  error: null,
  loading: false,
  viewport: {},
};

export default DocumentsLibrary;
