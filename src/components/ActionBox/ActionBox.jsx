import React from 'react';
import PropTypes from 'prop-types';
import ActionBoxContent from 'components/ActionBox/ActionBoxContent.jsx';
import 'components/ActionBox/ActionBox.scss';

const ActionBox = ({ children }) => (
  <div className="ActionBox">{children}</div>
);

ActionBox.Content = ActionBoxContent;

ActionBox.propTypes = {
  children: PropTypes.node,
};

ActionBox.defaultProps = {
  children: null,
};

export default ActionBox;
