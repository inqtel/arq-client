import React from 'react';
import PropTypes from 'prop-types';

function ActionBoxContent({ children }) {
  return <div className="ActionBox-content">{children}</div>;
}

ActionBoxContent.propTypes = {
  children: PropTypes.node,
};

ActionBoxContent.defaultProps = {
  children: null,
};

export default ActionBoxContent;
