import React from 'react';
import PropTypes from 'prop-types';

function ActionListItem({ children, onClick }) {
  return (
    <li className="ActionList-item" onClick={onClick}>
      {children}
    </li>
  );
}

ActionListItem.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired,
};

ActionListItem.defaultProps = {};

export default ActionListItem;
