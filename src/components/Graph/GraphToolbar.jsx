import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button/Button.jsx';
import Toolbar from 'components/Toolbar/Toolbar.jsx';
import Dropdown from 'components/Dropdown/Dropdown.jsx';
import { IconCaretDown, IconRevert } from 'components/Icons/Icons.jsx';
import ActiveGraphLayoutSelector from 'components/ActiveGraph/ActiveGraphLayoutSelector.jsx';

const GraphToolbar = ({
  undo,
  canUndo,
  layout,
  setLayout,
}) => (
  <Toolbar>
    <Toolbar.Group padded alignRight>
      <Toolbar.Item>
        <Button
          unstyled
          onClick={undo}
          readOnly={!canUndo}
        >
          Undo
          <IconRevert display="inlineBlock" position="right" />
        </Button>
      </Toolbar.Item>
    </Toolbar.Group>
    <Toolbar.Group padded>
      <Toolbar.Item>
        <Dropdown>
          <Dropdown.Toggle>
            <Button unstyled>
              Select Layout
              <IconCaretDown display="inlineBlock" position="right" />
            </Button>
          </Dropdown.Toggle>
          <Dropdown.Menu position="right">
            <ActiveGraphLayoutSelector
              selectedLayout={layout}
              updateSelectedLayout={setLayout}
            />
          </Dropdown.Menu>
        </Dropdown>
      </Toolbar.Item>
    </Toolbar.Group>
  </Toolbar>
);

GraphToolbar.propTypes = {
  canUndo: PropTypes.bool.isRequired,
  undo: PropTypes.func.isRequired,
  layout: PropTypes.string.isRequired,
  setLayout: PropTypes.func.isRequired,
};

export default GraphToolbar;
