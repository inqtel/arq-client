import React from 'react';
import Link from 'components/Link/Link.jsx';
import GraphDetailsType from 'components/Graph/GraphDetailsType.jsx';
import { architecturePropType } from 'proptypes';
import {
  ARCHITECTURE_DRAFT,
  ARCHITECTURE_SUBMITTED,
  ARCHITECTURE_IN_REVIEW,
  ARCHITECTURE_PUBLISHED,
  ARCHITECTURE_REJECTED,
} from 'constants/architectureStates';

const GraphDetailsArchitecture = ({ entity }) => (
  <>
    <GraphDetailsType>
      {entity.status === ARCHITECTURE_DRAFT && 'Architecture (Draft)'}
      {entity.status === ARCHITECTURE_SUBMITTED && 'Architecture (Submitted)'}
      {entity.status === ARCHITECTURE_IN_REVIEW && 'Architecture (In Review)'}
      {entity.status === ARCHITECTURE_PUBLISHED && 'Architecture (Published)'}
      {entity.status === ARCHITECTURE_REJECTED && 'Architecture (Rejected)'}
    </GraphDetailsType>
    <div className="Graph-nodeDetails-content">
      <div className="Graph-nodeDetails-content-title">
        {entity.status === ARCHITECTURE_DRAFT ? (
          entity.title
        ) : (
          <Link to={`/published_architectures/${entity.id}`}>
            {entity.title}
          </Link>
        )}
      </div>
    </div>
  </>
);

GraphDetailsArchitecture.propTypes = {
  entity: architecturePropType.isRequired,
};

export default GraphDetailsArchitecture;
