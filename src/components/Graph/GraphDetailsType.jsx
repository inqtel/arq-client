import React from 'react';
import PropTypes from 'prop-types';

const GraphDetailsType = ({ children }) => (
  <div className="Graph-nodeDetails-type">{children}</div>
);

GraphDetailsType.propTypes = {
  children: PropTypes.node.isRequired,
};

export default GraphDetailsType;
