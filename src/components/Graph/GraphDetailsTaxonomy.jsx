import React from 'react';
import { taxonomyPropType } from 'proptypes';
import Link from 'components/Link/Link.jsx';
import Popover from 'components/Popover/Popover.jsx';
import TaxonomyTreeInterface from 'components/TaxonomyTree/TaxonomyTreeInterface.jsx';
import { IconWordLinked } from 'components/Icons/Icons.jsx';
import GraphDetailsType from 'components/Graph/GraphDetailsType.jsx';

const GraphDetailsTaxonomy = ({ entity }) => {
  const renderItem = taxonomy => (
    <Link to={`/taxonomies/${taxonomy.id}`}>{taxonomy.term}</Link>
  );

  return (
    <>
      <GraphDetailsType>
        Capability
      </GraphDetailsType>
      <div className="Graph-nodeDetails-content">
        <div className="Graph-nodeDetails-content-title">
          <Link to={`/taxonomies/${entity.id}`}>
            {entity.term}
          </Link>
          <div className="Graph-nodeDetails-content-title-icon">
            <Popover
              width="sm"
              topOrBottom="bottom"
              leftOrRight="right"
            >
              <Popover.Toggle>
                <IconWordLinked height={9} display="block" />
              </Popover.Toggle>
              <Popover.Box>
                <Popover.Box.Content>
                  <TaxonomyTreeInterface id={entity.id} renderItem={renderItem} />
                </Popover.Box.Content>
              </Popover.Box>
            </Popover>
          </div>
        </div>
      </div>
    </>
  );
};

GraphDetailsTaxonomy.propTypes = {
  entity: taxonomyPropType.isRequired,
};

export default GraphDetailsTaxonomy;
