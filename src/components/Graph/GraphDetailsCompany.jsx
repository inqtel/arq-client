import React from 'react';
import Link from 'components/Link/Link.jsx';
import GraphDetailsType from 'components/Graph/GraphDetailsType.jsx';
import { companyPropType } from 'proptypes';

const GraphDetailsCompany = ({ entity }) => (
  <>
    <GraphDetailsType>
      Company
    </GraphDetailsType>
    <div className="Graph-nodeDetails-content">
      <div className="Graph-nodeDetails-content-title">
        <Link to={`/companies/${entity.id}`}>
          {entity.name}
        </Link>
      </div>
    </div>
  </>
);

GraphDetailsCompany.propTypes = {
  entity: companyPropType.isRequired,
};

export default GraphDetailsCompany;
