import React from 'react';
import { IconLink, IconBrokenLink } from 'components/Icons/Icons.jsx';
import Popover from 'components/Popover/Popover.jsx';
import Link from 'components/Link/Link.jsx';
import TaxonomyTreeInterface from 'components/TaxonomyTree/TaxonomyTreeInterface.jsx';
import GraphDetailsType from 'components/Graph/GraphDetailsType.jsx';
import { packagePropType } from 'proptypes';

const GraphDetailsPackage = ({ entity }) => {
  const renderItem = taxonomy => (
    <Link to={`/taxonomies/${taxonomy.id}`}>{taxonomy.term}</Link>
  );

  return (
    <>
      <GraphDetailsType>
        Term
      </GraphDetailsType>
      <div className="Graph-nodeDetails-content">
        <div className="Graph-nodeDetails-content-title">
          {entity.term}
          {entity.taxonomy && (
            <span className="Graph-nodeDetails-content-title-icon">
              <Popover
                width="sm"
                topOrBottom="bottom"
                leftOrRight="right"
              >
                <Popover.Toggle>
                  {entity.linkBroken
                    ? <IconBrokenLink height={9} display="block" />
                    : <IconLink height={9} display="block" />
                  }
                </Popover.Toggle>
                <Popover.Box>
                  <Popover.Box.Content>
                    <TaxonomyTreeInterface id={entity.taxonomy.id} renderItem={renderItem} />
                  </Popover.Box.Content>
                </Popover.Box>
              </Popover>
            </span>
          )}
        </div>
      </div>
    </>
  );
};

GraphDetailsPackage.propTypes = {
  entity: packagePropType.isRequired,
};

export default GraphDetailsPackage;
