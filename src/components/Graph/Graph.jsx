import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { architecturePropType, apolloClientPropType, taxonomyPropType } from 'proptypes';
import { pullAll, flatten, uniqBy } from 'lodash';
import {
  ARCHITECTURE_TYPENAME,
  PACKAGE_TYPENAME,
  TAXONOMY_TYPENAME,
  PROBLEM_TYPENAME,
  COMPANY_TYPENAME,
  CUSTOMER_TYPENAME,
  ARCHITECTURE_QUERY_ROOT,
  PACKAGE_QUERY_ROOT,
  TAXONOMY_QUERY_ROOT,
  PROBLEM_QUERY_ROOT,
  COMPANY_QUERY_ROOT,
  CUSTOMER_QUERY_ROOT,
} from 'constants/gql.js';
import {
  createArchitecturePackagesCy,
  createCompanyTaxonomiesCy,
  createCustomerProblemsCy,
  createProblemRelatedEls,
  createPackageRelatedEls,
  createTaxonomyRelatedEls,
  isViewableLink,
  initGraphElements,
} from 'utils/graph';
import Cytoscape from 'components/Cytoscape/Cytoscape.jsx';

import GraphDetails from 'components/Graph/GraphDetails.jsx';
import {
  GET_PACKAGE_RELATED_NODES,
  GET_TAXONOMY_RELATED_NODES,
  GET_PROBLEM_RELATED_NODES,
  GET_COMPANY_RELATED_NODES,
  GET_CUSTOMER_RELATED_NODES,
  GET_ARCHITECTURE_RELATED_NODES,
} from 'queries/graph';
import GraphToolbar from 'components/Graph/GraphToolbar.jsx';
import 'components/Graph/Graph.scss';

const Graph = ({
  item,
  client,
  layout,
  setLayout,
}) => {
  const [selectedEl, setSelectedEl] = useState(null);
  const [elements, setElements] = useState([]);

  const [undoStack, setUndoStack] = useState([]);
  const createUndoStackItem = (id, els) => ({ id, els });

  useEffect(() => {
    const els = initGraphElements(item);
    els.forEach((el, i) => {
      if (el.data.entity) els[i] = nodeSetInteractive(el, els);
    });

    setUndoStack([createUndoStackItem(item.id, els)]);
    setElements(els);
  }, [item, nodeSetInteractive]);

  const nodeSetInteractive = useCallback((nodeEl, allElements) => {
    const links = nodeGetLinkedNodeQIDs(nodeEl.data.entity);
    const unseenLinks = pullAll(links, allElements.map(ele => ele.data.id));
    if (unseenLinks.length > 0) nodeEl.classes += ' interactive';
    return nodeEl;
  }, [nodeGetLinkedNodeQIDs]);

  const onNodeMouseOver = (e) => {
    const el = e.target;

    el.successors().edges().addClass('highlight');
    el.predecessors().edges().addClass('highlight');
  };

  const onNodeMouseOut = (e) => {
    const el = e.target;

    el.successors().removeClass('highlight');
    el.predecessors().removeClass('highlight');
  };

  const onGraphClick = (e) => {
    const el = e.target;

    if (el.isNode && el.isNode()) {
      const deselect = !!selectedEl && el.data('id') === selectedEl.data('id');

      setSelectedEl(deselect ? null : el);
    } else {
      setSelectedEl(null);
    }
  };

  const onExpandClick = async (element) => {
    const nodeData = element.data();
    let els;

    switch (nodeData.label) {
      case PACKAGE_TYPENAME: {
        const data = await fetchData(nodeData, GET_PACKAGE_RELATED_NODES);
        els = createPackageRelatedEls(data[PACKAGE_QUERY_ROOT], item);

        break;
      }
      case TAXONOMY_TYPENAME: {
        const data = await fetchData(nodeData, GET_TAXONOMY_RELATED_NODES);
        els = createTaxonomyRelatedEls(data[TAXONOMY_QUERY_ROOT], item);

        break;
      }
      case PROBLEM_TYPENAME: {
        const data = await fetchData(nodeData, GET_PROBLEM_RELATED_NODES);
        els = createProblemRelatedEls(data[PROBLEM_QUERY_ROOT]);

        break;
      }
      case COMPANY_TYPENAME: {
        const data = await fetchData(nodeData, GET_COMPANY_RELATED_NODES);
        const { capabilities, id } = data[COMPANY_QUERY_ROOT];
        els = createCompanyTaxonomiesCy(capabilities, id);

        break;
      }
      case CUSTOMER_TYPENAME: {
        const data = await fetchData(nodeData, GET_CUSTOMER_RELATED_NODES);
        const { problemSets, id } = data[CUSTOMER_QUERY_ROOT];
        els = createCustomerProblemsCy(problemSets, id);

        break;
      }
      case ARCHITECTURE_TYPENAME: {
        const data = await fetchData(nodeData, GET_ARCHITECTURE_RELATED_NODES);
        const { packages, id } = data[ARCHITECTURE_QUERY_ROOT];
        els = createArchitecturePackagesCy(packages, id);

        break;
      }
      default:
        els = [];

        break;
    }

    if (els.length > 0) {
      const allElements = els.concat(elements);

      els.forEach((el, i) => {
        if (el.data.entity) els[i] = nodeSetInteractive(el, allElements);
      });

      const rootEl = elements.find(el => el.data.id === nodeData.id);
      rootEl.classes = rootEl.classes.replace('interactive', '').trim();

      setElements(allElements);

      setUndoStack([
        ...undoStack,
        createUndoStackItem(nodeData.id, els),
      ]);
    }
  };

  const nodeGetLinkedNodeQIDs = useCallback((entity) => {
    const linkedNodeQIDs = [];

    const types = [
      'parent',
      'children',
      'packages',
      'taxonomy',
      'referencePackage',
      'architecture',
      'capabilities',
      'problemSets',
      'companies',
      'referencedBy',
      'customer',
    ];

    types.forEach((type) => {
      if (entity[type]) {
        const relatedEntityArray = Array.isArray(entity[type]) ? entity[type] : [entity[type]];

        linkedNodeQIDs.push(...relatedEntityArray
          .filter(relatedEntity => isViewableLink(entity, relatedEntity, item))
          .map(relatedEntity => relatedEntity.id));
      }
    });

    return linkedNodeQIDs;
  }, [item]);

  const fetchData = async (nodeData, query) => {
    // TODO: Probably don't need a loading state; should handle error though
    const { data } = await client.query({
      query,
      variables: { id: nodeData.id },
      fetchPolicy: 'no-cache', // temp fix to prevent queries from triggering hook changes in parents
    });
    return data;
  };

  const undo = useCallback(() => {
    const undoItem = undoStack[undoStack.length - 1];
    const remainingStack = undoStack.slice(0, -1);

    const allElements = uniqBy(
      flatten(remainingStack.map(si => si.els)),
      e => e.data.id,
    );

    allElements.find(el => el.data.id === undoItem.id).classes += ' interactive';

    setSelectedEl(null);
    setElements(allElements);
    setUndoStack(remainingStack);
  }, [undoStack]);

  return (
    <>
      <GraphToolbar
        undo={undo}
        canUndo={undoStack.length > 1}
        layout={layout}
        setLayout={setLayout}
      />
      <div className="GraphCyWrapper">
        <Cytoscape
          layoutName={layout}
          elements={elements}
          onNodeMouseOver={onNodeMouseOver}
          onNodeMouseOut={onNodeMouseOut}
          onGraphClick={onGraphClick}
        />
        {selectedEl && (
          <GraphDetails
            node={selectedEl}
            onExpandClick={onExpandClick}
          />
        )}
      </div>
    </>
  );
};

Graph.propTypes = {
  item: PropTypes.oneOfType([
    architecturePropType,
    taxonomyPropType,
  ]).isRequired,
  layout: PropTypes.string.isRequired,
  setLayout: PropTypes.func.isRequired,
  client: apolloClientPropType.isRequired,
};

Graph.defaultProps = {};

export default Graph;
