import React from 'react';
import PropTypes from 'prop-types';
import Link from 'components/Link/Link.jsx';
import GraphDetailsType from 'components/Graph/GraphDetailsType.jsx';
import { problemPropType } from 'proptypes';

const GraphDetailsProblem = ({ entity, content }) => (
  <>
    <GraphDetailsType>
      Customer Problem
    </GraphDetailsType>
    <div className="Graph-nodeDetails-content">
      <div className="Graph-nodeDetails-content-title">
        <Link to={`/problems/${entity.id}`}>
          {content}
        </Link>
      </div>
    </div>
  </>
);

GraphDetailsProblem.propTypes = {
  entity: problemPropType.isRequired,
  content: PropTypes.string.isRequired,
};

export default GraphDetailsProblem;
