import React from 'react';
import PropTypes from 'prop-types';

function FilterableListContent({ children }) {
  return <div className="FilterableList-content">{children}</div>;
}

FilterableListContent.propTypes = {
  children: PropTypes.node,
};

FilterableListContent.defaultProps = {
  children: null,
};

export default FilterableListContent;
