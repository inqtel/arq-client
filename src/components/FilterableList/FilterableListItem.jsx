import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Checkbox from 'components/Checkbox/Checkbox.jsx';

class FilterableListItem extends Component {
  constructor(props) {
    super(props);

    const { item, checkedKey, checkedValue } = this.props;

    this.state = {
      checked: item[checkedKey] !== checkedValue,
    };
  }

  onChange = (checked) => {
    const {
      item, onSelect, onDeselect,
    } = this.props;

    if (checked) {
      this.setState({ checked });
      onSelect(item);
    } else {
      this.setState({ checked });
      onDeselect(item);
    }
  }

  onRowClick = () => {
    const { allowClickOnRow, disabled, item } = this.props;
    const { checked } = this.state;

    allowClickOnRow && !disabled(item) && this.onChange(!checked);
  }

  onCheckboxChange = (e) => {
    const { allowClickOnRow } = this.props;
    const { checked } = e.target;
    !allowClickOnRow && this.onChange(checked);
  }

  render() {
    const {
      visible,
      item,
      id,
      titleKey,
      allowClickOnRow,
      disabled,
    } = this.props;

    const { checked } = this.state;

    return (
      <li
        className={cx('FilterableList-item', {
          'FilterableList-item--isHidden': !visible,
          'FilterableList-item--clickable': allowClickOnRow,
        })}
        onClick={this.onRowClick}
      >
        <div className="FilterableList-item-avatar" />
        <span className="FilterableList-item-title">{item[titleKey]}</span>
        <Checkbox
          id={id}
          onChange={this.onCheckboxChange}
          checked={checked}
          disabled={disabled(item)}
        />
      </li>
    );
  }
}

FilterableListItem.propTypes = {
  visible: PropTypes.bool.isRequired,
  item: PropTypes.shape({}).isRequired,
  id: PropTypes.string.isRequired,
  titleKey: PropTypes.string.isRequired,
  checkedKey: PropTypes.string.isRequired,
  checkedValue: PropTypes.string.isRequired,
  onSelect: PropTypes.func.isRequired,
  onDeselect: PropTypes.func.isRequired,
  disabled: PropTypes.func.isRequired,
  allowClickOnRow: PropTypes.bool.isRequired,
};

export default FilterableListItem;
