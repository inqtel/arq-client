import React from 'react';
import PropTypes from 'prop-types';
import Toolbar from 'components/Toolbar/Toolbar.jsx';
import { IconCaretLeft } from 'components/Icons/Icons.jsx';
import Link from 'components/Link/Link.jsx';

const ActiveGraphToolbar = ({ backLink }) => (
  <Toolbar>
    <Toolbar.Group fill padded>
      {backLink && (
        <Toolbar.Item>
          <Link to={backLink.url}>
            <IconCaretLeft display="inlineBlock" position="left" />
            {backLink.text}
          </Link>
        </Toolbar.Item>
      )}
    </Toolbar.Group>
  </Toolbar>
);

ActiveGraphToolbar.propTypes = {
  backLink: PropTypes.shape({
    url: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
  }),
};

ActiveGraphToolbar.defaultProps = {
  backLink: null,
};

export default ActiveGraphToolbar;
