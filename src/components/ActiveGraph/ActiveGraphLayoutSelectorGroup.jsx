import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Radio from 'components/Radio/Radio.jsx';

class ActiveGraphLayoutSelectorGroup extends Component {
  onChange = () => {
    const { layout, onLayoutUpdate } = this.props;

    onLayoutUpdate(layout);
  }

  render() {
    const { layout, layoutName, selectedLayout } = this.props;

    return (
      <div className="ActiveGraph-layoutSelector-group">
        <span className="ActiveGraph-layoutSelector-layout">
          {layoutName}
        </span>
        <div className="ActiveGraph-layoutSelector-radio">
          <Radio
            id={layout}
            name="ActiveGraphLayout"
            checked={layout === selectedLayout}
            onChange={this.onChange}
          />
        </div>
      </div>
    );
  }
}

ActiveGraphLayoutSelectorGroup.propTypes = {
  layout: PropTypes.string.isRequired,
  layoutName: PropTypes.string.isRequired,
  onLayoutUpdate: PropTypes.func.isRequired,
  selectedLayout: PropTypes.string.isRequired,
};

ActiveGraphLayoutSelectorGroup.defaultProps = {};

export default ActiveGraphLayoutSelectorGroup;
