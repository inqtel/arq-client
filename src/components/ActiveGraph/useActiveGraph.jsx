import { useState } from 'react';
import { useQuery } from '@apollo/client';
import { getProblemShortname } from 'utils/problem';
import {
  GET_TAXONOMY_RELATED_NODES,
  GET_GRAPH_COMPANY,
  GET_GRAPH_CUSTOMER,
  GET_GRAPH_PROBLEM,
  GET_GRAPH_ARCHITECTURE,
} from 'queries/graph';
import { GRID } from 'constants/graph.js';
import {
  ARCHITECTURE_TYPENAME,
  TAXONOMY_TYPENAME,
  COMPANY_TYPENAME,
  CUSTOMER_TYPENAME,
  PROBLEM_TYPENAME,
  COMPANY_QUERY_ROOT,
  CUSTOMER_QUERY_ROOT,
  TAXONOMY_QUERY_ROOT,
  PROBLEM_QUERY_ROOT,
  ARCHITECTURE_QUERY_ROOT,
} from 'constants/gql';
import { get } from 'lodash';

const useActiveGraph = ({
  id,
  type,
  documentQID,
  published,
  shared,
}) => {
  let query;
  let getTitle;
  let backURL;
  let backText;
  let queryRoot;

  switch (type) {
    case TAXONOMY_TYPENAME:
      query = GET_TAXONOMY_RELATED_NODES;
      getTitle = e => e.term;
      backURL = `/taxonomies/${id}`;
      backText = 'Back to Taxonomy';
      queryRoot = TAXONOMY_QUERY_ROOT;
      break;
    case COMPANY_TYPENAME:
      query = GET_GRAPH_COMPANY;
      getTitle = e => e.name;
      backURL = `/companies/${id}`;
      backText = 'Back to Company';
      queryRoot = COMPANY_QUERY_ROOT;
      break;
    case CUSTOMER_TYPENAME:
      query = GET_GRAPH_CUSTOMER;
      getTitle = e => e.name;
      backURL = `/customers/${id}`;
      backText = 'Back to Customer';
      queryRoot = CUSTOMER_QUERY_ROOT;
      break;
    case PROBLEM_TYPENAME:
      query = GET_GRAPH_PROBLEM;
      getTitle = e => getProblemShortname(e);
      backURL = `/problems/${id}`;
      backText = 'Back to Problem';
      queryRoot = PROBLEM_QUERY_ROOT;
      break;
    case ARCHITECTURE_TYPENAME:
      query = GET_GRAPH_ARCHITECTURE;
      getTitle = e => e.title;
      if (published) {
        backURL = `/published_architectures/${id}`;
        backText = 'Back to Published Architecture';
      } else {
        backURL = shared ? `/shared/${documentQID}` : `/documents/${documentQID}`;
        backText = shared ? 'Back to Shared Document' : 'Back to Document';
      }
      queryRoot = ARCHITECTURE_QUERY_ROOT;
      break;
    default:
      break;
  }

  const {
    loading,
    error,
    data,
    client,
  } = useQuery(query, {
    variables: { id },
    fetchPolicy: 'network-only',
  });

  const entity = get(data, queryRoot, {});

  const [layout, setLayout] = useState(GRID);

  const backLink = {
    url: backURL,
    text: backText,
  };

  return {
    loading,
    error,
    entity,
    title: getTitle(entity) || null,
    client,
    layout,
    setLayout,
    backLink,
  };
};

export default useActiveGraph;
