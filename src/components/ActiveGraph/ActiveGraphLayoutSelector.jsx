import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { layouts } from 'constants/graph.js';
import ActiveGraphLayoutSelectorGroup from 'components/ActiveGraph/ActiveGraphLayoutSelectorGroup.jsx';

class ActiveGraphLayoutSelector extends Component {
  onLayoutUpdate = (layout) => {
    const { updateSelectedLayout } = this.props;

    updateSelectedLayout(layout);
  }

  render() {
    const { selectedLayout } = this.props;

    return (
      <div className="ActiveGraph-layoutSelector">
        {Object.keys(layouts).map(layout => (
          <ActiveGraphLayoutSelectorGroup
            key={layout}
            layout={layouts[layout]}
            layoutName={layout}
            selectedLayout={selectedLayout}
            onLayoutUpdate={this.onLayoutUpdate}
          />
        ))}
      </div>
    );
  }
}

ActiveGraphLayoutSelector.propTypes = {
  selectedLayout: PropTypes.string.isRequired,
  updateSelectedLayout: PropTypes.func.isRequired,
};

ActiveGraphLayoutSelector.defaultProps = {};

export default ActiveGraphLayoutSelector;
