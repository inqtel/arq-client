import React from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import cx from 'classnames';
import Checkbox from 'components/Checkbox/Checkbox.jsx';
import 'components/Checkbox/Checkbox.scss';

const Checkboxes = ({
  onChange,
  options,
  checkedMap,
  noWrap,
  getLabel,
}) => (
  <div
    className={cx('Checkboxes', {
      'Checkboxes--noWrap': noWrap,
    })}
  >
    {options.map(opt => (
      <Checkbox
        key={opt}
        id={opt}
        onChange={onChange(opt)}
        checked={checkedMap[opt]}
        label={getLabel ? getLabel(opt) : opt}
      />
    ))}
  </div>
);

Checkboxes.propTypes = {
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.string),
  checkedMap: PropTypes.shape({}),
  noWrap: PropTypes.bool,
  getLabel: PropTypes.func,
};

Checkboxes.defaultProps = {
  onChange: noop,
  options: [],
  checkedMap: {},
  noWrap: false,
  getLabel: null,
};

export default Checkboxes;
