import React from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import { IconCheck, IconDisc } from 'components/Icons/Icons.jsx';
import 'components/Checkbox/Checkbox.scss';

const Checkbox = ({
  checked,
  onChange,
  id,
  label,
  disabled,
}) => {
  const fill = disabled ? 'rgba(106, 113, 215, 0.5)' : '#6a71d7';

  return (
    <div className="Checkbox">
      <input
        type="checkbox"
        className="Checkbox-item"
        id={id}
        checked={checked}
        onChange={onChange}
        disabled={disabled}
      />
      <label
        htmlFor={id}
        className="Checkbox-item-label"
        onClick={e => e.stopPropagation()}
      >
        {checked ? (
          <IconCheck fill={fill} display="inlineBlock" height={20} />
        ) : (
          <IconDisc fill={fill} display="inlineBlock" height={20} />
        )}
        {label && <span className="Checkbox-label-text">{label}</span>}
      </label>
    </div>
  );
};

Checkbox.propTypes = {
  onChange: PropTypes.func,
  checked: PropTypes.bool.isRequired,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  disabled: PropTypes.bool,
};

Checkbox.defaultProps = {
  onChange: noop,
  label: '',
  disabled: false,
};

export default Checkbox;
