import React from 'react';
import PropTypes from 'prop-types';
import { userPropType } from 'proptypes';
import { isAdminLocalDev } from 'utils/user';
import Modal from 'components/Modal/Modal.jsx';
import Radio from 'components/Radio/Radio.jsx';
import { noop } from 'lodash';

const ToggleUserModal = ({
  closeToggleUser,
  toggleUserShown,
  user,
  users,
  toggleUserAction,
}) => {
  const actions = (
    <div>
      {users.map((u) => {
        const label = u.firstName && u.lastName ? `${u.firstName} ${u.lastName}` : u.email;
        return (!!u.email && (
          <Radio
            key={`${u.email}`}
            id={u.email}
            label={isAdminLocalDev(u.email) ? `${label} - [ADMIN]` : `${label}`}
            name={u.email}
            checked={u.email === user.email}
            onChange={() => toggleUserAction(u)}
          />
        ));
      })}
    </div>
  );

  return (
    <Modal
      title="Toggle User"
      actions={actions}
      isOpened={toggleUserShown}
      closeModal={closeToggleUser}
      learningModule={false}
    >
      Choose which user you want to be.
    </Modal>
  );
};

ToggleUserModal.propTypes = {
  closeToggleUser: PropTypes.func,
  toggleUserShown: PropTypes.bool,
  user: userPropType.isRequired,
  users: PropTypes.arrayOf(userPropType).isRequired,
  toggleUserAction: PropTypes.func.isRequired,
};

ToggleUserModal.defaultProps = {
  closeToggleUser: noop,
  toggleUserShown: false,
};

export default ToggleUserModal;
