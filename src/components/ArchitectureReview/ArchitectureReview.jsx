import React from 'react';
import PropTypes from 'prop-types';
import { adminArchitecturePropType } from 'proptypes';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import Architecture from 'components/Architecture/Architecture.jsx';
import ArchitectureReviewTabs from 'components/ArchitectureReview/ArchitectureReviewTabs.jsx';
import ArchitectureReviewPackageInterface from 'components/ArchitectureReview/ArchitectureReviewPackageInterface.jsx';
import Loader from 'components/Loader/Loader.jsx';
import 'components/ArchitectureReview/ArchitectureReview.scss';

const ArchitectureReview = ({
  adminArchitecture,
  patchAdminArchitectureStatusPublished,
  patchAdminArchitectureStatusRejected,
}) => {
  const { ui } = adminArchitecture;

  return (
    <div className="ArchitectureReview">
      <Dashboard>
        <Dashboard.Header showBreadcrumbs />
        <Dashboard.Section fit>
          <Dashboard.Column fit theme="light">
            <Dashboard.Content padding={['top', 'right', 'left']}>
              <div className="ArchitectureReview-architecture">
                {ui.loading ? (
                  <Loader size={36} isCentered />
                ) : (
                  !ui.error && (
                    <Architecture
                      architecture={adminArchitecture}
                      renderPackageInterface={
                        props => <ArchitectureReviewPackageInterface {...props} />
                      }
                      interactive
                    />
                  )
                )}
              </div>
            </Dashboard.Content>
          </Dashboard.Column>
          <Dashboard.Column fit size="lg" theme="white">
            <Dashboard.Content>
              {!ui.loading
                && !ui.error && (
                <ArchitectureReviewTabs
                  patchAdminArchitectureStatusPublished={
                    patchAdminArchitectureStatusPublished
                  }
                  patchAdminArchitectureStatusRejected={
                    patchAdminArchitectureStatusRejected
                  }
                />
              )}
            </Dashboard.Content>
          </Dashboard.Column>
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

ArchitectureReview.propTypes = {
  adminArchitecture: adminArchitecturePropType.isRequired,
  patchAdminArchitectureStatusPublished: PropTypes.func.isRequired,
  patchAdminArchitectureStatusRejected: PropTypes.func.isRequired,
};

ArchitectureReview.defaultProps = {};

export default ArchitectureReview;
