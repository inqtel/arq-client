import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { toggleAdminArchitecturePackageChildren } from 'actions/adminArchitecture';
import ArchitecturePackage from 'components/Architecture/ArchitecturePackage.jsx';

const ArchitectureReviewPackageInterface = (props) => {
  const packageToggleChildren = (archID, pkgID) => {
    props.toggleAdminArchitecturePackageChildren(archID, pkgID);
  };

  return (
    <ArchitecturePackage packageToggleChildren={packageToggleChildren} {...props} />
  );
};

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      toggleAdminArchitecturePackageChildren,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(
  ArchitectureReviewPackageInterface,
);
