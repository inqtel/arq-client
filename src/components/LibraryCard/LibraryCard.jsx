import React from 'react';
import PropTypes from 'prop-types';
import { architecturePropType, documentPropType, userPropType } from 'proptypes';
import { nicetime } from 'utils/time';
import Card from 'components/Card/Card.jsx';
import Architecture from 'components/Architecture/Architecture.jsx';
import 'components/LibraryCard/LibraryCard.scss';

const LibraryCard = ({
  item,
  link,
  onArchiveClick,
  statusString,
  renderArchiveModal,
  previewArchitecture,
  createdAt,
  updatedAt,
  publishedAt,
  author,
  isNew,
  fullWidth,
}) => (
  <Card
    url={link}
    onArchiveClick={onArchiveClick}
    renderArchiveModal={renderArchiveModal}
    fullWidth={fullWidth}
  >
    <Card.Header title={item.title} />
    <Card.Meta>{statusString}</Card.Meta>
    <Card.Preview>
      {previewArchitecture ? (
        <Architecture
          architecture={previewArchitecture}
          publishedView
          interactive={false}
          size="sm"
        />
      ) : (
        <span className="LibraryCard-card-architecture-placeholder">
          No architecture defined yet
        </span>
      )}
    </Card.Preview>
    <Card.Footer isNew={isNew}>
      {author && (
        <span className="LibraryCard-card-footer-timestamp">
          <strong>Author:</strong>
          {' '}
          {author.email}
        </span>
      )}
      {createdAt && (
        <span className="LibraryCard-card-footer-timestamp">
          <strong>Created:</strong>
          {' '}
          {nicetime(createdAt)}
        </span>
      )}
      {updatedAt && (
        <span className="LibraryCard-card-footer-timestamp">
          <strong>Last Updated:</strong>
          {' '}
          {nicetime(updatedAt)}
        </span>
      )}
      {publishedAt && (
        <span className="LibraryCard-card-footer-timestamp">
          <strong>Published:</strong>
          {' '}
          {nicetime(publishedAt)}
        </span>
      )}
    </Card.Footer>
  </Card>
);

LibraryCard.propTypes = {
  item: PropTypes.oneOfType([architecturePropType, documentPropType]).isRequired,
  link: PropTypes.string.isRequired,
  previewArchitecture: architecturePropType,
  statusString: PropTypes.string,
  onArchiveClick: PropTypes.func,
  renderArchiveModal: PropTypes.func,
  author: userPropType,
  createdAt: PropTypes.string,
  updatedAt: PropTypes.string,
  publishedAt: PropTypes.string,
  isNew: PropTypes.bool,
  fullWidth: PropTypes.bool,
};

LibraryCard.defaultProps = {
  previewArchitecture: null,
  statusString: null,
  onArchiveClick: null,
  renderArchiveModal: () => {},
  author: null,
  createdAt: null,
  updatedAt: null,
  publishedAt: null,
  isNew: false,
  fullWidth: false,
};

export default LibraryCard;
