import React from 'react';
import CommentThread from 'components/CommentThread/CommentThread.jsx';

describe('<CommentThread />', () => {
  it('should render with .CommentThread class', () => {
    const wrapper = shallow(<CommentThread checked />);
    expect(wrapper.is('.CommentThread')).toBe(true);
  });
});
