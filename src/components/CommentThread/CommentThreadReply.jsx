import React from 'react';
import PropTypes from 'prop-types';
import { NEW_COMMENT } from 'constants/commentStates.js';
import CommentBox from 'components/CommentBox/CommentBox.jsx';

const CommentThreadReply = ({
  user,
  comments,
  onAddNewComment,
}) => (
  <div className="CommentThread-reply">
    {comments.length > 0 ? (
      <CommentBox
        user={user}
        status={NEW_COMMENT}
        placeholder="Reply..."
        showActions={false}
        onPost={onAddNewComment}
      />
    ) : (
      <CommentBox
        user={user}
        status={NEW_COMMENT}
        placeholder="New comment..."
        showActions
        onPost={onAddNewComment}
      />
    )}
  </div>
);

CommentThreadReply.propTypes = {
  user: PropTypes.object.isRequired,
  comments: PropTypes.array.isRequired,
  onAddNewComment: PropTypes.func.isRequired,
};

CommentThreadReply.defaultProps = {};

export default CommentThreadReply;
