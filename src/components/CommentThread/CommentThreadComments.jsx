import React from 'react';
import PropTypes from 'prop-types';
import { userPropType, commentPropType } from 'proptypes';
import { OLD_COMMENT } from 'constants/commentStates.js';
import CommentBoxDeleted from 'components/CommentBox/CommentBoxDeleted.jsx';
import CommentBox from 'components/CommentBox/CommentBox.jsx';

const CommentThreadComments = ({
  user,
  comments,
  onDeleteComment,
  shared,
}) => {
  if (comments.length === 0) {
    return null;
  }

  return (
    <div className="CommentThread-comments">
      {comments.map((comment) => {
        const showTools = !shared || user.email === comment.author.email;

        return comment.deletedAt ? (
          <CommentBoxDeleted
            key={comment.id}
            author={comment.author}
            user={user}
          />
        ) : (
          <CommentBox
            key={comment.id}
            user={user}
            status={OLD_COMMENT}
            comment={comment}
            onDeleteComment={onDeleteComment}
            showTools={showTools}
          />
        );
      })}
    </div>
  );
};

CommentThreadComments.propTypes = {
  user: userPropType.isRequired,
  comments: PropTypes.arrayOf(commentPropType).isRequired,
  onDeleteComment: PropTypes.func.isRequired,
  shared: PropTypes.bool.isRequired,
};

CommentThreadComments.defaultProps = {};

export default CommentThreadComments;
