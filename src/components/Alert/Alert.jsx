import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import 'components/Alert/Alert.scss';

function Alert({
  heading,
  subheading,
  isFullscreen,
  padded,
}) {
  return (
    <div
      className={cx('Alert', {
        'Alert--isFullscreen': isFullscreen,
        'Alert--padded': padded,
      })}
    >
      <div className="Alert-content">
        <h1 className="Alert-heading">{heading}</h1>
        <h2 className="Alert-subheading">{subheading}</h2>
      </div>
    </div>
  );
}

Alert.propTypes = {
  heading: PropTypes.string.isRequired,
  subheading: PropTypes.string.isRequired,
  isFullscreen: PropTypes.bool,
  padded: PropTypes.bool,
};

Alert.defaultProps = {
  isFullscreen: false,
  padded: false,
};

export default Alert;
