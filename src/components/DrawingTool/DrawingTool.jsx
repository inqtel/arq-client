import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { isEmpty } from 'lodash';
import {
  drawingToolThemePropType,
  architecturePropType,
  uiPropType,
  drawingRasterPropsPropType,
} from 'proptypes';
import { drawPowerPoint } from 'utils/drawingPowerPoint';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import DrawingToolToolbar from 'components/DrawingTool/DrawingToolToolbar.jsx';
import DrawingToolContent from 'components/DrawingTool/DrawingToolContent.jsx';
import DrawingToolStatusBar from 'components/DrawingTool/DrawingToolStatusBar.jsx';
import DrawingToolBreadcrumbs from 'components/DrawingTool/DrawingToolBreadcrumbs.jsx';
import DrawingToolArchitecture from 'components/DrawingTool/DrawingToolArchitecture.jsx';
import DrawingToolCanvas from 'components/DrawingTool/DrawingToolCanvas.jsx';
import 'components/DrawingTool/DrawingTool.scss';

const DrawingTool = ({
  documentID,
  shared,
  selectedTheme,
  setSelectedTheme,
  setFocusID,
  drawingRasterProps,
  setDrawingRasterProps,
  architecture,
  packages,
  ui,
  editable,
}) => {
  const $architecture = useRef(null);
  const $canvas = useRef(null);

  const onTakeScreenshot = async () => {
    await $architecture.current.setDrawingRasterProps();
    $canvas.current.draw();
  };

  const onConvertToPowerPoint = () => {
    const rect = $architecture.current.$architecture.getBoundingClientRect();
    drawPowerPoint(architecture, packages, rect);
  };

  const drawingToolClassNames = cx(
    'DrawingTool',
    !isEmpty(selectedTheme)
      && `DrawingTool--theme-${selectedTheme.name.toLowerCase()}`,
  );

  return (
    <div className={drawingToolClassNames}>
      <Dashboard>
        <Dashboard.Header showBreadcrumbs />
        {!ui.loading
          && !ui.error
          && architecture.id && (
          <Dashboard.Section above>
            <Dashboard.Toolbar>
              <DrawingToolToolbar
                shared={shared}
                documentID={documentID}
                selectedTheme={selectedTheme}
                setSelectedTheme={setSelectedTheme}
                onTakeScreenshot={onTakeScreenshot}
                onConvertToPowerPoint={onConvertToPowerPoint}
              />
            </Dashboard.Toolbar>
          </Dashboard.Section>
        )}
        <Dashboard.Section fit>
          <Dashboard.Column fit>
            <Dashboard.Content padding={['top', 'right', 'bottom', 'left']}>
              <DrawingToolContent>
                <DrawingToolStatusBar status={ui.status} />
                <DrawingToolBreadcrumbs
                  breadcrumbs={ui.breadcrumbs}
                  setFocusID={setFocusID}
                />
                {!ui.loading
                  && !ui.error
                  && architecture.id && (
                  <DrawingToolArchitecture
                    ref={$architecture}
                    architectureID={architecture.id}
                    architectureTitle={architecture.title}
                    visibleRootPackageQIDs={packages.visibleRootQIDs}
                    packagesByQID={packages.byQID}
                    selectedTheme={selectedTheme}
                    setFocusID={setFocusID}
                    setDrawingRasterProps={setDrawingRasterProps}
                    editable={editable}
                  />
                )}
              </DrawingToolContent>
              {!ui.loading
                && !ui.error
                && architecture.id && (
                <DrawingToolCanvas
                  ref={$canvas}
                  drawingToolClassNames={drawingToolClassNames}
                  drawingRasterProps={drawingRasterProps}
                />
              )}
            </Dashboard.Content>
            <Dashboard.Footer />
          </Dashboard.Column>
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

DrawingTool.propTypes = {
  shared: PropTypes.bool.isRequired,
  documentID: PropTypes.string,
  selectedTheme: drawingToolThemePropType.isRequired,
  setSelectedTheme: PropTypes.func.isRequired,
  setFocusID: PropTypes.func.isRequired,
  drawingRasterProps: drawingRasterPropsPropType.isRequired,
  setDrawingRasterProps: PropTypes.func.isRequired,
  architecture: architecturePropType,
  packages: PropTypes.shape({
    visibleRootQIDs: PropTypes.arrayOf(PropTypes.string),
    byQID: PropTypes.shape({}),
  }).isRequired,
  ui: uiPropType.isRequired,
  editable: PropTypes.bool.isRequired,
};

DrawingTool.defaultProps = {
  documentID: null,
  architecture: null,
};

export default DrawingTool;
