import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { keyBy, get } from 'lodash';
import { timeago } from 'utils/time';
import moment from 'moment';
import { generateDrawingBreadcrumbsTrail } from 'utils/drawingTool';
import useDrawingState from 'components/DrawingTool/hooks/useDrawingState.jsx';
import useDrawingArchitecture from 'components/DrawingTool/hooks/useDrawingArchitecture.jsx';
import DrawingTool from 'components/DrawingTool/DrawingTool.jsx';

const DrawingToolInterface = ({
  documentID,
  architectureID,
  shared,
  published,
}) => {
  const editable = !shared && !published;

  const {
    selectedTheme,
    setSelectedTheme,
    focusID,
    setFocusID,
    drawingRasterProps,
    setDrawingRasterProps,
    initDrawing,
    resetDrawing,
  } = useDrawingState();

  // set/reset drawing state
  useEffect(() => {
    initDrawing(architectureID);
  }, [initDrawing, architectureID]);

  useEffect(() => () => resetDrawing(), [resetDrawing]);

  const {
    loading,
    error,
    architecture,
  } = useDrawingArchitecture(architectureID);

  const byQID = keyBy(get(architecture, 'allPackages', []), p => p.id);

  // Compute values for DrawingTool component tree
  let breadcrumbs = [];
  let status = '';
  let visibleRootQIDs = [];

  if (loading) status = 'Loading...';

  if (error) status = 'Error rendering drawing...';

  if (!loading && !error && focusID && architecture.id) {
    const updatedAt = moment().format();
    status = timeago(updatedAt, 'Drawing last updated');

    breadcrumbs = generateDrawingBreadcrumbsTrail(architecture, byQID, focusID);

    visibleRootQIDs = focusID === architectureID
      ? architecture.packages.map(p => p.id)
      : [focusID];
  }

  const ui = {
    loading,
    error,
    breadcrumbs,
    status,
  };

  const packages = {
    byQID,
    visibleRootQIDs,
  };

  return (
    <DrawingTool
      documentID={documentID}
      shared={shared}
      selectedTheme={selectedTheme}
      setSelectedTheme={setSelectedTheme}
      setFocusID={setFocusID}
      drawingRasterProps={drawingRasterProps}
      setDrawingRasterProps={setDrawingRasterProps}
      architecture={architecture}
      packages={packages}
      ui={ui}
      editable={editable}
    />
  );
};

DrawingToolInterface.propTypes = {
  documentID: PropTypes.string,
  architectureID: PropTypes.string.isRequired,
  shared: PropTypes.bool,
  published: PropTypes.bool,
};

DrawingToolInterface.defaultProps = {
  documentID: null,
  shared: false,
  published: false,
};

export default DrawingToolInterface;
