import React from 'react';
import PropTypes from 'prop-types';
import { drawingToolThemePropType } from 'proptypes';
import Toolbar from 'components/Toolbar/Toolbar.jsx';
import Link from 'components/Link/Link.jsx';
import Dropdown from 'components/Dropdown/Dropdown.jsx';
import Button from 'components/Button/Button.jsx';
import { IconCaretLeft, IconCaretDown } from 'components/Icons/Icons.jsx';
import DrawingToolThemeSelector from 'components/DrawingTool/DrawingToolThemeSelector.jsx';

const DrawingToolToolbar = ({
  documentID,
  selectedTheme,
  setSelectedTheme,
  onTakeScreenshot,
  shared,
  onConvertToPowerPoint,
}) => (
  <Toolbar>
    <Toolbar.Group fill padded>
      <Toolbar.Item>
        {!shared
          && documentID && (
          <Link to={documentID ? `/documents/${documentID}` : '/'}>
            <IconCaretLeft display="inlineBlock" position="left" />
            Back to Document
          </Link>
        )}
        {shared
          && documentID && (
          <Link to={documentID ? `/shared/${documentID}` : '/'}>
            <IconCaretLeft display="inlineBlock" position="left" />
            Back to Shared Document
          </Link>
        )}
      </Toolbar.Item>
    </Toolbar.Group>
    <Toolbar.Group padded>
      <Toolbar.Item>
        <Button unstyled onClick={onConvertToPowerPoint}>
          Convert to PowerPoint
        </Button>
      </Toolbar.Item>
    </Toolbar.Group>
    <Toolbar.Group padded>
      <Toolbar.Item>
        <Button unstyled onClick={onTakeScreenshot}>
          Take Screenshot
        </Button>
      </Toolbar.Item>
    </Toolbar.Group>
    <Toolbar.Group padded>
      <Toolbar.Item>
        <Dropdown>
          <Dropdown.Toggle>
            <Button unstyled>
              Select Theme
              <IconCaretDown display="inlineBlock" position="right" />
            </Button>
          </Dropdown.Toggle>
          <Dropdown.Menu position="right">
            <DrawingToolThemeSelector
              selectedTheme={selectedTheme}
              setSelectedTheme={setSelectedTheme}
            />
          </Dropdown.Menu>
        </Dropdown>
      </Toolbar.Item>
    </Toolbar.Group>
  </Toolbar>
);

DrawingToolToolbar.propTypes = {
  documentID: PropTypes.string,
  selectedTheme: drawingToolThemePropType.isRequired,
  setSelectedTheme: PropTypes.func.isRequired,
  onTakeScreenshot: PropTypes.func.isRequired,
  onConvertToPowerPoint: PropTypes.func.isRequired,
  shared: PropTypes.bool,
};

DrawingToolToolbar.defaultProps = {
  documentID: null,
  shared: false,
};

export default DrawingToolToolbar;
