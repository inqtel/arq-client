import React from 'react';
import PropTypes from 'prop-types';
import { wrapShape } from 'react-shape-editor';
import { getArrowCoordsFromAnnotation } from 'utils/drawingTool';
import DrawingToolAnnotationControls from 'components/DrawingTool/DrawingToolOverlay/DrawingToolAnnotationControls.jsx';

const DrawingToolAnnotation = ({
  width,
  height,
  rotation,
  imageURL,
  deleteAnnotation,
  rotateAnnotation,
  selectedTheme,
  active,
}) => {
  const renderSVG = () => {
    if (imageURL) {
      return (
        <image
          href={imageURL}
          x={0}
          y={0}
          height={height}
          width={width}
        />
      );
    }

    const coords = getArrowCoordsFromAnnotation(width, height, rotation);
    return (
      <line
        x1={coords.x1}
        y1={coords.y1}
        x2={coords.x2}
        y2={coords.y2}
        stroke="black"
        strokeWidth={10}
        markerEnd="url(#triangle)"
      />
    );
  };

  return (
    <>
      {renderSVG()}
      {active && (
      // NOTE: w & h must be larger than dimensions of controls or they will get clipped
      <foreignObject x="0" y="-44" width="100" height="100">
        <DrawingToolAnnotationControls
          onDelete={deleteAnnotation}
          onRotate={imageURL ? null : rotateAnnotation}
          selectedTheme={selectedTheme}
        />
      </foreignObject>
      )}
    </>
  );
};

DrawingToolAnnotation.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  rotation: PropTypes.number.isRequired,
  imageURL: PropTypes.string,
  deleteAnnotation: PropTypes.func.isRequired,
  rotateAnnotation: PropTypes.func,
  selectedTheme: PropTypes.shape({}).isRequired,
  active: PropTypes.bool,
};

DrawingToolAnnotation.defaultProps = {
  imageURL: null,
  active: false,
  rotateAnnotation: null,
};

export default wrapShape(DrawingToolAnnotation);
