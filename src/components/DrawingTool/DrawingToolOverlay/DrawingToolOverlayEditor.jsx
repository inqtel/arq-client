import React from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import { ShapeEditor } from 'react-shape-editor';
import DrawingToolAnnotation from 'components/DrawingTool/DrawingToolOverlay/DrawingToolAnnotation.jsx';

const DrawingToolOverlayEditor = ({
  annotations,
  updateAnnotation,
  rotateAnnotation,
  selectedAnnotationID,
  setSelectedAnnotationID,
  removeAnnotation,
  selectedTheme,
  size,
}) => {
  const shapes = annotations.map((item, index) => {
    const {
      id,
      x,
      y,
      width,
      height,
      rotation,
      organization,
    } = item;

    const imageURL = get(organization, 'logo');

    return (
      <DrawingToolAnnotation
        key={id}
        active={selectedAnnotationID === id}
        isInSelectionGroup={selectedAnnotationID !== id}
        shapeId={id}
        shapeIndex={index}
        height={height}
        width={width}
        x={x}
        y={y}
        imageURL={imageURL}
        rotation={rotation}
        onFocus={() => setSelectedAnnotationID(id)}
        onChange={(newRect) => {
          updateAnnotation(id, {
            x: newRect.x,
            y: newRect.y,
            width: newRect.width,
            height: newRect.height,
            rotation,
          });
        }}
        onDelete={() => removeAnnotation(id)}
        deleteAnnotation={() => removeAnnotation(id)}
        rotateAnnotation={() => rotateAnnotation(id)}
        selectedTheme={selectedTheme}
      />
    );
  });

  return (
    <ShapeEditor vectorWidth={size.width} vectorHeight={size.height}>
      <marker
        id="triangle"
        viewBox="0 0 10 10"
        refX="0"
        refY="5"
        markerUnits="strokeWidth"
        markerWidth="4"
        markerHeight="3"
        orient="auto"
      >
        <path d="M 0 0 L 10 5 L 0 10 z" />
      </marker>
      {shapes}
    </ShapeEditor>
  );
};

DrawingToolOverlayEditor.propTypes = {
  annotations: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  updateAnnotation: PropTypes.func.isRequired,
  removeAnnotation: PropTypes.func.isRequired,
  rotateAnnotation: PropTypes.func.isRequired,
  selectedAnnotationID: PropTypes.string,
  setSelectedAnnotationID: PropTypes.func.isRequired,
  size: PropTypes.shape({
    width: PropTypes.number,
    height: PropTypes.number,
  }).isRequired,
  selectedTheme: PropTypes.shape({}).isRequired,
};

DrawingToolOverlayEditor.defaultProps = {
  selectedAnnotationID: null,
};

export default DrawingToolOverlayEditor;
