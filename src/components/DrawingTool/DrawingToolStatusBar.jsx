import React from 'react';
import PropTypes from 'prop-types';

const DrawingToolStatusBar = ({ status }) => (
  <div className="DrawingTool-statusBar">
    <span className="DrawingTool-statusBar-status">{status}</span>
  </div>
);

DrawingToolStatusBar.propTypes = {
  status: PropTypes.string,
};

DrawingToolStatusBar.defaultProps = {
  status: '',
};

export default DrawingToolStatusBar;
