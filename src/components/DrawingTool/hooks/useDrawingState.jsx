import { useCallback } from 'react';
import { useQuery } from '@apollo/client';
import { themes } from 'constants/drawingTool';
import { GET_DRAWING_STATE } from 'queries/drawing';
import { drawingStateVar, drawingState as initialDrawingState } from 'apollo/cache';

const useDrawingState = () => {
  // get drawing state
  const { data: { drawingState } } = useQuery(GET_DRAWING_STATE);

  const {
    architectureID,
    focusID,
    selectedThemeName,
    drawingRasterProps,
  } = drawingState;

  const initDrawing = useCallback((id) => {
    drawingStateVar({
      ...initialDrawingState,
      architectureID: id,
      focusID: id,
    });
  }, []);

  const resetDrawing = useCallback(() => {
    drawingStateVar(initialDrawingState);
  }, []);

  const setSelectedTheme = useCallback((theme) => {
    drawingStateVar({
      ...drawingState,
      selectedThemeName: theme.name,
    });
  }, [drawingState]);

  const setDrawingRasterProps = useCallback((html, width, height, devicePixelRatio) => {
    drawingStateVar({
      ...drawingState,
      drawingRasterProps: {
        ...drawingRasterProps,
        html,
        width,
        height,
        devicePixelRatio,
      },
    });
  }, [drawingRasterProps, drawingState]);

  const setFocusID = useCallback((id) => {
    drawingStateVar({
      ...drawingState,
      focusID: id,
    });
  }, [drawingState]);

  const selectedTheme = themes.find(t => t.name === selectedThemeName);

  return {
    architectureID,
    selectedTheme,
    setSelectedTheme,
    focusID,
    setFocusID,
    drawingRasterProps,
    setDrawingRasterProps,
    initDrawing,
    resetDrawing,
  };
};

export default useDrawingState;
