import { useMutation, useQuery } from '@apollo/client';
import { v4 as uuid } from 'uuid';
import { get } from 'lodash';
import {
  COMPANY_TYPENAME,
  PACKAGE_TYPENAME,
  PACKAGE_ANNOTATION_TYPENAME,
  PACKAGE_QUERY_ROOT,
} from 'constants/gql';
import {
  CREATE_ANNOTATION_SPEC,
  UPDATE_ANNOTATION_SPEC,
  DELETE_ANNOTATION_SPEC,
  GET_PACKAGE_ANNOTATIONS,
} from 'queries/drawing';
import fragments from 'queries/fragments';

const usePackageAnnotations = ({ packageQID: packageId, editable }) => {
  // get annotations for root package
  const { data, client } = useQuery(GET_PACKAGE_ANNOTATIONS, { variables: { id: packageId } });
  const annotations = get(data, [PACKAGE_QUERY_ROOT, 'annotations'], []);

  // helper to create new annotation
  const createAnnotationSpec = (company) => {
    const annotationBase = {
      id: uuid(),
      x: 40,
      y: 40,
      width: 100,
      height: 100,
      rotation: 0,
    };

    // Would be nice to clean this up so writes to cache and DB are isomorphic
    return {
      newAnnotation: {
        ...annotationBase,
        organization: company ? {
          ...company,
          __typename: COMPANY_TYPENAME,
        } : null,
        __typename: PACKAGE_ANNOTATION_TYPENAME,
      },
      object: {
        ...annotationBase,
        organizationId: company ? company.id : null,
        packageId,
      },
    };
  };

  // helper to update package annotations in cache
  const updatePackageAnnotationsCache = (newAnnotation) => {
    client.writeQuery({
      query: GET_PACKAGE_ANNOTATIONS,
      data: {
        [PACKAGE_QUERY_ROOT]: {
          id: packageId,
          annotations: [...annotations, newAnnotation],
          __typename: PACKAGE_TYPENAME,
        },
      },
    });
  };

  // add annotation in store and DB, link to package
  const [createAnnotation] = useMutation(CREATE_ANNOTATION_SPEC);
  const addPackageAnnotation = (company) => {
    const { newAnnotation, object } = createAnnotationSpec(company);
    updatePackageAnnotationsCache(newAnnotation);

    editable && createAnnotation({
      variables: { object },
    });
  };

  // delete annotation from DB and unlink in store
  const [deleteAnnotation] = useMutation(DELETE_ANNOTATION_SPEC);
  const removePackageAnnotation = (id) => {
    client.writeQuery({
      query: GET_PACKAGE_ANNOTATIONS,
      data: {
        [PACKAGE_QUERY_ROOT]: {
          id: packageId,
          annotations: annotations.filter(a => a.id !== id),
          __typename: PACKAGE_TYPENAME,
        },
      },
    });

    editable && deleteAnnotation({ variables: { id } });
  };

  // update drawing spec in store and DB
  const [updateAnnotationSpec] = useMutation(UPDATE_ANNOTATION_SPEC);
  const updatePackageAnnotation = (id, annotationData) => {
    client.writeFragment({
      id: `${PACKAGE_ANNOTATION_TYPENAME}:${id}`,
      fragment: fragments.annotationFields,
      data: annotationData,
    });

    editable && updateAnnotationSpec({
      variables: {
        id,
        ...annotationData,
      },
    });
  };

  // rotate annotation in store and DB
  const rotatePackageAnnotation = (id) => {
    const annotation = client.readFragment({
      id: `${PACKAGE_ANNOTATION_TYPENAME}:${id}`,
      fragment: fragments.annotationFields,
    });

    const rotation = annotation.rotation >= 315 ? 0 : annotation.rotation + 45;
    const { organization, __typename, ...object } = annotation;

    updatePackageAnnotation(annotation.id, {
      ...object,
      rotation,
    });
  };

  return {
    annotations,
    addPackageAnnotation,
    removePackageAnnotation,
    updatePackageAnnotation,
    rotatePackageAnnotation,
  };
};

export default usePackageAnnotations;
