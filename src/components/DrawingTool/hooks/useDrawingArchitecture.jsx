import { get } from 'lodash';
import { useQuery } from '@apollo/client';
import { GET_DRAWING_ARCHITECTURE } from 'queries/drawing';
import { ARCHITECTURE_QUERY_ROOT } from 'constants/gql';

const useDrawingArchitecture = (id) => {
  // Get architecture data
  // Needs to be fetched from network first time in case new packages added
  // thereafter fetched from cache to avoid drawing re-renders
  const { loading, error, data } = useQuery(GET_DRAWING_ARCHITECTURE, {
    variables: { id },
    fetchPolicy: 'network-only',
    nextFetchPolicy: 'cache-only',
  });

  const architecture = get(data, ARCHITECTURE_QUERY_ROOT, {});

  return {
    loading,
    error,
    architecture,
  };
};

export default useDrawingArchitecture;
