import { keyBy, get } from 'lodash';
import { setUp, setUpMount, checkProps } from 'utils/testing';
import { architectureFlatPackages as architecture } from 'mocks/architectures';
import { themes } from 'constants/drawingTool';
import { generateDrawingBreadcrumbsTrail } from 'utils/drawingTool';
import { drawingState } from 'apollo/cache';
import DrawingTool from 'components/DrawingTool/DrawingTool.jsx';
import DrawingToolArchitecture from 'components/DrawingTool/DrawingToolArchitecture.jsx';

const byQID = keyBy(get(architecture, 'allPackages', []), p => p.id);
const visibleRootQIDs = architecture.packages.map(p => p.id);
const breadcrumbs = generateDrawingBreadcrumbsTrail(architecture, byQID, architecture.id);

describe('<DrawingTool />', () => {
  let wrapper;

  const expectedProps = {
    ui: {
      loading: false,
      error: null,
      breadcrumbs,
    },
    shared: false,
    architecture,
    packages: {
      visibleRootQIDs,
      byQID,
    },
    setFocusID: () => {},
    selectedTheme: themes[0],
    setSelectedTheme: () => {},
    drawingRasterProps: drawingState.drawingRasterProps,
    setDrawingRasterProps: () => {},
    editable: false,
  };

  beforeEach(() => {
    wrapper = setUp(DrawingTool, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(DrawingTool, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .DrawingTool class', () => {
    expect(wrapper.is('.DrawingTool')).toBe(true);
  });
});

describe('<DrawingToolArchitecture />', () => {
  let wrapper;

  const expectedProps = {
    visibleRootPackageQIDs: [],
    packagesByQID: {},
    selectedTheme: themes[0],
    setFocusID: () => {},
    setDrawingRasterProps: () => {},
    editable: false,
  };

  beforeEach(() => {
    wrapper = setUpMount(DrawingToolArchitecture, expectedProps);
    const instance = wrapper.instance();
    instance.$architecture = { getBoundingClientRect: jest.fn() };
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(DrawingToolArchitecture, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .DrawingTool-architecture class', () => {
    expect(wrapper.childAt(0).is('.DrawingTool-architecture')).toBe(true);
  });
});
