import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button/Button.jsx';

class DrawingToolBreadcrumbsItem extends Component {
  setFocusID = () => {
    const { breadcrumb, setFocusID } = this.props;

    setFocusID(breadcrumb.id);
  }

  render() {
    const { breadcrumb } = this.props;

    return (
      <li className="DrawingTool-breadcrumbs-item">
        <Button unstyled onClick={this.setFocusID}>
          {breadcrumb.title}
        </Button>
      </li>
    );
  }
}

DrawingToolBreadcrumbsItem.propTypes = {
  breadcrumb: PropTypes.shape({
    qid: PropTypes.string,
    title: PropTypes.string,
  }).isRequired,
  setFocusID: PropTypes.func.isRequired,
};

DrawingToolBreadcrumbsItem.defaultProps = {};

export default DrawingToolBreadcrumbsItem;
