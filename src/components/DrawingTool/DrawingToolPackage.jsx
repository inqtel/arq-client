/* eslint-disable import/no-cycle */
import React from 'react';
import PropTypes from 'prop-types';
import { packagePropType, drawingToolThemePropType } from 'proptypes';
import {
  POSITION_DEFAULT,
  POSITION_LEFT,
  POSITION_RIGHT,
} from 'constants/drawingTool.js';
import DrawingToolPackagePositionDefault from 'components/DrawingTool/DrawingToolPackagePositionDefault.jsx';
import DrawingToolPackagePositionLeft from 'components/DrawingTool/DrawingToolPackagePositionLeft.jsx';
import DrawingToolPackagePositionRight from 'components/DrawingTool/DrawingToolPackagePositionRight.jsx';

const DrawingToolPackage = ({
  pkg,
  packagesByQID,
  level,
  position,
  selectedTheme,
  setFocusID,
  numColumns,
  editable,
}) => {
  if (position === POSITION_DEFAULT) {
    return (
      <DrawingToolPackagePositionDefault
        pkg={pkg}
        packagesByQID={packagesByQID}
        level={level}
        position={POSITION_DEFAULT}
        selectedTheme={selectedTheme}
        setFocusID={setFocusID}
        numColumns={numColumns}
        editable={editable}
      />
    );
  } if (position === POSITION_LEFT) {
    return (
      <DrawingToolPackagePositionLeft
        pkg={pkg}
        packagesByQID={packagesByQID}
        level={level}
        position={POSITION_LEFT}
        selectedTheme={selectedTheme}
        setFocusID={setFocusID}
        editable={editable}
      />
    );
  } if (position === POSITION_RIGHT) {
    return (
      <DrawingToolPackagePositionRight
        pkg={pkg}
        packagesByQID={packagesByQID}
        level={level}
        position={POSITION_RIGHT}
        selectedTheme={selectedTheme}
        setFocusID={setFocusID}
        editable={editable}
      />
    );
  }
  return null;
};

DrawingToolPackage.propTypes = {
  pkg: packagePropType.isRequired,
  packagesByQID: PropTypes.shape({}).isRequired,
  level: PropTypes.number.isRequired,
  position: PropTypes.number.isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  setFocusID: PropTypes.func.isRequired,
  numColumns: PropTypes.number,
  editable: PropTypes.bool.isRequired,
};

DrawingToolPackage.defaultProps = {
  numColumns: null,
};

export default DrawingToolPackage;
