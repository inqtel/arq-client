import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { drawingToolThemePropType } from 'proptypes';
import DrawingToolPackages from 'components/DrawingTool/DrawingToolPackages.jsx';
import DrawingToolFooter from 'components/DrawingTool/DrawingToolFooter.jsx';

class DrawingToolArchitecture extends Component {
  componentDidMount() {
    this.setDrawingRasterProps();
  }

  setDrawingRasterProps = () => {
    const devicePixelRatio = (() => {
      if (window.devicePixelRatio && window.devicePixelRatio > 1) {
        return window.devicePixelRatio;
      }

      return 1;
    })();

    const { setDrawingRasterProps } = this.props;

    const architectureRect = this.$architecture.getBoundingClientRect();

    setDrawingRasterProps(
      this.$architecture.innerHTML,
      architectureRect.width,
      architectureRect.height,
      devicePixelRatio,
    );
  }

  render() {
    const {
      visibleRootPackageQIDs,
      packagesByQID,
      selectedTheme,
      setFocusID,
      editable,
    } = this.props;

    return (
      <div
        className="DrawingTool-architecture"
        ref={($architecture) => {
          this.$architecture = $architecture;
        }}
      >
        <DrawingToolPackages
          packageQIDs={visibleRootPackageQIDs}
          packagesByQID={packagesByQID}
          selectedTheme={selectedTheme}
          setFocusID={setFocusID}
          editable={editable}
        />
        <DrawingToolFooter />
      </div>
    );
  }
}

DrawingToolArchitecture.propTypes = {
  visibleRootPackageQIDs: PropTypes.arrayOf(PropTypes.string).isRequired,
  packagesByQID: PropTypes.shape({}).isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  setFocusID: PropTypes.func.isRequired,
  setDrawingRasterProps: PropTypes.func.isRequired,
  editable: PropTypes.bool.isRequired,
};

DrawingToolArchitecture.defaultProps = {};

export default DrawingToolArchitecture;
