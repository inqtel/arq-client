import React from 'react';
import PropTypes from 'prop-types';

function DrawingToolContent({ children }) {
  return <div className="DrawingTool-content">{children}</div>;
}

DrawingToolContent.propTypes = {
  children: PropTypes.node,
};

DrawingToolContent.defaultProps = {
  children: null,
};

export default DrawingToolContent;
