import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { packagePropType, drawingToolThemePropType } from 'proptypes';
import {
  POSITION_DEFAULT,
  POSITION_LEFT,
  POSITION_RIGHT,
} from 'constants/drawingTool.js';
import DrawingToolPackageControlsPositionDefault from 'components/DrawingTool/DrawingToolPackageControls/DrawingToolPackageControlsPositionDefault.jsx';
import DrawingToolPackageControlsPositionLeft from 'components/DrawingTool/DrawingToolPackageControls/DrawingToolPackageControlsPositionLeft.jsx';
import DrawingToolPackageControlsPositionRight from 'components/DrawingTool/DrawingToolPackageControls/DrawingToolPackageControlsPositionRight.jsx';
import DrawingToolCompanyModal from 'components/DrawingTool/DrawingToolCompanyModal.jsx';
import useDrawingToolPackageControls from 'components/DrawingTool/DrawingToolPackageControls/useDrawingToolPackageControls.jsx';
import usePackageAnnotations from 'components/DrawingTool/hooks/usePackageAnnotations.jsx';

const DrawingToolPackageControls = ({
  pkg,
  level,
  position,
  selectedTheme,
  setFocusID,
  numColumns,
  editable,
}) => {
  const [controlsOpen, setControlsOpen] = useState(false);
  const [companyModalOpen, setCompanyModalOpen] = useState(false);

  const {
    onPackagePositionDefault,
    onPackagePositionLeft,
    onPackagePositionRight,
    onPackageTitlePositionDefault,
    onPackageTitlePositionLeft,
    onPackageTitlePositionRight,
    onPackageSetColumns,
    onPackageToggleChildren,
  } = useDrawingToolPackageControls({
    pkg,
    level,
    editable,
  });

  const { addPackageAnnotation } = usePackageAnnotations({
    packageQID: pkg.id,
    editable,
  });

  const onPackageAddImage = (company) => {
    if (company.logo) {
      addPackageAnnotation(company);
    } else {
      setCompanyModalOpen(true);
    }
  };

  const onPackageAddArrow = () => {
    addPackageAnnotation();
  };

  if (position === POSITION_DEFAULT) {
    return (
      <>
        <DrawingToolPackageControlsPositionDefault
          pkg={pkg}
          level={level}
          selectedTheme={selectedTheme}
          setFocusID={setFocusID}
          packagePositionDefault={onPackagePositionDefault}
          packagePositionLeft={onPackagePositionLeft}
          packagePositionRight={onPackagePositionRight}
          packageTitlePositionDefault={onPackageTitlePositionDefault}
          packageTitlePositionLeft={onPackageTitlePositionLeft}
          packageTitlePositionRight={onPackageTitlePositionRight}
          packageAddAnnotation={onPackageAddArrow}
          packageAddImage={onPackageAddImage}
          setNumColumns={onPackageSetColumns}
          packageToggleChildren={onPackageToggleChildren}
          controlsOpen={controlsOpen}
          onControlsOpen={() => setControlsOpen(true)}
          onControlsClose={() => setControlsOpen(false)}
          numColumns={numColumns}
        />
        <DrawingToolCompanyModal
          companyModalOpen={companyModalOpen}
          setCompanyModalOpen={setCompanyModalOpen}
        />
      </>
    );
  } if (position === POSITION_LEFT) {
    return (
      <DrawingToolPackageControlsPositionLeft
        pkg={pkg}
        level={level}
        selectedTheme={selectedTheme}
        setFocusID={setFocusID}
        packagePositionDefault={onPackagePositionDefault}
        packagePositionLeft={onPackagePositionLeft}
        packagePositionRight={onPackagePositionRight}
        controlsOpen={controlsOpen}
        onControlsOpen={() => setControlsOpen(true)}
        onControlsClose={() => setControlsOpen(false)}
      />
    );
  } if (position === POSITION_RIGHT) {
    return (
      <DrawingToolPackageControlsPositionRight
        pkg={pkg}
        level={level}
        selectedTheme={selectedTheme}
        setFocusID={setFocusID}
        packagePositionDefault={onPackagePositionDefault}
        packagePositionLeft={onPackagePositionLeft}
        packagePositionRight={onPackagePositionRight}
        controlsOpen={controlsOpen}
        onControlsOpen={() => setControlsOpen(true)}
        onControlsClose={() => setControlsOpen(false)}
      />
    );
  }
  return null;
};

DrawingToolPackageControls.propTypes = {
  pkg: packagePropType.isRequired,
  level: PropTypes.number.isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  setFocusID: PropTypes.func.isRequired,
  position: PropTypes.number.isRequired,
  numColumns: PropTypes.number,
  editable: PropTypes.bool.isRequired,
};

DrawingToolPackageControls.defaultProps = {
  numColumns: null,
};

export default DrawingToolPackageControls;
