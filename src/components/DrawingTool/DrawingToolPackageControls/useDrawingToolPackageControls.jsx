import usePackageDrawing from 'components/DrawingTool/hooks/usePackageDrawing.jsx';

const useDrawingToolPackageControls = ({ pkg, level, editable }) => {
  const { updatePackageDrawings } = usePackageDrawing({ editable });

  const updatePackageDrawing = (key, value) => {
    updatePackageDrawings([
      {
        id: pkg.id,
        [key]: value,
      },
    ]);
  };

  const packagePositionDefault = () => updatePackageDrawing(`level${level}position`, 0);
  const packagePositionLeft = () => updatePackageDrawing(`level${level}position`, 1);
  const packagePositionRight = () => updatePackageDrawing(`level${level}position`, 2);

  const packageTitlePositionDefault = () => updatePackageDrawing(`level${level}titlePosition`, 0);
  const packageTitlePositionLeft = () => updatePackageDrawing(`level${level}titlePosition`, 1);
  const packageTitlePositionRight = () => updatePackageDrawing(`level${level}titlePosition`, 2);

  const packageSetColumns = columns => updatePackageDrawing('level1columns', columns);

  const packageToggleChildren = () => updatePackageDrawing('level1hideChildren', !pkg.level1hideChildren);

  return {
    onPackagePositionDefault: packagePositionDefault,
    onPackagePositionLeft: packagePositionLeft,
    onPackagePositionRight: packagePositionRight,
    onPackageTitlePositionDefault: packageTitlePositionDefault,
    onPackageTitlePositionLeft: packageTitlePositionLeft,
    onPackageTitlePositionRight: packageTitlePositionRight,
    onPackageSetColumns: packageSetColumns,
    onPackageToggleChildren: packageToggleChildren,
  };
};

export default useDrawingToolPackageControls;
