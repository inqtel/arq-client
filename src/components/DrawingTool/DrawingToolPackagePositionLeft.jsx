import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { packagePropType, drawingToolThemePropType } from 'proptypes';
import cx from 'classnames';
import DrawingToolPackageControls from 'components/DrawingTool/DrawingToolPackageControls/DrawingToolPackageControls.jsx';

class DrawingToolPackagePositionLeft extends Component {
  onDoubleClick = (e) => {
    e.preventDefault();
    e.stopPropagation();

    const { pkg, level, setFocusID } = this.props;

    if (level === 1 || level === 2) {
      setFocusID(pkg.id);
    }
  }

  render() {
    const {
      pkg,
      level,
      position,
      selectedTheme,
      setFocusID,
      editable,
    } = this.props;

    return (
      <div
        className={cx('DrawingTool-packagePositionLeft', `level-${level}`)}
        onDoubleClick={this.onDoubleClick}
      >
        {level <= 1 && (
          <DrawingToolPackageControls
            pkg={pkg}
            level={level}
            position={position}
            selectedTheme={selectedTheme}
            setFocusID={setFocusID}
            editable={editable}
          />
        )}
        <div
          className={cx(
            'DrawingTool-packagePositionLeft-header',
            `level-${level}`,
          )}
        >
          <h2
            className={cx(
              'DrawingTool-packagePositionLeft-heading',
              `level-${level}`,
            )}
          >
            {pkg.term}
          </h2>
        </div>
      </div>
    );
  }
}

DrawingToolPackagePositionLeft.propTypes = {
  pkg: packagePropType.isRequired,
  level: PropTypes.number.isRequired,
  position: PropTypes.number.isRequired,
  selectedTheme: drawingToolThemePropType.isRequired,
  setFocusID: PropTypes.func.isRequired,
  editable: PropTypes.bool.isRequired,
};

DrawingToolPackagePositionLeft.defaultProps = {};

export default DrawingToolPackagePositionLeft;
