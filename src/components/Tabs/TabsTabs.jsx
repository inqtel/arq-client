import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

class TabsTabs extends PureComponent {
  onTabChange = (index, e) => {
    e.preventDefault();

    const { onTabChange } = this.props;
    onTabChange(index);
  }

  render() {
    const { children, selected, tabOptions } = this.props;

    return (
      <ul className="Tabs-tabs">
        {React.Children.toArray(children).map((child, index) => (
          <li
            key={child.key}
            className={cx('Tabs-tab',
              tabOptions && `Tabs--align-${tabOptions.tabAlign}`,
              tabOptions && `Tabs--bgColor-${tabOptions.bgColor}`,
              tabOptions && tabOptions.hasOutline && 'Tabs--outline', {
                'is-active': selected === index,
              })}
            onClick={this.onTabChange.bind(this, index)}
          >
            {child.props.label}
          </li>
        ))}
      </ul>
    );
  }
}

TabsTabs.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.array]),
  onTabChange: PropTypes.func.isRequired,
  selected: PropTypes.number,
  tabOptions: PropTypes.shape({
    color: PropTypes.string,
    textAlign: PropTypes.string,
  }),
};

TabsTabs.defaultProps = {
  selected: null,
  tabOptions: {
    tabAlign: null,
    color: null,
  },
};

export default TabsTabs;
