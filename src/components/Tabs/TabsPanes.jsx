import React from 'react';

const TabsPanes = ({ children, selected }) => (
  <div className="Tabs-panes">
    {React.Children.toArray(children)[selected]}
  </div>
);

TabsPanes.propTypes = {};

TabsPanes.defaultProps = {};

export default TabsPanes;
