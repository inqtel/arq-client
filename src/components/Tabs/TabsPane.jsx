import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const TabsPane = ({ children, hasBackground }) => (
  <div
    className={cx('Tabs-pane', {
      'Tabs--pane-background': hasBackground,
    })}
  >
    {children}
  </div>
);

TabsPane.propTypes = {
  children: PropTypes.node,
  hasBackground: PropTypes.bool,
};

TabsPane.defaultProps = {
  children: null,
  hasBackground: null,
};

export default TabsPane;
