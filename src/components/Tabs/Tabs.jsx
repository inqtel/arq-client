import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { noop } from 'lodash';
import TabsTabs from 'components/Tabs/TabsTabs.jsx';
import TabsPanes from 'components/Tabs/TabsPanes.jsx';
import TabsPane from 'components/Tabs/TabsPane.jsx';
import 'components/Tabs/Tabs.scss';

const Tabs = ({
  children,
  compact,
  onTabChange,
  selectedTab,
  tabOptions,
}) => (
  <div
    className={cx('Tabs', {
      'Tabs--compact': compact,
    })}
  >
    <TabsTabs
      selected={selectedTab}
      onTabChange={onTabChange}
      tabOptions={tabOptions}
    >
      {children}
    </TabsTabs>
    <TabsPanes
      selected={selectedTab}
    >
      {children}
    </TabsPanes>
  </div>
);

Tabs.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.array]).isRequired,
  compact: PropTypes.bool,
  onTabChange: PropTypes.func,
  selectedTab: PropTypes.number.isRequired,
  tabOptions: PropTypes.shape({
    color: PropTypes.string,
    textAlign: PropTypes.string,
  }),
};

Tabs.defaultProps = {
  compact: false,
  onTabChange: noop,
  tabOptions: null,
};

Tabs.Pane = TabsPane;

export default Tabs;
