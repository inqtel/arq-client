import React from 'react';
import PropTypes from 'prop-types';
import { architecturePropType } from 'proptypes';
import Heading from 'components/Heading/Heading.jsx';
import ArchitecturesLibraryCard from 'components/ArchitecturesLibrary/ArchitecturesLibraryCard.jsx';

const ArchitecturesLibraryContent = ({
  publishedArchitectures,
  column,
}) => (
  <div className="ArchitecturesLibrary-content">
    <Heading level={2}>
      {publishedArchitectures.length > 0
        ? 'Published Architectures'
        : 'No Published Architectures'}
    </Heading>
    <div className="ArchitecturesLibrary-cards">
      {publishedArchitectures.map(architecture => (
        <ArchitecturesLibraryCard
          key={architecture.id}
          architecture={architecture}
          fullWidth={column}
        />
      ))}
    </div>
  </div>
);

ArchitecturesLibraryContent.propTypes = {
  publishedArchitectures: PropTypes.arrayOf(architecturePropType).isRequired,
  column: PropTypes.bool,
};

ArchitecturesLibraryContent.defaultProps = {
  column: false,
};

export default ArchitecturesLibraryContent;
