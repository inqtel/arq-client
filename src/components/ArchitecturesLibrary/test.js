import React from 'react';
import ArchitecturesLibrary from 'components/ArchitecturesLibrary/ArchitecturesLibrary.jsx';

describe('<ArchitecturesLibrary />', () => {
  it('should render with .ArchitectureReviewTerms class', () => {
    const wrapper = shallow(<ArchitecturesLibrary />);
    expect(wrapper.is('.ArchitecturesLibrary')).toBe(true);
  });
});
