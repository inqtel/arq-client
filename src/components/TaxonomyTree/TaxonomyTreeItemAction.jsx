import React from 'react';
import PropTypes from 'prop-types';
import { isEmpty } from 'lodash';
import Tooltip from 'components/Tooltip/Tooltip.jsx';
import Button from 'components/Button/Button.jsx';

const TaxonomyTreeItemAction = ({
  taxonomy,
  action,
}) => {
  const { title, icon, handler } = action;

  const handleClick = () => {
    handler(taxonomy);
  };

  if (isEmpty(action)) return <div className="TaxonomyTree-item-action-spacer" />;

  return (
    <div className="TaxonomyTree-item-action">
      <Tooltip content={title}>
        <Tooltip.Wrapper>
          <Button unstyled onClick={handleClick}>
            {icon}
          </Button>
        </Tooltip.Wrapper>
      </Tooltip>
    </div>
  );
};

TaxonomyTreeItemAction.propTypes = {
  taxonomy: PropTypes.object.isRequired,
  action: PropTypes.object.isRequired,
};

TaxonomyTreeItemAction.defaultProps = {};

export default TaxonomyTreeItemAction;
