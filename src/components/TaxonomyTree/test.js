import React from 'react';
import { taxonomy } from 'mocks/taxonomy';
import { treeActions } from 'mocks/treeActions';
import TaxonomyTree from 'components/TaxonomyTree/TaxonomyTree.jsx';

describe('<TaxonomyTree />', () => {
  it('should render with .TaxonomyTree class', () => {
    const wrapper = shallow(
      <TaxonomyTree taxonomy={taxonomy} />,
    );

    expect(wrapper.is('.TaxonomyTree')).toBe(true);
  });

  it('should render actions when passed in', () => {
    const wrapper = mount(
      <TaxonomyTree taxonomy={taxonomy} actions={treeActions} />,
    );

    expect(wrapper.exists('.TaxonomyTree-item-actions')).toEqual(true);
  });
});
