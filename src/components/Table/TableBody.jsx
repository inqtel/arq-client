import React from 'react';
import PropTypes from 'prop-types';

function TableBody({ children, ...props }) {
  return (
    <tbody className="Table-body" {...props}>
      {children}
    </tbody>
  );
}

TableBody.propTypes = {
  children: PropTypes.node,
};

TableBody.defaultProps = {
  children: null,
};

export default TableBody;
