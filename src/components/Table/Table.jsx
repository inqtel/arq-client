import React from 'react';
import PropTypes from 'prop-types';
import TableHeader from 'components/Table/TableHeader.jsx';
import TableBody from 'components/Table/TableBody.jsx';
import TableFooter from 'components/Table/TableFooter.jsx';
import TableRow from 'components/Table/TableRow.jsx';
import TableCell from 'components/Table/TableCell.jsx';
import 'components/Table/Table.scss';

function Table({ children, ...props }) {
  return (
    <table className="Table" {...props}>
      {children}
    </table>
  );
}

Table.propTypes = {
  children: PropTypes.node,
};

Table.defaultProps = {
  children: null,
};

Table.Header = TableHeader;
Table.Body = TableBody;
Table.Footer = TableFooter;
Table.Row = TableRow;
Table.Cell = TableCell;

export default Table;
