import React from 'react';
import PropTypes from 'prop-types';

function TableRow({ children, ...props }) {
  return (
    <tr className="Table-row" {...props}>
      {children}
    </tr>
  );
}

TableRow.propTypes = {
  children: PropTypes.node,
};

TableRow.defaultProps = {
  children: null,
};

export default TableRow;
