import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

function ButtonGroup({
  children, fill, align, gutter,
}) {
  return (
    <div
      className={cx('ButtonGroup', {
        'ButtonGroup--fill': fill,
        [`ButtonGroup--align-${align}`]: align,
        [`ButtonGroup--gutter-${gutter}`]: gutter,
      })}
    >
      {children}
    </div>
  );
}

ButtonGroup.propTypes = {
  children: PropTypes.node.isRequired,
  fill: PropTypes.bool,
  align: PropTypes.string,
  gutter: PropTypes.oneOf(['sm', 'md', 'lg', 'xl']),
};

ButtonGroup.defaultProps = {
  fill: false,
  align: 'left',
  gutter: null,
};

export default ButtonGroup;
