import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { noop } from 'lodash';
import ButtonGroup from 'components/Button/ButtonGroup.jsx';
import Link from 'components/Link/Link.jsx';
import 'components/Button/Button.scss';

const Button = (props) => {
  const {
    children,
    display,
    appearance,
    size,
    icon,
    iconOnly,
    unstyled,
    readOnly,
    onClick,
    asLink,
    linkTo,
  } = props;

  const buttonClassnames = cx('Button', {
    [`Button--display-${display}`]: display,
    [`Button--appearance-${appearance}`]: appearance && !unstyled,
    [`Button--size-${size}`]: size && !unstyled,
    'Button--icon': icon,
    'Button--iconOnly': iconOnly && !unstyled,
    'Button--unstyled': unstyled,
    'Button--readOnly': readOnly,
  });

  if (asLink) {
    return (
      <Link to={linkTo} className={buttonClassnames}>
        {children}
      </Link>
    );
  }
  return (
    <button
      className={buttonClassnames}
      onClick={onClick}
      disabled={readOnly}
      type="button"
    >
      {children}
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.node.isRequired,
  display: PropTypes.string,
  appearance: PropTypes.string,
  size: PropTypes.string,
  icon: PropTypes.bool,
  iconOnly: PropTypes.bool,
  unstyled: PropTypes.bool,
  readOnly: PropTypes.bool,
  onClick: PropTypes.func,
  asLink: PropTypes.bool,
  linkTo: PropTypes.string,
};

Button.defaultProps = {
  display: 'inlineBlock',
  appearance: 'primary',
  size: 'sm',
  icon: false,
  iconOnly: false,
  unstyled: false,
  readOnly: false,
  onClick: noop,
  asLink: false,
  linkTo: null,
};

Button.Group = ButtonGroup;

export default Button;
