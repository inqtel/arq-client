import React from 'react';
import { PropTypes } from 'prop-types';

const Panel = ({ children }) => <div className="Panel-heading">{children}</div>;

Panel.propTypes = {
  children: PropTypes.node,
};

Panel.defaultProps = {
  children: null,
};

export default Panel;
