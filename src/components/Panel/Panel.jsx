import React from 'react';
import { PropTypes } from 'prop-types';
import PanelHeading from 'components/Panel/PanelHeading.jsx';
import PanelContent from 'components/Panel/PanelContent.jsx';
import 'components/Panel/Panel.scss';

const Panel = ({ children }) => <div className="Panel">{children}</div>;

Panel.propTypes = {
  children: PropTypes.node,
};

Panel.defaultProps = {
  children: null,
};

Panel.Heading = PanelHeading;
Panel.Content = PanelContent;

export default Panel;
