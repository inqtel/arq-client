import { useState } from 'react';
import { useQuery } from '@apollo/client';
import { GET_PITCHBOOK_INTERSECTION } from 'queries/companies';
import { COMPANIES_QUERY_ROOT } from 'constants/gql';
import { get, keyBy } from 'lodash';

const usePitchbookIntersectionHooks = () => {
  const [pitchbookById, setPitchbookById] = useState([]);
  const [pitchbookIds, setPitchbookIds] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const [filterToggle, setFilterToggle] = useState(false);

  const pitchbookIdsStrings = pitchbookIds.map(b => `${b}\\,`);

  const {
    loading,
    error,
    data,
    refetch,
  } = useQuery(GET_PITCHBOOK_INTERSECTION, {
    variables: { pitchbookIds: `{${pitchbookIdsStrings}}` },
    fetchPolicy: 'cache-and-network',
  });

  const intersection = get(data, COMPANIES_QUERY_ROOT, []);
  const intersectionByPitchbookId = keyBy(intersection, 'pitchbookId');
  const intersectionIds = intersection.map(item => item.pitchbookId);

  return {
    error,
    filterToggle,
    intersection,
    intersectionIds,
    intersectionByPitchbookId,
    loading,
    pitchbookById,
    pitchbookIds,
    refetch,
    searchTerm,
    setFilterToggle,
    setPitchbookById,
    setPitchbookIds,
    setSearchTerm,
  };
};

export default usePitchbookIntersectionHooks;
