import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button/Button.jsx';
import Table from 'components/Table/Table.jsx';
import Link from 'components/Link/Link.jsx';

const PitchbookIntersectionTable = ({
  ids,
  pitchbookById,
  intersectionByPitchbookId,
  setA,
  setB,
}) => {
  const labelSetA = `Open in ${setA}`;
  const labelSetB = `Open in ${setB}`;

  return (
    <Table>
      <Table.Header>
        <Table.Row>
          <Table.Cell header>Name</Table.Cell>
          <Table.Cell header>
            {labelSetA}
          </Table.Cell>
          <Table.Cell header>
            {labelSetB}
          </Table.Cell>
          <Table.Cell header />
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {ids.map((id) => {
          const company = pitchbookById[id];
          const arqCompany = intersectionByPitchbookId[id];
          return (
            <Table.Row key={id}>
              <Table.Cell>
                {arqCompany && (
                  <Link to={`/companies/${arqCompany.id}`} external>
                    {arqCompany.name}
                  </Link>
                )}
                {!arqCompany && company.name}
              </Table.Cell>
              <Table.Cell>
                <Link to={`${PITCHBOOK_URL}/${company.id}/company/profile`} external>
                  <Button
                    appearance="secondary"
                  >
                    {labelSetA}
                  </Button>
                </Link>
              </Table.Cell>
              <Table.Cell>
                {arqCompany && (
                  <Link to={`${SALESFORCE_URL}/lightning/r/Account/${arqCompany.id}/view`} external>
                    <Button
                      appearance="secondary"
                    >
                      {labelSetB}
                    </Button>
                  </Link>
                )}
              </Table.Cell>
            </Table.Row>
          );
        })}
      </Table.Body>
    </Table>
  );
};

PitchbookIntersectionTable.propTypes = {
  ids: PropTypes.arrayOf(PropTypes.string).isRequired,
  pitchbookById: PropTypes.shape({}).isRequired,
  intersectionByPitchbookId: PropTypes.shape({}).isRequired,
  setA: PropTypes.string.isRequired,
  setB: PropTypes.string.isRequired,
};

PitchbookIntersectionTable.defaultProps = {};

export default PitchbookIntersectionTable;
