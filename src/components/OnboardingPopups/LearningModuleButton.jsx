import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button/Button.jsx';
import { onboardingModuleShownVar } from 'apollo/cache';

const LearningModuleButton = ({ children }) => (
  <Button
    unstyled
    onClick={() => onboardingModuleShownVar(false)}
  >
    <span className="Popover-box--highlight-text">{children}</span>
  </Button>
);

LearningModuleButton.propTypes = {
  children: PropTypes.node,
};

LearningModuleButton.defaultProps = {
  children: 'Learn',
};

export default LearningModuleButton;
