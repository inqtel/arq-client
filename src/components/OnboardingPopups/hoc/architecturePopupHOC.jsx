import React from 'react';
import { useQuery } from '@apollo/client';
import { GET_ARCHITECTURE_POPUP_STATE } from 'queries/navigation';
import { architecturePopupVar } from 'apollo/cache';

const architecturePopupHOC = Wrapped => (props) => {
  const { data } = useQuery(GET_ARCHITECTURE_POPUP_STATE);

  const { architecturePopup, controlsPopup } = data;

  const setArchitecturePopupPanel = (panel, completed = false) => {
    architecturePopupVar({
      ...architecturePopup,
      panel,
      completed,
    });
  };

  const architecturePopupSeen = () => setArchitecturePopupPanel(1);

  const searchPopupSeen = () => setArchitecturePopupPanel(2);

  const drawingToolPopupSeen = () => {
    window.localStorage.setItem('architecturePopupCompleted', true);
    setArchitecturePopupPanel(0, true);
  };

  return (
    <Wrapped
      architecturePopup={architecturePopup}
      controlsPopup={controlsPopup}
      setArchitecturePopupPanel={setArchitecturePopupPanel}
      architecturePopupSeen={architecturePopupSeen}
      searchPopupSeen={searchPopupSeen}
      drawingToolPopupSeen={drawingToolPopupSeen}
      {...props}
    />
  );
};

export default architecturePopupHOC;
