import React from 'react';
import { useQuery } from '@apollo/client';
import { GET_CONTROLS_POPUP_STATE } from 'queries/navigation';
import { controlsPopupVar } from 'apollo/cache';

const controlsPopupHOC = Wrapped => (props) => {
  const { data } = useQuery(GET_CONTROLS_POPUP_STATE);
  const { controlsPopup } = data;

  const setControlsPopupPanel = (panel, completed = false) => {
    controlsPopupVar({
      ...controlsPopup,
      panel,
      completed,
    });
  };

  const insertArchitecturePopupSeen = () => setControlsPopupPanel(1);

  const collaboratePopupSeen = () => {
    window.localStorage.setItem('controlsPopupCompleted', true);
    setControlsPopupPanel(1, true);
  };

  return (
    <Wrapped
      controlsPopup={controlsPopup}
      setControlsPopupPanel={setControlsPopupPanel}
      insertArchitecturePopupSeen={insertArchitecturePopupSeen}
      collaboratePopupSeen={collaboratePopupSeen}
      {...props}
    />
  );
};

export default controlsPopupHOC;
