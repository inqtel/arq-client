import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { onboardingPopupPropType } from 'proptypes';
import Popover from 'components/Popover/Popover.jsx';
import PopoverBoxActionsNav from 'components/Popover/PopoverBoxActionsNav.jsx';
import controlsPopupHOC from 'components/OnboardingPopups/hoc/controlsPopupHOC.jsx';

class InsertArchitecturePopup extends PureComponent {
  setPanel = ind => () => {
    const { setControlsPopupPanel } = this.props;
    setControlsPopupPanel(ind);
  }

  render() {
    const { insertArchitecturePopupSeen, controlsPopup } = this.props;
    const { completed, panel } = controlsPopup;

    if (completed || panel !== 0) return null;

    return (
      <Popover width="md" opened preventCloseOnClickOutside>
        <Popover.Box>
          <Popover.Box.Content>
            <div className="Document-architecture-popup">
              <div className="Document-architecture-popup--content">
                <div className="Document-architecture-popup--header">
                  {' '}
                  Insert Architecture
                  {' '}
                </div>
                <p>
                  {' '}
                  An architecture is an outline used to communicate problem sets.
                  {' '}
                </p>
              </div>
              <div className="Document-architecture-popup--actions">
                <PopoverBoxActionsNav hide />
                <ul className="Document-architecture-popup--breadcrumbs">
                  <li
                    className="Document-architecture-popup--bullet Document-architecture-popup--bullet-active"
                    onClick={this.setPanel(0)}
                  />
                  <li
                    className="Document-architecture-popup--bullet"
                    onClick={this.setPanel(1)}
                  />
                </ul>
                <PopoverBoxActionsNav
                  onClick={insertArchitecturePopupSeen}
                >
                  Next
                </PopoverBoxActionsNav>
              </div>
            </div>
          </Popover.Box.Content>
        </Popover.Box>
      </Popover>
    );
  }
}

InsertArchitecturePopup.propTypes = {
  controlsPopup: onboardingPopupPropType.isRequired,
  setControlsPopupPanel: PropTypes.func.isRequired,
  insertArchitecturePopupSeen: PropTypes.func.isRequired,
};

InsertArchitecturePopup.defaultProps = {};

export default controlsPopupHOC(InsertArchitecturePopup);
