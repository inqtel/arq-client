import React from 'react';
import PropTypes from 'prop-types';
import { problemPropType } from 'proptypes';
import cx from 'classnames';
import {
  getProblemShortname,
  getProblemSuffix,
  getProblemPriorities,
} from 'utils/problem';
import Link from 'components/Link/Link.jsx';
import 'components/Problem/Problem.scss';

const Problem = ({
  problem,
  customer,
  highlight,
  linkTitle,
  size,
}) => {
  const priorities = getProblemPriorities(problem);

  return (
    <div
      className={cx('Problem', {
        'Problem--priorityHigh': priorities.indexOf('high') > -1,
        'Problem--priorityMedium': priorities.indexOf('medium') > -1,
        'Problem--priorityLow': priorities.indexOf('low') > -1,
        'Problem--sm': size === 'sm',
      })}
    >
      <div className="Problem-header">
        <div>
          <div className="Problem-customer">
            {linkTitle && highlight && (
              <Link to={`/problems/${problem.id}`}>
                {`${customer.name} ${problem.fiscalYear} `}
                <span
                  // eslint-disable-next-line react/no-danger
                  dangerouslySetInnerHTML={{
                    __html: getProblemSuffix(problem),
                  }}
                />
              </Link>
            )}
            {linkTitle && !highlight && (
              <Link to={`/problems/${problem.id}`}>
                {getProblemShortname(problem, customer)}
              </Link>
            )}
            {!linkTitle && highlight && (
              <>
                {`${customer.name} ${problem.fiscalYear} `}
                <span
                  // eslint-disable-next-line react/no-danger
                  dangerouslySetInnerHTML={{
                    __html: getProblemSuffix(problem),
                  }}
                />
              </>
            )}
            {!linkTitle && !highlight && getProblemShortname(problem, customer)}
          </div>
          <div className="Problem-categories">
            {problem.customerCategory
              && problem.customerCategory.split('|').map(category => (
                <div className="Problem-categories-category" key={category}>{category}</div>
              ))}
          </div>
        </div>
        <div className="Problem-header-right">
          <div className="Problem-priority">
            {problem.priority && problem.priority.length
              ? `${problem.priority}`
              : 'No priority'}
          </div>
          <div className="Problem-need">{problem.categoryOfNeed}</div>
        </div>
      </div>
      {size !== 'sm' && (
        <div className="Problem-description">
          <p className="Problem-description-text">
            <a
              target="_blank"
              rel="noopener noreferrer"
              href={`${SALESFORCE_URL}/lightning/r/Customer_Problem_Sets__c/${problem.id}/view`}
            >
              View in Salesforce
            </a>
          </p>
        </div>
      )}
    </div>
  );
};

Problem.propTypes = {
  problem: problemPropType.isRequired,
  customer: PropTypes.shape({ name: PropTypes.string }),
  highlight: PropTypes.bool,
  linkTitle: PropTypes.bool,
  size: PropTypes.string,
};

Problem.defaultProps = {
  customer: null,
  highlight: false,
  linkTitle: false,
  size: null,
};

export default Problem;
