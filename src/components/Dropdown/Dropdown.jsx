import React, {
  useRef,
  useState,
  useImperativeHandle,
  forwardRef,
} from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import useOnClickOutside from 'hooks/useOnClickOutside.jsx';
import DropdownToggle from './DropdownToggle.jsx';
import DropdownMenu from './DropdownMenu.jsx';
import 'components/Dropdown/Dropdown.scss';

const Dropdown = forwardRef(({
  children,
  onOpen,
  onClose,
}, ref) => {
  const dropdown = useRef();
  useImperativeHandle(ref, () => ({ close }));

  const [isOpened, setIsOpened] = useState(false);

  useOnClickOutside(dropdown, () => close());

  const onToggle = (e) => {
    e.preventDefault();
    isOpened ? close() : open();
  };

  const open = () => {
    setIsOpened(true);
    onOpen();
  };

  const close = () => {
    setIsOpened(false);
    onClose();
  };

  return (
    <div className="Dropdown" ref={dropdown}>
      {React.Children.map(children, (child) => {
        if (child.type === DropdownToggle) {
          return React.cloneElement(child, { onToggle });
        }

        if (child.type === DropdownMenu && isOpened) {
          return React.cloneElement(child, { close });
        }

        return null;
      })}
    </div>
  );
});

Dropdown.Toggle = DropdownToggle;
Dropdown.Menu = DropdownMenu;

Dropdown.propTypes = {
  children: PropTypes.node,
  onOpen: PropTypes.func,
  onClose: PropTypes.func,
};

Dropdown.defaultProps = {
  children: null,
  onOpen: noop,
  onClose: noop,
};

export default Dropdown;
