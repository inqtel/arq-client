import React from 'react';
import Dropdown from 'components/Dropdown/Dropdown.jsx';

describe('<Dropdown />', () => {
  it('should render with .Dropdown class', () => {
    const wrapper = shallow(
      <Dropdown />,
    );

    expect(wrapper.is('.Dropdown')).toBe(true);
  });
});
