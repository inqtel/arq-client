import React from 'react';
import PropTypes from 'prop-types';
import SidebarNavigation from 'components/SidebarNavigation/SidebarNavigation.jsx';
import useSidebarNavigation from 'components/SidebarNavigation/useSidebarNavigation.jsx';

const SidebarNavigationInterface = ({ active }) => {
  const { links, unseenItems, user } = useSidebarNavigation();
  return (
    <SidebarNavigation
      active={active}
      links={links}
      unseenItems={unseenItems}
      user={user}
    />
  );
};

SidebarNavigationInterface.propTypes = {
  active: PropTypes.string,
};

SidebarNavigationInterface.defaultProps = {
  active: null,
};

export default SidebarNavigationInterface;
