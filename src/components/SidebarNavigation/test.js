import React from 'react';
import SidebarNavigation from './SidebarNavigation.jsx';

describe('<SidebarNavigation />', () => {
  const LINKS = [
    {
      name: 'Architectures',
      heading: true,
      shortname: 'architectures',
      path: '/',
    },
    {
      name: 'My Documents',
      shortname: 'home',
      path: '/',
    },
    {
      name: 'Shared Documents',
      shortname: 'shared',
      path: '/shared',
    },
  ];

  const user = {
    firstName: '',
    lastName: '',
    email: 'bob@email.com',
    permission: 'admin',
  };

  const active = 'home';
  const unseenItems = {
    shared: 0,
    admin: 0,
  };
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(
      <SidebarNavigation
        links={LINKS}
        active={active}
        user={user}
        unseenItems={unseenItems}
      />,
    );
  });
  it('should render with .SidebarNavigation class', () => {
    expect(wrapper.is('.SidebarNavigation')).toBe(true);
  });
});
