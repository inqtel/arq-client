import React from 'react';
import PropTypes from 'prop-types';
import Link from 'components/Link/Link.jsx';
import 'components/SidebarNavigation/SidebarNavigation.scss';

const SidebarNavigationLink = ({
  linkName,
  linkPath,
  unseenItems,
}) => (
  <Link to={linkPath} className="SidebarNavigation-link">
    {linkName}
    {!!unseenItems && unseenItems > 0 && (
      ` (${unseenItems} NEW)`
    )}
  </Link>
);

SidebarNavigationLink.propTypes = {
  linkName: PropTypes.string.isRequired,
  linkPath: PropTypes.string.isRequired,
  unseenItems: PropTypes.number,
};

SidebarNavigationLink.defaultProps = {
  unseenItems: null,
};

export default SidebarNavigationLink;
