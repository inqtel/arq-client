import React from 'react';
import PropTypes from 'prop-types';
import { navigationPropType, userPropType } from 'proptypes';
import cx from 'classnames';
import SidebarNavigationLink from 'components/SidebarNavigation/SidebarNavigationLink.jsx';
import SidebarNavigationSublink from 'components/SidebarNavigation/SidebarNavigationSublink.jsx';
import 'components/SidebarNavigation/SidebarNavigation.scss';

const SidebarNavigation = ({
  active,
  links,
  unseenItems,
  user,
}) => (
  <nav className="SidebarNavigation">
    <ul className="SidebarNavigation-items">
      {links.map((link) => {
        if (link.permissions === 'admin' && !user.admin) {
          return null;
        }

        const isActive = active === link.shortname
          || (link.sublinks || []).find(s => s.shortname === active);

        return (
          <li
            key={link.shortname}
            className={cx('SidebarNavigation-item', {
              'is-active': isActive,
            })}
          >
            <SidebarNavigationLink
              linkName={link.name}
              linkPath={link.path}
              unseenItems={unseenItems[link.shortname]}
            />
            {link.sublinks && (
              <ul className="SidebarNavigation-subitems">
                {link.sublinks.map(sublink => (
                  <li
                    key={sublink.shortname}
                    className={cx('SidebarNavigation-subitem', {
                      'is-active': sublink.shortname === active,
                    })}
                  >
                    <SidebarNavigationSublink
                      linkName={sublink.name}
                      linkPath={sublink.path}
                      unseenItems={unseenItems[sublink.shortname]}
                    />
                  </li>
                ))}
              </ul>
            )}
          </li>
        );
      })}
    </ul>
  </nav>
);

SidebarNavigation.propTypes = {
  active: PropTypes.string,
  links: PropTypes.arrayOf(navigationPropType).isRequired,
  unseenItems: PropTypes.shape({
    admin: PropTypes.number,
    shared: PropTypes.number,
  }),
  user: userPropType.isRequired,
};

SidebarNavigation.defaultProps = {
  active: null,
  unseenItems: {
    admin: 0,
    shared: 0,
  },
};

export default SidebarNavigation;
