import { setUp, checkProps } from 'utils/testing';
import DashboardHeaderNavigation from 'components/DashboardHeaderNavigation/DashboardHeaderNavigation.jsx';

describe('<DashboardHeaderNavigation />', () => {
  let wrapper;

  const expectedProps = {
    closeOnboardingModule: () => {},
    showToggleUser: () => {},
    closeToggleUser: () => {},
    toggleUserShown: false,
    toggleUserAction: () => {},
    showOnboardingModule: () => {},
    onboardingModuleShown: true,
    users: [],
  };

  beforeEach(() => {
    wrapper = setUp(DashboardHeaderNavigation, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(DashboardHeaderNavigation, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render a Navigation component', () => {
    expect(wrapper.exists('Navigation')).toBe(true);
  });
});
