import React from 'react';
import DashboardHeaderNavigation from 'components/DashboardHeaderNavigation/DashboardHeaderNavigation.jsx';
import useUser from 'hooks/useUser.jsx';
import useUsers from 'hooks/useUsers.jsx';
import useDashboardHeaderNavigation from 'components/DashboardHeaderNavigation/useDashboardHeaderNavigation.jsx';

const DashboardHeaderNavigationInterface = () => {
  const {
    closeOnboardingModule,
    closeToggleUser,
    onboardingModuleShown,
    showOnboardingModule,
    showToggleUser,
    toggleUserShown,
    toggleUserAction,
  } = useDashboardHeaderNavigation();

  const { user } = useUser();
  const { users } = useUsers();

  return (
    <DashboardHeaderNavigation
      closeOnboardingModule={closeOnboardingModule}
      closeToggleUser={closeToggleUser}
      onboardingModuleShown={onboardingModuleShown}
      showOnboardingModule={showOnboardingModule}
      showToggleUser={showToggleUser}
      toggleUserShown={toggleUserShown}
      toggleUserAction={toggleUserAction}
      user={user}
      users={users}
    />
  );
};

export default DashboardHeaderNavigationInterface;
