import React from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType } from 'proptypes';
import { get } from 'lodash';
import { useQuery } from '@apollo/client';
import RelatedPanel from 'components/RelatedPanel/RelatedPanel.jsx';
import { ARCHITECTURES_QUERY_ROOT } from 'constants/gql';
import { GET_ARCHITECTURES_BY_CAPABILITIES } from 'queries/architectures';
import useCreateDoc from 'hooks/useCreateDoc.jsx';

const RelatedArchitecturesPanelInterface = ({ capabilities }) => {
  const { createDoc } = useCreateDoc();

  const { loading, error, data } = useQuery(
    GET_ARCHITECTURES_BY_CAPABILITIES,
    {
      fetchPolicy: 'cache-and-network',
      variables: {
        capabilities: capabilities.map(c => c.id),
      },
    },
  );

  const architectures = get(data, ARCHITECTURES_QUERY_ROOT, []);

  return (
    <RelatedPanel
      loading={loading}
      error={error}
      resource={architectures}
      resourceName="architectures"
      createDoc={createDoc}
    />
  );
};

RelatedArchitecturesPanelInterface.propTypes = {
  capabilities: PropTypes.arrayOf(taxonomyPropType),
};

RelatedArchitecturesPanelInterface.defaultProps = {
  capabilities: [],
};

export default RelatedArchitecturesPanelInterface;
