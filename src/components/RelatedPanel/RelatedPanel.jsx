import React from 'react';
import PropTypes from 'prop-types';
import {
  apolloErrorPropType,
  companyPropType,
  problemPropType,
  taxonomyPropType,
} from 'proptypes';
import RelatedPanelHeading from 'components/RelatedPanel/RelatedPanelHeading.jsx';
import RelatedPanelContent from 'components/RelatedPanel/RelatedPanelContent.jsx';
import Panel from 'components/Panel/Panel.jsx';

const RelatedPanel = ({
  loading,
  error,
  resource,
  resourceName,
  searchLink,
  onSearchLinkClick,
  createDoc,
}) => (
  <Panel>
    <RelatedPanelHeading
      searchLink={searchLink}
      resourceName={resourceName}
      onSearchLinkClick={onSearchLinkClick}
    />
    <RelatedPanelContent
      loading={loading}
      error={error}
      resource={resource}
      resourceName={resourceName}
      createDoc={createDoc}
    />
  </Panel>
);

RelatedPanel.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: apolloErrorPropType,
  resource: PropTypes.oneOfType([
    PropTypes.arrayOf(companyPropType),
    PropTypes.arrayOf(problemPropType),
    PropTypes.arrayOf(taxonomyPropType),
  ]).isRequired,
  resourceName: PropTypes.string.isRequired,
  searchLink: PropTypes.string,
  onSearchLinkClick: PropTypes.func,
  createDoc: PropTypes.func,
};

RelatedPanel.defaultProps = {
  error: null,
  createDoc: null,
  searchLink: null,
  onSearchLinkClick: null,
};

export default RelatedPanel;
