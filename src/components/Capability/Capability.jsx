import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import { taxonomyPropType } from 'proptypes';
import './Capability.scss';

const Capability = ({ capability, size }) => (
  <div
    className={cx('Capability', {
      [`Capability-${size}`]: size,
    })}
  >
    <span className="Capability-term">{capability.term}</span>
  </div>
);

Capability.propTypes = {
  capability: taxonomyPropType.isRequired,
  size: PropTypes.oneOf(['sm', 'md']),
};

Capability.defaultProps = {
  size: 'sm',
};

export default Capability;
