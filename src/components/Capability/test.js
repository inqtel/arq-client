import React from 'react';
import Capability from 'components/Capability/Capability.jsx';

describe('<Capability />', () => {
  it('should render with .Capability class', () => {
    const wrapper = shallow(<Capability capability={{}} />);
    expect(wrapper.is('.Capability')).toBe(true);
  });
});
