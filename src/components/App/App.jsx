import React from 'react';
import PropTypes from 'prop-types';
import Loader from 'components/Loader/Loader.jsx';
import Alert from 'components/Alert/Alert.jsx';
import useApp from 'components/App/useApp.jsx';

const App = ({
  children,
}) => {
  const { loading, error, authUser } = useApp();

  if (error || !authUser) {
    return (
      <Alert
        heading="Uh Oh..."
        subheading="The app encountered an error starting up. Please refresh and try again."
        isFullscreen
      />
    );
  }

  if (!loading) return <div className="App">{children}</div>;

  return <Loader size={48} isFullscreen />;
};

App.propTypes = {
  children: PropTypes.node.isRequired,
};

export default App;
