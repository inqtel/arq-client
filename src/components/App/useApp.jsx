import { useQuery } from '@apollo/client';
import { get } from 'lodash';

import {
  GET_AUTHENTICATED_USER,
} from 'queries/auth';

const useApp = () => {
  const { loading, error, data } = useQuery(GET_AUTHENTICATED_USER, {
    fetchPolicy: 'network-only',
    pollInterval: 30000,
  });

  const authUser = get(data, ['arq_authenticated_user', 0]);

  return {
    loading,
    error,
    authUser,
  };
};

export default useApp;
