import React from 'react';
import useSearch from 'hooks/useSearch.jsx';
import TaxonomySearch from 'components/TaxonomySearch/TaxonomySearch.jsx';

const TaxonomySearchInterface = () => {
  const {
    onSearch,
    hits,
    searching,
  } = useSearch();

  return (
    <TaxonomySearch
      hits={hits}
      onSearch={onSearch}
      searching={searching}
    />
  );
};

export default TaxonomySearchInterface;
