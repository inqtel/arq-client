import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import useOnClickOutside from 'hooks/useOnClickOutside.jsx';
import TaxonomySearchTaxonomy from 'components/TaxonomySearch/TaxonomySearchTaxonomy.jsx';

const TaxonomiesSearchResults = ({ hits, handleClickOutside }) => {
  const searchResults = useRef();
  useOnClickOutside(searchResults, handleClickOutside);

  return (
    <div className="TaxonomySearch-results" ref={searchResults}>
      {hits.length === 0 && 'No results.'}
      {hits.length > 0 && hits.map(hit => (
        <TaxonomySearchTaxonomy
          key={hit._source.id} // eslint-disable-line no-underscore-dangle
          taxonomy={hit._source} // eslint-disable-line no-underscore-dangle
        />
      ))}
    </div>
  );
};

TaxonomiesSearchResults.propTypes = {
  hits: PropTypes.arrayOf(PropTypes.object).isRequired,
  handleClickOutside: PropTypes.func.isRequired,
};

export default TaxonomiesSearchResults;
