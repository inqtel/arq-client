import React from 'react';
import { elasticResult } from 'mocks/elastic';
import { StaticRouter } from 'react-router'; // eslint-disable-line import/no-extraneous-dependencies
import TaxonomySearch from 'components/TaxonomySearch/TaxonomySearch.jsx';

describe('<TaxonomySearch />', () => {
  it('should render without error', () => {
    shallow(<TaxonomySearch />);
  });

  it('should accept hits prop', () => {
    shallow(<TaxonomySearch hits={elasticResult.hits.hits} />);
  });

  it('should open results on input change', () => {
    const taxonomySearch = mount(
      <StaticRouter>
        <TaxonomySearch hits={elasticResult.hits.hits} />
      </StaticRouter>,
    );

    const input = taxonomySearch.find('input');
    input.simulate('change', { target: { value: 'Test' } });
    expect(taxonomySearch.exists('.TaxonomySearch-results')).toEqual(true);
  });
});
