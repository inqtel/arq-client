import cytoscape from 'cytoscape';

const color = ['#cbd5e8', '#fdcdac', '#b3e2cd', '#f4cae4', '#ff6c68'];
// darker palette
// const color = ['#006999', '#7c6cba', '#dd60a7', '#ff6c68', '#ffa600'];

// TODO: Move these styles into scss file if possible
export default cytoscape
  .stylesheet()
  .selector('[content]')
  .css({
    'font-family': 'Arial, sans-serif',
    'font-weight': 500,
    'font-size': '7.5rem',
    'min-zoomed-font-size': '10rem',
    'text-wrap': 'wrap',
    'text-max-width': '52rem',
    'text-valign': 'center',
    content: 'data(content)',
  })
  .selector('edge')
  .css({
    'curve-style': 'bezier',
    'target-arrow-shape': 'triangle',
    'line-color': '#dae0e1',
    'target-arrow-color': '#dae0e1',
  })
  .selector('.highlight')
  .css({
    'line-color': '#6a71d7',
    'target-arrow-color': '#6a71d7',
    opacity: 0.65,
  })
  .selector('node')
  .css({
    color: '#5d5c6b',
    'background-opactiy': 0.9,
    'text-outline-width': 1,
  })
  .selector('.ellipse') // SHAPES
  .css({
    shape: 'ellipse',
  })
  .selector('.triangle')
  .css({
    shape: 'triangle',
  })
  .selector('.rectangle')
  .css({
    shape: 'rectangle',
  })
  .selector('.roundrectangle')
  .css({
    shape: 'roundrectangle',
  })
  .selector('.bottomroundrectangle')
  .css({
    shape: 'bottomroundrectangle',
  })
  .selector('.cutrectangle')
  .css({
    shape: 'cutrectangle',
  })
  .selector('.barrel')
  .css({
    shape: 'barrel',
  })
  .selector('.rhomboid')
  .css({
    shape: 'rhomboid',
  })
  .selector('.diamond')
  .css({
    shape: 'diamond',
  })
  .selector('.pentagon')
  .css({
    shape: 'pentagon',
  })
  .selector('.hexagon')
  .css({
    shape: 'hexagon',
  })
  .selector('.heptagon')
  .css({
    shape: 'heptagon',
  })
  .selector('.octagon')
  .css({
    shape: 'octagon',
  })
  .selector('.color-0') // COLORS
  .css({
    'background-color': color[0],
    'border-color': color[0],
    'text-outline-color': color[0],
  })
  .selector('.color-1')
  .css({
    'background-color': color[1],
    'border-color': color[1],
    'text-outline-color': color[1],
  })
  .selector('.color-2')
  .css({
    'background-color': color[2],
    'border-color': color[2],
    'text-outline-color': color[2],
  })
  .selector('.color-3')
  .css({
    'background-color': color[3],
    'border-color': color[3],
    'text-outline-color': color[3],
  })
  .selector('.color-4')
  .css({
    'background-color': color[4],
    'border-color': color[4],
    'text-outline-color': color[4],
  })
  .selector('.color-5')
  .css({
    'background-color': color[5],
    'border-color': color[5],
    'text-outline-color': color[5],
  })
  .selector('.interactive')
  .css({
    'border-color': '#6a71d7',
    'border-width': 2,
  })
  .selector('node:selected') // INTERACTION STYLES
  .css({
    'background-color': '#6a71d7',
    'text-outline-color': '#6a71d7',
    color: '#fff',
  });
