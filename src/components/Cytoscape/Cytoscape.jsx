import React, { useRef, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import cytoscape from 'cytoscape';
import coseBilkent from 'cytoscape-cose-bilkent';
import BASE_STYLE from 'components/Cytoscape/CytoscapeStyle.jsx';
import {
  TREE,
  GRID,
  FORCE,
} from 'constants/graph.js';
import { ARCHITECTURE_TYPENAME } from 'constants/gql';
import 'components/Cytoscape/Cytoscape.scss';

const Cytoscape = ({
  elements,
  layoutName,
  onNodeAdd,
  onNodeMouseOver,
  onNodeMouseOut,
  onGraphClick,
}) => {
  const container = useRef(null);
  const graph = useRef();
  const layout = useRef();

  useEffect(() => {
    if (graph.current) {
      if (layout.current) {
        layout.current.stop();
      }

      graph.current.json({ elements });

      layout.current = graph.current.elements().makeLayout(getLayoutOptions(layoutName));
      layout.current.run();
    }
  }, [elements, getLayoutOptions, layoutName]);

  useEffect(() => {
    if (graph.current) {
      if (layout.current) {
        layout.current.stop();
      }
      layout.current = graph.current.elements().makeLayout(getLayoutOptions(layoutName));
      layout.current.run();
    }
  }, [layoutName, getLayoutOptions]);

  useEffect(() => {
    if (!container.current) {
      return false;
    }
    try {
      if (!graph.current) {
        cytoscape.use(coseBilkent);
        graph.current = cytoscape({
          elements,
          style: BASE_STYLE,
          maxZoom: 3,
          minZoom: 0.3,
          container: container.current,
        });

        onNodeAdd && graph.current.on('add', 'node', e => onNodeAdd(e));
        onNodeMouseOver && graph.current.on('mouseover', 'node', e => onNodeMouseOver(e));
        onNodeMouseOut && graph.current.on('mouseout', 'node', e => onNodeMouseOut(e));
        onGraphClick && graph.current.on('click', e => onGraphClick(e));
      }
    } catch (error) {
      console.error(error); // eslint-disable-line
    }
    return () => {
      graph.current && graph.current.destroy();
    };
  }, []); // eslint-disable-line

  const getLayoutOptions = useCallback((name) => {
    const baseOptions = {
      name,
      fit: true,
      animate: true,
      nodeDimensionsIncludeLabels: true,
    };

    let typeOptions = {};

    switch (name) {
      case TREE:
        typeOptions = {
          roots: `node[label = "${ARCHITECTURE_TYPENAME}"]`,
          directed: false,
          spacingFactor: 1,
        };
        break;
      case GRID:
        typeOptions = {
          condense: true,
          avoidOverlap: true,
        };
        break;
      case FORCE:
        typeOptions = {
          animate: 'end',
        };
        break;
      default:
        break;
    }

    return {
      ...baseOptions,
      ...typeOptions,
    };
  }, []);

  return <div className="Cytoscape" ref={container} />;
};

Cytoscape.propTypes = {
  elements: PropTypes.arrayOf(PropTypes.object).isRequired,
  layoutName: PropTypes.string.isRequired,
  onNodeAdd: PropTypes.func,
  onNodeMouseOver: PropTypes.func,
  onNodeMouseOut: PropTypes.func,
  onGraphClick: PropTypes.func,
};

Cytoscape.defaultProps = {
  onNodeAdd: null,
  onNodeMouseOver: null,
  onNodeMouseOut: null,
  onGraphClick: null,
};

export default Cytoscape;
