import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Link from 'components/Link/Link.jsx';
import Button from 'components/Button/Button.jsx';
import { IconClose } from 'components/Icons/Icons.jsx';
import CardHeader from 'components/Card/CardHeader.jsx';
import CardMeta from 'components/Card/CardMeta.jsx';
import CardPreview from 'components/Card/CardPreview.jsx';
import CardFooter from 'components/Card/CardFooter.jsx';
import 'components/Card/Card.scss';

const Card = ({
  children,
  className,
  url,
  onArchiveClick,
  renderArchiveModal,
  fullWidth,
}) => (
  <div className={cx('Card', className, { 'Card--fullWidth': fullWidth })}>
    {onArchiveClick && (
      <div className="Card-card-archive">
        <Button unstyled display="block" onClick={onArchiveClick}>
          {/* TODO: Do we need height here? */}
          <IconClose display="block" height={16} />
        </Button>
      </div>
    )}
    <Link to={url}>
      <div className="Card-card-inner">{children}</div>
    </Link>
    {onArchiveClick && renderArchiveModal()}
  </div>
);

Card.propTypes = {
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  url: PropTypes.string.isRequired,
  onArchiveClick: PropTypes.func,
  renderArchiveModal: PropTypes.func,
  fullWidth: PropTypes.bool,
};

Card.defaultProps = {
  className: null,
  onArchiveClick: null,
  renderArchiveModal: () => {},
  fullWidth: false,
};

Card.Header = CardHeader;
Card.Meta = CardMeta;
Card.Preview = CardPreview;
Card.Footer = CardFooter;

export default Card;
