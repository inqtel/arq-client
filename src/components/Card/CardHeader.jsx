import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const CardHeader = ({ title }) => (
  <header className="Card-card-header">
    <h2
      className={cx('Card-card-header-title', {
        'Card-card-header-title--untitled':
          !title,
      })}
    >
      {title || 'Untitled'}
    </h2>
  </header>
);

CardHeader.propTypes = {
  title: PropTypes.node,
};

CardHeader.defaultProps = {
  title: '',
};

export default CardHeader;
