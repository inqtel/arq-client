import React from 'react';
import Card from 'components/Card/Card.jsx';
import { StaticRouter } from 'react-router'; // eslint-disable-line import/no-extraneous-dependencies

describe('<Card />', () => {
  it('should render with .Card class', () => {
    const wrapper = shallow(<Card capability={{}} />);
    expect(wrapper.is('.Card')).toBe(true);
  });

  it('should render an archive button when onArchive passed in', () => {
    const wrapper = shallow(<Card capability={{}} onArchiveClick={() => {}} />);
    expect(wrapper.exists('.Card-card-archive')).toEqual(true);
  });

  it('should render children when passed in', () => {
    const wrapper = mount(
      <StaticRouter>
        <Card capability={{}}>
          <Card.Header />
          <Card.Meta />
          <Card.Preview />
          <Card.Footer />
        </Card>
      </StaticRouter>,
    );
    expect(wrapper.exists('.Card-card-header')).toEqual(true);
    expect(wrapper.exists('.Card-card-meta')).toEqual(true);
    expect(wrapper.exists('.Card-card-preview')).toEqual(true);
    expect(wrapper.exists('.Card-card-footer')).toEqual(true);
  });
});
