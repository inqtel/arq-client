import React from 'react';
import PropTypes from 'prop-types';
import { esSearchResultPropType, taxonomyPropType } from 'proptypes';
import Loader from 'components/Loader/Loader.jsx';
import SearchBar from 'components/SearchBar/SearchBar.jsx';
import SearchResults from 'components/SearchResults/SearchResults.jsx';

const DocumentSearch = ({
  onChange,
  term,
  searching,
  hits,
  actions,
  hoverAction,
  defaultsLoading,
  defaultTaxonomies,
}) => {
  const renderResults = () => {
    if (term.length === 0) {
      if (defaultsLoading) {
        return <Loader />;
      }
      const defaultHits = defaultTaxonomies.map(t => ({ _source: t }));
      return (
        <SearchResults hits={defaultHits} actions={actions} hoverAction={hoverAction} />
      );
    }

    if (hits.length === 0) {
      return <div className="DocumentSearch-results-noresults">No results.</div>;
    }

    return <SearchResults hits={hits} actions={actions} hoverAction={hoverAction} />;
  };

  return (
    <div className="DocumentSearch">
      <div className="DocumentSearch-searchbar">
        <SearchBar
          onChange={onChange}
          value={term}
          searching={searching}
          placeholder="Search all taxonomies"
          enableOnboarding
        />
      </div>
      <div className="DocumentSearch-results">
        {renderResults()}
      </div>
    </div>
  );
};

DocumentSearch.propTypes = {
  onChange: PropTypes.func.isRequired,
  term: PropTypes.string.isRequired,
  searching: PropTypes.bool.isRequired,
  hits: PropTypes.arrayOf(esSearchResultPropType).isRequired,
  actions: PropTypes.shape({}).isRequired,
  hoverAction: PropTypes.shape({}).isRequired,
  defaultsLoading: PropTypes.bool.isRequired,
  defaultTaxonomies: PropTypes.arrayOf(taxonomyPropType).isRequired,
};

export default DocumentSearch;
