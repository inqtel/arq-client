import React from 'react';
import PropTypes from 'prop-types';

function DocumentBlock({ children }) {
  return <div className="Document-block">{children}</div>;
}

DocumentBlock.propTypes = {
  children: PropTypes.node,
};

DocumentBlock.defaultProps = {
  children: null,
};

export default DocumentBlock;
