import React from 'react';
import Document from 'components/Document/Document.jsx';

describe('<Document />', () => {
  it('should render with .Document class', () => {
    const wrapper = shallow(
      <Document
        activeDoc={{
          title: '',
          ui: {},
        }}
        activeComments={{}}
      />,
    );

    expect(wrapper.is('.Document')).toBe(true);
  });
});
