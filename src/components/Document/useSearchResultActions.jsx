import { useSelector, useDispatch } from 'react-redux';
import {
  linkDocArchitecturePackageTaxonomy as linkDocArchitecturePackageTaxonomyRaw,
  docArchitecturePackageInsertTaxonomyTree as docArchitecturePackageInsertTaxonomyTreeRaw,
  linkDocArchitecturePackageReference as linkDocArchitecturePackageReferenceRaw,
  docArchitecturePackageInsertReferenceTree as docArchitecturePackageInsertReferenceTreeRaw,
} from 'actions/activeDoc';

const useDocumentSearchResults = () => {
  const { activePackageQID } = useSelector(state => state.activeDoc.ui);
  const dispatch = useDispatch();

  const linkDocArchitecturePackageTaxonomy = pkg => dispatch(
    linkDocArchitecturePackageTaxonomyRaw(pkg),
  );

  const docArchitecturePackageInsertTaxonomyTree = pkg => dispatch(
    docArchitecturePackageInsertTaxonomyTreeRaw(pkg),
  );

  const linkDocArchitecturePackageReference = pkg => dispatch(
    linkDocArchitecturePackageReferenceRaw(pkg),
  );

  const docArchitecturePackageInsertReferenceTree = pkg => dispatch(
    docArchitecturePackageInsertReferenceTreeRaw(pkg),
  );

  return {
    activePackageQID,
    linkDocArchitecturePackageTaxonomy,
    docArchitecturePackageInsertTaxonomyTree,
    linkDocArchitecturePackageReference,
    docArchitecturePackageInsertReferenceTree,
  };
};

export default useDocumentSearchResults;
