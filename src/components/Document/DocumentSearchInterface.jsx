import React, { useState } from 'react';
import { pick } from 'lodash';
import {
  createPackagesFromTaxonomy,
  createPackagesFromReferencePackage,
} from 'utils/entities';
import { isPackage } from 'utils/gql';
import DocumentSearch from 'components/Document/DocumentSearch.jsx';
import { IconLink, IconInsert } from 'components/Icons/Icons.jsx';
import useSearch from 'hooks/useSearch.jsx';
import useCoreTaxonomyRoots from 'hooks/useCoreTaxonomyRoots.jsx';
import useSearchResultsActions from 'components/Document/useSearchResultActions.jsx';

const DocumentSearchInterface = () => {
  const [term, setTerm] = useState('');
  const {
    onSearch,
    searching,
    hits,
  } = useSearch();

  const onChange = (e) => {
    const newTerm = e.target.value;
    setTerm(newTerm);
    onSearch(newTerm);
  };

  const {
    activePackageQID,
    linkDocArchitecturePackageTaxonomy,
    docArchitecturePackageInsertTaxonomyTree,
    linkDocArchitecturePackageReference,
    docArchitecturePackageInsertReferenceTree,
  } = useSearchResultsActions();

  const onTaxonomyLink = (taxonomy) => {
    linkDocArchitecturePackageTaxonomy(taxonomy);
  };

  const onTaxonomyInsertTree = (taxonomy) => {
    docArchitecturePackageInsertTaxonomyTree(
      createPackagesFromTaxonomy(taxonomy),
    );
  };

  const onReferenceLink = (pkg) => {
    linkDocArchitecturePackageReference(pkg);
  };

  const onReferenceInsertTree = (pkg) => {
    docArchitecturePackageInsertReferenceTree(
      createPackagesFromReferencePackage(pkg),
    );
  };

  const onLink = (item, event, keys) => {
    item = keys ? pick(item, keys) : item;
    // Do nothing if active package isn't set; can't actually link a package
    // @todo: disable action if no activePackageQID
    if (activePackageQID) {
      if (isPackage(item)) {
        onReferenceLink(item);
      } else {
        onTaxonomyLink(item);
      }
    }
  };

  const onInsertTree = (item) => {
    // Do nothing if active package isn't set
    // @todo: disable action if no activePackageQID
    if (activePackageQID) {
      if (isPackage(item)) {
        onReferenceInsertTree(item);
      } else {
        onTaxonomyInsertTree(item);
      }
    }
  };

  const actions = {
    primary: [{
      title: 'Link to this term',
      icon: <IconLink height={13} />,
      handler: onLink,
    }, {
      title: 'Import this tree',
      icon: <IconInsert height={13} />,
      handler: onInsertTree,
    }],
  };

  // default results
  const { loading, taxonomies } = useCoreTaxonomyRoots();

  return (
    <DocumentSearch
      onChange={onChange}
      term={term}
      searching={searching}
      hits={hits}
      actions={actions}
      hoverAction={actions.primary[0]}
      defaultTaxonomies={taxonomies}
      defaultsLoading={loading}
    />
  );
};

export default DocumentSearchInterface;
