import React, { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import {
  initDoc as initDocRaw,
  getDoc as getDocRaw,
  resetDoc as resetDocRaw,
  updateDocTitle as updateDocTitleRaw,
  updateDocHTMLBlock as updateDocHTMLBlockRaw,
  insertDocArchitectureBlock as insertDocArchitectureBlockRaw,
  removeDocArchitecture as removeDocArchitectureRaw,
  setDocActiveEditorQID as setDocActiveEditorQIDRaw,
  unsetDocActiveEditorQID as unsetDocActiveEditorQIDRaw,
  updateDocEditor as updateDocEditorRaw,
  setDocActiveArchitectureQID as setDocActiveArchitectureQIDRaw,
  unsetDocActiveArchitectureQID as unsetDocActiveArchitectureQIDRaw,
  updateDocArchitecture as updateDocArchitectureRaw,
  setDocActivePackageQID as setDocActivePackageQIDRaw,
  unsetDocActivePackageQID as unsetDocActivePackageQIDRaw,
  docArchitecturePackageInsertTaxonomyTree as docArchitecturePackageInsertTaxonomyTreeRaw,
  docArchitecturePackageInsertReferenceTree as docArchitecturePackageInsertReferenceTreeRaw,
  linkDocArchitecturePackageTaxonomy as linkDocArchitecturePackageTaxonomyRaw,
  linkDocArchitecturePackageReference as linkDocArchitecturePackageReferenceRaw,
} from 'actions/activeDoc';
import Document from 'components/Document/Document.jsx';
import EntityError from 'components/EntityError/EntityError.jsx';
import {
  ARCHITECTURE_DRAFT,
  ARCHITECTURE_SUBMITTED,
} from 'constants/architectureStates.js';
import {
  patchArchitectureStatusSubmitted as patchArchitectureStatusSubmittedRaw,
  patchArchitectureStatusDraft as patchArchitectureStatusDraftRaw,
} from 'actions/architecture';
import usePackageComments from 'hooks/usePackageComments.jsx';
import useCommentsThread from 'hooks/useCommentsThread.jsx';
import useUser from 'hooks/useUser.jsx';

const DocumentInterface = ({
  id,
  initDoc,
  getDoc,
  resetDoc,
  updateDocTitle,
  updateDocEditor,
  updateDocHTMLBlock,
  insertDocArchitectureBlock,
  removeDocArchitecture,
  setDocActiveEditorQID,
  unsetDocActiveEditorQID,
  setDocActiveArchitectureQID,
  unsetDocActiveArchitectureQID,
  updateDocArchitecture,
  setDocActivePackageQID,
  unsetDocActivePackageQID,
  linkDocArchitecturePackageTaxonomy,
  linkDocArchitecturePackageReference,
  docArchitecturePackageInsertTaxonomyTree,
  docArchitecturePackageInsertReferenceTree,
  patchArchitectureStatusSubmitted,
  patchArchitectureStatusDraft,
  activeDoc,
}) => {
  const history = useHistory();
  const { user } = useUser();

  const {
    addPackageComment,
    deletePackageComment,
  } = usePackageComments();

  const {
    activeComments,
    setDocActiveComments,
    unsetDocActiveComments,
  } = useCommentsThread();

  useEffect(
    () => {
      initDoc();
      return () => resetDoc();
    },
    [initDoc, resetDoc],
  );

  useEffect(
    () => {
      const checkShared = async () => {
        const author = await getDoc(id);

        if (author.email !== user.email) {
          history.push(`/shared/${id}`);
        }
      };

      checkShared();
    },
    [getDoc, history, id, user],
  );

  const patchArchitecture = useCallback(({ id: archID, status }) => {
    if (status === ARCHITECTURE_DRAFT) {
      // submit
      patchArchitectureStatusDraft(archID);
    } else if (status === ARCHITECTURE_SUBMITTED) {
      // recall
      patchArchitectureStatusSubmitted(archID);
    }
  }, [patchArchitectureStatusDraft, patchArchitectureStatusSubmitted]);

  // TODO: handle graphql error to render not found
  const docUIError = activeDoc.ui.error;

  if (
    docUIError
    && (docUIError.status === 403 || docUIError.status === 404)
  ) {
    return (
      <EntityError
        error={docUIError}
        entityName="Document"
        canRequestAccess
      />
    );
  }
  return (
    <Document
      user={user}
      activeDoc={activeDoc}
      updateDocTitle={updateDocTitle}
      updateDocEditor={updateDocEditor}
      insertDocArchitectureBlock={insertDocArchitectureBlock}
      setDocActiveEditorQID={setDocActiveEditorQID}
      unsetDocActiveEditorQID={unsetDocActiveEditorQID}
      updateDocHTMLBlock={updateDocHTMLBlock}
      setDocActiveArchitectureQID={setDocActiveArchitectureQID}
      unsetDocActiveArchitectureQID={
          unsetDocActiveArchitectureQID
        }
      updateDocArchitecture={updateDocArchitecture}
      removeDocArchitecture={removeDocArchitecture}
      linkDocArchitecturePackageTaxonomy={
          linkDocArchitecturePackageTaxonomy
        }
      linkDocArchitecturePackageReference={
          linkDocArchitecturePackageReference
        }
      docArchitecturePackageInsertTaxonomyTree={
          docArchitecturePackageInsertTaxonomyTree
        }
      docArchitecturePackageInsertReferenceTree={
          docArchitecturePackageInsertReferenceTree
        }
      setDocActivePackageQID={setDocActivePackageQID}
      unsetDocActivePackageQID={unsetDocActivePackageQID}
      setDocActiveCommentThread={setDocActiveComments}
      unsetDocActiveCommentThread={unsetDocActiveComments}
      addDocPackageComment={addPackageComment}
      deleteDocPackageComment={deletePackageComment}
      activeComments={activeComments}
      patchArchitecture={patchArchitecture}
    />
  );
};

DocumentInterface.propTypes = {
  id: PropTypes.string.isRequired,
  initDoc: PropTypes.func.isRequired,
  getDoc: PropTypes.func.isRequired,
  resetDoc: PropTypes.func.isRequired,
  updateDocTitle: PropTypes.func.isRequired,
  updateDocEditor: PropTypes.func.isRequired,
  updateDocHTMLBlock: PropTypes.func.isRequired,
  insertDocArchitectureBlock: PropTypes.func.isRequired,
  removeDocArchitecture: PropTypes.func.isRequired,
  setDocActiveEditorQID: PropTypes.func.isRequired,
  unsetDocActiveEditorQID: PropTypes.func.isRequired,
  setDocActiveArchitectureQID: PropTypes.func.isRequired,
  unsetDocActiveArchitectureQID: PropTypes.func.isRequired,
  updateDocArchitecture: PropTypes.func.isRequired,
  setDocActivePackageQID: PropTypes.func.isRequired,
  unsetDocActivePackageQID: PropTypes.func.isRequired,
  linkDocArchitecturePackageTaxonomy: PropTypes.func.isRequired,
  linkDocArchitecturePackageReference: PropTypes.func.isRequired,
  docArchitecturePackageInsertTaxonomyTree: PropTypes.func.isRequired,
  docArchitecturePackageInsertReferenceTree: PropTypes.func.isRequired,
  patchArchitectureStatusSubmitted: PropTypes.func.isRequired,
  patchArchitectureStatusDraft: PropTypes.func.isRequired,
  activeDoc: PropTypes.shape({
    ui: PropTypes.shape({
      error: PropTypes.shape({
        status: PropTypes.number,
      }),
    }),
  }).isRequired,
};

DocumentInterface.defaultProps = {};

function mapStateToProps(state) {
  return {
    activeDoc: state.activeDoc,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      initDoc: initDocRaw,
      getDoc: getDocRaw,
      resetDoc: resetDocRaw,
      updateDocTitle: updateDocTitleRaw,
      updateDocEditor: updateDocEditorRaw,
      updateDocHTMLBlock: updateDocHTMLBlockRaw,
      insertDocArchitectureBlock: insertDocArchitectureBlockRaw,
      removeDocArchitecture: removeDocArchitectureRaw,
      setDocActiveEditorQID: setDocActiveEditorQIDRaw,
      unsetDocActiveEditorQID: unsetDocActiveEditorQIDRaw,
      setDocActiveArchitectureQID: setDocActiveArchitectureQIDRaw,
      unsetDocActiveArchitectureQID: unsetDocActiveArchitectureQIDRaw,
      updateDocArchitecture: updateDocArchitectureRaw,
      setDocActivePackageQID: setDocActivePackageQIDRaw,
      unsetDocActivePackageQID: unsetDocActivePackageQIDRaw,
      linkDocArchitecturePackageTaxonomy: linkDocArchitecturePackageTaxonomyRaw,
      linkDocArchitecturePackageReference: linkDocArchitecturePackageReferenceRaw,
      docArchitecturePackageInsertTaxonomyTree: docArchitecturePackageInsertTaxonomyTreeRaw,
      docArchitecturePackageInsertReferenceTree: docArchitecturePackageInsertReferenceTreeRaw,
      patchArchitectureStatusSubmitted: patchArchitectureStatusSubmittedRaw,
      patchArchitectureStatusDraft: patchArchitectureStatusDraftRaw,
    },
    dispatch,
  );
}

export default connect(mapStateToProps, mapDispatchToProps)(
  DocumentInterface,
);
