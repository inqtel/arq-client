import React, { useRef } from 'react';
import { RichUtils } from 'draft-js';
import DocumentCollaboratorsListInterface from 'components/Document/DocumentCollaboratorsListInterface.jsx';
import Dropdown from 'components/Dropdown/Dropdown.jsx';
import DocumentControl from 'components/Document/DocumentControl.jsx';
import InsertArchitecturePopup from 'components/OnboardingPopups/InsertArchitecturePopup.jsx';
import CollaboratePopup from 'components/OnboardingPopups/CollaboratePopup.jsx';

import {
  IconBold,
  IconItalic,
  IconUnderline,
  IconStrikethrough,
  IconUnorderedList,
  IconOrderedList,
} from 'components/Icons/Icons.jsx';

const INLINE_STYLES = [
  {
    name: 'bold',
    style: 'BOLD',
    icon: <IconBold display="block" />,
  },
  {
    name: 'italic',
    style: 'ITALIC',
    icon: <IconItalic display="block" />,
  },
  {
    name: 'underline',
    style: 'UNDERLINE',
    icon: <IconUnderline display="block" />,
  },
  {
    name: 'strikethrough',
    style: 'STRIKETHROUGH',
    icon: <IconStrikethrough display="block" />,
  },
];

const BLOCK_TYPES = [
  {
    name: 'unordered-list-item',
    style: 'unordered-list-item',
    icon: <IconUnorderedList display="block" />,
  },
  {
    name: 'ordered-list-item',
    style: 'ordered-list-item',
    icon: <IconOrderedList display="block" />,
  },
];

const DocumentControls = ({
  activeEditorQID,
  entities,
  activeDocQID,
  updateDocEditor,
  insertDocArchitectureBlock,
}) => {
  const dropdown = useRef();

  const editorToggleBlockType = (blockType) => {
    const { editorState } = entities.editors[activeEditorQID];

    updateDocEditor(
      activeEditorQID,
      RichUtils.toggleBlockType(editorState, blockType),
    );
  };

  const editorToggleInlineStyle = (inlineStyle) => {
    const { editorState } = entities.editors[activeEditorQID];

    updateDocEditor(
      activeEditorQID,
      RichUtils.toggleInlineStyle(editorState, inlineStyle),
    );
  };

  const onCollaboratorsUpdateComplete = () => {
    dropdown && dropdown.current.close();
  };

  const appendArchitectureBlock = () => insertDocArchitectureBlock();

  let editorState;
  let currentInlineStyle;
  let currentSelection;
  let currentBlockType;

  if (activeEditorQID) {
    ({ editorState } = entities.editors[activeEditorQID]);
    currentInlineStyle = editorState.getCurrentInlineStyle();
    currentSelection = editorState.getSelection();
    currentBlockType = editorState
      .getCurrentContent()
      .getBlockForKey(currentSelection.getStartKey())
      .getType();
  }

  return (
    <div className="Document-controls">
      <div className="Document-controls-group">
        {INLINE_STYLES.map(inlineStyle => (
          <DocumentControl
            key={inlineStyle.name}
            name={inlineStyle.name}
            controlStyle={inlineStyle.style}
            icon={inlineStyle.icon}
            active={
                currentInlineStyle
                && currentInlineStyle.has(inlineStyle.style)
              }
            onToggle={editorState && editorToggleInlineStyle}
            iconOnly
          />
        ))}
      </div>
      <div className="Document-controls-group">
        {BLOCK_TYPES.map(blockType => (
          <DocumentControl
            key={blockType.name}
            name={blockType.name}
            controlStyle={blockType.style}
            icon={blockType.icon}
            active={
                currentBlockType && currentBlockType === blockType.style
              }
            onToggle={editorState && editorToggleBlockType}
            iconOnly
          />
        ))}
      </div>
      <div className="Document-controls-group">
        <InsertArchitecturePopup />
        <DocumentControl
          name="architecture"
          controlStyle="architecture"
          text="Insert Architecture"
          onToggle={appendArchitectureBlock}
          textOnly
        />
      </div>
      <div className="Document-controls-group">
        <CollaboratePopup />
        <Dropdown ref={dropdown}>
          <Dropdown.Toggle>
            <DocumentControl
              name="collaborators"
              controlStyle="collaborators"
              text="Collaborate"
              textOnly
            />
          </Dropdown.Toggle>
          <Dropdown.Menu position="left">
            <DocumentCollaboratorsListInterface
              documentQID={activeDocQID}
              onUpdateComplete={onCollaboratorsUpdateComplete}
            />
          </Dropdown.Menu>
        </Dropdown>
      </div>
    </div>
  );
};

DocumentControls.propTypes = {};

DocumentControls.defaultProps = {};

export default DocumentControls;
