import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Editor from 'components/Editor/Editor.jsx';

class DocumentEditor extends Component {
  onFocus = () => {
    const {
      editor,
      setDocActiveEditorQID,
      unsetDocActivePackageQID,
    } = this.props;

    unsetDocActivePackageQID();
    setDocActiveEditorQID(editor.id);
  }

  onBlur = (qid) => {
    const { unsetDocActiveEditorQID } = this.props;

    unsetDocActiveEditorQID(qid);
  }

  onChange = (qid, editorState) => {
    const { updateDocEditor, updateDocHTMLBlock } = this.props;

    updateDocEditor(qid, editorState);

    if (editorState.getLastChangeType()) {
      updateDocHTMLBlock();
    }
  }

  render() {
    const { editor, index } = this.props;

    const placeholder = index === 0
      ? 'This is your notepad. Use it for notes, checklists or anything. Click LEARN in the top right corner to learn more.'
      : '';

    return (
      <Editor
        qid={editor.id}
        editorState={editor.editorState}
        onFocus={this.onFocus}
        onBlur={this.onBlur}
        onChange={this.onChange}
        placeholder={placeholder}
      />
    );
  }
}

DocumentEditor.propTypes = {
  editor: PropTypes.object.isRequired,
  updateDocEditor: PropTypes.func.isRequired,
  setDocActiveEditorQID: PropTypes.func.isRequired,
  unsetDocActiveEditorQID: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
};

DocumentEditor.defaultProps = {};

export default DocumentEditor;
