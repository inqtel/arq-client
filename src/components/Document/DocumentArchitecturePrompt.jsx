import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { noop } from 'lodash';
import cx from 'classnames';

const DocumentArchitecturePrompt = ({ children, onAnimationEnd, animate }) => {
  const headerRef = useRef();
  const [animating, setAnimating] = useState(false);

  useEffect(() => {
    const { current } = headerRef;
    current && current.addEventListener('animationend', animateDone);

    return function cleanup() {
      current.removeEventListener('animationend', animateDone);
    };
  });

  const animateDone = () => {
    onAnimationEnd();
    setAnimating(false);
  };

  useEffect(() => {
    animate && setAnimating(true);
  }, [animate]);

  return (
    <div className="Document-architecture-prompt-wrapper">
      <h3
        className={cx('Document-architecture-prompt', { animate: animating })}
        ref={headerRef}
      >
        {children}
      </h3>
    </div>
  );
};

DocumentArchitecturePrompt.propTypes = {
  children: PropTypes.node,
  animate: PropTypes.bool,
  onAnimationEnd: PropTypes.func,
};

DocumentArchitecturePrompt.defaultProps = {
  children: null,
  animate: false,
  onAnimationEnd: noop,
};

export default DocumentArchitecturePrompt;
