import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const DashboardContent = ({ children, align, padding }) => (
  <div
    className={cx('Dashboard-content', {
      [`Dashboard-content--align-${align}`]: align,
      [padding
        .map(direction => `Dashboard-content--padding-${direction}`)
        .join(' ')]: padding,
    })}
  >
    {children}
  </div>
);

DashboardContent.propTypes = {
  children: PropTypes.node,
  padding: PropTypes.arrayOf(PropTypes.string),
  align: PropTypes.string,
};

DashboardContent.defaultProps = {
  children: null,
  padding: [],
  align: null,
};

export default DashboardContent;
