import React from 'react';
import Dashboard from './Dashboard.jsx';

describe('<Dashboard />', () => {
  it('should render with .Dashboard class', () => {
    const wrapper = shallow(<Dashboard />);
    expect(wrapper.is('.Dashboard')).toBe(true);
  });

  it('should render children when passed in', () => {
    const wrapper = shallow(
      <Dashboard>
        <div className="test" />
      </Dashboard>,
    );
    expect(wrapper.find('div.test').length).toEqual(1);
  });
});

describe('<DashboardContent />', () => {
  it('should render with .Dashboard-content class', () => {
    const wrapper = shallow(<Dashboard.Content />);
    expect(wrapper.is('.Dashboard-content')).toBe(true);
  });

  it('should render children when passed in', () => {
    const wrapper = shallow(
      <Dashboard.Content>
        <div className="test" />
      </Dashboard.Content>,
    );
    expect(wrapper.find('div.test').length).toEqual(1);
  });
});

describe('<DashboardColumn />', () => {
  it('should render children when passed in', () => {
    const wrapper = shallow(
      <Dashboard.Column>
        <div className="test" />
      </Dashboard.Column>,
    );
    expect(wrapper.find('div.test').length).toEqual(1);
  });

  it('allows us to set size props', () => {
    const wrapper = mount(<Dashboard.Column size="md" />);
    expect(wrapper.props().size).toEqual('md');
  });

  it('allows us to set theme props', () => {
    const wrapper = mount(<Dashboard.Column theme="light" />);
    expect(wrapper.props().theme).toEqual('light');
  });
});
