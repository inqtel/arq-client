import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const DashboardSection = ({ children, fit, above }) => (
  <div
    className={cx('Dashboard-section', {
      'Dashboard-section--fit': fit,
      'Dashboard-section--above': above,
    })}
  >
    {children}
  </div>
);

DashboardSection.propTypes = {
  children: PropTypes.node,
  fit: PropTypes.bool,
  above: PropTypes.bool,
};

DashboardSection.defaultProps = {
  children: null,
  fit: false,
  above: false,
};

export default DashboardSection;
