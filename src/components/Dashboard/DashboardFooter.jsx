import React from 'react';
import PropTypes from 'prop-types';

const DashboardFooter = ({ children }) => (
  <div className="Dashboard-footer">
    {children}
    <div className="Dashboard-footer--text">
      {COLOPHON}
    </div>
  </div>
);

DashboardFooter.propTypes = {
  children: PropTypes.node,
};

DashboardFooter.defaultProps = {
  children: null,
};

export default DashboardFooter;
