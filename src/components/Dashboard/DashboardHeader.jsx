import React from 'react';
import PropTypes from 'prop-types';
import DashboardHeaderNavigationInterface from 'components/DashboardHeaderNavigation/DashboardHeaderNavigationInterface.jsx';
import Navigation from 'components/Navigation/Navigation.jsx';
import { AppLogo } from 'components/Logos/Logos.jsx';
import Link from 'components/Link/Link.jsx';
import { IconHome } from 'components/Icons/Icons.jsx';

const DashboardHeader = ({ showBreadcrumbs }) => (
  <div className="Dashboard-header">
    <div className="Dashboard-header-breadcrumbs">
      {showBreadcrumbs && (
        <Navigation>
          <Navigation.Item to="/">
            <IconHome
              position="left"
              display="inlineBlock"
              fill="#fff"
              height={18}
            />
            Home
          </Navigation.Item>
        </Navigation>
      )}
    </div>
    <div className="Dashboard-header-logo">
      <Link to="/">
        <AppLogo display="block" size={36} fill="#fff" />
      </Link>
    </div>
    <div className="Dashboard-header-navigation">
      <DashboardHeaderNavigationInterface />
    </div>
  </div>
);

DashboardHeader.propTypes = {
  showBreadcrumbs: PropTypes.bool,
};

DashboardHeader.defaultProps = {
  showBreadcrumbs: false,
};

export default DashboardHeader;
