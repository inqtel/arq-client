import React from 'react';
import PropTypes from 'prop-types';
import DashboardHeader from 'components/Dashboard/DashboardHeader.jsx';
import DashboardSection from 'components/Dashboard/DashboardSection.jsx';
import DashboardColumn from 'components/Dashboard/DashboardColumn.jsx';
import DashboardToolbar from 'components/Dashboard/DashboardToolbar.jsx';
import DashboardContent from 'components/Dashboard/DashboardContent.jsx';
import DashboardFooter from 'components/Dashboard/DashboardFooter.jsx';
import 'components/Dashboard/Dashboard.scss';

const Dashboard = ({ children }) => <div className="Dashboard">{children}</div>;

Dashboard.propTypes = {
  children: PropTypes.node,
};

Dashboard.defaultProps = {
  children: null,
};

Dashboard.Header = DashboardHeader;
Dashboard.Section = DashboardSection;
Dashboard.Column = DashboardColumn;
Dashboard.Toolbar = DashboardToolbar;
Dashboard.Content = DashboardContent;
Dashboard.Footer = DashboardFooter;

export default Dashboard;
