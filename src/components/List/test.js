import React from 'react';
import { setUp, setUpMount, checkProps } from 'utils/testing';
import List from 'components/List/List.jsx';

describe('<List />', () => {
  let wrapper;

  const expectedProps = { children: <List.Item>Test</List.Item> };

  beforeEach(() => {
    wrapper = setUp(List, expectedProps);
  });

  describe('checking PropTypes', () => {
    it('should not throw a warning', () => {
      const propsErr = checkProps(List, expectedProps);

      expect(propsErr).toBeUndefined();
    });
  });

  it('should render with .List class', () => {
    expect(wrapper.is('.List')).toBe(true);
  });

  it('should render a ListItem when passed in', () => {
    const props = { children: <List.Item>Test</List.Item> };

    const wrapperMount = setUpMount(List, props);

    const listItem = wrapperMount.find('.List-item');

    expect(listItem.text()).toBe('Test');
  });
});
