import React from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@material-ui/core/CircularProgress';

const IconLoader = ({ height }) => (
  <CircularProgress
    variant="indeterminate"
    size={height}
  />
);

IconLoader.propTypes = {
  height: PropTypes.number,
};

IconLoader.defaultProps = {
  height: 10,
};


export default IconLoader;
