import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import SettingsOutlinedIcon from '@material-ui/icons/SettingsOutlined';

const IconSettings = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <SettingsOutlinedIcon style={{ color: fill }} />
  </Icon>
);

IconSettings.propTypes = {
  fill: PropTypes.string,
};

IconSettings.defaultProps = {
  fill: '#6a71d7',
};

export default IconSettings;
