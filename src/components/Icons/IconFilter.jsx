import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import FilterListSharpIcon from '@material-ui/icons/FilterListSharp';

const IconFilter = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <FilterListSharpIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconFilter.propTypes = {
  fill: PropTypes.string,
};

IconFilter.defaultProps = {
  fill: '#6a71d7',
};

export default IconFilter;
