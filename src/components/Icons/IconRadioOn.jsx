import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import RadioButtonCheckedOutlinedIcon from '@material-ui/icons/RadioButtonCheckedOutlined';

const IconRadioOn = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <RadioButtonCheckedOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconRadioOn.propTypes = {
  fill: PropTypes.string,
};

IconRadioOn.defaultProps = {
  fill: '#6a71d7',
};

export default IconRadioOn;
