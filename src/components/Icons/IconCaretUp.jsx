import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

const IconCaretUp = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <KeyboardArrowUpIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconCaretUp.propTypes = {
  fill: PropTypes.string,
};

IconCaretUp.defaultProps = {
  fill: '#6a71d7',
};

export default IconCaretUp;
