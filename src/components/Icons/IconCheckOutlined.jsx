import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';

const IconCheckOutlined = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <CheckCircleOutlineIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconCheckOutlined.propTypes = {
  fill: PropTypes.string,
};

IconCheckOutlined.defaultProps = {
  fill: '#6a71d7',
};

export default IconCheckOutlined;
