import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import RemoveCircleOutlineOutlinedIcon from '@material-ui/icons/RemoveCircleOutlineOutlined';

const IconRemove = ({ fill, ...iconProps }) => (
  <Icon flip {...iconProps}>
    <RemoveCircleOutlineOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconRemove.propTypes = {
  fill: PropTypes.string,
};

IconRemove.defaultProps = {
  fill: '#6a71d7',
};

export default IconRemove;
