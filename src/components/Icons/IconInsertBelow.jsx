import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import SubdirectoryArrowRightOutlinedIcon from '@material-ui/icons/SubdirectoryArrowRightOutlined';

const IconInsertBelow = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <SubdirectoryArrowRightOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconInsertBelow.propTypes = {
  fill: PropTypes.string,
};

IconInsertBelow.defaultProps = {
  fill: '#6a71d7',
};

export default IconInsertBelow;
