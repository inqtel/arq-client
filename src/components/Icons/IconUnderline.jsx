import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import FormatUnderlinedOutlinedIcon from '@material-ui/icons/FormatUnderlinedOutlined';

const IconUnderline = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <FormatUnderlinedOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconUnderline.propTypes = {
  fill: PropTypes.string,
};

IconUnderline.defaultProps = {
  fill: '#6a71d7',
};

export default IconUnderline;
