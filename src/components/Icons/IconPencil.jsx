import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import EditOutlinedIcon from '@material-ui/icons/EditOutlined';

const IconPencil = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <EditOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconPencil.propTypes = {
  fill: PropTypes.string,
};

IconPencil.defaultProps = {
  fill: '#6a71d7',
};

export default IconPencil;
