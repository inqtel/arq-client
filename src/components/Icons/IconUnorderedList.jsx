import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import FormatListBulletedOutlinedIcon from '@material-ui/icons/FormatListBulletedOutlined';

const IconUnorderedList = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <FormatListBulletedOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconUnorderedList.propTypes = {
  fill: PropTypes.string,
};

IconUnorderedList.defaultProps = {
  fill: '#6a71d7',
};

export default IconUnorderedList;
