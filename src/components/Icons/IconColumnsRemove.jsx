import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import ViewColumnOutlinedIcon from '@material-ui/icons/ViewColumnOutlined';

const IconColumnsRemove = ({ fill, ...iconProps }) => (
  <div style={{ display: 'flex' }}>
    <Icon {...iconProps}>
      <ViewColumnOutlinedIcon style={{ color: fill }} fontSize="inherit" />
    </Icon>
    <sup style={{ top: 2, right: 2, color: fill }}>
      -
    </sup>
  </div>
);

IconColumnsRemove.propTypes = {
  fill: PropTypes.string,
};

IconColumnsRemove.defaultProps = {
  fill: '#6a71d7',
};

export default IconColumnsRemove;
