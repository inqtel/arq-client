import React from 'react';
import Icon from './Icon.jsx';

describe('<Icon />', () => {
  it('should render with .Icon class', () => {
    const wrapper = shallow(<Icon>children</Icon>);
    expect(wrapper.is('.Icon')).toBe(true);
  });
});
