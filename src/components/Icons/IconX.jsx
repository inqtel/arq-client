import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import SvgIcon from '@material-ui/core/SvgIcon';

const IconX = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <SvgIcon
      style={{ color: fill }}
      fontSize="inherit"
    >
      {Path}
    </SvgIcon>
  </Icon>
);

const Path = <path fillRule="evenodd" d="M14.59 8L12 10.59 9.41 8 8 9.41 10.59 12 8 14.59 9.41 16 12 13.41 14.59 16 16 14.59 13.41 12 16 9.41 14.59 8zM12 2C6.47 2 2 6.47 2 12s4.47 10 10 10 10-4.47 10-10S17.53 2 12 2z" />;

IconX.propTypes = {
  fill: PropTypes.string,
};

IconX.defaultProps = {
  fill: '#6a71d7',
};

export default IconX;
