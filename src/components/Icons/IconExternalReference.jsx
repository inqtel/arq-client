import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import OpenInNewOutlinedIcon from '@material-ui/icons/OpenInNewOutlined';

const IconExternalReference = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <OpenInNewOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconExternalReference.propTypes = {
  fill: PropTypes.string,
};

IconExternalReference.defaultProps = {
  fill: '#6a71d7',
};

export default IconExternalReference;
