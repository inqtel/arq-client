import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';

const IconCaretDown = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <KeyboardArrowDownIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconCaretDown.propTypes = {
  fill: PropTypes.string,
};

IconCaretDown.defaultProps = {
  fill: '#6a71d7',
};

export default IconCaretDown;
