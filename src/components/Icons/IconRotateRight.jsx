import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import RotateRightOutlinedIcon from '@material-ui/icons/RotateRightOutlined';

const IconRotateLeft = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <RotateRightOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconRotateLeft.propTypes = {
  fill: PropTypes.string,
};

IconRotateLeft.defaultProps = {
  fill: '#6a71d7',
};

export default IconRotateLeft;
