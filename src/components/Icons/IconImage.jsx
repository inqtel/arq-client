import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import ImageOutlinedIcon from '@material-ui/icons/ImageOutlined';

const IconImage = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <ImageOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconImage.propTypes = {
  fill: PropTypes.string,
};

IconImage.defaultProps = {
  fill: '#6a71d7',
};

export default IconImage;
