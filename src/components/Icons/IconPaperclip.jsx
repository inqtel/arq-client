import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import AttachFileOutlinedIcon from '@material-ui/icons/AttachFileOutlined';

const IconPaperclip = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <AttachFileOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconPaperclip.propTypes = {
  fill: PropTypes.string,
};

IconPaperclip.defaultProps = {
  fill: '#6a71d7',
};

export default IconPaperclip;
