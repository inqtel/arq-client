import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import SearchOutlinedIcon from '@material-ui/icons/SearchOutlined';

const IconSettings = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <SearchOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconSettings.propTypes = {
  fill: PropTypes.string,
};

IconSettings.defaultProps = {
  fill: '#6a71d7',
};

export default IconSettings;
