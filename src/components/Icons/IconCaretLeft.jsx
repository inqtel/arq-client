import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import KeyboardArrowLeftIcon from '@material-ui/icons/KeyboardArrowLeft';

const IconCaretLeft = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <KeyboardArrowLeftIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconCaretLeft.propTypes = {
  fill: PropTypes.string,
};

IconCaretLeft.defaultProps = {
  fill: '#6a71d7',
};

export default IconCaretLeft;
