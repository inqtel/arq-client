import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import DashboardOutlinedIcon from '@material-ui/icons/DashboardOutlined';

const IconDrawing = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <DashboardOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconDrawing.propTypes = {
  fill: PropTypes.string,
};

IconDrawing.defaultProps = {
  fill: '#6a71d7',
};

export default IconDrawing;
