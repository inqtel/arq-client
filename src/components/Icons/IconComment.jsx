import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import QuestionAnswerOutlinedIcon from '@material-ui/icons/QuestionAnswerOutlined';

const IconComment = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <QuestionAnswerOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconComment.propTypes = {
  fill: PropTypes.string,
};

IconComment.defaultProps = {
  fill: '#6a71d7',
};

export default IconComment;
