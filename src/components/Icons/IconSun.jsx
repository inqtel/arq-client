import React from 'react';
import PropTypes from 'prop-types';
import Icon from 'components/Icons/Icon.jsx';
import SentimentVerySatisfiedOutlinedIcon from '@material-ui/icons/SentimentVerySatisfiedOutlined';

const IconSun = ({ fill, ...iconProps }) => (
  <Icon {...iconProps}>
    <SentimentVerySatisfiedOutlinedIcon style={{ color: fill }} fontSize="inherit" />
  </Icon>
);

IconSun.propTypes = {
  fill: PropTypes.string,
};

IconSun.defaultProps = {
  fill: '#6a71d7',
};

export default IconSun;
