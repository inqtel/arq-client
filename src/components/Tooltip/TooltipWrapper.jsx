import React from 'react';
import PropTypes from 'prop-types';

const TooltipWrapper = ({
  children,
  showTooltip,
  hideTooltip,
}) => (
  <span
    className="Tooltip-wrapper"
    onMouseEnter={showTooltip}
    onMouseLeave={hideTooltip}
  >
    {children}
  </span>
);

TooltipWrapper.propTypes = {
  children: PropTypes.node.isRequired,
  showTooltip: PropTypes.func,
  hideTooltip: PropTypes.func,
};

TooltipWrapper.defaultProps = {};

export default TooltipWrapper;
