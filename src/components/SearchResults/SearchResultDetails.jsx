import React from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType } from 'proptypes';
import { get, unionBy } from 'lodash';
import { useQuery } from '@apollo/client';
import { listToTree } from 'utils/tree';
import { GET_TAXONOMY_TREE } from 'queries/taxonomies';
import { TAXONOMY_QUERY_ROOT } from 'constants/gql';
import Tree from 'components/Tree/Tree.jsx';
import Loader from 'components/Loader/Loader.jsx';

const SearchResultDetails = ({ taxonomy, actions }) => {
  const { loading, error, data } = useQuery(
    GET_TAXONOMY_TREE, {
      variables: { id: taxonomy.id },
    },
  );

  const fullTaxonomy = get(data, TAXONOMY_QUERY_ROOT);

  let tree;

  if (fullTaxonomy) {
    const { ancestors, allChildren } = fullTaxonomy;

    [tree] = listToTree(unionBy(ancestors, allChildren, 'id'));
  }

  return (
    <div className="SearchResult-details">
      {loading && <Loader />}
      {!loading && !error && tree && (
        <Tree
          item={tree}
          expanded={fullTaxonomy.allChildren.length < 1000}
          actions={actions}
          highlightItem={item => item.id === taxonomy.id}
        />
      )}
    </div>
  );
};

SearchResultDetails.propTypes = {
  taxonomy: taxonomyPropType.isRequired,
  actions: PropTypes.shape({
    primary: PropTypes.arrayOf(PropTypes.shape({})),
  }).isRequired,
};

export default SearchResultDetails;
