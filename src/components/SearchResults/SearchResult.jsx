import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType, treeItemActionPropType } from 'proptypes';
import cx from 'classnames';
import { sourceNames, SOURCE_ARQ } from 'constants/sources';
import { orderAncestorsByParentId } from 'utils/elastic';
import Button from 'components/Button/Button.jsx';
import { IconCaretDown } from 'components/Icons/Icons.jsx';
import SearchResultDetails from 'components/SearchResults/SearchResultDetails.jsx';
import 'components/SearchResults/SearchResults.scss';

const SearchResult = ({ taxonomy, actions, hoverAction }) => {
  const [expanded, setExpanded] = useState(false);

  const {
    ancestors,
    source,
    source_name: esSourceName,
  } = taxonomy;

  const sourceName = esSourceName || source.name;

  let metaString = null;
  if (ancestors) {
    const ordered = orderAncestorsByParentId(ancestors);
    metaString = `${ordered[0].term} >`;
    if (ordered.length > 3) metaString += ' ... >';
    if (ordered.length > 2) metaString += ` ${ordered[ordered.length - 2].term} >`;
  }

  return (
    <div className="SearchResult">
      <div className="SearchResult-result">
        <div className="SearchResult-result-info">
          <div className="SearchResult-result-info-meta">
            <div className="SearchResult-source" style={{ color: sourceName === SOURCE_ARQ ? '#6a71d7' : '#5d5c6b' }}>
              {sourceNames[sourceName]}
            </div>
            {metaString && <div className="SearchResult-breadcrumbs">{metaString}</div>}
          </div>
          <span
            className={cx('SearchResult-term', { 'SearchResult-term--hover': hoverAction })}
          >
            {hoverAction ? (
              <Button
                unstyled
                onClick={() => hoverAction.handler(taxonomy)}
              >
                {taxonomy.term}
                {hoverAction.icon}
              </Button>
            ) : (<span>{taxonomy.term}</span>)}
          </span>
        </div>
        <div className="SearchResult-result-expand">
          <Button unstyled onClick={() => setExpanded(!expanded)}>
            <IconCaretDown rotate={expanded ? 180 : null} />
          </Button>
        </div>
      </div>
      {expanded && <SearchResultDetails taxonomy={taxonomy} actions={actions} />}
    </div>
  );
};

SearchResult.propTypes = {
  taxonomy: taxonomyPropType.isRequired,
  actions: PropTypes.shape({
    primary: PropTypes.arrayOf(treeItemActionPropType),
  }).isRequired,
  hoverAction: treeItemActionPropType,
};

SearchResult.defaultProps = {
  hoverAction: null,
};

export default SearchResult;
