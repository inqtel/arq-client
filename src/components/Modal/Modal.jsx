import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Portal } from 'react-portal';
import cx from 'classnames';
import Loader from 'components/Loader/Loader.jsx';
import Button from 'components/Button/Button.jsx';
import { IconClose } from 'components/Icons/Icons.jsx';
import 'components/Modal/Modal.scss';
import 'components/LearningModule/LearningModule.scss';
import { noop } from 'lodash';

const Modal = ({
  actions,
  children,
  className,
  closeModal,
  learningModule,
  isOpened,
  isProgressing,
  title,
}) => {
  const [isOpenedState, setIsOpenedState] = useState(isOpened);
  const [isProgressingState, setIsProgressingState] = useState(isProgressing);

  useEffect(() => {
    if (
      isOpenedState !== isOpened
      && !isProgressingState
    ) {
      setIsOpenedState(isOpened);
    }
    if (isProgressingState !== isProgressing) {
      setIsProgressingState(isProgressing);
    }
  }, [isOpenedState, isProgressingState, isOpened, isProgressing]);

  const close = (e) => {
    e.preventDefault();

    if (!isProgressingState) {
      setIsOpenedState(false);
      closeModal();
    }
  };

  if (!isOpenedState) return null;

  if (learningModule) {
    return (
      <Portal closeOnOutsideClick>
        <div className={cx('Modal', className)}>
          <div
            className={cx('Modal-content', {
              'Modal--padding': false,
            })}
          >
            <div className="Modal-close">
              <Button unstyled onClick={close}><IconClose position="right" height={16} /></Button>
            </div>
            {children}
            {isProgressingState && (
              <div className="Modal-progress">
                <Loader size={36} />
              </div>
            )}
          </div>
          <div role="presentation" className="Modal-mask" onClick={close} />
        </div>
      </Portal>
    );
  }
  return (
    <Portal closeOnOutsideClick>
      <div className="Modal">
        <div
          className={cx('Modal-content', {
            'Modal--padding': true,
          })}
        >
          <h2 className="Modal-title">{title}</h2>
          <p className="Modal-text">{children}</p>
          <div className="Modal-actions">{actions}</div>
          {isProgressingState && (
          <div className="Modal-progress">
            <Loader size={36} />
          </div>
          )}
        </div>
        <div role="presentation" className="Modal-mask" onClick={close} />
      </div>
    </Portal>
  );
};

Modal.propTypes = {
  actions: PropTypes.node,
  children: PropTypes.node,
  className: PropTypes.string,
  closeModal: PropTypes.func,
  learningModule: PropTypes.bool,
  title: PropTypes.string,
  isOpened: PropTypes.bool,
  isProgressing: PropTypes.bool,
};

Modal.defaultProps = {
  actions: null,
  className: '',
  children: null,
  closeModal: noop,
  learningModule: false,
  title: '',
  isOpened: false,
  isProgressing: false,
};

export default Modal;
