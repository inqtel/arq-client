import React from 'react';
import PropTypes from 'prop-types';
import { IconClose } from 'components/Icons/Icons.jsx';

const SearchKitSelectedFilter = ({
  bemBlocks,
  labelKey,
  labelValue,
  removeFilter,
}) => (
  <div className={bemBlocks.option()}>
    <div className={bemBlocks.option('name')}>
      {`${labelKey}: ${labelValue}`}
    </div>
    <div className={bemBlocks.option('remove-action')} onClick={removeFilter}>
      <IconClose />
    </div>
  </div>
);

SearchKitSelectedFilter.propTypes = {
  bemBlocks: PropTypes.shape({
    option: PropTypes.func,
  }).isRequired,
  labelKey: PropTypes.string.isRequired,
  labelValue: PropTypes.string.isRequired,
  removeFilter: PropTypes.func.isRequired,
};

export default SearchKitSelectedFilter;
