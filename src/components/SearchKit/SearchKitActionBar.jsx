import React from 'react';
import PropTypes from 'prop-types';
import Grid from 'components/Grid/Grid.jsx';

const SearchKitActionBar = ({ children }) => (
  <div className="SearchKit-action-bar">
    <Grid direction="row">{children}</Grid>
  </div>
);

SearchKitActionBar.propTypes = {
  children: PropTypes.node.isRequired,
};

export default SearchKitActionBar;
