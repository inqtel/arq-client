import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { SORT_OPTIONS } from 'constants/elastic.js';
import Dropdown from 'components/Dropdown/Dropdown.jsx';
import ActionBox from 'components/ActionBox/ActionBox.jsx';
import ActionList from 'components/ActionList/ActionList.jsx';

const SearchKitRefinementListSortToggle = ({
  taxonomiesSort,
  setTaxonomiesSort,
}) => {
  const dropdown = useRef();

  const handleOptionSelect = (option) => {
    setTaxonomiesSort(option);
    dropdown && dropdown.current.close();
  };

  return (
    <Dropdown ref={dropdown}>
      <Dropdown.Toggle>
        <span className="SearchKit-refinement-list-operator-toggle">
          {taxonomiesSort.term}
        </span>
      </Dropdown.Toggle>
      <Dropdown.Menu position="right">
        <ActionBox>
          <ActionList>
            {SORT_OPTIONS.map(option => (
              <ActionList.Item
                key={option.key}
                onClick={() => handleOptionSelect(option)}
              >
                <span style={{ whiteSpace: 'nowrap' }}>{option.term}</span>
              </ActionList.Item>
            ))}
          </ActionList>
        </ActionBox>
      </Dropdown.Menu>
    </Dropdown>
  );
};

SearchKitRefinementListSortToggle.propTypes = {
  taxonomiesSort: PropTypes.shape({
    term: PropTypes.string.isRequired,
    key: PropTypes.string.isRequired,
    direction: PropTypes.string.isRequired,
  }).isRequired,
  setTaxonomiesSort: PropTypes.func.isRequired,
};

export default SearchKitRefinementListSortToggle;
