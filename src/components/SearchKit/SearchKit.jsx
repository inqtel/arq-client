import React from 'react';
import PropTypes from 'prop-types';
import { SearchkitProvider } from 'searchkit';
import SearchKitError from 'components/SearchKit/SearchKitError.jsx';
import SearchKitActionBar from 'components/SearchKit/SearchKitActionBar.jsx';
import SearchKitFilterOption from 'components/SearchKit/SearchKitFilterOption.jsx';
import SearchKitReset from 'components/SearchKit/SearchKitReset.jsx';
import SearchKitSelectedFilter from 'components/SearchKit/SearchKitSelectedFilter.jsx';
import 'components/SearchKit/SearchKit.scss';

const SearchKit = ({ searchkit, children }) => (
  <SearchkitProvider searchkit={searchkit}>{children}</SearchkitProvider>
);

SearchKit.propTypes = {
  searchkit: PropTypes.shape({
    host: PropTypes.string.isRequired,
  }).isRequired,
  children: PropTypes.node.isRequired,
};

SearchKit.Error = SearchKitError;
SearchKit.ActionBar = SearchKitActionBar;
SearchKit.FilterOption = SearchKitFilterOption;
SearchKit.Reset = SearchKitReset;
SearchKit.SelectedFilter = SearchKitSelectedFilter;

export default SearchKit;
