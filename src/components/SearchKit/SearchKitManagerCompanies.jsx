import { SearchkitManager } from 'searchkit';

const SearchkitManagerCompanies = new SearchkitManager(`${ES_HOST}/organizations`);

export default SearchkitManagerCompanies;
