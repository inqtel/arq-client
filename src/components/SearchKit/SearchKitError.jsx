import React from 'react';
import PropTypes from 'prop-types';
import Alert from 'components/Alert/Alert.jsx';

const SearchKitError = ({ bemBlocks }) => (
  <div data-qa="no-hits" className={bemBlocks.container()}>
    <div className={bemBlocks.container('info')}>
      <Alert
        padded
        heading="Uh Oh..."
        subheading="There was an Elasticsearch error."
      />
    </div>
  </div>
);

SearchKitError.propTypes = {
  bemBlocks: PropTypes.shape({
    container: PropTypes.func.isRequired,
  }).isRequired,
};

export default SearchKitError;
