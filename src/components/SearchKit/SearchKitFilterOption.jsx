import React from 'react';
import PropTypes from 'prop-types';
import Checkbox from 'components/Checkbox/Checkbox.jsx';

const SearchKitFilterOption = ({
  label,
  onClick,
  active,
  count,
}) => (
  <div className="SearchKit-filter-option">
    <Checkbox
      id={`checkbox-${label}`}
      label={label}
      onChange={onClick}
      checked={active}
    />
    <div className="SearchKit-filter-option-count">{count}</div>
  </div>
);

SearchKitFilterOption.propTypes = {
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  active: PropTypes.bool.isRequired,
  count: PropTypes.number,
};

SearchKitFilterOption.defaultProps = {
  count: null,
};

export default SearchKitFilterOption;
