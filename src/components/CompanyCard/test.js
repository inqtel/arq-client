import React from 'react';
import CompanyCard from 'components/CompanyCard/CompanyCard.jsx';

describe('<CompanyCard />', () => {
  it('should render with .CompanyCard class', () => {
    const wrapper = shallow(<CompanyCard />);
    expect(wrapper.is('.CompanyCard')).toBe(true);
  });
});
