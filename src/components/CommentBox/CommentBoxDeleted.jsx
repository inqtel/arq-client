import React from 'react';
import { userPropType } from 'proptypes';
import { IconBlock } from 'components/Icons/Icons.jsx';

const CommentBoxDeleted = ({ author, user }) => (
  <div className="CommentBoxDeleted">
    <IconBlock fill="#5d5c6b" />
    {` ${user.email === author.email ? 'You' : author.email} `}
    deleted this comment
  </div>
);

CommentBoxDeleted.propTypes = {
  author: userPropType.isRequired,
  user: userPropType.isRequired,
};

export default CommentBoxDeleted;
