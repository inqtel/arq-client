import React, { PureComponent } from 'react';
import Button from 'components/Button/Button.jsx';
import { IconSettings } from 'components/Icons/Icons.jsx';
import PropTypes from 'prop-types';
import Popover from 'components/Popover/Popover.jsx';

class CommentBoxTools extends PureComponent {
  render() {
    const { onDeleteComment, comment } = this.props;
    return (
      <div className="CommentBox-tools-popover">
        <Popover leftOrRight="right" topOrBottom="top" width="auto">
          <Popover.Toggle>
            <IconSettings height={16} />
          </Popover.Toggle>
          <Popover.Box>
            <Popover.Box.Content>
              <Button unstyled onClick={() => onDeleteComment(comment)}>
                Delete
              </Button>
            </Popover.Box.Content>
          </Popover.Box>
        </Popover>
      </div>
    );
  }
}

CommentBoxTools.propTypes = {
  comment: PropTypes.shape({
    qid: PropTypes.string,
  }).isRequired,
  onDeleteComment: PropTypes.func.isRequired,
};

CommentBoxTools.defaultProps = {};

export default CommentBoxTools;
