import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import Button from 'components/Button/Button.jsx';
import { IconClose } from 'components/Icons/Icons.jsx';
import Loader from 'components/Loader/Loader.jsx';

const CommentBoxActions = ({
  onPost,
  onCancel,
  isSubmitting,
}) => (
  <div
    className={cx('CommentBox-actions', {
      'CommentBox-actions--isSubmitting': isSubmitting,
    })}
  >
    <span className="CommentBox-action">
      <Button unstyled display="block" onClick={onCancel}>
        <IconClose display="block" height={15} />
      </Button>
    </span>
    <span className="CommentBox-action">
      <Button unstyled display="block" onClick={onPost}>
        Post
      </Button>
    </span>
    {isSubmitting && (
      <div className="CommentBox-actions-progress">
        <Loader size={12} />
      </div>
    )}
  </div>
);

CommentBoxActions.propTypes = {
  onPost: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  isSubmitting: PropTypes.bool.isRequired,
};

CommentBoxActions.defaultProps = {};

export default CommentBoxActions;
