import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import 'components/Logos/Logos.scss';

const PitchbookLogo = ({ display, fill, size }) => (
  <svg
    className={cx('Logo', `Logo--display-${display}`)}
    width={size}
    height={size}
    viewBox="0.5 0.5 114.18107 79.913374"
  >
    <path
      fill={fill}
      d="m 68.809331,0 c -53.6,4.3 -75.7999999,47.5 -66.8999999,74.7 5.2,-7.8 14.7999999,-16.2 34.4999999,-19.3 -1.2,-22.9 7.4,-39 32.4,-55.4"
    />
    <path
      fill={fill}
      d="m 41.409331,58.7 c -13.5,2.2 -23,5.8 -28.9,11.2 0.2,7.2 2.2,14.2 6.2,20.5 4.6,-10.9 19.6,-18.6 42.5,-18.7 -5.6,-26.1
      6.8,-50 26.200002,-66.1 -10.4,1.6 -19.700002,4.1 -27.900002,7.3 -14.8,13.8 -20.3,31.6 -18.1,45.8"
    />
  </svg>
);

PitchbookLogo.propTypes = {
  display: PropTypes.oneOf(['inlineBlock', 'block']),
  fill: PropTypes.string,
  size: PropTypes.number,
};

PitchbookLogo.defaultProps = {
  display: 'inlineBlock',
  fill: '#426893',
  size: 206,
};

export default PitchbookLogo;
