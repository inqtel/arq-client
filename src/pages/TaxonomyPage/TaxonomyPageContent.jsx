import React from 'react';
import { taxonomyPropType } from 'proptypes';
import { sourceNames } from 'constants/sources/';
import { getTaxonomyRootAncestor } from 'utils/taxonomies';
import { TAXONOMY_QUERY_ROOT } from 'constants/gql';
import Panel from 'components/Panel/Panel.jsx';
import Badge from 'components/Badge/Badge.jsx';
import TaxonomyTree from 'components/TaxonomyTree/TaxonomyTree.jsx';
import GraphPanelInterface from 'components/GraphPanel/GraphPanelInterface.jsx';
import RelatedPanelInterface from 'components/RelatedPanel/RelatedPanelInterface.jsx';
import { GET_TAXONOMY_COMPANIES, GET_TAXONOMY_PROBLEMS, GET_TAXONOMY_ARCHITECTURES } from 'queries/taxonomies';
import { GET_TAXONOMY_RELATED_NODES } from 'queries/graph';

const TaxonomyPageContent = ({ taxonomy }) => {
  const root = getTaxonomyRootAncestor(taxonomy.ancestors);

  const renderItem = (tax) => {
    if (tax.source.name !== root.source.name) {
      return (
        <>
          {tax.term}
          {' '}
          <Badge text={sourceNames[tax.source.name]} />
        </>
      );
    }

    return `${tax.term}`;
  };

  return (
    <div className="TaxonomyPage-content">
      <Panel>
        <Panel.Heading>
          <div className="TaxonomyPage-title">{taxonomy.term}</div>
        </Panel.Heading>
        <Panel.Content>
          <TaxonomyTree
            taxonomy={root}
            allTaxonomies={taxonomy.ancestors}
            renderItem={renderItem}
            expanded
          />
        </Panel.Content>
      </Panel>
      <RelatedPanelInterface
        id={taxonomy.id}
        query={GET_TAXONOMY_ARCHITECTURES}
        parentKey={TAXONOMY_QUERY_ROOT}
        childKey="architectures"
      />
      <RelatedPanelInterface
        id={taxonomy.id}
        query={GET_TAXONOMY_COMPANIES}
        parentKey={TAXONOMY_QUERY_ROOT}
        childKey="companies"
      />
      <RelatedPanelInterface
        id={taxonomy.id}
        query={GET_TAXONOMY_PROBLEMS}
        parentKey={TAXONOMY_QUERY_ROOT}
        childKey="problemSets"
      />
      <GraphPanelInterface
        id={taxonomy.id}
        query={GET_TAXONOMY_RELATED_NODES}
        type={TAXONOMY_QUERY_ROOT}
      />
    </div>
  );
};

TaxonomyPageContent.propTypes = {
  taxonomy: taxonomyPropType.isRequired,
};

TaxonomyPageContent.defaultProps = {};

export default TaxonomyPageContent;
