import React from 'react';
import { matchPropType } from 'proptypes';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import TaxonomyPageContent from 'pages/TaxonomyPage/TaxonomyPageContent.jsx';
import Loader from 'components/Loader/Loader.jsx';
import useTaxonomyDetails from 'hooks/useTaxonomyDetails.jsx';
import 'pages/TaxonomyPage/TaxonomyPage.scss';

const TaxonomyPage = ({ match }) => {
  const { id } = match.params;

  const { taxonomy, error, loading } = useTaxonomyDetails({ id });

  return (
    <div className="TaxonomyPage">
      <Dashboard>
        <Dashboard.Header showBreadcrumbs />
        <Dashboard.Section fit>
          <Dashboard.Column minWidth="sm" fit>
            <Dashboard.Content padding={['top', 'right', 'bottom', 'left']}>
              {loading && <Loader isCentered size={36} />}
              {!loading && error && <div>Error</div>}
              {!loading && taxonomy && (
                <TaxonomyPageContent taxonomy={taxonomy} />
              )}
            </Dashboard.Content>
          </Dashboard.Column>
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

TaxonomyPage.propTypes = {
  match: matchPropType.isRequired,
};

TaxonomyPage.defaultProps = {};

export default TaxonomyPage;
