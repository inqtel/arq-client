import { useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getSharedDoc as getSharedDocRaw } from 'actions/sharedDoc';

const useSharedDocumentPage = ({ id }) => {
  // legacy redux
  const dispatch = useDispatch();

  const sharedDoc = useSelector(state => state.sharedDoc);

  const getSharedDoc = useCallback(() => dispatch(getSharedDocRaw(id)), [id, dispatch]);

  return {
    data: sharedDoc.data,
    ui: sharedDoc.ui,
    entities: sharedDoc.entities,
    getSharedDoc,
  };
};

export default useSharedDocumentPage;
