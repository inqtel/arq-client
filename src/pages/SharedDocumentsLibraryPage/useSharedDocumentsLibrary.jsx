import { useQuery } from '@apollo/client';
import { SHARED_DOCUMENTS } from 'queries/documents';
import { SHARED_DOCUMENTS_QUERY_ROOT } from 'constants/gql';
import { get } from 'lodash';

const useSharedDocumentsLibrary = () => {
  const {
    loading,
    error,
    data,
    refetch,
  } = useQuery(SHARED_DOCUMENTS, {
    fetchPolicy: 'cache-and-network',
  });

  const docs = get(data, SHARED_DOCUMENTS_QUERY_ROOT, []);

  return {
    docs,
    error,
    loading,
    refetch,
  };
};

export default useSharedDocumentsLibrary;
