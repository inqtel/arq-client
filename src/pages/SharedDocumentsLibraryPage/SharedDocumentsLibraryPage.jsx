import React from 'react';
import { SHARED_DOC } from 'constants/doctypes.js';
import DocumentsLibrary from 'components/DocumentsLibrary/DocumentsLibrary.jsx';
import useSharedDocumentsLibrary from 'pages/SharedDocumentsLibraryPage/useSharedDocumentsLibrary.jsx';
import useViewport from 'hooks/useViewport.jsx';

const SharedDocumentsLibraryPage = () => {
  const {
    docs,
    error,
    loading,
    refetch,
  } = useSharedDocumentsLibrary();

  const { viewport } = useViewport();

  return (
    <DocumentsLibrary
      active="shared"
      doctype={SHARED_DOC}
      docs={docs}
      error={error}
      loading={loading}
      refetch={refetch}
      viewport={viewport}
    />
  );
};

SharedDocumentsLibraryPage.propTypes = {};

SharedDocumentsLibraryPage.defaultProps = {};

export default SharedDocumentsLibraryPage;
