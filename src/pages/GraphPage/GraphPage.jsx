import React from 'react';
import PropTypes from 'prop-types';
import { matchPropType } from 'proptypes';
import ActiveGraphInterface from 'components/ActiveGraph/ActiveGraphInterface.jsx';

const GraphPage = ({ match, type }) => <ActiveGraphInterface id={match.params.id} type={type} />;

GraphPage.propTypes = {
  match: matchPropType.isRequired,
  type: PropTypes.string.isRequired,
};

GraphPage.defaultProps = {};

export default GraphPage;
