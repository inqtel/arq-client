import { useQuery } from '@apollo/client';
import { get, sortBy } from 'lodash';
import useAdminCheck from 'hooks/useAdminCheck.jsx';
import { GET_TAXONOMY_SOURCE_REFERENCED_TAXONOMIES } from 'queries/sources';
import {
  SOURCE_ACM,
  SOURCE_IAS,
  SOURCE_IEEE,
  SOURCE_PLOS,
} from 'constants/sources';
import { SOURCES_QUERY_ROOT } from 'constants/gql';

const useAdminTaxonomies = () => {
  useAdminCheck();

  const { loading, error, data } = useQuery(
    GET_TAXONOMY_SOURCE_REFERENCED_TAXONOMIES, {
      fetchPolicy: 'cache-and-network',
      variables: { source_in: [SOURCE_ACM, SOURCE_IAS, SOURCE_IEEE, SOURCE_PLOS] },
    },
  );
  const sources = get(data, SOURCES_QUERY_ROOT, []);

  let linkedTaxonomies = [];
  sources.forEach((s) => {
    linkedTaxonomies = linkedTaxonomies.concat(s.referencedTaxonomyRoots);
  });

  linkedTaxonomies = linkedTaxonomies.map(tax => ({
    ...tax,
    architectureCount: tax.referencedBy.filter(p => p.architecture.isLatestArch).length,
  }));

  linkedTaxonomies = sortBy(linkedTaxonomies, t => -t.architectureCount);

  return {
    loading,
    error,
    linkedTaxonomies,
  };
};

export default useAdminTaxonomies;
