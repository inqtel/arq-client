import React from 'react';
import WelcomeBackInterface from 'components/WelcomeBack/WelcomeBackInterface.jsx';
import SidebarNavigationInterface from 'components/SidebarNavigation/SidebarNavigationInterface.jsx';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import Toolbar from 'components/Toolbar/Toolbar.jsx';
import Link from 'components/Link/Link.jsx';
import Grid from 'components/Grid/Grid.jsx';
import Loader from 'components/Loader/Loader.jsx';
import Alert from 'components/Alert/Alert.jsx';
import { IconCaretRight } from 'components/Icons/Icons.jsx';
import AdminTaxonomiesPageContent from 'pages/AdminTaxonomiesPage/AdminTaxonomiesPageContent.jsx';
import useAdminTaxonomies from 'pages/AdminTaxonomiesPage/useAdminTaxonomies.jsx';
import 'pages/AdminTaxonomiesPage/AdminTaxonomiesPage.scss';

const AdminTaxonomiesPage = () => {
  const { loading, error, linkedTaxonomies } = useAdminTaxonomies();

  return (
    <div className="AdminTaxonomiesPage">
      <Dashboard>
        <Dashboard.Header showBreadcrumbs />
        <Dashboard.Section fit>
          <Dashboard.Column padded size="sm" theme="dark">
            <Grid direction="column" justify="spaceBetween">
              <SidebarNavigationInterface active="merge" />
              <Dashboard.Footer>
                <WelcomeBackInterface />
              </Dashboard.Footer>
            </Grid>
          </Dashboard.Column>
          <Dashboard.Column fit theme="light">
            <Dashboard.Section>
              <Dashboard.Toolbar>
                <Toolbar>
                  <Toolbar.Group fill />
                  <Toolbar.Group padded>
                    <Toolbar.Item>
                      <Link style={{ textAlign: 'right' }} to="/admin/merge/">
                        Go to Taxonomy Editor
                        <IconCaretRight display="inlineBlock" position="right" />
                      </Link>
                    </Toolbar.Item>
                  </Toolbar.Group>
                </Toolbar>
              </Dashboard.Toolbar>
            </Dashboard.Section>
            <Dashboard.Content padding={['top', 'right', 'bottom', 'left']}>
              {loading && <Loader size={36} isCentered isHeight100 />}
              {!loading && error && (
                <Alert
                  padded
                  heading="Uh Oh..."
                  subheading="There was an error. Check your network connection and try again."
                />
              )}
              {!loading && !error
                && <AdminTaxonomiesPageContent linkedTaxonomies={linkedTaxonomies} />
              }
            </Dashboard.Content>
          </Dashboard.Column>
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

AdminTaxonomiesPage.propTypes = {};

export default AdminTaxonomiesPage;
