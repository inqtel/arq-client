import React from 'react';
import { matchPropType } from 'proptypes';
import ArchitectureContent from 'components/Architecture/ArchitectureContent.jsx';
import usePublishedArchitecture from 'pages/PublishedArchitecturePage/usePublishedArchitecture.jsx';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import Loader from 'components/Loader/Loader.jsx';
import Alert from 'components/Alert/Alert.jsx';
import 'pages/PublishedArchitecturePage/PublishedArchitecturePage.scss';

const PublishedArchitecturePage = ({ match }) => {
  const {
    publishedArchitecture,
    error,
    loading,
  } = usePublishedArchitecture({ id: match.params.id });

  return (
    <div className="PublishedArchitecturePage">
      <Dashboard>
        <Dashboard.Header showBreadcrumbs />
        <Dashboard.Section fit>
          <Dashboard.Column fit theme="light">
            <Dashboard.Content minWidth="sm" padding={['top', 'right', 'left']}>
              {error && (
                <Alert
                  heading="Uh Oh..."
                  subheading="There was an error fetching this architecture."
                />
              )}
              {!error && loading && <Loader size={48} isFullscreen />}
              {!loading && !error && !publishedArchitecture && (
                <Alert
                  heading="Uh Oh..."
                  subheading="This architecture hasn't been published."
                />
              )}
              {!loading && !error && publishedArchitecture && (
                <ArchitectureContent
                  publishedArchitecture={publishedArchitecture}
                  publishedView
                />
              )}
            </Dashboard.Content>
            <Dashboard.Footer />
          </Dashboard.Column>
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};
PublishedArchitecturePage.propTypes = {
  match: matchPropType.isRequired,
};

PublishedArchitecturePage.defaultProps = {};

export default PublishedArchitecturePage;
