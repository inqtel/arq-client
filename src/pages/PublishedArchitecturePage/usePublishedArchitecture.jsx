import { useQuery } from '@apollo/client';
import { GET_PUBLISHED_ARCHITECTURE } from 'queries/architectures';
import { ARCHITECTURE_QUERY_ROOT } from 'constants/gql';
import { constructPackageTree } from 'utils/gql';
import { get } from 'lodash';

const usePublishedArchitecture = ({
  id,
}) => {
  const {
    loading,
    error,
    data,
  } = useQuery(GET_PUBLISHED_ARCHITECTURE, {
    variables: { id },
  });

  // if no latestArch, then qid is for a draft with no published version
  const architecture = get(data, ARCHITECTURE_QUERY_ROOT);
  let publishedArchitecture = get(architecture, 'latestArch[0]');

  if (publishedArchitecture) {
    publishedArchitecture = {
      ...publishedArchitecture,
      packages: constructPackageTree(publishedArchitecture.allPackages),
    };
  }

  return {
    architecture,
    publishedArchitecture,
    error,
    loading,
  };
};

export default usePublishedArchitecture;
