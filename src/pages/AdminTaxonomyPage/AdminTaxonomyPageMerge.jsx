import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import Loader from 'components/Loader/Loader.jsx';
import AdminTaxonomyPageMergeTree from 'pages/AdminTaxonomyPage/AdminTaxonomyPageMergeTree.jsx';
import AdminTaxonomyPageMergeControls from 'pages/AdminTaxonomyPage/AdminTaxonomyPageMergeControls.jsx';
import useMergeTaxonomies from 'pages/AdminTaxonomyPage/useMergeTaxonomies.jsx';
import useMergeSourceTree from 'pages/AdminTaxonomyPage/useMergeSourceTree.jsx';
import useMergeState from 'pages/AdminTaxonomyPage/useMergeState.jsx';

const AdminTaxonomyPageMerge = ({ sourceID }) => {
  const { sourceTree, onDelete, expanded } = useMergeSourceTree(sourceID);

  const { onInsert, onMerge } = useMergeTaxonomies();

  const { target } = useMergeState();

  const handleInsert = useCallback(
    () => target && onInsert(target.id, sourceTree),
    [onInsert, sourceTree, target],
  );

  const handleMerge = useCallback(
    () => target && onMerge(target.id, sourceTree),
    [onMerge, sourceTree, target],
  );

  if (!sourceTree) return <Loader isCentered size={36} />;

  return (
    <div className="AdminTaxonomyPage-merge">
      <AdminTaxonomyPageMergeTree
        sourceTree={sourceTree}
        onDelete={onDelete}
        expanded={expanded}
      />
      <AdminTaxonomyPageMergeControls
        sourceTree={sourceTree}
        onInsert={handleInsert}
        onMerge={handleMerge}
        selected={target}
      />
    </div>
  );
};

AdminTaxonomyPageMerge.propTypes = {
  sourceID: PropTypes.string,
};

AdminTaxonomyPageMerge.defaultProps = {
  sourceID: null,
};

export default React.memo(AdminTaxonomyPageMerge);
