import React from 'react';
import PropTypes from 'prop-types';
import Tree from 'components/Tree/Tree.jsx';
import Loader from 'components/Loader/Loader.jsx';
import useSourceTree from 'hooks/useSourceTree.jsx';

const AdminTaxonomyPageSource = ({
  sourceName,
  actions,
}) => {
  const { loading, error, roots } = useSourceTree(sourceName);

  return (
    <div className="AdminTaxonomyPage-source">
      {loading && <Loader isCentered />}
      {!loading && error && 'Oops! An error has occurred.'}
      {!loading && !error && roots.map(root => (
        <Tree
          key={root.id}
          item={root}
          allItems={root.allChildren}
          actions={actions}
        />
      ))}
    </div>
  );
};

AdminTaxonomyPageSource.propTypes = {
  sourceName: PropTypes.string.isRequired,
  actions: PropTypes.shape({}),
};

AdminTaxonomyPageSource.defaultProps = {
  actions: null,
};

export default AdminTaxonomyPageSource;
