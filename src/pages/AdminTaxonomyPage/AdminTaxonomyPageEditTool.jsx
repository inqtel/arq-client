import React, { useState, useRef, useCallback } from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType } from 'proptypes';
import Popover from 'components/Popover/Popover.jsx';
import Button from 'components/Button/Button.jsx';
import { IconPencil, IconCheckOutlined } from 'components/Icons/Icons.jsx';

const AdminTaxonomyPageEditTool = ({ item, updateTaxonomyTerm }) => {
  const popoverRef = useRef();
  const [ref, setRef] = useState(null);
  const [value, setValue] = useState(item.term);

  const inputCallback = useCallback((node) => {
    setRef(node);
    ref && ref.focus();
  }, [ref]);

  const resetInput = () => setValue(item.term);

  const onChange = e => setValue(e.target.value);

  const onConfirm = () => updateTaxonomyTerm(item, value);

  const onKeyUp = (e) => {
    switch (e.key) {
      case 'Enter':
        updateTaxonomyTerm(item, value);
        break;
      case 'Escape':
        resetInput();
        popoverRef && popoverRef.current.close();
        break;
      default:
        break;
    }
  };

  return (
    <Popover
      ref={popoverRef}
      width="md"
      leftOrRight="right"
    >
      <Popover.Toggle>
        <Button unstyled>
          <IconPencil />
        </Button>
      </Popover.Toggle>
      <Popover.Box>
        <Popover.Box.Content>
          <div className="Popover-box-edit-term">
            <input
              ref={inputCallback}
              id={item.id}
              type="text"
              placeholder="Term"
              value={value}
              onChange={onChange}
              onKeyUp={onKeyUp}
            />
            <span
              onClick={onConfirm}
              onKeyDown={() => {}}
              role="button"
              tabIndex={0}
            >
              <IconCheckOutlined height={18} />
            </span>
          </div>
        </Popover.Box.Content>
      </Popover.Box>
    </Popover>
  );
};

AdminTaxonomyPageEditTool.propTypes = {
  item: taxonomyPropType.isRequired,
  updateTaxonomyTerm: PropTypes.func.isRequired,
};

export default AdminTaxonomyPageEditTool;
