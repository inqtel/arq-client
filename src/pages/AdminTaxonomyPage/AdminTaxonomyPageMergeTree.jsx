import React from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType } from 'proptypes';
import { taxonomyAncestorsToBreadcrumbs } from 'utils/taxonomies';
import { SOURCE_ARQ, sourceNames } from 'constants/sources';
import { IconRemove } from 'components/Icons/Icons.jsx';
import Heading from 'components/Heading/Heading.jsx';
import Tree from 'components/Tree/Tree.jsx';

const AdminTaxonomyPageMergeTree = ({ sourceTree, onDelete, expanded }) => {
  const sourceTreeActions = {
    primary: [
      {
        title: 'Exclude from merge',
        icon: <IconRemove />,
        handler: onDelete,
      },
    ],
  };

  const renderBadges = (i) => {
    const badges = [];

    const links = (i.referencedBy || []).filter(r => r.architecture.isLatestArch);
    if (links.length > 0) badges.push(`Archs: ${links.length}`);

    if (i.source && i.source.name === SOURCE_ARQ) badges.push(sourceNames[SOURCE_ARQ]);

    return badges;
  };

  return (
    <div className="AdminTaxonomyPage-merge-tree">
      <Heading level={3}>Edit Source Tree</Heading>
      <div className="AdminTaxonomyPage-merge-source">
        {sourceNames[sourceTree.source.name]}
      </div>
      <Tree
        item={sourceTree}
        allItems={sourceTree.allChildren}
        actions={sourceTreeActions}
        renderBadges={renderBadges}
        expanded={expanded}
        size="sm"
      />
      <div className="AdminTaxonomyPage-merge-breadcrumbs">
        {taxonomyAncestorsToBreadcrumbs(sourceTree.ancestors)}
      </div>
    </div>
  );
};

AdminTaxonomyPageMergeTree.propTypes = {
  sourceTree: taxonomyPropType.isRequired,
  onDelete: PropTypes.func.isRequired,
  expanded: PropTypes.bool.isRequired,
};

export default React.memo(AdminTaxonomyPageMergeTree);
