import React from 'react';
import PropTypes from 'prop-types';
import { taxonomyPropType } from 'proptypes';
import Button from 'components/Button/Button.jsx';
import { IconTrash, IconXOutlined } from 'components/Icons/Icons.jsx';

const AdminTaxonomyPageDeleteTool = ({
  item,
  deleteTaxonomy,
  setDeleteTaxonomy,
  onDeleteTaxonomy,
}) => {
  const handleClick = (e) => {
    e.stopPropagation();
    if (deleteTaxonomy && item.id === deleteTaxonomy.id) {
      onDeleteTaxonomy(item);
    } else {
      setDeleteTaxonomy(item);
    }
  };

  const taxonomyDeleteCancel = (e) => {
    e.stopPropagation();
    setDeleteTaxonomy(null);
  };

  return (
    <Button
      unstyled
      onClick={handleClick}
    >
      {deleteTaxonomy && item.id === deleteTaxonomy.id ? (
        <Button.Group align="right" gutter="sm">
          <Button unstyled onClick={handleClick}>
            <IconTrash />
            {' '}
            Yes, Delete
          </Button>
          <Button unstyled onClick={taxonomyDeleteCancel} appearance="secondary">
            <IconXOutlined />
            {' '}
            Cancel
          </Button>
        </Button.Group>
      ) : (
        <IconTrash />
      )}
    </Button>
  );
};

AdminTaxonomyPageDeleteTool.propTypes = {
  item: taxonomyPropType.isRequired,
  deleteTaxonomy: taxonomyPropType,
  setDeleteTaxonomy: PropTypes.func.isRequired,
  onDeleteTaxonomy: PropTypes.func.isRequired,
};

AdminTaxonomyPageDeleteTool.defaultProps = {
  deleteTaxonomy: null,
};

export default AdminTaxonomyPageDeleteTool;
