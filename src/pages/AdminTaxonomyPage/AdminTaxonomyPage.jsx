import React from 'react';
import { matchPropType } from 'proptypes';
import { SOURCE_ARQ, sourceNames } from 'constants/sources';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import Heading from 'components/Heading/Heading.jsx';
import Loader from 'components/Loader/Loader.jsx';
import Toolbar from 'components/Toolbar/Toolbar.jsx';
import Link from 'components/Link/Link.jsx';
import { IconCaretLeft } from 'components/Icons/Icons.jsx';
import AdminTaxonomyPageBrowse from 'pages/AdminTaxonomyPage/AdminTaxonomyPageBrowse.jsx';
import AdminTaxonomyPageCore from 'pages/AdminTaxonomyPage/AdminTaxonomyPageCore.jsx';
import AdminTaxonomyPageMerge from 'pages/AdminTaxonomyPage/AdminTaxonomyPageMerge.jsx';
import useSourceTree from 'hooks/useSourceTree.jsx';
import 'pages/AdminTaxonomyPage/AdminTaxonomyPage.scss';

const AdminTaxonomyPage = ({ match }) => {
  const { id: sourceID } = match.params;

  const { loading, error, roots } = useSourceTree(SOURCE_ARQ);

  return (
    <div className="AdminTaxonomyPage">
      <Dashboard>
        <Dashboard.Header showBreadcrumbs />
        <Dashboard.Section above>
          <Dashboard.Toolbar>
            <Toolbar>
              <Toolbar.Group fill padded>
                <Toolbar.Item>
                  <Link to="/admin/merge">
                    <IconCaretLeft display="inlineBlock" position="left" />
                    Back to List
                  </Link>
                </Toolbar.Item>
              </Toolbar.Group>
            </Toolbar>
          </Dashboard.Toolbar>
        </Dashboard.Section>
        <Dashboard.Section fit>
          <Dashboard.Column fit>
            <Dashboard.Content padding={['top', 'right', 'bottom', 'left']}>
              <Heading level={1}>{`${sourceNames[SOURCE_ARQ]} Taxonomy`}</Heading>
              {loading && <Loader isCentered size={36} />}
              {!loading && !error && roots && (
                <AdminTaxonomyPageCore roots={roots} />
              )}
            </Dashboard.Content>
          </Dashboard.Column>
          <Dashboard.Column fit minWidth="md" size="lg" theme="white">
            <div className="AdminTaxonomyPage-sidebar">
              {sourceID ? (
                <AdminTaxonomyPageMerge sourceID={sourceID} />
              ) : (
                <AdminTaxonomyPageBrowse />
              )}
            </div>
          </Dashboard.Column>
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

AdminTaxonomyPage.propTypes = {
  match: matchPropType.isRequired,
};

export default AdminTaxonomyPage;
