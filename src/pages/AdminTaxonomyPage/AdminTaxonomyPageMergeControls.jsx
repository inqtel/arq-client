import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { taxonomyPropType } from 'proptypes';
import { sourceNames, SOURCE_ARQ } from 'constants/sources';
import Button from 'components/Button/Button.jsx';
import Radio from 'components/Radio/Radio.jsx';
import Heading from 'components/Heading/Heading.jsx';
import AdminTaxonomyPageHelpModal from 'pages/AdminTaxonomyPage/AdminTaxonomyPageHelpModal.jsx';

const AdminTaxonomyPageMergeControls = ({
  selected,
  sourceTree,
  onMerge,
  onInsert,
}) => {
  const history = useHistory();

  const [infoModalOpen, setInfoModalOpen] = useState(false);

  const [actionIndex, setActionIndex] = useState(0);

  const mergeActions = [onMerge, onInsert];

  const sourceArQName = sourceNames[SOURCE_ARQ];

  return (
    <div className="AdminTaxonomyPage-merge-controls">
      <div className="AdminTaxonomyPage-merge-controls-preview">
        <Heading level={4}>Source</Heading>
        {`${sourceNames[sourceTree.source.name]}: ${sourceTree.term}`}
        <br />
        <br />
        <Heading level={4}>Target</Heading>
        {selected ? `${sourceArQName}: ${selected.term}` : (
          <span className="AdminTaxonomyPage-merge-controls-preview-placeholder">
            {`Select from ${sourceArQName}`}
          </span>
        )}
      </div>
      <Radio
        id="mergeRadio1"
        name="mergeRadio"
        label="Merge source into target"
        onChange={() => setActionIndex(0)}
        checked={actionIndex === 0}
      />
      <Radio
        id="mergeRadio2"
        name="mergeRadio"
        label="Insert source under target"
        onChange={() => setActionIndex(1)}
        checked={actionIndex === 1}
      />
      <div className="AdminTaxonomyPage-merge-help">
        Select a target tree from
        {` ${sourceArQName} `}
        to merge. Need more info on how this works?&nbsp;
        <span role="presentation" onClick={() => setInfoModalOpen(true)}>Click here</span>
        <AdminTaxonomyPageHelpModal
          infoModalOpen={infoModalOpen}
          setInfoModalOpen={setInfoModalOpen}
        />
      </div>
      <Button.Group gutter="sm">
        <Button
          appearance="secondary"
          onClick={() => history.push('/admin/merge/')}
        >
          Cancel
        </Button>
        <Button
          onClick={mergeActions[actionIndex]}
          readOnly={!sourceTree || !selected}
        >
          Confirm
        </Button>
      </Button.Group>
    </div>
  );
};

AdminTaxonomyPageMergeControls.propTypes = {
  selected: taxonomyPropType,
  sourceTree: taxonomyPropType.isRequired,
  onMerge: PropTypes.func.isRequired,
  onInsert: PropTypes.func.isRequired,
};

AdminTaxonomyPageMergeControls.defaultProps = {
  selected: null,
};

export default AdminTaxonomyPageMergeControls;
