import React from 'react';
import { matchPropType } from 'proptypes';
import ArchitectureReview from 'components/ArchitectureReview/ArchitectureReview.jsx';
import useArchitectureReview from 'pages/ArchitectureReviewPage/useArchitectureReview.jsx';

const ArchitectureReviewPage = ({ match }) => {
  const { id } = match.params;

  const {
    adminArchitecture,
    patchAdminArchitectureStatusRejected,
    patchAdminArchitectureStatusPublished,
  } = useArchitectureReview({ id });

  return (
    <ArchitectureReview
      adminArchitecture={adminArchitecture}
      patchAdminArchitectureStatusPublished={patchAdminArchitectureStatusPublished}
      patchAdminArchitectureStatusRejected={patchAdminArchitectureStatusRejected}
    />
  );
};

ArchitectureReviewPage.propTypes = {
  match: matchPropType.isRequired,
};

ArchitectureReviewPage.defaultProps = {};

export default ArchitectureReviewPage;
