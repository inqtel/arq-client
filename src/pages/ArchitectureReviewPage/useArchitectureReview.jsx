import { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { ARCHITECTURE_SUBMITTED } from 'constants/architectureStates.js';

import {
  getAdminArchitecture as getAdminArchitectureRaw,
  patchAdminArchitectureStatusRejected as patchAdminArchitectureStatusRejectedRaw,
  patchAdminArchitectureStatusInReview as patchAdminArchitectureStatusInReviewRaw,
  patchAdminArchitectureStatusPublished as patchAdminArchitectureStatusPublishedRaw,
  resetAdminArchitecture as resetAdminArchitectureRaw,
} from 'actions/adminArchitecture';

const useArchitectureReview = ({ id }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const adminArchitecture = useSelector(state => state.adminArchitecture);

  const getAdminArchitecture = useCallback(
    async () => dispatch(getAdminArchitectureRaw(id)),
    [dispatch, id],
  );

  useEffect(() => {
    getAdminArchitecture(id);
  }, [id, getAdminArchitecture]);

  useEffect(() => () => dispatch(resetAdminArchitectureRaw()), [dispatch]);

  useEffect(() => {
    if (adminArchitecture.status === ARCHITECTURE_SUBMITTED) {
      dispatch(patchAdminArchitectureStatusInReviewRaw(id));
    }
  }, [adminArchitecture, id, dispatch]);

  const patchAdminArchitectureStatusPublished = useCallback(
    async () => {
      await dispatch(patchAdminArchitectureStatusPublishedRaw(id));
      history.push('/admin/architectures');
    },
    [dispatch, history, id],
  );

  const patchAdminArchitectureStatusRejected = useCallback(
    async () => {
      await dispatch(patchAdminArchitectureStatusRejectedRaw(id));
      history.push('/admin/architectures');
    },
    [dispatch, history, id],
  );

  return {
    adminArchitecture,
    patchAdminArchitectureStatusRejected,
    patchAdminArchitectureStatusPublished,
  };
};

export default useArchitectureReview;
