import React from 'react';
import { matchPropType } from 'proptypes';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import Loader from 'components/Loader/Loader.jsx';
import CustomerPageContent from 'pages/CustomerPage/CustomerPageContent.jsx';
import useCustomer from 'hooks/useCustomer.jsx';
import 'pages/CustomerPage/CustomerPage.scss';

const CustomerPage = ({ match }) => {
  const { id } = match.params;

  const {
    customer,
    error,
    loading,
  } = useCustomer({ id });

  return (
    <div className="CustomerPage">
      <Dashboard>
        <Dashboard.Header showBreadcrumbs />
        <Dashboard.Section fit>
          <Dashboard.Column fit minWidth="md">
            <Dashboard.Content padding={['top', 'right', 'bottom', 'left']}>
              {loading && <Loader isCentered size={36} />}
              {!loading && error && <div>Error</div>}
              {!loading && customer && (
                <CustomerPageContent customer={customer} />
              )}
            </Dashboard.Content>
          </Dashboard.Column>
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

CustomerPage.propTypes = {
  match: matchPropType.isRequired,
};

export default CustomerPage;
