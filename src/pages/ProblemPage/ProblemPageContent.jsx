import React from 'react';
import { problemPropType } from 'proptypes';
import Panel from 'components/Panel/Panel.jsx';
import Problem from 'components/Problem/Problem.jsx';
import CardList from 'components/CardList/CardList.jsx';
import CapabilityCard from 'components/CapabilityCard/CapabilityCard.jsx';
import RelatedPanelInterface from 'components/RelatedPanel/RelatedPanelInterface.jsx';
import RelatedArchitecturesPanelInterface from 'components/RelatedPanel/RelatedArchitecturesPanelInterface.jsx';
import GraphPanelInterface from 'components/GraphPanel/GraphPanelInterface.jsx';
import { GET_PROBLEM_PROBLEMS, GET_PROBLEM_COMPANIES } from 'queries/problems';
import { GET_GRAPH_PROBLEM } from 'queries/graph';

const ProblemPageContent = ({ problem }) => (
  <div className="ProblemPage-content">
    <Panel>
      <Panel.Heading>
        <div className="ProblemPage-problem">
          <Problem problem={problem} customer={problem.customer} />
        </div>
      </Panel.Heading>
      <Panel.Content>
        <CardList
          items={problem.capabilities}
          renderCard={capability => (
            <CapabilityCard
              key={capability.id}
              capability={capability}
            />
          )}
          renderEmpty={() => 'No capability tags'}
        />
      </Panel.Content>
    </Panel>
    <RelatedArchitecturesPanelInterface capabilities={problem.capabilities} />
    <RelatedPanelInterface
      id={problem.id}
      query={GET_PROBLEM_COMPANIES}
      parentKey="ProblemSetsByID"
      childKey="companies"
    />
    <RelatedPanelInterface
      id={problem.id}
      query={GET_PROBLEM_PROBLEMS}
      parentKey="ProblemSetsByID"
      childKey="problemSets"
    />
    <GraphPanelInterface
      id={problem.id}
      query={GET_GRAPH_PROBLEM}
      type="ProblemSetsByID"
    />
  </div>
);

ProblemPageContent.propTypes = {
  problem: problemPropType.isRequired,
};

export default ProblemPageContent;
