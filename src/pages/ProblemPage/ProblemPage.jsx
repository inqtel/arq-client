import React from 'react';
import { matchPropType } from 'proptypes';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import ProblemPageContent from 'pages/ProblemPage/ProblemPageContent.jsx';
import Loader from 'components/Loader/Loader.jsx';
import useProblemPage from 'pages/ProblemPage/useProblemPage.jsx';
import 'pages/ProblemPage/ProblemPage.scss';

const ProblemPage = ({ match }) => {
  const { id } = match.params;

  const {
    problem,
    error,
    loading,
  } = useProblemPage({ id });

  return (
    <div className="ProblemPage">
      <Dashboard>
        <Dashboard.Header showBreadcrumbs />
        <Dashboard.Section fit>
          <Dashboard.Column fit minWidth="sm">
            <Dashboard.Content padding={['top', 'right', 'left']}>
              {loading && <Loader isCentered size={36} />}
              {!loading && error && <div>Error</div>}
              {!loading && problem && (
                <ProblemPageContent problem={problem} />
              )}
            </Dashboard.Content>
            <Dashboard.Footer />
          </Dashboard.Column>
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

ProblemPage.propTypes = {
  match: matchPropType.isRequired,
};

ProblemPage.defaultProps = {};

export default ProblemPage;
