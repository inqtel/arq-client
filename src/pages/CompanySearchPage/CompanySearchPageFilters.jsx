import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { RefinementListFilter } from 'searchkit';
import SearchKitRefinementListContainer from 'components/SearchKit/SearchKitRefinementListContainer.jsx';
import RefinementListFilterExtended from 'components/SearchKit/RefinementListFilterExtended.jsx';
import SearchKitRefinementListTools from 'components/SearchKit/SearchKitRefinementListTools.jsx';
import SearchKitRefinementListOperatorToggle from 'components/SearchKit/SearchKitRefinementListOperatorToggle.jsx';
import SearchKitRefinementListSortToggle from 'components/SearchKit/SearchKitRefinementListSortToggle.jsx';
import SearchKitRefinementListSearchBar from 'components/SearchKit/SearchKitRefinementListSearchBar.jsx';
import SearchKitRefinementListSearchButton from 'components/SearchKit/SearchKitRefinementListSearchButton.jsx';
import SearchKit from 'components/SearchKit/SearchKit.jsx';

import { LABEL_TECH_TAGS } from 'constants/problems.js';

const CompanySearchPageFilters = ({
  taxonomiesOperator,
  setTaxonomiesOperator,
  taxonomiesSort,
  setTaxonomiesSort,
  filter,
  setFilter,
  filterOpen,
  setFilterOpen,
  include,
}) => {
  const [bucketNumber, setBucketNumber] = useState(0);

  return (
    <div className="CompanySearchPage-filters">
      <RefinementListFilterExtended
        id="taxonomies"
        title={LABEL_TECH_TAGS}
        field="capabilities.term.raw"
        fieldOptions={{ type: 'nested', options: { path: 'capabilities' } }}
        operator={taxonomiesOperator}
        orderKey={taxonomiesSort.key}
        orderDirection={taxonomiesSort.direction}
        include={include}
        filter={filter}
        size={filter.length > 0 ? 999 : 4}
        showMore={filter.length <= 0}
        itemComponent={SearchKit.FilterOption}
        setBucketNumber={setBucketNumber}
        containerComponent={
          (
            <SearchKitRefinementListContainer
              rightComponent={(
                <SearchKitRefinementListTools>
                  <SearchKitRefinementListOperatorToggle
                    taxonomiesOperator={taxonomiesOperator}
                    setTaxonomiesOperator={setTaxonomiesOperator}
                  />
                  <SearchKitRefinementListSortToggle
                    taxonomiesSort={taxonomiesSort}
                    setTaxonomiesSort={setTaxonomiesSort}
                  />
                  <SearchKitRefinementListSearchButton
                    filter={filter}
                    filterOpen={filterOpen}
                    setFilterOpen={setFilterOpen}
                    bucketNumber={bucketNumber}
                  />
                </SearchKitRefinementListTools>
              )}
              bottomComponent={(
                <SearchKitRefinementListSearchBar
                  value={filter}
                  setFilter={setFilter}
                  open={filterOpen}
                />
              )}
            />
          )
        }
      />
      <RefinementListFilter
        id="portfolio"
        title="Portfolio Company"
        field="portfolio"
        operator="OR"
        size={10}
        orderKey="_term"
        orderDirection="asc"
        itemComponent={SearchKit.FilterOption}
      />
    </div>
  );
};

CompanySearchPageFilters.propTypes = {
  taxonomiesOperator: PropTypes.string.isRequired,
  setTaxonomiesOperator: PropTypes.func.isRequired,
  taxonomiesSort: PropTypes.shape({
    term: PropTypes.string.isRequired,
    key: PropTypes.string.isRequired,
    direction: PropTypes.string.isRequired,
  }).isRequired,
  setTaxonomiesSort: PropTypes.func.isRequired,
  filter: PropTypes.string,
  setFilter: PropTypes.func.isRequired,
  include: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.array,
  ]).isRequired,
  filterOpen: PropTypes.bool.isRequired,
  setFilterOpen: PropTypes.func.isRequired,
};

CompanySearchPageFilters.defaultProps = {
  filter: '',
};

export default CompanySearchPageFilters;
