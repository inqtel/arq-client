import React from 'react';
import PropTypes from 'prop-types';
import Tree from 'components/Tree/Tree.jsx';
import Loader from 'components/Loader/Loader.jsx';
import Link from 'components/Link/Link.jsx';
import useSourceTree from 'hooks/useSourceTree.jsx';

const TaxonomiesPageSource = ({ sourceName }) => {
  const { loading, error, roots } = useSourceTree(sourceName);

  const renderItem = taxonomy => (
    <Link to={`/taxonomies/${taxonomy.id}`}>{taxonomy.term}</Link>
  );

  return (
    <div className="TaxonomiesPage-source">
      {loading && <Loader isCentered size={36} />}
      {!loading && error && 'Oops! An error has occurred.'}
      {!loading && !error && roots.map(root => (
        <Tree
          key={root.id}
          item={root}
          renderItem={renderItem}
          allItems={root.allChildren}
          size="md"
        />
      ))}
    </div>
  );
};

TaxonomiesPageSource.propTypes = {
  sourceName: PropTypes.string.isRequired,
};

export default TaxonomiesPageSource;
