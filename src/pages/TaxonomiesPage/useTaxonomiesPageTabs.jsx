import { useQuery } from '@apollo/client';
import { selectedSourceIndexVar } from 'apollo/cache';
import { GET_SOURCE_TAB_SELECTED_INDEX } from 'queries/sources';

const useTaxonomiesPageTabs = () => {
  const { data } = useQuery(GET_SOURCE_TAB_SELECTED_INDEX);
  const { selectedSourceIndex } = data;

  const setSourceTabSelectedIndex = index => selectedSourceIndexVar(index);

  return {
    setSourceTabSelectedIndex,
    sourceTabSelectedIndex: selectedSourceIndex,
  };
};

export default useTaxonomiesPageTabs;
