import React from 'react';
import Loader from 'components/Loader/Loader.jsx';
import Tabs from 'components/Tabs/Tabs.jsx';
import TabsInterface from 'components/Tabs/TabsInterface.jsx';
import { sourceNames } from 'constants/sources';
import useSources from 'hooks/useSources.jsx';
import TaxonomiesPageSource from 'pages/TaxonomiesPage/TaxonomiesPageSource.jsx';
import useTaxonomiesPageTabs from 'pages/TaxonomiesPage/useTaxonomiesPageTabs.jsx';

const TaxonomiesPageContent = () => {
  const {
    error,
    loading,
    sources,
  } = useSources();

  const {
    setSourceTabSelectedIndex,
    sourceTabSelectedIndex,
  } = useTaxonomiesPageTabs();

  const tabOptions = {
    bgColor: 'teal',
    hasOutline: true,
  };

  return (
    <div className="TaxonomiesPage-content">
      {loading && <Loader size={36} isCentered isHeight100 />}
      {!loading && error && 'Error!'}
      <TabsInterface
        tabOptions={tabOptions}
        selected={sourceTabSelectedIndex}
        setSourceTabSelectedIndex={setSourceTabSelectedIndex}
      >
        {sources.map(source => (
          <Tabs.Pane
            label={sourceNames[source.name]}
            key={source.name}
            hasBackground
          >
            <div key={source.name}>
              <TaxonomiesPageSource sourceName={source.name} />
            </div>
          </Tabs.Pane>
        ))}
      </TabsInterface>
    </div>
  );
};

export default TaxonomiesPageContent;
