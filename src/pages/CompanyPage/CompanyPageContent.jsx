import React from 'react';
import { companyPropType } from 'proptypes';
import Panel from 'components/Panel/Panel.jsx';
import CardList from 'components/CardList/CardList.jsx';
import CapabilityCard from 'components/CapabilityCard/CapabilityCard.jsx';
import RelatedPanelInterface from 'components/RelatedPanel/RelatedPanelInterface.jsx';
import RelatedArchitecturesPanelInterface from 'components/RelatedPanel/RelatedArchitecturesPanelInterface.jsx';
import GraphPanelInterface from 'components/GraphPanel/GraphPanelInterface.jsx';
import { GET_COMPANY_PROBLEMS, GET_COMPANY_COMPANIES } from 'queries/companies';
import { GET_GRAPH_COMPANY } from 'queries/graph';
import { COMPANY_QUERY_ROOT } from 'constants/gql';
import Link from 'components/Link/Link.jsx';
import { PitchbookLogo, SalesforceLogo } from 'components/Logos/Logos.jsx';
import Tooltip from 'components/Tooltip/Tooltip.jsx';
import Button from 'components/Button/Button.jsx';
import useIntegrationFlags from 'hooks/useIntegrationFlags.jsx';

const CompanyPageContent = ({ company }) => {
  const { salesforceEnabled } = useIntegrationFlags();

  return (
    <div className="CompanyPage-content">
      <Panel>
        <Panel.Heading>
          <div className="CompanyPage-name">{company.name}</div>
          {(salesforceEnabled || company.pitchbookId) && (
            <div className="CompanyPage-links">
              <Button.Group gutter="md">
                {salesforceEnabled && (
                  <Button unstyled>
                    <Tooltip content="View in Salesforce" leftOrRight="right">
                      <Tooltip.Wrapper>
                        <Link to={`${SALESFORCE_URL}/lightning/r/Account/${company.id}/view`} external>
                          <SalesforceLogo fill="#6a71d7" size={25} display="block" />
                        </Link>
                      </Tooltip.Wrapper>
                    </Tooltip>
                  </Button>
                )}
                {company.pitchbookId && (
                  <Button unstyled>
                    <Tooltip content="View in Pitchbook" leftOrRight="right">
                      <Tooltip.Wrapper>
                        <Link to={`${PITCHBOOK_URL}/${company.pitchbookId}/company/profile`} external>
                          <PitchbookLogo fill="#6a71d7" size={25} />
                        </Link>
                      </Tooltip.Wrapper>
                    </Tooltip>
                  </Button>
                )}
              </Button.Group>
            </div>
          )}
        </Panel.Heading>
        <Panel.Content>
          <CardList
            title="Capabilities"
            items={company.capabilities}
            renderCard={capability => (
              <CapabilityCard
                key={capability.id}
                capability={capability}
              />
            )}
          />
        </Panel.Content>
      </Panel>
      <RelatedArchitecturesPanelInterface capabilities={company.capabilities} />
      <RelatedPanelInterface
        id={company.id}
        query={GET_COMPANY_COMPANIES}
        parentKey={COMPANY_QUERY_ROOT}
        childKey="companies"
      />
      <RelatedPanelInterface
        id={company.id}
        query={GET_COMPANY_PROBLEMS}
        parentKey={COMPANY_QUERY_ROOT}
        childKey="problemSets"
      />
      <GraphPanelInterface
        id={company.id}
        query={GET_GRAPH_COMPANY}
        type={COMPANY_QUERY_ROOT}
      />
    </div>
  );
};

CompanyPageContent.propTypes = {
  company: companyPropType.isRequired,
};

export default CompanyPageContent;
