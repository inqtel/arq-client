import React from 'react';
import { matchPropType } from 'proptypes';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import CompanyPageContent from 'pages/CompanyPage/CompanyPageContent.jsx';
import Loader from 'components/Loader/Loader.jsx';
import useCompanyPage from 'pages/CompanyPage/useCompanyPage.jsx';
import 'pages/CompanyPage/CompanyPage.scss';

const CompanyPage = ({ match }) => {
  const { id } = match.params;

  const {
    company,
    loading,
    error,
  } = useCompanyPage({ id });

  return (
    <div className="CompanyPage">
      <Dashboard>
        <Dashboard.Header showBreadcrumbs />
        <Dashboard.Section fit>
          <Dashboard.Column fit minWidth="sm">
            <Dashboard.Content padding={['top', 'right', 'bottom', 'left']}>
              {loading && <Loader isCentered size={36} />}
              {!loading && error && <div>Error</div>}
              {!loading && company && (
                <CompanyPageContent
                  company={company}
                />
              )}
            </Dashboard.Content>
          </Dashboard.Column>
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

CompanyPage.propTypes = {
  match: matchPropType.isRequired,
};

export default CompanyPage;
