import React from 'react';
import WelcomeBackInterface from 'components/WelcomeBack/WelcomeBackInterface.jsx';
import SidebarNavigationInterface from 'components/SidebarNavigation/SidebarNavigationInterface.jsx';
import Dashboard from 'components/Dashboard/Dashboard.jsx';
import Grid from 'components/Grid/Grid.jsx';
import List from 'components/List/List.jsx';
import Loader from 'components/Loader/Loader.jsx';
import Link from 'components/Link/Link.jsx';
import useCustomers from 'hooks/useCustomers.jsx';
import 'pages/CustomersPage/CustomersPage.scss';

const CustomersPage = () => {
  const {
    customers,
    error,
    loading,
  } = useCustomers();

  return (
    <div className="CustomersPage">
      <Dashboard>
        <Dashboard.Header />
        <Dashboard.Section fit>
          <Dashboard.Column padded minWidth="sm" size="sm" theme="dark">
            <Grid direction="column" justify="spaceBetween">
              <SidebarNavigationInterface active="customers" />
              <Dashboard.Footer>
                <WelcomeBackInterface />
              </Dashboard.Footer>
            </Grid>
          </Dashboard.Column>
          <Dashboard.Column fit>
            <Dashboard.Content padding={['top', 'right', 'left']}>
              {loading && <Loader isCentered size={36} />}
              {!loading && error && <div>Error</div>}
              {!loading && customers && (
                <div className="CustomersPage-list">
                  <List>
                    {Object.keys(customers).map(key => (
                      <List.Item key={key}>
                        <Link key={key} to={`/customers/${customers[key].id}`}>
                          { customers[key].name }
                        </Link>
                      </List.Item>
                    ))}
                  </List>
                </div>
              )}
            </Dashboard.Content>
          </Dashboard.Column>
        </Dashboard.Section>
      </Dashboard>
    </div>
  );
};

CustomersPage.propTypes = {};

CustomersPage.defaultProps = {};

export default CustomersPage;
